
// testProjectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "testProject.h"
#include "testProjectDlg.h"
#include "afxdialogex.h"
#include "SileadTestApi.h"
#include <math.h>
#include "testResult.h"
#include "SileadFactoryApi.h"
#include <iostream>
#include <fstream>
#include <mutex>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define  DEVICES          0

#define  QRNUM_BYTE       25
#define  X_SCALE    2
#define  Y_SCALE    2

#define  TESTTHRESHOLDDATA     "D:\\KLO19H\\1.BF_test\\SettingData.ini"
#define  MAINCONFIG            "D:\\KLO19H\\1.BF_test\\config\\GSL7011_160x208_20180731.h"
#define  MAINCONFIG7012        "D:\\KLO19H\\1.BF_test\\config\\GSL7012_188X188_12bit_20190314.h"

#define LoopTestFlag	0	//debug mode
#define PathToLog	"C:\\"
#define FileToLog	"MTFLog"

float max_mtf=0.0;  
float max_mtf2=0.0;  
float max_mtf3=0.0;  
float max_mtf4=0.0;  
float max_mtf5=0.0; 
float max_mtf_mag=0.0; 
//CString tmpBarcode;
CString tmpSensorID;
BOOL bTestEndFlg;
int Cnt;
using namespace std;
//struct testdatainfo data;
mutex gMutex;

void ThreadTest2(LPVOID p)
{
	((CtestProjectDlg *)p)->mtf_info();
	return;
}


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CtestProjectDlg 对话框



CtestProjectDlg::CtestProjectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CtestProjectDlg::IDD, pParent)
	, m_result(_T(""))
	//, m_QrTest(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CtestProjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Text(pDX, IDC_EDIT_RESULT, m_result);
	DDX_Control(pDX, IDC_RICHEDIT_XIANSHI, m_xianshi);
}

BEGIN_MESSAGE_MAP(CtestProjectDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_MTF, &CtestProjectDlg::OnBnClickedButtonMtf)
	//ON_BN_CLICKED(IDC_CHECK_DEADPOINT, &CtestProjectDlg::OnBnClickedCheckDeadpoint)
	ON_WM_TIMER()
	ON_MESSAGE(WM_USER_SILEAD_STARTTEST, OnSilead_StartTest)
	ON_MESSAGE(WM_USER_SILEAD_STARTTEST_BF, OnSilead_StartTest_BF)
	ON_MESSAGE(WM_USER_SAVE_LOG, SaveLog)
	ON_MESSAGE(WM_USER_RESET_PROGRAM, ResetBoard)
	ON_MESSAGE(WM_USER_SAVE_LOG2, exceptionSaveLog)
	ON_MESSAGE(WM_USER_EXPOSURE, OnExposure)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON1, &CtestProjectDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CtestProjectDlg 消息处理程序

BOOL CtestProjectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	
	
	// TODO:  在此添加额外的初始化代码
	CEdit *pEdit = (CEdit *)this->GetDlgItem(IDC_EDIT_QRNUM);
	pEdit->SetLimitText(25); 
	result_data = 0;

	Cnt=0;
	bTestEndFlg=1;
	SaveTestImageMode=0;
	CreateDirectory("D:\\MTF_test\\", NULL );
	CreateDirectory("D:\\MTF_Capture\\", NULL );
	CreateDirectory("D:\\MTF_Capture\\every\\", NULL );
	SaveTestImageMode = GetPrivateProfileIntA("TestObject", "SaveTestImageMode", 0, "./TestObject.ini");
	MTF0Mode		  = GetPrivateProfileIntA("TestObject", "MTF0", 0, "./TestObject.ini");
	bExposure		  = GetPrivateProfileIntA("TestObject", "Exposure", 1, "./TestObject.ini");
	nMTF_Product	  = GetPrivateProfileIntA("TestObject", "MTF_Product", 1, "./TestObject.ini"); //0=KLO17H;  1=KLO18H 2=KLO19H
	SetDlgItemText(IDC_STATIC2,"KLO19H-20191126");	

	if (nMTF_Product)
	{
		IMAGE_WDITH   =  188;
		IMAGE_HEIGHT  =  188;
	}
	else
	{  
		IMAGE_WDITH   =  160;
		IMAGE_HEIGHT  =  208;
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CtestProjectDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

HBRUSH CtestProjectDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性


	CString stredit;
	//GetDlgItemText(IDC_EDIT_RESULT, stredit);
	m_Brush.CreateSolidBrush(RGB(180,180,180));
	//pDC->SetBkColor(RGB(150,150,150));
	pDC->SetBkMode(TRANSPARENT);
	//pDC->SetTextColor(RGB(250,250,250));
	//if (pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT && result_data == 0)
	//{
	//	CBrush brush(RGB(255, 255, 255));// 构造白色刷子，RGB值控制颜色
	//	CRect rcClient;
	//	pWnd->GetClientRect(rcClient);
	//	pDC->SetBkMode(TRANSPARENT);
	//	pDC->SelectObject(&brush);
	//	pDC->FillRect(&rcClient, &brush);
	//	return brush;
	//}
	//else if (pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT && result_data == 1)
	//{
	//	CBrush brush(RGB(0, 255, 0));// 构造白色刷子，RGB值控制颜色
	//	CRect rcClient;
	//	pWnd->GetClientRect(rcClient);
	//	pDC->SetBkMode(TRANSPARENT);
	//	pDC->SelectObject(&brush);
	//	pDC->FillRect(&rcClient, &brush);
	//	return brush;
	//}
	//else if (pWnd->GetDlgCtrlID() == IDC_EDIT_RESULT && result_data == 2)
	//{
	//	CBrush brush(RGB(255, 0, 0));// 构造白色刷子，RGB值控制颜色
	//	CRect rcClient;
	//	pWnd->GetClientRect(rcClient);
	//	pDC->SetBkMode(TRANSPARENT);
	//	pDC->SelectObject(&brush);
	//	pDC->FillRect(&rcClient, &brush);
	//	return brush;
	//}
	//Qrtest_result = TRUE
// 	if ((pWnd->GetDlgCtrlID() == IDC_EDIT_QRNUM) && (Qrtest_result == FALSE))
// 	{
// 		CBrush brush(RGB(255, 255, 255));// 构造白色刷子，RGB值控制颜色
// 		CRect rcClient;
// 		pWnd->GetClientRect(rcClient);
// 		pDC->SetBkMode(TRANSPARENT);
// 		pDC->SelectObject(&brush);
// 		pDC->FillRect(&rcClient, &brush);
// 		return brush;
// 	}
// 	else if ((pWnd->GetDlgCtrlID() == IDC_EDIT_QRNUM) && (Qrtest_result == TRUE))
// 	{
// 		CBrush brush(RGB(255, 0, 0));// 构造白色刷子，RGB值控制颜色
// 		CRect rcClient;
// 		pWnd->GetClientRect(rcClient);
// 		pDC->SetBkMode(TRANSPARENT);
// 		pDC->SelectObject(&brush);
// 		pDC->FillRect(&rcClient, &brush);
// 		return brush;
// 	}
	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}


void CtestProjectDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
		
	}
	else
	{

		CRect   rect;
		CPaintDC   dc(this);
		//HBRUSH hbr = CDialogEx::OnCtlColor(dc,  pWnd, UINT nCtlColor);
		GetClientRect(rect);
		//dc.FillSolidRect(rect, RGB(240, 240, 240));   //设置为绿色背景   
		dc.FillSolidRect(rect, RGB(120, 130, 110));   //设置为绿色背景   

		CDialogEx::OnPaint();
		//GetDlgItem(IDC_EDIT_QRNUM)->SetFocus();
	}
}

HCURSOR CtestProjectDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CtestProjectDlg::display(unsigned char *buffer, int w, int h, int w_center,int h_center)
{
	CClientDC dc(this);
	int i, j; 
	int n = 0;
	unsigned char c;
	int i_s = 34;
	int j_s = 32;

	for (j = 0; j < h; j++)//高
	{
		for (i = 0; i < w; i++)//宽
		{
			c = buffer[j * w + i];
			CRect rcDraw(i_s + i*X_SCALE, j_s + j * X_SCALE, i_s + i * X_SCALE + 2, j_s + j * X_SCALE + 2);
			dc.FillSolidRect(&rcDraw, RGB(c, c, c));
		}
	}
}

void CtestProjectDlg::display_clear(unsigned char *buffer, int w, int h, int w_center, int h_center)
{
	CClientDC dc(this);
	int i, j;
	int n = 0;
	unsigned char c;
	int i_s = 34;
	int j_s = 32;

	for (j = 0; j < h; j++)//高
	{
		for (i = 0; i < w; i++)//宽
		{
			c = 240;
			CRect rcDraw(i_s + i * X_SCALE, j_s + j * X_SCALE, i_s + i * X_SCALE + 2, j_s + j * X_SCALE + 2);
			dc.FillSolidRect(&rcDraw, RGB(c, c, c));
		}
	}
}

void CtestProjectDlg::gsl_ShowId(int devices, CString str, BOOL color)//color=1红色，0-heise,2-绿色，3-蓝色
{
	CHARFORMAT cFmt1;
	if (color == 1)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(255, 0, 0);
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR;
	}
	else if (color == 2)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 255, 0);  //绿
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR;
	}
	else if (color == 3)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 0, 255);   //蓝
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR;
	}
	else
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 0, 0);
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR;
	}
	cFmt1.cbSize = sizeof(CHARFORMAT);
	cFmt1.dwEffects = 0;
	cFmt1.dwMask = CFM_COLOR;
	//if (devices == 0)
	{
		//m_chipid.SetSel(0, -1);//选取第一行字符
		//m_chipid.SetSelectionCharFormat(cFmt1);//设置颜色
		//m_chipid.ReplaceSel(str, 0);
	}


}

void CtestProjectDlg::gsl_ShowResult(int devices, CString str, BOOL color)//color=1红色，0-heise,2-绿色，3-蓝色
{
	CHARFORMAT cFmt1;
	if (color == 1)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(255, 0, 0);
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR | CFE_BOLD | CFM_SIZE;
		cFmt1.yHeight = 300;
	}
	else if (color == 2)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 255, 0);  //绿
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR | CFE_BOLD | CFM_SIZE;
		cFmt1.yHeight = 300;
	}
	else if (color == 3)
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 0, 255);   //蓝
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR | CFE_BOLD | CFM_SIZE;
		cFmt1.yHeight = 200;
//		cFmt1.dwMask = CFM_COLOR | CFE_BOLD;
	}
	else
	{
		cFmt1.cbSize = sizeof(CHARFORMAT);
		cFmt1.crTextColor = RGB(0, 0, 0);
		cFmt1.dwEffects = 0;
		cFmt1.dwMask = CFM_COLOR;
		cFmt1.yHeight = 180;
		cFmt1.dwMask = CFM_COLOR;
	}
	cFmt1.cbSize = sizeof(CHARFORMAT);
	cFmt1.dwEffects = 0;
	//if (devices == 0)
	{
		m_xianshi.SetSel(-1, -1);//选取第一行字符
		m_xianshi.SetSelectionCharFormat(cFmt1);//设置颜色
		m_xianshi.ReplaceSel(str, 0);
	}
}

void CtestProjectDlg::OnBnClickedButtonMtf()
{
	//mtf_info();
	ResetBoard(0, 0);
//		OnExposure(0,0);
}

void CtestProjectDlg::mtf_info(bool isChkImg /* = false */)
{		
	gMutex.lock();
	display_clear(NULL, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
	fstream FlagFile;
	
	int TestFlag=1;
	int nMTFResult=1; //0:Pass 1:Fail 255=SPI Fail

	CString tmpName;
	tmpName.Empty();
	if(LoopTestFlag)
		SetDlgItemText(IDC_EDIT_QRNUM,"TEST"); //NO SN MODE
	GetDlgItemText(IDC_EDIT_QRNUM,tmpName); //SN MODE
	if(tmpName=="" && strBarcode=="")
	{
		strBarcode=="" ;
		Cnt=0;
		bTestEndFlg=0;
		TestFlag=0;
	}
	else
	{
		//bTestEndFlg=1;
		TestFlag=1;
	}

	unsigned char *image = (unsigned char *)malloc(320 * 320);
	unsigned char tmpImage[320*320];

	char szFlagFileName[MAX_PATH];
	sprintf_s(szFlagFileName, "C:\\TestFlag.ini");
	remove(szFlagFileName);
	CString str;
	unsigned int temp_data = 0;

	struct testdatainfo data1;
	gx_malloc(&data1);
	if(Cnt==0 && tmpName!="")  //NO SN MODE
	{
		strBarcode.Empty();
		GetDlgItemText(IDC_EDIT_QRNUM,strBarcode); //SN MODE
		WriteLog(strBarcode, PathToLog, FileToLog);
		SetDlgItemText(IDC_RICHEDIT_XIANSHI, "");
		
		gx_read(DEVICES, 0xfc, &temp_data, 1);
		
		Sleep(10);
		gx_initdata(DEVICES, TESTTHRESHOLDDATA);
		result_data = 0;
		Sleep(50);
		int devices = gx_opendevices(DEVICES);
		if (devices == 0) //open test board fail
		{
			WriteLog("First-Board fail", PathToLog, FileToLog);
			SetDlgItemText(IDC_RICHEDIT_XIANSHI, "init test board fail\r\n");
			TestFlag=0;
		}
		else  //open test board success
		{
			imagecnt=0;
			Sleep(50);
			gx_vdd_output(DEVICES, 1);
			Sleep(50);
			gx_rst_output(DEVICES, 0);
			Sleep(10);
			gx_rst_output(DEVICES, 1);
			Sleep(20);
			
			gx_read(DEVICES, 0xfc, &temp_data, 1);
			gx_rst_output(DEVICES, 0);
			Sleep(10);
			gx_rst_output(DEVICES, 1);
			Sleep(20);
			gx_read(DEVICES, 0xfc, &temp_data, 1);
			CString str_info;
			str_info.Empty();
			str_info.Format("%08x", temp_data);

			//if ((temp_data&0xffff0000) != 0x70120000) //connect module fail
			if ((temp_data&0xfff00000) != 0x70100000)
			{
				gsl_ShowId(DEVICES, str_info, 1);
				SetDlgItemText(IDC_RICHEDIT_XIANSHI, "********** connect fail *********\r\n");
				str_barcode.Empty();
				SetDlgItemText(IDC_EDIT_QRNUM,"");
				TestFlag=0;
				//tmpBarcode.Format("%s","ERROR");
				WriteLog("First-connect fail", PathToLog, FileToLog);
			} 
			else //connect module success
			{
				struct  testObjectResult result_object;
				memset(&result_object, TRUE, sizeof(struct  testObjectResult));

				getSensorid(DEVICES, &data1);
				str_info.Format("Sensorid=%s\r\n", data1.sensorid);
				//gsl_ShowResult(DEVICES, str_info, 0);
				tmpSensorID.Format("%s", data1.sensorid);
				//str_info.Format("\r\n");
				//gsl_ShowResult(DEVICES, str_info, 0);

				gx_rst_output(DEVICES, 0);
				Sleep(10);
				gx_rst_output(DEVICES, 1);
				Sleep(20);
				int load = 0;
				if(nMTF_Product)//7011 & 7012 flag
					load = gx_loadmain(DEVICES,MAINCONFIG7012);
				else
					load = gx_loadmain(DEVICES,MAINCONFIG);

				if (load != 0)
				{
					str_info = "mainconfig no exist\r\n";
					gsl_ShowResult(DEVICES, str_info, 1);
					TestFlag=0;
				}

				SetDlgItemText(IDC_EDIT_QRNUM,"");
				
				HWND hDlg = this->GetSafeHwnd();
				HWND hEdit1 = ::GetDlgItem(hDlg, IDC_EDIT_QRNUM);
				::SetFocus(hEdit1);
				
				if(bExposure)
					gx_autoAgc(DEVICES);
			}
		}
	}
	/*NO SN MODE*/
	else if(strBarcode=="" && Cnt==0) // SN Fail (no test and 1st,2sc SN same)
	{
		SetDlgItemText(IDC_RICHEDIT_XIANSHI, "SN Fail");
		TestFlag=0;
		data1.mtf_data[0] =0;
		WriteDataIntoFile("SNFail",data1,tmpSensorID,nMTFResult);
		WriteLog("SN fail", PathToLog, FileToLog);
	}
	/**/

	if(bTestEndFlg==1 && TestFlag!=0)    //TestEndFlag = init success flag 
										 //SaveLog = if BF sendMsg SaveLog, bTestEndFlg=0;
	{
		WriteLog("Test-Start", PathToLog, FileToLog);
		Cnt++;

		display_clear(NULL, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
		if (data1.image !=NULL )
		{
			WriteLog("Test-MTF", PathToLog, FileToLog);
			//for(int i=0; i<3; i++)
			//{
				nMTFResult=gx_mft_t(DEVICES, &data1);
				//Sleep(100);		
			//}

			//Check image status
			if (isChkImg)
			{
				int image_status = gx_imageTestBf_t(DEVICES, &data1);
				if (image_status != 0)
				{
					data1.mtf_data[0] = -1.00;
					CString errMsg;
					errMsg.Format("Image check fail, error type = %d, set MTF data as \"-1.00\"", image_status);
					WriteLog(errMsg, PathToLog, FileToLog);
				}
			}

			CString strMTF;
			strMTF.Format("%f",data1.mtf_data[0]);
			WriteLog(strMTF, PathToLog, FileToLog);
			WriteLog("Test-End", PathToLog, FileToLog);
			
			if(SaveTestImageMode)
			{
				CString strLogFile;
				SYSTEMTIME curTime;
				GetLocalTime(&curTime);
			
				strLogFile.Format(_T("%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF_Capture\\every\\", "MTF", curTime.wMonth, curTime.wDay,curTime.wHour, curTime.wMinute,curTime.wSecond,curTime.wMilliseconds);
				GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);
			}

			memcpy(image, data1.image, IMAGE_WDITH * IMAGE_HEIGHT);
			display(image, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
			memcpy(tmpImage, data1.image, IMAGE_WDITH * IMAGE_HEIGHT);
			display(tmpImage, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
			CString str_info;
			str_info.Format("Sensorid=%s\r\n\r\nmtf5=%.2f%%\r\nmtf7=%.2f%%\r\nmtf9=%.2f%%\r\nmtf1=%.2f%%\r\nmtf3=%.2f%%\r\n\r\nmtf_mag=%.2f\r\n",
				 tmpSensorID,data1.mtf_data[0], data1.mtf_data[1], data1.mtf_data[2], data1.mtf_data[3], data1.mtf_data[4],  data1.mtf_mag);
			
			//CString strLogFile;
			//SYSTEMTIME curTime;
			//strLogFile.Format(_T("%s%s_%d.bmp"), "D:\\MTF0\\", "MTF", imagecnt);
			//imagecnt++;
			//GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);

			//if(data1.mtf_data[0]>max_mtf)
			{
				max_mtf=data1.mtf_data[0];
				max_mtf2=data1.mtf_data[1];
				max_mtf3=data1.mtf_data[2];
				max_mtf4=data1.mtf_data[3];
				max_mtf5=data1.mtf_data[4];
			
				max_mtf_mag=data1.mtf_mag;
			}

			if(MTF0Mode && data1.mtf_data[0]==0)
			{
				CString strLogFile;
				SYSTEMTIME curTime;
				GetLocalTime(&curTime);

				strLogFile.Format(_T("%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF0\\", "MTF0", curTime.wMonth, curTime.wDay,curTime.wHour, curTime.wMinute,curTime.wSecond,curTime.wMilliseconds);
				GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);
			}

			CString strMTFResult;
			if(nMTFResult==255)
			{
				strMTFResult.Format("SPI:%d",nMTFResult);
				WriteLog(strMTFResult, PathToLog, FileToLog);
			}
			else if(nMTFResult==99)
			{
				strMTFResult.Format("Get0Img:%d",nMTFResult);
				WriteLog(strMTFResult, PathToLog, FileToLog);
			}

			SetDlgItemText(IDC_RICHEDIT_XIANSHI, str_info);
			Sleep(100);
			WriteDataIntoFile(strBarcode,data1,tmpSensorID, nMTFResult);
		}
	}
	else if(bTestEndFlg==0 && TestFlag==1)// test end
	{
		WriteLog("SaveLog-1", PathToLog, FileToLog);
		//save log
		CString strLogFile;
		SYSTEMTIME curTime;
		GetLocalTime(&curTime);	
		strLogFile.Format(_T("%s%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF_Capture\\", "MTF_",strBarcode, curTime.wMonth, curTime.wDay,curTime.wHour, curTime.wMinute,curTime.wSecond,curTime.wMilliseconds);
		GFP_Write_Bmp_8(strLogFile, tmpImage, IMAGE_HEIGHT, IMAGE_WDITH);
			
		data1.mtf_data[0] = max_mtf;
		data1.mtf_data[1] = max_mtf2;
		data1.mtf_data[2] = max_mtf3;
		data1.mtf_data[3] = max_mtf4;
		data1.mtf_data[4] = max_mtf5;
		data1.mtf_mag = max_mtf_mag;
		data1.sensorid = (char*)(LPCTSTR)tmpSensorID;
		//Savetestinfo(data1,  strBarcode);
		bTestEndFlg=1;	
		Cnt=0;
		tmpSensorID="0x0000";
		strBarcode.Empty();
		WriteLog("SaveLog-2", PathToLog, FileToLog);
	}
	else
	{
		WriteDataIntoFile(strBarcode,data1,tmpSensorID, 255);
		CString strTest;
		strTest.Format("else-----%d,%d",bTestEndFlg,TestFlag);
		WriteLog(strTest, PathToLog, FileToLog);
	}

	//WriteLog("gx_free-Image", PathToLog, FileToLog);
	free(image);
	//WriteLog("gx_free-Data", PathToLog, FileToLog);
	gx_free(&data1);
	WriteLog("gx_free-End", PathToLog, FileToLog);

	remove(szFlagFileName);
	//WriteLog("Final-1", PathToLog, FileToLog);
	WriteLogFinal(1);
	//WriteLog("Final-2", PathToLog, FileToLog);
	
	GetDlgItem(IDC_EDIT_QRNUM)->SetFocus();
	WriteLog("Final-SetFocus", PathToLog, FileToLog);

	gMutex.unlock();
}

void CtestProjectDlg::mtf_info_BF()
{
	gMutex.lock();
	display_clear(NULL, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
	fstream FlagFile;

	int TestFlag = 1;
	int nMTFResult = 1; //0:Pass 1:Fail 255=SPI Fail

	CString tmpName;
	tmpName.Empty();
	if (LoopTestFlag)
		SetDlgItemText(IDC_EDIT_QRNUM, "TEST"); //NO SN MODE
	GetDlgItemText(IDC_EDIT_QRNUM, tmpName); //SN MODE
	if (tmpName == "" && strBarcode == "")
	{
		strBarcode == "";
		Cnt = 0;
		bTestEndFlg = 0;
		TestFlag = 0;
	}
	else
	{
		//bTestEndFlg=1;
		TestFlag = 1;
	}

	unsigned char *image = (unsigned char *)malloc(320 * 320);
	unsigned char tmpImage[320 * 320];

	char szFlagFileName[MAX_PATH];
	sprintf_s(szFlagFileName, "C:\\TestFlag.ini");
	remove(szFlagFileName);
	CString str;
	unsigned int temp_data = 0;

	struct testdatainfo data1;
	gx_malloc(&data1);
	if (Cnt == 0 && tmpName != "")  //NO SN MODE
	{
		strBarcode.Empty();
		GetDlgItemText(IDC_EDIT_QRNUM, strBarcode); //SN MODE
		WriteLog(strBarcode, PathToLog, FileToLog);
		SetDlgItemText(IDC_RICHEDIT_XIANSHI, "");

		gx_read(DEVICES, 0xfc, &temp_data, 1);

		Sleep(10);
		gx_initdata(DEVICES, TESTTHRESHOLDDATA);
		result_data = 0;
		Sleep(50);
		int devices = gx_opendevices(DEVICES);
		if (devices == 0) //open test board fail
		{
			WriteLog("First-Board fail", PathToLog, FileToLog);
			SetDlgItemText(IDC_RICHEDIT_XIANSHI, "init test board fail\r\n");
			TestFlag = 0;
		}
		else  //open test board success
		{
			imagecnt = 0;
			Sleep(50);
			gx_vdd_output(DEVICES, 1);
			Sleep(50);
			gx_rst_output(DEVICES, 0);
			Sleep(10);
			gx_rst_output(DEVICES, 1);
			Sleep(20);

			gx_read(DEVICES, 0xfc, &temp_data, 1);
			gx_rst_output(DEVICES, 0);
			Sleep(10);
			gx_rst_output(DEVICES, 1);
			Sleep(20);
			gx_read(DEVICES, 0xfc, &temp_data, 1);
			CString str_info;
			str_info.Empty();
			str_info.Format("%08x", temp_data);

			//if ((temp_data&0xffff0000) != 0x70120000) //connect module fail
			if ((temp_data & 0xfff00000) != 0x70100000)
			{
				gsl_ShowId(DEVICES, str_info, 1);
				SetDlgItemText(IDC_RICHEDIT_XIANSHI, "********** connect fail *********\r\n");
				str_barcode.Empty();
				SetDlgItemText(IDC_EDIT_QRNUM, "");
				TestFlag = 0;
				//tmpBarcode.Format("%s","ERROR");
				WriteLog("First-connect fail", PathToLog, FileToLog);
			}
			else //connect module success
			{
				struct  testObjectResult result_object;
				memset(&result_object, TRUE, sizeof(struct  testObjectResult));

				getSensorid(DEVICES, &data1);
				str_info.Format("Sensorid=%s\r\n", data1.sensorid);
				//gsl_ShowResult(DEVICES, str_info, 0);
				tmpSensorID.Format("%s", data1.sensorid);
				//str_info.Format("\r\n");
				//gsl_ShowResult(DEVICES, str_info, 0);

				gx_rst_output(DEVICES, 0);
				Sleep(10);
				gx_rst_output(DEVICES, 1);
				Sleep(20);
				int load = 0;
				if (nMTF_Product)//7011 & 7012 flag
					load = gx_loadmain(DEVICES, MAINCONFIG7012);
				else
					load = gx_loadmain(DEVICES, MAINCONFIG);

				if (load != 0)
				{
					str_info = "mainconfig no exist\r\n";
					gsl_ShowResult(DEVICES, str_info, 1);
					TestFlag = 0;
				}

				SetDlgItemText(IDC_EDIT_QRNUM, "");

				HWND hDlg = this->GetSafeHwnd();
				HWND hEdit1 = ::GetDlgItem(hDlg, IDC_EDIT_QRNUM);
				::SetFocus(hEdit1);

				if (bExposure)
					gx_autoAgc(DEVICES);
			}
		}
	}
	/*NO SN MODE*/
	else if (strBarcode == "" && Cnt == 0) // SN Fail (no test and 1st,2sc SN same)
	{
		SetDlgItemText(IDC_RICHEDIT_XIANSHI, "SN Fail");
		TestFlag = 0;
		data1.mtf_data[0] = 0;
		WriteDataIntoFile("SNFail", data1, tmpSensorID, nMTFResult);
		WriteLog("SN fail", PathToLog, FileToLog);
	}
	/**/

	if (bTestEndFlg == 1 && TestFlag != 0)    //TestEndFlag = init success flag 
											  //SaveLog = if BF sendMsg SaveLog, bTestEndFlg=0;
	{
		WriteLog("Test-Start", PathToLog, FileToLog);
		Cnt++;

		display_clear(NULL, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
		if (data1.image != NULL)
		{
			WriteLog("Test-MTF", PathToLog, FileToLog);
			//for(int i=0; i<3; i++)
			//{
			nMTFResult = gx_mft_t_BF(DEVICES, &data1);
			//Sleep(100);		
			//}
			CString strMTF;
			strMTF.Format("%f", data1.mtf_data[0]);
			WriteLog(strMTF, PathToLog, FileToLog);
			WriteLog("Test-End", PathToLog, FileToLog);

			if (SaveTestImageMode)
			{
				CString strLogFile;
				SYSTEMTIME curTime;
				GetLocalTime(&curTime);

				strLogFile.Format(_T("%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF_Capture\\every\\", "MTF", curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond, curTime.wMilliseconds);
				GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);
			}

			memcpy(image, data1.image, IMAGE_WDITH * IMAGE_HEIGHT);
			display(image, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
			memcpy(tmpImage, data1.image, IMAGE_WDITH * IMAGE_HEIGHT);
			display(tmpImage, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
			CString str_info;
			str_info.Format("Sensorid=%s\r\n\r\nmtf5=%.2f%%\r\nmtf7=%.2f%%\r\nmtf9=%.2f%%\r\nmtf1=%.2f%%\r\nmtf3=%.2f%%\r\n\r\nmtf_mag=%.2f\r\n",
				tmpSensorID, data1.mtf_data[0], data1.mtf_data[1], data1.mtf_data[2], data1.mtf_data[3], data1.mtf_data[4], data1.mtf_mag);

			//CString strLogFile;
			//SYSTEMTIME curTime;
			//strLogFile.Format(_T("%s%s_%d.bmp"), "D:\\MTF0\\", "MTF", imagecnt);
			//imagecnt++;
			//GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);

			//if(data1.mtf_data[0]>max_mtf)
			{
				max_mtf = data1.mtf_data[0];
				max_mtf2 = data1.mtf_data[1];
				max_mtf3 = data1.mtf_data[2];
				max_mtf4 = data1.mtf_data[3];
				max_mtf5 = data1.mtf_data[4];

				max_mtf_mag = data1.mtf_mag;
			}

			if (MTF0Mode && data1.mtf_data[0] == 0)
			{
				CString strLogFile;
				SYSTEMTIME curTime;
				GetLocalTime(&curTime);

				strLogFile.Format(_T("%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF0\\", "MTF0", curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond, curTime.wMilliseconds);
				GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);
			}

			CString strMTFResult;
			if (nMTFResult == 255)
			{
				strMTFResult.Format("SPI:%d", nMTFResult);
				WriteLog(strMTFResult, PathToLog, FileToLog);
			}
			else if (nMTFResult == 99)
			{
				strMTFResult.Format("Get0Img:%d", nMTFResult);
				WriteLog(strMTFResult, PathToLog, FileToLog);
			}

			SetDlgItemText(IDC_RICHEDIT_XIANSHI, str_info);
			Sleep(100);
			WriteDataIntoFile(strBarcode, data1, tmpSensorID, nMTFResult);
		}
	}
	else if (bTestEndFlg == 0 && TestFlag == 1)// test end
	{
		WriteLog("SaveLog-1", PathToLog, FileToLog);
		//save log
		CString strLogFile;
		SYSTEMTIME curTime;
		GetLocalTime(&curTime);
		strLogFile.Format(_T("%s%s%s_%02d-%02d-%02d-%02d-%02d-%03d.bmp"), "D:\\MTF_Capture\\", "MTF_", strBarcode, curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond, curTime.wMilliseconds);
		GFP_Write_Bmp_8(strLogFile, tmpImage, IMAGE_HEIGHT, IMAGE_WDITH);

		data1.mtf_data[0] = max_mtf;
		data1.mtf_data[1] = max_mtf2;
		data1.mtf_data[2] = max_mtf3;
		data1.mtf_data[3] = max_mtf4;
		data1.mtf_data[4] = max_mtf5;
		data1.mtf_mag = max_mtf_mag;
		data1.sensorid = (char*)(LPCTSTR)tmpSensorID;
		//Savetestinfo(data1,  strBarcode);
		bTestEndFlg = 1;
		Cnt = 0;
		tmpSensorID = "0x0000";
		strBarcode.Empty();
		WriteLog("SaveLog-2", PathToLog, FileToLog);
	}
	else
	{
		WriteDataIntoFile(strBarcode, data1, tmpSensorID, 255);
		CString strTest;
		strTest.Format("else-----%d,%d", bTestEndFlg, TestFlag);
		WriteLog(strTest, PathToLog, FileToLog);
	}

	//WriteLog("gx_free-Image", PathToLog, FileToLog);
	free(image);
	//WriteLog("gx_free-Data", PathToLog, FileToLog);
	gx_free(&data1);
	WriteLog("gx_free-End", PathToLog, FileToLog);

	remove(szFlagFileName);
	//WriteLog("Final-1", PathToLog, FileToLog);
	WriteLogFinal(1);
	//WriteLog("Final-2", PathToLog, FileToLog);

	GetDlgItem(IDC_EDIT_QRNUM)->SetFocus();
	WriteLog("Final-SetFocus", PathToLog, FileToLog);

	gMutex.unlock();
}

void CtestProjectDlg::WriteDataIntoFile(CString strBarcode, struct testdatainfo data1, CString tmpSensorID, int nMTFResult)
{
	fstream MTFFile;
	char szMTFFileName[MAX_PATH];
	sprintf_s(szMTFFileName, "D:\\BF_test\\MTF.ini");
	MTFFile.open(szMTFFileName, ios::out);

	if (!MTFFile.fail())
	{
		//*******************
		//	Write Data into File
		//*******************
		char strTemp[128];
		sprintf_s(strTemp, "[Device]\n\r");
		MTFFile.write(strTemp, strlen(strTemp));
		sprintf_s(strTemp, "SN=%s\r\n", strBarcode);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_Tmp = data1.mtf_data[0];
		sprintf_s(strTemp, "MTF=%.2f\r\n", fMtf_data_Tmp);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_Tmp2 = data1.mtf_data[1];
		sprintf_s(strTemp, "MTF2=%.2f\r\n", fMtf_data_Tmp2);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_Tmp3 = data1.mtf_data[2];
		sprintf_s(strTemp, "MTF3=%.2f\r\n", fMtf_data_Tmp3);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_Tmp4 = data1.mtf_data[3];
		sprintf_s(strTemp, "MTF4=%.2f\r\n", fMtf_data_Tmp4);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_Tmp5 = data1.mtf_data[4];
		sprintf_s(strTemp, "MTF5=%.2f\r\n", fMtf_data_Tmp5);
		MTFFile.write(strTemp, strlen(strTemp));

		float fMtf_data_mag = data1.mtf_mag;
		sprintf_s(strTemp, "mtf_mag=%.2f\r\n", fMtf_data_mag);
		MTFFile.write(strTemp, strlen(strTemp));

		sprintf_s(strTemp, "SensorID=%s\r\n", tmpSensorID);
		MTFFile.write(strTemp, strlen(strTemp));

		sprintf_s(strTemp, "MTFResult=%d\r\n", nMTFResult);
		MTFFile.write(strTemp, strlen(strTemp));
	}
	MTFFile.close();
}

void CtestProjectDlg::FWriteLog(const char* fileName, char *logMsg)   //写入;
{
#if 1
	FILE *fLog;

#if 0//_UNICODE
	_wfopen_s(&fLog, fileName, TEXT("a+"));
#else
	fopen_s(&fLog, fileName, "a+");
#endif
	if (fLog == NULL)
	{
		return;
	}
#if 0 //_UNICODE
	fwprintf(fLog, TEXT("%s"), logMsg);
#else
	fprintf(fLog, "%s", logMsg);
#endif

	fflush(fLog);
	fclose(fLog);

#endif
}

void CtestProjectDlg::Savetestinfo(struct testdatainfo info, CString strBarcode)
{
	char szLogFolderPath[MAX_PATH];
	GetModuleFileName(NULL,szLogFolderPath,MAX_PATH);
	*( strrchr(szLogFolderPath,'\\'))=0;
	strcat_s(szLogFolderPath, MAX_PATH, "\\Log");
	CreateDirectory( szLogFolderPath, NULL );

	char szLogFilePath[MAX_PATH];
	strcpy_s(szLogFilePath, MAX_PATH, szLogFolderPath);

	SYSTEMTIME curTime;
	GetLocalTime( &curTime );

	CString strLogFileName;
	strLogFileName.Format("%s\\LogData_%04d-%02d-%02d.CSV",  szLogFilePath, curTime.wYear, curTime.wMonth, curTime.wDay);

	CString strHeader, strSFCColumn;
	/*******************************************************************************************************************************
	Example : 
	strSFCColumn = 	CString("SensorResponseMTT_Result,SensorResponseMTT1,SensorResponseMTT2,");	// <------ 叫穝糤逆
	*******************************************************************************************************************************/

		strSFCColumn  = CString("SensorID,mtf_data1,mtf_data2,mtf_data3,mtf_data4,mtf_data5,mtf_mag,");
		CFile LogFile;
		if(LogFile.Open( strLogFileName,CFile::modeWrite))
		{
			LogFile.SeekToEnd();
		}
		else
		{
			if(LogFile.Open( strLogFileName,CFile::modeCreate|CFile::modeWrite))
			{
				strHeader	=	CString("SN,Date_Time,");
				strHeader	+=	strSFCColumn;
				strHeader	+=	CString("\r\n");
				#ifdef UNICODE
				ConvertUnicode2UTF8ThenWriteLog(strHeader, &LogFile, TRUE);
				#else
				LogFile.Write(strHeader,strHeader.GetLength());
				#endif
			}
			else
			{
				MessageBoxA(NULL,_T("Can't write data to the log file!\nPlease check if the file was opened previously.\nIf so, please close it and try again."),  MB_OK | MB_ICONERROR);
				return;
			}
		}

		CString strDateTime, strData;
		strDateTime.Format("%04d/%02d/%02d %02d:%02d:%02d",curTime.wYear,curTime.wMonth,curTime.wDay,curTime.wHour,curTime.wMinute,curTime.wSecond);

		/*******************************************************************************************************************************
		Example:

 		strData.Format(	"%s,",	//////////////  <----- 穝糤璶纗戈
 						(Data.nResult==Result_Pass)?CString("Pass"):(Data.nResult==Result_Fail)?CString("Fail"):CString("Null"),	
						);
		*******************************************************************************************************************************/
		
		

		LogFile.Close();
}

void CtestProjectDlg::TimeStart()
{
	QueryPerformanceFrequency(&time_freq_sl);//获得整机基准频率
	QueryPerformanceCounter(&time_start_sl);//获得整机基准时钟计数
	sl_time_all = 0;
}

void CtestProjectDlg::TimeOut()
{
	CString str_temp;
	LARGE_INTEGER time_end;
	QueryPerformanceCounter(&time_end);
	UINT real_time = (UINT)((time_end.QuadPart - time_start_sl.QuadPart) * 1000 / time_freq_sl.QuadPart);
	sl_time_all += real_time;
	time_start_sl = time_end;
}

BOOL CtestProjectDlg::FolderExists(CString s)
{
	DWORD attr;
	attr = GetFileAttributes(s);
	return (attr != (DWORD)(-1)) &&
		(attr & FILE_ATTRIBUTE_DIRECTORY);
}

BOOL CtestProjectDlg::CreateMuliteDirectory(CString P)
{
	int len = P.GetLength();
	if (len < 2) return false;
	if ('\\' == P[len - 1])
	{
		P = P.Left(len - 1);
		len = P.GetLength();
	}
	if (len <= 0) return false;
	if (len <= 3)
	{
		if (FolderExists(P))
			return true;
		else
			return false;
	}
	if (FolderExists(P))
		return true;
	CString Parent;
	Parent = P.Left(P.ReverseFind('\\'));
	if (Parent.GetLength() <= 0)
		return false;
	BOOL Ret = CreateMuliteDirectory(Parent);
	if (Ret)
	{
		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor = NULL;
		sa.bInheritHandle = 0;
		Ret = (CreateDirectory(P, &sa) == TRUE);
		return Ret;
	}
	else
		return FALSE;
}

BOOL CtestProjectDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
// 	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE) 
// 		return TRUE;
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN) 
		return TRUE;

#if 1
	unsigned int m_barcode = QRNUM_BYTE;
	bool barcode_start = FALSE;//m_QrTest;//test.s_QrTest;//FALSE;
//	FILE *fp;
	CString str_bb;
	if (barcode_start == TRUE)
	{
		if (pMsg->message == WM_KEYDOWN)
		{
			if (m_barcode)
			{
				if (pMsg->wParam == '\n' || pMsg->wParam == '\r')
				{
					if (strlen(str_barcode) == (UINT)m_barcode)
					{
						KillTimer(5);
						return TRUE;
					}
					str_barcode.Empty();
				}
				else if ((pMsg->wParam >= '0' && pMsg->wParam <= '9')//0x30~0x39
					|| (pMsg->wParam >= 'A' && pMsg->wParam <= 'Z')
					|| (pMsg->wParam >= 0x60 && pMsg->wParam <= 0x69)//小键盘区
					)
				{
					char key;
					if (pMsg->wParam >= 0x60 && pMsg->wParam <= 0x69)
						key = pMsg->wParam - 0x60 + '0';
					else
						key = pMsg->wParam;
					str_barcode += key;
					/***********************/
					// 					str_bb.Format("_length = %d\n", strlen(str_barcode));
					// 					fopen_s(&fp, "./Log/erma.txt", "a+");
					// 					fputs(str_barcode, fp);
					// 					fputs(str_bb, fp);
					// 					fclose(fp);
					/*******************************/
					//					if (barcode_start)
					SetTimer(5, 200, NULL);
				}
				if (strlen(str_barcode) == (UINT)m_barcode)
				{
					KillTimer(5);
				}
				return TRUE;
			}
		}
	}
#endif
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CtestProjectDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == 5)
	{
		KillTimer(5);
	//	str_barcode.Empty();
	}
	CDialogEx::OnTimer(nIDEvent);
}

int CtestProjectDlg::GetSystemPorts(CString *pNameList, CString *pPortList)
{
	CRegKey RegKey;
	int nCount = 0;

	if (RegKey.Open(HKEY_LOCAL_MACHINE, "Hardware\\DeviceMap\\SerialComm") == ERROR_SUCCESS)
	{
		while (true)
		{
			char ValueName[_MAX_PATH];
			unsigned char ValueData[_MAX_PATH];
			DWORD nValueSize = _MAX_PATH;
			DWORD nDataSize = _MAX_PATH;
			DWORD nType;

			if (::RegEnumValue(HKEY(RegKey), nCount, ValueName, &nValueSize, NULL, &nType, ValueData, &nDataSize) == ERROR_NO_MORE_ITEMS)
			{
				break;
			}

			if (pNameList)
				pNameList[nCount] = ValueName;

			if (pPortList)
				pPortList[nCount] = ValueData;

			nCount++;
		}
	}

	return nCount;
}

void CtestProjectDlg::OnClose()
{
	CDialogEx::OnClose();
}

LRESULT CtestProjectDlg::OnSilead_StartTest(WPARAM wParam, LPARAM lParam)
{
	//GetDlgItem(IDC_EDIT_QRNUM)->EnableWindow(false);
	bool isChkImg = ((int)wParam == 1) ? true : false;
	WriteLog("OnSilead_StartTest-1", PathToLog, FileToLog);
	bTestEndFlg=1;
	mtf_info(isChkImg);
	WriteLog("OnSilead_StartTest-2", PathToLog, FileToLog);
	return 0L;
}

LRESULT CtestProjectDlg::OnSilead_StartTest_BF(WPARAM wParam, LPARAM lParam)
{
	//GetDlgItem(IDC_EDIT_QRNUM)->EnableWindow(false);
	WriteLog("OnSilead_StartTest_BF-1", PathToLog, FileToLog);
	bTestEndFlg = 1;
	mtf_info_BF();
	WriteLog("OnSilead_StartTest_BF-2", PathToLog, FileToLog);
	return 0L;
}

LRESULT CtestProjectDlg::SaveLog(WPARAM wParam, LPARAM lParam)
{
	WriteLog("SaveLogFunc-1", PathToLog, FileToLog);
	bTestEndFlg = 0;
	mtf_info();
	max_mtf=0.0;
	max_mtf2=0.0;
	max_mtf3=0.0;
	max_mtf4=0.0;
	max_mtf5=0.0;
	max_mtf_mag=0.0;
	Cnt=0;
	WriteLog("SaveLogFunc-2------------------\r\n", PathToLog, FileToLog);
	//GetDlgItem(IDC_EDIT_QRNUM)->EnableWindow(true);
	return 0L;
}

LRESULT CtestProjectDlg::ResetBoard(WPARAM wParam, LPARAM lParam)
{
	//mtf_info();
	//sleep test
	//DWORD dwStartTick = GetTickCount();
	//int SleepTime=6000;
	//while(1)
	//{
	//	DWORD dwNowTick = GetTickCount();
	//	if ((dwNowTick - dwStartTick) >= SleepTime)
	//	{
	//		break;
	//	}
	//	CString xxx;
	//	xxx.Format("%d",(dwNowTick - dwStartTick));
	//	SetDlgItemText(IDC_RICHEDIT_XIANSHI, xxx);
	//	MSG msg;
	//	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	//	{
	//		TranslateMessage(&msg);
	//		DispatchMessage(&msg);
	//	}
	//	//break;
	//}


	unsigned char *image = (unsigned char *)malloc(320 * 320);
	CString str;
	unsigned int temp_data = 0;

	struct testdatainfo data1;
	gx_malloc(&data1);
	//if(strBarcode != tmpBarcode && Cnt==0 )  //when change module and first test to init
	
	strBarcode.Empty();
	GetDlgItemText(IDC_EDIT_QRNUM,strBarcode); //SN MODE
	WriteLog(strBarcode, PathToLog, FileToLog);
	SetDlgItemText(IDC_RICHEDIT_XIANSHI, "");
		
	gx_read(DEVICES, 0xfc, &temp_data, 1);

	Sleep(10);
	gx_initdata(DEVICES, TESTTHRESHOLDDATA);
	result_data = 0;
	Sleep(50);
	int devices = gx_opendevices(DEVICES);
	if (devices == 0) //open test board fail
	{
		SetDlgItemText(IDC_RICHEDIT_XIANSHI, "init test board fail\r\n");
	}
	else  //open test board success
	{
		Sleep(50);
		gx_vdd_output(DEVICES, 1);
		Sleep(50);
		gx_rst_output(DEVICES, 0);
		Sleep(10);
		gx_rst_output(DEVICES, 1);
		Sleep(20);
			
		gx_read(DEVICES, 0xfc, &temp_data, 1);
		gx_rst_output(DEVICES, 0);
		Sleep(10);
		gx_rst_output(DEVICES, 1);
		Sleep(20);
		gx_read(DEVICES, 0xfc, &temp_data, 1);
		CString str_info;
		str_info.Empty();
		str_info.Format("%08x", temp_data);

		if ((temp_data&0xfff00000) != 0x70100000) //connect module fail
		{
			gsl_ShowId(DEVICES, str_info, 1);
			SetDlgItemText(IDC_RICHEDIT_XIANSHI, "********** connect fail *********\r\n");
			str_barcode.Empty();
			SetDlgItemText(IDC_EDIT_QRNUM,"");
		} 
		else //connect module success
		{
			struct  testObjectResult result_object;
			memset(&result_object, TRUE, sizeof(struct  testObjectResult));
		
			getSensorid(DEVICES, &data1);
			gsl_ShowResult(DEVICES, tmpSensorID, 0);
			tmpSensorID.Format("%s", data1.sensorid);
			str_info.Format("\r\n");
			

			gx_rst_output(DEVICES, 0);
			Sleep(10);
			gx_rst_output(DEVICES, 1);
			Sleep(20);
			int load;
			if(nMTF_Product)//7011 & 7012 flag
				load = gx_loadmain(DEVICES,MAINCONFIG7012);
			else
				load = gx_loadmain(DEVICES,MAINCONFIG);
			if (load != 0)
			{
				str_info = "mainconfig no exist\r\n";
				gsl_ShowResult(DEVICES, str_info, 1);
			}
			gx_autoAgc(DEVICES);
			for(int i=0; i<1; i++)//pat
			{
				int nMTFResult=gx_mft_t(DEVICES, &data1);
				//Sleep(600);
							CString strLogFile;
				SYSTEMTIME curTime;
				strLogFile.Format(_T("%s%s_%d.bmp"), "D:\\MTF0\\", "MTF", i);
				imagecnt++;
				GFP_Write_Bmp_8(strLogFile, data1.image, IMAGE_HEIGHT, IMAGE_WDITH);
			}
			//*
			memcpy(image, data1.image, IMAGE_WDITH * IMAGE_HEIGHT);
			display(image, IMAGE_WDITH, IMAGE_HEIGHT, -1, -1);
			CString str_info;
			str_info.Format("SensorID=%s\r\n\r\nmtf5=%.2f%%\r\nmtf7=%.2f%%\r\nmtf9=%.2f%%\r\nmtf1=%.2f%%\r\nmtf3=%.2f%%\r\n\r\nmtf_mag=%.2f\r\n",
				data1.sensorid, data1.mtf_data[0], data1.mtf_data[1], data1.mtf_data[2], data1.mtf_data[3], data1.mtf_data[4],  data1.mtf_mag);
			SetDlgItemText(IDC_RICHEDIT_XIANSHI, str_info);	
			//*

		}
	}
	GetDlgItem(IDC_EDIT_QRNUM)->SetFocus();
	free(image);
	gx_free(&data1);
	return TRUE;
}

void CtestProjectDlg::WriteLog(CString strData, CString LogPath, CString LogFileName)
{
	//if (LoopTestFlag)
	//	return;

//#ifdef UNICODE
	CFile LogFile;
	CString strLogFile;
	SYSTEMTIME curTime;
	GetLocalTime(&curTime);

	strLogFile.Format(_T("%s%s_%04d-%02d-%02d.txt"), LogPath, LogFileName, curTime.wYear, curTime.wMonth, curTime.wDay);

	if (LogFile.Open(strLogFile, CFile::modeWrite))
	{
		LogFile.SeekToEnd();
	}
	else
	{
		LogFile.Open(strLogFile, CFile::modeCreate | CFile::modeWrite);
	}
	
	CStringA strTempA; // initialized somehow (May need the CStringA if your project is unicode)
	CStringA strTempTime; // initialized somehow (May need the CStringA if your project is unicode)
	strTempA.Empty();strTempTime.Empty();
	strTempTime.Format(_T("[%02d-%02d-%02d-%02d-%02d-%03d]"),  curTime.wMonth, curTime.wDay, curTime.wHour, curTime.wMinute, curTime.wSecond,curTime.wMilliseconds);
	strTempA = _T("\r\n") + strTempTime+ strData;
	const size_t newsizea = (strTempA.GetLength() + 1);

	LogFile.Write(strTempA, newsizea);
	LogFile.Close();
}

void CtestProjectDlg::WriteLogFinal(int nFinal)
{
	CFile FinalLogFile;
	CString strLogFile;
	if (LoopTestFlag)
	return;

	strLogFile.Format(_T("C:\\TestFlag.ini"));//, LogPath, LogFileName, curTime.wYear, curTime.wMonth, curTime.wDay);

	if (FinalLogFile.Open(strLogFile, CFile::modeWrite))
	{
		FinalLogFile.SeekToEnd();
		//WriteLog("write final ini 1", PathToLog, FileToLog);
		FinalLogFile.Close();
	}
	else
	{
		FinalLogFile.Open(strLogFile, CFile::modeCreate | CFile::modeWrite);
	
		CStringA strTempA; // initialized somehow (May need the CStringA if your project is unicode)
		strTempA.Empty();
		strTempA.Format(_T("[Flag]\r\nMTF=%d"),nFinal);
		const size_t newsizea = (strTempA.GetLength() + 1);
		FinalLogFile.Write(strTempA, newsizea);
		FinalLogFile.Close();
		WriteLog("write final ini end", PathToLog, FileToLog);
	}
}

void CtestProjectDlg::OnBnClickedButton1()
{
	
	ResetBoard(NULL,NULL);
	// TODO: 北兜矪瞶盽Α祘Α絏
}


LRESULT CtestProjectDlg::exceptionSaveLog(WPARAM wParam, LPARAM lParam)
{
	WriteLog("exception-1", PathToLog, FileToLog);
	bTestEndFlg = 0;
	mtf_info();
	max_mtf=0.0;
	max_mtf2=0.0;
	max_mtf3=0.0;
	max_mtf4=0.0;
	max_mtf5=0.0;
	max_mtf_mag=0.0;
	Cnt=0;
	SetDlgItemText(IDC_RICHEDIT_XIANSHI, "");
	WriteLog("exception-2------------------\r\n", PathToLog, FileToLog);
	//GetDlgItem(IDC_EDIT_QRNUM)->EnableWindow(true);
	return 0L;
}

LRESULT CtestProjectDlg::OnExposure(WPARAM wParam, LPARAM lParam)
{
	gx_autoAgc(DEVICES);
	WriteLog("gx_autoAgc", PathToLog, FileToLog);
	Sleep(50);
	WriteLogFinal(2);
	WriteLog("WriteExposureLog", PathToLog, FileToLog);
	return 1;
}