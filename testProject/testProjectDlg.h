
// testProjectDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CtestProjectDlg 对话框
class CtestProjectDlg : public CDialogEx
{
// 构造
public:
	CtestProjectDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_TESTPROJECT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg LRESULT OnSilead_StartTest(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSilead_StartTest_BF(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT SaveLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT exceptionSaveLog(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnExposure(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT CloseProgram(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT ResetBoard(WPARAM wParam, LPARAM lParam);
	CString m_result;
	CRichEditCtrl m_xianshi;
	void display(unsigned char *buffer, int w, int h, int w_center, int h_center);
	void gsl_ShowId(int devices, CString str, BOOL color);
	void gsl_ShowResult(int devices, CString str, BOOL color);
	void display_clear(unsigned char *buffer, int w, int h, int w_center, int h_center);
	afx_msg void OnBnClickedButtonMtf();
	void mtf_info(bool isChkImg = false);
	void mtf_info_BF();
	int result_data;
	void gsl_testAllresult(int d);
	LOGFONT lf;
	LOGFONT f;
	CFont m_font;
	int IMAGE_WDITH;
	int IMAGE_HEIGHT;
	BOOL SaveTestImageMode;
	BOOL MTF0Mode;
	BOOL bExposure;
	int	 nMTF_Product;//0=KLO17H;  1=KLO18H & KLO19H
	CString TESTTHRESHOLDDATA;
	void Savetestinfo(struct testdatainfo info, CString str_barcode);
	void FWriteLog(const char* fileName, char *logMsg);   //写入;;
	LARGE_INTEGER time_freq_sl, time_start_sl;
	UINT sl_time_all;
	void TimeStart();
	void TimeOut();
	BOOL CreateMuliteDirectory(CString P);
	BOOL FolderExists(CString s);
	int m_delay_time;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CString str_barcode;
	void WriteLog(CString strData, CString LogPath, CString LogFileName);
	void WriteLogFinal(int nFinal);
	void WriteDataIntoFile(CString strBarcode, struct testdatainfo data1, CString tmpSensorID, int nMTFResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//BOOL m_QrTest;
	bool flag_file;
	char logfilename[300];
	//CString Total_LogDirect;
	CString time;
	CTime ttime;
	CString strBarcode;
	int imagecnt;
	//HANDLE hCom;//全局变量,串口句柄
	int GetSystemPorts(CString *pNameList, CString *pPortList);
	afx_msg void OnClose();
	afx_msg void OnBnClickedButton1();
	CBrush m_Brush;
};
