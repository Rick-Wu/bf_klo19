
struct testObjectResult
{
	bool _openshortResult;
	bool _sramResult;
	bool _flashResult;
	bool _mtfResult;
	bool _shadingResult;
	bool _centerResult;
	bool _pkgAlgResult;
	bool _blotResult;
	bool _lightdpResult;
	bool _apertureResult;
	bool _darkdpResult;
	bool _linearchangeResult;
	bool _expotimeResult;
};


struct TestObject
{
	bool s_openshort;
	bool s_flash;
	bool s_sram;
	bool s_mtf;
	bool s_shading;
	bool s_opticscenter;
	bool s_checkblot;
	bool s_lightbadpoint;
	bool s_apture;
	bool s_darkbadpoint;
	bool s_checkotp;
	bool s_write_sensorid;
	bool s_linearchange;
	bool s_expotime;
	bool s_brightsource;
	bool s_darksource;
	bool s_chart;
	bool s_brightSource;
};