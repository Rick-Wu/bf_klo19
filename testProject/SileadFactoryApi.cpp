#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "SileadFactoryApi.h"
//#include "FingerDetect.h"


#define BMP_COE           256
#define  waterline        64
#define  TRUE             1
#define  FALSE            0

//#ifdef  SPIOPERATION
//#include "OFToSilead_SPIApi.h"
//#endif

#define  ABS(x)  (((x) >= 0) ? (x) : -(x))
#define  GetMin(a,b,c)  ((a>b?b:a)>c ? c:(a>b?b:a))
#ifndef ELEM_SWAP
#define ELEM_SWAP(a,b) { int t=(a);(a)=(b);(b)=t; }
#endif
#define	LINE_COE			256		//注意，这个数在160*205还是安全，更大的尺寸图形会溢出。
#define	WLJ_w_gap			1		//竖线间隔
#define	WLJ_w_gap2			1		//竖线间隔
#define	WLJ_h_gap			1		//横线间隔，不包括最后的判断
#define	WLJ_del_max			32
#define	WLJ_rema_coe		25		//25 == 250% 尾数边和全屏均值比，阈值放大系数
#define	WLJ_delete_w		0		//边缘删除的大小，竖向线
#define	WLJ_delete_rame		0		//尾数边再删边数量
#define	WLJ_ignore_w		1		//在删除边缘的基础上，边缘放宽阈值的大小
#define	WLJ_delete_h		1		//边缘删除的大小，横向线
#define	WLJ_ignore_h		1		//在删除边缘的基础上，边缘放宽阈值的大小
#define	WLJ_del_aw			0		//算竖向平均值的时候，删掉最大值。

/*预处理 ArangeBuffer Cut
a.IamgeBuffer                     图像数据
a.w                               图像宽
a.h                               图像高
a.up                              图像上边缘需要切掉的数据
a.down                            图像下边缘需要切掉的数据
a.left                            图像左边缘需要切掉的数据
a.right                           图像右边缘需要切掉的数据     */
void ArangeBuffer(MEANPERCENT a)
{
	int i = 0;
	unsigned char* retbuffer = new unsigned char[(a.w - (a.left + a.right)) * (a.h - (a.up + a.down))];
	for (i = 0; i < a.h - (a.up + a.down); i++)
	{
		memcpy(retbuffer + i * (a.w - (a.left + a.right)), a.IamgeBuffer + (i + a.up) * a.w + a.left, a.w - (a.left + a.right));
	}
	memcpy(a.IamgeBuffer, retbuffer, (a.w - (a.left + a.right)) * (a.h - (a.up + a.down)));
	delete[] retbuffer;
}

void Cut(MEANPERCENT a)
{

}

/***  InBuffer保存排序并去掉最大和最小值后的平均值，N为需要采集的图像数量，Aver_f，Aver_b分别为去掉最大的Aver_f帧和最小的Aver_b帧的数据 ***/
static int array_and_average(unsigned char *InBuffer, int N, int Aver_f, int Aver_b, bool flag)
{             //array_and_average(a0, m0, 0, 2, TRUE);
	int temp = 0, mean = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N - 1 - i; j++)
		{
			if (InBuffer[j] > InBuffer[j + 1])
			{
				temp = InBuffer[j + 1];
				InBuffer[j + 1] = InBuffer[j];
				InBuffer[j] = temp;
			}
		}
	}

	int sum = 0;
	for (int i = Aver_f; i < N - Aver_b; i++)
	{
		sum += InBuffer[i];
	}
	//	mean = (sum / (N - (Aver_f + Aver_b)) + 0.5);
	mean = sum / (N - (Aver_f + Aver_b));
	if (mean < 5 && flag == TRUE)
		mean = 5;
	return mean;
}

/*** InBuffer保存N张图片，outFrame保存排序后去掉最大最小值后的图像 x/y为图像的长宽，a,b为需要去掉的最大最小的图片数量***/
int PixelArrayAverage(MEANPERCENT a)

{
	unsigned char *TempBuffer = new unsigned char[a.Aver_Num];
	for (int i = 0; i < a.w * a.h; i++)
	{
		for (int j = 0; j < a.Aver_Num; j++)
		{
			TempBuffer[j] = a.Inprebuffer[j][i];
		}
		if (a.Aver_Num >= 3)
		{
			a.IamgeBuffer[i] = array_and_average(TempBuffer, a.Aver_Num, a.Aver_f, a.Aver_b, FALSE);
		}
		else if (a.Aver_Num == 2)
		{
			a.IamgeBuffer[i] = (unsigned char)((TempBuffer[0] + TempBuffer[1]) / 2);
		}
		else
		{
			a.IamgeBuffer[i] = TempBuffer[0];
		}
	}
	delete[] TempBuffer;
	return 0;
}

void  SortMax(int t[], int size)
{
	int temp = 0;
	int m, n;
	for (m = 0; m < size; m++)
	{
		for (n = m + 1; n < size; n++)
		{
			if (t[m] < t[n])
			{
				temp = t[m];
				t[m] = t[n];
				t[n] = temp;
			}
		}
	}
}

void SortMaxCopy(int t[], int t_out[], int size, int n)
{
	if (n > size || n <= 0)
		return;
	int i;
	for (i = 0; i<n; i++)
		t_out[i] = t[i];
	SortMax(t_out, n);
	for (i = n; i<size; i++)
	{
		if (t[i] > t_out[n - 1])
		{
			t_out[n - 1] = t[i];
			SortMax(t_out, n);
		}
	}
}

int  DetectLineAdd(unsigned short *line_buf, unsigned int line_now, unsigned int *line_len, unsigned int line_max)
{
	unsigned int i;
	if (*line_len >= line_max)
		return *line_len;
	for (i = 0; i<*line_len; i++)
	{
		if (line_now == line_buf[i])
			return *line_len;
	}
	line_buf[*line_len] = line_now;
	(*line_len)++;
	return *line_len;
}


void DetectLineSlope(int *buf, int len, int goal)
{
#define slope_coe		1
	int i, j, l;
	int a[200 / 8 + 2];
	int t[8];
	if (len < 24)
		return;
	for (j = 0; j < (len + 7) / 8; j++)
	{
		a[j + 1] = 0;
		for (i = 0; i < 8 && j * 8 + i < len; i++)
			t[i] = buf[j * 8 + i];
		l = i;
		SortMax(t, l);
		if (l == 8)
		{
			for (i = 2; i<l - 2; i++)
				a[j + 1] += t[i];
			a[j + 1] = a[j + 1] * slope_coe / (l - 4);
		}
		else if (l)
		{
			for (i = 0; i<l; i++)
				a[j + 1] += t[i];
			a[j + 1] = a[j + 1] * slope_coe / l;
			a[j + 1] = a[j + 1] + (a[j + 1] - a[j])*(8 - l) / (8 + l);
		}
	}
	a[0] = a[1] * 2 - a[2];
	a[j + 1] = a[j] * 2 - a[j - 1];
	for (i = 0; i <= j; i++)
		a[i] = (a[i] + a[i + 1]);
	a[j + 1] = 0;
	for (i = 0; i < len; i++)
	{
		//buf[i] = buf[i]*goal*16*2 * slope_coe / (a[i/8]*((i/8*8+8-i)*2-1) + a[i/8+1]*((i-i/8*8)*2+1));
		l = ((a[i / 8] * ((i / 8 * 8 + 8 - i) * 2 - 1) + a[i / 8 + 1] * ((i - i / 8 * 8) * 2 + 1)) / 16 / 2);
		if (l)
			buf[i] = buf[i] * goal * slope_coe / l;
	}
}

/*划痕检测  DetectLine
a.IamgeBuffer                     进行划痕测试的图像
a.w = sCutWidth;                  图像的宽
a.h = sCutHeight;                 图像的高
a.DeteValue                       划痕测试的阈值
返回结果：非0为此项测试不过*/
bool DetectLineScan(MEANPERCENT a)
{
#define	LINE_COE	256
	int i = 0, j = 0, m = 0, n = 0;
	int sum = 0;
	bool ret11 = 0;
	int* DetectData_w = new  int[a.w];
	int* DetectData_h = new  int[a.h];
	int* DetectData_w_t = new  int[a.w - 1];
	int* DetectData_h_t = new  int[a.h - 1];
	int ahl = 0, awl = 0;
	//-------------------------------------------------------------------------------------------
#define	line_del_max		32
	int del_max[line_del_max];
	//-------------------------------------------------------------------------------------------
	*a.DetelineNum = 0;
	memset(DetectData_w, 0, a.w);
	memset(DetectData_h, 0, a.h);
	memset(DetectData_w_t, 0, a.w - 1);
	memset(DetectData_h_t, 0, a.h - 1);
	for (i = 0; i < a.w; i++)
	{
		for (j = 0; j < a.h; j++)
		{
			sum += a.IamgeBuffer[a.w * j + i];
		}
		DetectData_w[i] = sum * LINE_COE / a.h;  //DetectData_w表示Y方向每一列的数据相加所求的均值；
		sum = 0;
	}

	for (j = 0; j < a.h; j++)
	{
		for (i = 0; i < a.w; i++)
		{
			sum += a.IamgeBuffer[a.w * j + i];
		}
		DetectData_h[j] = sum * LINE_COE / a.w;  //DetectData_w表示X方向每一列的数据相加所求的均值；
		sum = 0;
	}
	for (m = 0; m < a.w - 8; m++)
	{

		if (abs(DetectData_w[m] - DetectData_w[m + 8]) >= a.DeteV_20 * LINE_COE)
		{
			if (abs(DetectData_w[m] - a.mean * LINE_COE) > abs(DetectData_w[m + 8] - a.mean * LINE_COE))
				DetectLineAdd(a.Deteposition, m + 1, a.DetelineNum, 12);
			else
				DetectLineAdd(a.Deteposition, m + 8 + 1, a.DetelineNum, 12);
		}
		if (abs(DetectData_w[m] - a.mean * LINE_COE) > a.DeteV_m * LINE_COE)
		{
			DetectLineAdd(a.Deteposition, m + 1, a.DetelineNum, 12);
		}
	}

	for (n = 0; n < a.h - 8; n++)
	{
		if (abs(DetectData_h[n] - DetectData_h[n + 8]) >= a.DeteV_20 * LINE_COE)
		{
			if (abs(DetectData_h[n] - a.mean * LINE_COE) >= abs(DetectData_h[n + 8] - a.mean * LINE_COE))
				DetectLineAdd(a.Deteposition, n + a.w + 1, a.DetelineNum, 12);
			else
				DetectLineAdd(a.Deteposition, n + a.w + 8 + 1, a.DetelineNum, 12);
		}
		if (abs(DetectData_h[n] - a.mean * LINE_COE) > a.DeteV_m * LINE_COE)
		{
			DetectLineAdd(a.Deteposition, n + a.w + 1, a.DetelineNum, 12);
		}
	}

	//	DetectLineSlope(DetectData_w,a.w,a.mean*LINE_COE);//减小按压不均匀的影响。

	for (m = 0; m < a.w - 8; m++)
	{
		DetectData_w_t[m] = abs(DetectData_w[m] - DetectData_w[m + 8]);
		awl += DetectData_w_t[m];
	}

	for (n = 0; n < a.h - 1; n++)
	{
		DetectData_h_t[n] = abs(DetectData_h[n] - DetectData_h[n + 1]);
		ahl += DetectData_h_t[n];
	}

	//----------------------------------------------------------------------
	SortMaxCopy(DetectData_w_t, del_max, a.w - 8, line_del_max);
	for (i = 0; i<line_del_max; i++)
		awl -= del_max[i];
	SortMaxCopy(DetectData_h_t, del_max, a.h - 1, line_del_max);
	for (i = 0; i<line_del_max; i++)
		ahl -= del_max[i];
	//----------------------------------------------------------------------
	if (ahl>0 && awl>0)
	{
		for (j = 0; j < a.w - 8; j++)
		{
			if (DetectData_w_t[j] * (a.w - 8 - line_del_max) / awl >(int)a.Deteline_w*((j <= 8 / 4 || j >= a.w - 8 - 8 / 4 - 1) ? 20 : 10) / 10)//10拉出来可设
			{
				if (abs(DetectData_w[j] - a.mean * LINE_COE) >= abs(DetectData_w[j + 8] - a.mean * LINE_COE))
					DetectLineAdd(a.Deteposition, j + 1, a.DetelineNum, 12);
				else
					DetectLineAdd(a.Deteposition, j + 8 + 1, a.DetelineNum, 12);
			}
		}
		for (j = 0; j < a.h - 1; j++)
		{
			if (DetectData_h_t[j] * (a.h - 1 - line_del_max) / ahl >(int)a.Deteline_h)//10拉出来可设
			{
				if (abs(DetectData_h[j] - a.mean * LINE_COE) >= abs(DetectData_h[j + 1] - a.mean * LINE_COE))
					DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
				else
					DetectLineAdd(a.Deteposition, j + a.w + 1 + 1, a.DetelineNum, 12);
			}
		}
	}
	else
		ret11 = 1;     //不可能整屏无噪声
	//--------------------------------------------------------------------------------------------------
	// ∑abs(data[j][i]-(data[j][i+1]+data[j][i-1])/2) 较大的行
	memset(DetectData_h, 0, a.h*sizeof(DetectData_h[0]));
	ahl = 0;

	j = 0;
	for (i = 0; i<a.w; i++)
	{
		DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - (int)a.IamgeBuffer[a.w * (j + 1) + i]);
	}
	ahl += DetectData_h[j];

	j = a.h - 1;
	for (i = 0; i<a.w; i++)
	{
		DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - (int)a.IamgeBuffer[a.w * (j - 1) + i]);
	}
	ahl += DetectData_h[j];

	for (j = 1; j<a.h - 1; j++)
	{
		for (i = 0; i<a.w; i++)
		{
			DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - ((int)a.IamgeBuffer[a.w * (j + 1) + i] + (int)a.IamgeBuffer[a.w * (j - 1) + i]) / 2);
		}
		ahl += DetectData_h[j];
	}

	SortMaxCopy(DetectData_h, del_max, a.h, line_del_max);
	for (i = 0; i<line_del_max; i++)
		ahl -= del_max[i];

	if (ahl > 0)
	{
		for (j = 0; j < a.h; j++)
		{
			DetectData_h[j] = DetectData_h[j] * (a.h - line_del_max) * 4 / ahl;//4,让两个判断阈值相近
		}
		sum = 0;
		for (j = 0; j < a.h; j++)
		{
			if (DetectData_h[j] >= (int)a.Deteline_h)
			{
				m = DetectData_h[j] * 70 / 100;
				n = DetectData_h[j] * 30 / 100;
				if (j == 0 || j == a.h - 1
					|| (DetectData_h[j - 1] >= n && DetectData_h[j - 1] <= m && DetectData_h[j + 1] >= n && DetectData_h[j + 1] <= m))
				{
					sum = 0;
					DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
				}
				else if (sum == 0)
				{
					sum++;
				}
				else
				{//存在连续坏线
					ret11 = 1;
					break;
				}
			}
			else
			{
				sum = 0;
			}
		}
	}
	else
		ret11 = 1;
	//--------------------------------------------------------------------------------------------------
	//DetectLine_ret:
	delete[] DetectData_w;
	delete[] DetectData_h;
	delete[] DetectData_w_t;
	delete[] DetectData_h_t;
	return ret11;
}

bool DetectLineTest(MEANPERCENT a)
{
	int DeteNum = *a.DetelineNum;
	int i = 0, j = 0, m = 0, n = 0;
	unsigned char Dete_w[10] = { 0 };
	unsigned char Dete_h[10] = { 0 };

	if (DeteNum <= 0)
		return 0;
	if (DeteNum > 10)
		DeteNum = 10;
	for (i = 0; i < DeteNum; i++)
	{
		if ((a.Deteposition[i] <= (a.w + a.h)))
		{
			if (a.Deteposition[i] <= a.w)
			{
				Dete_w[m] = a.Deteposition[i];
				m++;
			}
			else
			{
				Dete_h[n] = a.Deteposition[i] - a.w;
				n++;
			}
		}
		else
			return 1;
	}
	int mm = m;      //竖向坏线的个数；
	int nn = n;      //横向坏线的个数；
	int w_flag = 0;   //记录竖向坏线在几个不允许范围内的数目
	//	int h_flag = 0;   //记录竖向坏线在几个不允许范围内的数目
	int temp = 0;

	if (mm > a.DeteW_Num)        //最多允许多少条坏线
		return 1;
	if (nn > a.DeteH_Num)        //横向最多允许多少条坏线
		return 1;

	if (mm > 2)
	{
		for (i = 0; i < mm; i++)
		{
			for (int j = 0; j < mm - 1 - i; j++)
			{
				if (Dete_w[j] > Dete_w[j + 1])
				{
					temp = Dete_w[j + 1];
					Dete_w[j + 1] = Dete_w[j];
					Dete_w[j] = temp;
				}
			}
		}
	}
	else if (mm == 2)
	{
		if (Dete_w[0] > Dete_w[1])
		{
			temp = Dete_w[0];
			Dete_w[0] = Dete_w[1];
			Dete_w[1] = temp;
		}
	}

	for (i = 0; i<mm; i++)
	{
		if ((Dete_w[i] == 1) || (Dete_w[i] == a.w))
			return 1;

	}

	if (mm >= 2)
	{
		for (i = 0; i < mm - 1; i++)
		{
			if ((Dete_w[i + 1] - Dete_w[i]) < 8)
				return 1;
		}
	}

	w_flag = 0;
	if (mm >= 2)
	{
		for (i = 0; i < mm; i++)
		{
			if ((Dete_w[i] >= (33 - a.scut) && Dete_w[i] <= (40 - a.scut)) || (Dete_w[i] >= (65 - a.scut) && Dete_w[i] <= (72 - a.scut)) || (Dete_w[i] >= (89 - a.scut) && Dete_w[i] <= (96 - a.scut)))
				w_flag++;//从1计数
			if (w_flag >= 2)
				return 1;
		}
	}

	if (nn > 2)
	{
		for (i = 0; i < nn; i++)
		{
			for (int j = 0; j < nn - 1 - i; j++)
			{
				if (Dete_h[j] > Dete_h[j + 1])
				{
					temp = Dete_h[j + 1];
					Dete_h[j + 1] = Dete_h[j];
					Dete_h[j] = temp;
				}
			}
		}
	}
	else if (nn == 2)
	{
		if (Dete_h[0] > Dete_h[1])
		{
			temp = Dete_h[0];
			Dete_h[0] = Dete_h[1];
			Dete_h[1] = temp;
		}
	}

	for (i = 0; i<nn; i++)
	{
		if ((Dete_h[i] == 1) || (Dete_h[i] == a.h))
			return 1;
	}

	if (nn >= 2)
	{
		for (i = 0; i < nn - 1; i++)
		{
			if ((Dete_h[i + 1] - Dete_h[i]) < 8)
				return 1;
		}
	}

	for (i = 0; i < nn; i++)
	{
		if ((Dete_h[i] == (25 - a.scut)) || (Dete_h[i] == (61 - a.scut)) || (Dete_h[i] == (88 - a.scut)))//从1计数
			return 1;
	}
	return 0;
}

/*(1)判断待测图像每个像素值是否在图像均值的0~200%范围内(2)判断待测图像每个像素是否都小于等于128
(3)判断待测图像每个像素值与128之差是否在均值与128差值的20%~200%之内*/
int RuinpixelTest_one(MEANPERCENT a)
{
	int singleRuinPixel = 0;
	int mean_diff = 0;
	int ruinflag = 0;
	int mean_aver;
	unsigned char *diffBuffer = new unsigned char[a.w * a.h];
	for (int i = 0; i < a.w; i++)
	{
		switch (i % 8)
		{
		case 0:
			mean_aver = a.mean_aver[0];
			break;
		case 1:
			mean_aver = a.mean_aver[1];
			break;
		case 2:
			mean_aver = a.mean_aver[2];
			break;
		case 3:
			mean_aver = a.mean_aver[3];
			break;
		case 4:
			mean_aver = a.mean_aver[4];
			break;
		case 5:
			mean_aver = a.mean_aver[5];
			break;
		case 6:
			mean_aver = a.mean_aver[6];
			break;
		case 7:
			mean_aver = a.mean_aver[7];
			break;
		default:
			break;
		}
		for (int j = 0; j < a.h; j++)
		{
			if (abs((a.Twobuffer[a.w * j + i] - mean_aver)) > a.DeadP_20)
			{
				ruinflag = 1;          //表示检测到死点
				a.Twobuffer[a.w * j + i] = 255;
			}

			if (abs((a.Twobuffer[a.w * j + i] - mean_aver)) > a.DeadP_5)
			{
				if ((a.Twobuffer[a.w * j + i] > mean_aver * a.mean_percent_high / 100) || (a.Twobuffer[a.w * j + i] < mean_aver * a.mean_percent_low / 100))
				{
					ruinflag = 1;          //表示检测到死点
					a.Twobuffer[a.w * j + i] = 255;
				}
			}
			if (a.Onebuffer[a.w * j + i] >= a.Twobuffer[a.w * j + i])
			{
				diffBuffer[a.w * j + i] = (int)a.Onebuffer[a.w * j + i] - (int)a.Twobuffer[a.w * j + i];
				if ((diffBuffer[a.w * j + i] < (a.avr_mean_one - mean_aver) *a.diff_low / 100) || (diffBuffer[a.w * j + i] > (a.avr_mean_one - mean_aver) * a.diff_high / 100))
				{
					a.Twobuffer[a.w * j + i] = 0;
					ruinflag = 1;       //表示检测到死点
				}
			}
			else
			{
				a.Twobuffer[a.w * j + i] = 255;
				ruinflag = 1;          //表示检测到死点
			}
			if (ruinflag == 1)
			{
				singleRuinPixel++;
			}
			ruinflag = 0;
		}

	}
	delete[] diffBuffer;
	return singleRuinPixel;
}

/*(1)判断待测图像每个像素值是否在图像均值的30%~200%范围内(2)判断待测图像每个像素是否都小于等于128
(3)判断待测图像每个像素值与128之差是否在均值与128差值的20%~200%之内*/
int RuinpixelTest_two(MEANPERCENT a)
{
	int singleRuinPixel = 0;
	int mean_diff = 0;
	int ruinflag = 0;
	unsigned char *diffBuffer = new unsigned char[a.w * a.h];

	mean_diff = a.avr_mean_two - a.avr_mean_one;

	for (int i = 0; i < a.w * a.h; i++)
	{
		int ruinflag = 0;
		if (a.mean_percent_low > 100)
			a.mean_percent_low = 100;

		if ((a.Twobuffer[i] > a.avr_mean_two * a.mean_percent_high / 100) || (a.Twobuffer[i] < a.avr_mean_two * a.mean_percent_low / 100))
		{
			ruinflag = 1;          //表示检测到死点
			a.Twobuffer[i] = 0;
		}

		if (a.Twobuffer[i] >= a.Onebuffer[i])
		{
			diffBuffer[i] = (int)a.Twobuffer[i] - (int)a.Onebuffer[i];
			if ((diffBuffer[i] < a.diff_low * mean_diff / 100) || (diffBuffer[i] > a.diff_high * mean_diff / 100))
			{
				a.Twobuffer[i] = 0;
				ruinflag = 1;       //表示检测到死点
			}
		}
		else
		{
			a.Twobuffer[i] = 255;
			ruinflag = 1;          //表示检测到死点
		}

		if (ruinflag == 1)
		{
			singleRuinPixel++;
		}
		ruinflag = 0;
	}

	delete[] diffBuffer;
	return singleRuinPixel;
}


//int Imageaverage(unsigned char *InBuffer, int w, int h)MEANPERCENT a
int Imageaverage(MEANPERCENT a)
{
	int mean = 0;
	int sum = 0;
	if (a.w * a.h == 0)
		return 0;
	for (int i = 0; i < a.w * a.h; i++)
	{
		sum = sum + a.IamgeBuffer[i];
	}
	mean = sum / (a.w * a.h);
	return mean;
}

int Imageaverage_new(MEANPERCENT a)
{
	int mean = 0;
	int sum = 0;
	if (a.w * a.h == 0)
		return 0;
	for (int i = 0; i < a.w * a.h; i++)
	{
		sum = sum + a.IamgeBuffer[i];
	}
	mean = sum * 256 / (a.w * a.h);
	return mean;
}

void DiffunitTest(MEANPERCENT a)
{
	int unit_w = (a.w + 2) / 4;
	int unit_h = (a.h + 2) / 4;
	int i, j, m, n;
	double sum = 0;
	int unit_num = 0;

	for (j = 0; j < 4; j++)
	{
		if (j == 3)
		{
			for (i = 0; i < 4; i++)
			{
				sum = 0;
				unit_num = 0;
				if (i == 3)
				{
					for (n = j*unit_h; n < a.h; n++)
					{
						for (m = i*unit_w; m < a.w; m++)
						{
							//sum += Inbuf[m + n * w];
							sum += a.IamgeBuffer[m + n * a.w];
							unit_num++;
						}
					}
				}
				else
				{
					for (n = j*unit_h; n < a.h; n++)
					{
						for (m = i*unit_w; m < (i + 1)*unit_w; m++)
						{
							//	sum += Inbuf[m + n * a.w];
							sum += a.IamgeBuffer[m + n * a.w];
							unit_num++;
						}
					}
				}
				//			Obuf[4 * j + i] = sum / unit_num;
				a.p[4 * j + i] = sum / unit_num;
			}

		}
		else
		{
			for (i = 0; i < 4; i++)
			{
				sum = 0;
				unit_num = 0;
				if (i == 3)
				{
					for (n = j*unit_h; n < (j + 1)*unit_h; n++)
					{
						for (m = i*unit_w; m < a.w; m++)
						{
							//sum += Inbuf[m + n * a.w];
							sum += a.IamgeBuffer[m + n * a.w];
							unit_num++;
						}
					}
				}
				else
				{
					for (n = j*unit_h; n < (j + 1)*unit_h; n++)
					{
						for (m = i*unit_w; m < (i + 1)*unit_w; m++)
						{
							//sum += Inbuf[m + n * w];
							sum += a.IamgeBuffer[m + n * a.w];
							unit_num++;
						}
					}
				}
				//Obuf[4 * j + i] = sum / unit_num;
				a.p[4 * j + i] = sum / unit_num;
			}
		}
	}
}

//void UnitTest(unsigned char *Inbuf, unsigned char *Obuf, int w, int h)
void UnitTest(MEANPERCENT a)
{
	int w_small = (a.w + 2) / 4;
	int h_small = (a.h + 2) / 4;
	int i_block = 0;
	int j_block = 0;
	int i, j;
	int k = 0;
	int iMean_sum[4][4] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int iMean_num[4][4] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	unsigned char iMean[4][4] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	for (j = 0; j < a.h; j++)
	{
		for (i = 0; i < a.w; i++)
		{
			if (i < 3 * w_small)
				i_block = i / w_small;
			else
				i_block = 3;

			if (j < 3 * h_small)
				j_block = j / h_small;
			else
				j_block = 3;

			iMean_sum[j_block][i_block] += a.IamgeBuffer[i + j*a.w];//Inbuf[i + j*w];
			iMean_num[j_block][i_block] ++;
		}
	}

	for (j = 0; j < 4; j++)
	{
		for (i = 0; i < 4; i++)
		{
			iMean[i][j] = iMean_sum[i][j] / iMean_num[i][j];
			//Obuf[k++] = iMean[i][j];
			a.p[k++] = iMean[i][j];
		}
	}
}


/*灵敏度检测
a.avr_mean_one                 图像的整体均值
a.avr_mean_two                 图像的整体均值
返回结果：avr_mean_one - avr_mean_two,即灵敏度大小           */
int SensitiveTest(MEANPERCENT a)
{
	int avr = 0;

	avr = a.avr_mean_one - a.avr_mean_two;

	return avr;
}

int quick_select_medianw(int arr[], unsigned int n)
{
	unsigned int low, high;
	unsigned int median;
	unsigned int middle, ll, hh;
	low = 0; high = n - 1; median = (low + high) / 2;
	for (;;) {
		if (high <= low)
			return arr[median];
		if (high == low + 1) {
			if (arr[low] > arr[high])
				ELEM_SWAP(arr[low], arr[high]);
			return arr[median];
		}
		middle = (low + high) / 2;
		if (arr[middle] > arr[high])
			ELEM_SWAP(arr[middle], arr[high]);
		if (arr[low] > arr[high])
			ELEM_SWAP(arr[low], arr[high]);
		if (arr[middle] > arr[low])
			ELEM_SWAP(arr[middle], arr[low]);
		ELEM_SWAP(arr[middle], arr[low + 1]);
		ll = low + 1;
		hh = high;
		for (;;) {
			do ll++; while (arr[low] > arr[ll]);
			do hh--; while (arr[hh] > arr[low]);
			if (hh < ll)
				break;
			ELEM_SWAP(arr[ll], arr[hh]);
		}
		ELEM_SWAP(arr[low], arr[hh]);
		if (hh <= median)
			low = ll;
		if (hh >= median)
			high = hh - 1;
	}
	return arr[median];
}

void SileadfpFirstLine(unsigned char* lpFingerImage, int width, int height)
{
	int i;
	unsigned char *lpSrc = 0;
	unsigned char *lpDst = 0;

	lpSrc = lpFingerImage + 1;
	lpDst = lpFingerImage;
	for (i = 0; i < height; i++)
	{
		*lpDst = *lpSrc;

		lpSrc += width;
		lpDst += width;
	}
}


void SileadfpInsertRow(unsigned char* lpFingerImage, unsigned char Id, int width, int height)
{
	int i, sub, sum;
	int shift0 = width - 1;
	int shift1 = width;
	int shift2 = width + 1;

	unsigned int g;
	unsigned int r0, r1, r2;

	unsigned char *lpSrc = 0;
	unsigned char *lpDst = 0;

	// edge
	lpSrc = lpFingerImage + Id*width;
	lpDst = lpFingerImage + Id*width;
	sum = *(lpSrc + shift1) + *(lpSrc - shift1);
	*lpDst = (sum) >> 1;

	lpSrc = lpFingerImage + width - 1 + Id*width;
	lpDst = lpFingerImage + width - 1 + Id*width;
	sum = *(lpSrc + shift1) + *(lpSrc - shift1);
	*lpDst = (sum) >> 1;

	// pointer initialization
	lpSrc = lpFingerImage + Id*width;
	lpDst = lpFingerImage + Id*width;
	for (i = 1; i < width - 1; i++)
	{
		lpSrc++;
		lpDst++;

		sub = *(lpSrc + shift0) - *(lpSrc - shift0);
		sum = (*(lpSrc + shift0) + *(lpSrc - shift0)) >> 1;
		sub = ABS(sub);
		r0 = (sub << 16) | (sum);
		sub = *(lpSrc + shift1) - *(lpSrc - shift1);
		sum = (*(lpSrc + shift1) + *(lpSrc - shift1)) >> 1;
		sub = ABS(sub);
		r1 = (sub << 16) | (sum);
		sub = *(lpSrc + shift2) - *(lpSrc - shift2);
		sum = (*(lpSrc + shift2) + *(lpSrc - shift2)) >> 1;
		sub = ABS(sub);
		r2 = (sub << 16) | (sum);
		g = GetMin(r0, r1, r2);
		*lpDst = (unsigned char)(g & 0x0000ffff);
	}
}


void SileadfpInsertCol(unsigned char* lpFingerImage, unsigned char Id, int width, int height)
{
	int i, sub, sum;
	int shift0 = 1 - width;
	int shift1 = 1;
	int shift2 = width + 1;

	unsigned int g;
	unsigned int r0, r1, r2;

	unsigned char *lpSrc = 0;
	unsigned char *lpDst = 0;

	// edge
	lpSrc = lpFingerImage + Id;
	lpDst = lpFingerImage + Id;
	sum = *(lpSrc + shift1) + *(lpSrc - shift1);
	*lpDst = (sum) >> 1;

	lpSrc = lpFingerImage + Id + (height - 1)*width;
	lpDst = lpFingerImage + Id + (height - 1)*width;
	sum = *(lpSrc + shift1) + *(lpSrc - shift1);
	*lpDst = (sum) >> 1;

	// pointer initialization
	lpSrc = lpFingerImage + Id;
	lpDst = lpFingerImage + Id;
	for (i = 1; i < height - 1; i++)
	{
		lpSrc += width;
		lpDst += width;

		sub = *(lpSrc + shift0) - *(lpSrc - shift0);
		sum = (*(lpSrc + shift0) + *(lpSrc - shift0)) >> 1;
		sub = ABS(sub);
		r0 = (sub << 16) | (sum);
		sub = *(lpSrc + shift1) - *(lpSrc - shift1);
		sum = (*(lpSrc + shift1) + *(lpSrc - shift1)) >> 1;
		sub = ABS(sub);
		r1 = (sub << 16) | (sum);
		sub = *(lpSrc + shift2) - *(lpSrc - shift2);
		sum = (*(lpSrc + shift2) + *(lpSrc - shift2)) >> 1;
		sub = ABS(sub);
		r2 = (sub << 16) | (sum);
		r2 = (sub << 16) | (sum);
		g = GetMin(r0, r1, r2);
		*lpDst = (unsigned char)(g & 0x0000ffff);
	}
}

void SileadfpMedian(unsigned char* lpFingerImage, int width, int height, unsigned char* rowID, unsigned char* colID, unsigned char Rnum, unsigned char Cnum)
{
	int i, j, crow, ccol, widthi = width - 1, heighti = height - 1;
	unsigned char *lpSrc = 0;
	unsigned char *lpDst = 0;
	int widval[9] = { 0 };

	if (Rnum != 0)
	{
		for (i = 1; i < Rnum; i++)
		{
			crow = *(rowID + i);
			for (j = 1; j < widthi; j++)
			{
				lpSrc = lpFingerImage + crow * width + j;
				lpDst = lpFingerImage + crow * width + j;
				widval[0] = *(lpSrc - width - 1);
				widval[1] = *(lpSrc - width - 0);
				widval[2] = *(lpSrc - width + 1);
				widval[3] = *(lpSrc - 0 - 1);
				widval[4] = *(lpSrc - 0 - 0);
				widval[5] = *(lpSrc - 0 + 1);
				widval[6] = *(lpSrc + width - 1);
				widval[7] = *(lpSrc + width - 0);
				widval[8] = *(lpSrc + width + 1);
				*lpDst = quick_select_medianw(widval, 9);
			}
		}
	}

	if (Cnum != 0)
	{
		for (i = 1; i < Cnum; i++)
		{
			ccol = *(colID + i);
			for (j = 1; j < heighti; j++)
			{
				lpSrc = lpFingerImage + j * width + ccol;
				lpDst = lpFingerImage + j * width + ccol;

				widval[0] = *(lpSrc - width - 1);
				widval[1] = *(lpSrc - width - 0);
				widval[2] = *(lpSrc - width + 1);
				widval[3] = *(lpSrc - 0 - 1);
				widval[4] = *(lpSrc - 0 - 0);
				widval[5] = *(lpSrc - 0 + 1);
				widval[6] = *(lpSrc + width - 1);
				widval[7] = *(lpSrc + width - 0);
				widval[8] = *(lpSrc + width + 1);

				*lpDst = quick_select_medianw(widval, 9);
			}
		}
	}

}

/*差值函数 SileadfpInsert_618285  SileadfpInsert61XX
Img.IamgeBuffer               图像数据
Img.w                         图像宽
Img.h                         图像高           */
void SileadfpInsert_618285(MEANPERCENT a)  //差值
//void SileadfpInsert_618285(unsigned char* lpFingerImage, int width, int height, int sign)
{
	//    int i, j, 
	int k;
	int n = 8;
	int st0 = 0;
	int st1 = 0;
	int end0 = a.h - 1;
	int end1 = a.w - 1;
	int sign = 0;
	unsigned char *lpSrc = 0;
	unsigned char *lpDst = 0;
	unsigned char rowId[9] = { 0, 24, 39, 56, 71, 88, 103, 120, 135 };
	unsigned char colId[9] = { 0, 24, 39, 56, 71, 88, 103, 120, 135 };

	for (k = 1; k <= n; k++)
	{
		st0 = rowId[k];
		st1 = colId[k];
		SileadfpInsertRow(a.IamgeBuffer, st0, a.w, a.h);
		SileadfpInsertCol(a.IamgeBuffer, st1, a.w, a.h);
	}
	SileadfpMedian(a.IamgeBuffer, a.w, a.h, rowId, colId, 9, 9);

	if (sign == 1)
	{
		SileadfpFirstLine(a.IamgeBuffer, a.w, a.h);
	}
}

void SileadfpInsert61XX(MEANPERCENT a)  //差值
{

}

//void sArangeBuffer(unsigned char *intputbuffer, unsigned char *outputbuffer, int width, int height, int scut)
void sArangeBuffer(MEANPERCENT a)
{
	int i = 0;
	memset(a.Twobuffer, 0, (a.w - 2 * a.scut) * (a.h - 2 * a.scut));

	for (i = 0; i < (a.h - 2 * a.scut); i++)
	{
		memcpy(a.Twobuffer + i*(a.w - 2 * a.scut), a.Onebuffer + (a.scut + i)*a.w + a.scut, (a.w - 2 * a.scut));
	}
}

static int Load_Img(const char filename[], unsigned char *ucImg)
{
	FILE *fp;
	unsigned char *p;
	int ImgSize = -1;

	fopen_s(&fp, filename, "rb");

	if (fp == NULL)
	{
		return ImgSize;
	}

	p = ucImg;

	while (!feof(fp))
	{
		*p++ = fgetc(fp);
		ImgSize++;
	}

	fclose(fp);

	return ImgSize;
}

static int Load_Bmp(const char filename[], unsigned char *ucImg, int *h, int *w)
{
	int i, j, k, l, LineBytes;
	unsigned char c;
	int ImgSize = -1;

	FILE *fp;

	WL_BITMAPFILEHEADER		bmfHdr;
	WL_BITMAPINFOHEADER		bmiHdr;
	unsigned int			palNum;

	fopen_s(&fp, filename, "rb");

	if (fp == NULL)
		return ImgSize;

	fread(&bmfHdr, 1, sizeof(WL_BITMAPFILEHEADER), fp);
	if (bmfHdr.bfType != BMP_TYPE)
	{
		return ImgSize;
	}

	fread(&bmiHdr, 1, sizeof(WL_BITMAPINFOHEADER), fp);
	if (bmiHdr.biSize != sizeof(WL_BITMAPINFOHEADER))
	{
		return ImgSize;
	}

	switch (bmiHdr.biBitCount)
	{
	case 1:
	case 8:
		palNum = (bmiHdr.biClrUsed != 0) ? bmiHdr.biClrUsed : (1 << bmiHdr.biBitCount);
		break;
	case 24:
	case 32:
		palNum = 0;
		break;
	default:
		return ImgSize;
	}

	ImgSize = bmiHdr.biHeight * bmiHdr.biWidth;

	*h = bmiHdr.biHeight;
	*w = bmiHdr.biWidth;

	//fseek(fp,sizeof(WL_RGBQUAD)*palNum,SEEK_CUR);
	fseek(fp, 1024 + 54, SEEK_SET);
	switch (bmiHdr.biBitCount)
	{
	case 1:
		LineBytes = WIDTHBYTES(bmiHdr.biWidth);
		for (i = 0; i < bmiHdr.biHeight; i++)
		for (j = 0; j < LineBytes; j++)
		{
			c = fgetc(fp);
			k = j * 8;
			for (l = 0; l < 8; l++)
			if (k + l < bmiHdr.biWidth)
				ucImg[(bmiHdr.biHeight - 1 - i)*bmiHdr.biWidth + k + l] = (c >> (7 - l)) & 1;
		}
		break;

	case 8:  // 假设为8位灰度图，不考虑8位索引图
		LineBytes = WIDTHBYTES(bmiHdr.biWidth * 8);
		for (i = 0; i < bmiHdr.biHeight; i++)
		for (j = 0; j < LineBytes; j++)
		{
			c = fgetc(fp);

			if (j < bmiHdr.biWidth)
				ucImg[(bmiHdr.biHeight - 1 - i)*bmiHdr.biWidth + j] = c;
		}
		break;

	case 24:
		LineBytes = WIDTHBYTES(bmiHdr.biWidth * 3 * 8);
		for (i = 0; i < bmiHdr.biHeight; i++)
		{
			for (j = 0; j < bmiHdr.biWidth; j++)  // 0.2989 * R + 0.5870 * G + 0.1140 * B
			{
				unsigned char r, g, b;

				b = fgetc(fp);
				g = fgetc(fp);
				r = fgetc(fp);

				ucImg[(bmiHdr.biHeight - 1 - i)*bmiHdr.biWidth + j] = (unsigned char)(0.2989*r + 0.5870*g + 0.1140*b + 0.5);
			}

			for (j = 3 * (bmiHdr.biWidth); j < LineBytes; j++) fgetc(fp);
		}
		break;
	case 32:
		LineBytes = WIDTHBYTES(bmiHdr.biWidth * 3 * 8);
		for (i = 0; i < bmiHdr.biHeight; i++)
		{
			for (j = 0; j < bmiHdr.biWidth; j++)  // 0.2989 * R + 0.5870 * G + 0.1140 * B
			{
				unsigned char r, g, b;

				b = fgetc(fp);
				g = fgetc(fp);
				r = fgetc(fp);
				fgetc(fp);

				ucImg[(bmiHdr.biHeight - 1 - i)*bmiHdr.biWidth + j] = (unsigned char)(0.2989*r + 0.5870*g + 0.1140*b + 0.5);
			}
		}
	}

	fclose(fp);

	return ImgSize;
}

int GFPLoadImage(const char filename[], unsigned char *ucImage, int *h, int *w)
{
	int i;
	char str[50] = { 0 };
	int ImgSize = 0;

	*h = 0;
	*w = 0;

	for (i = strlen(filename) - 1; i >= 0; i--)
	{
		if (filename[i] == '.')
		{
			strcpy_s(str, filename + i + 1);
			break;
		}
	}
	if (str == "")
		return ImgSize;

	_strlwr_s(str);

	if (strcmp(str, "img") == 0 || strcmp(str, "dat") == 0)
	{
		ImgSize = Load_Img(filename, ucImage);

		return ImgSize;
	}

	if (strcmp(str, "bmp") == 0)
	{
		ImgSize = Load_Bmp(filename, ucImage, h, w);

		return ImgSize;
	}

	return ImgSize;
}

void GFP_Write_Bmp_8(const char filename[], unsigned char *ucImg, int h, int w)
{
	FILE *fp;
	int i, j, LineBytes, palNum = 256;

	WL_BITMAPFILEHEADER  bmfHdr;
	WL_BITMAPINFOHEADER  bmiHdr;
	LPWL_RGBQUAD         pPalette;

	fopen_s(&fp, filename, "wb");

	if (fp == NULL)
	{
		printf("save file error!\n");
		return;
	}

	// WL_BITMAPFILEHEADER
	bmfHdr.bfType = 0x4d42;
	bmfHdr.bfSize = sizeof(WL_BITMAPFILEHEADER)+
		sizeof(WL_BITMAPINFOHEADER)+
		sizeof(WL_RGBQUAD)*palNum +
		WIDTHBYTES(w * 8)* h;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits = sizeof(WL_BITMAPFILEHEADER)+
		sizeof(WL_BITMAPINFOHEADER)+
		sizeof(WL_RGBQUAD)*palNum;
	fwrite(&bmfHdr, 1, sizeof(WL_BITMAPFILEHEADER), fp);

	// WL_BITMAPINFOHEADER
	bmiHdr.biSize = 40;
	bmiHdr.biWidth = w;
	bmiHdr.biHeight = h;
	bmiHdr.biPlanes = 1;
	bmiHdr.biBitCount = 8;
	bmiHdr.biCompression = BI_RGB;
	bmiHdr.biSizeImage = WIDTHBYTES(w * 8) * h;
	bmiHdr.biXPelsPerMeter = 0;
	bmiHdr.biYPelsPerMeter = 0;
	bmiHdr.biClrUsed = 0;
	bmiHdr.biClrImportant = 0;
	fwrite(&bmiHdr, 1, sizeof(WL_BITMAPINFOHEADER), fp);

	// PALETTE
	pPalette = (LPWL_RGBQUAD)malloc(sizeof(WL_RGBQUAD)*palNum);
	for (i = 0; i < palNum; i++)
	{
		pPalette[i].rgbBlue = i;
		pPalette[i].rgbGreen = i;
		pPalette[i].rgbRed = i;
		pPalette[i].rgbReserved = 0;
	}
	fwrite((void*)pPalette, sizeof(WL_RGBQUAD), palNum, fp);
	//计算图像每行像素所占的字节数（必须是4的倍数）
	LineBytes = WIDTHBYTES(w * 8);

	//PIXEL DATA
	for (i = 0; i < h; i++)
	for (j = 0; j < LineBytes; j++)
	if (j < w)
		fputc(ucImg[(h - 1 - i)*w + j], fp);
	else
		fputc('0', fp);

	fclose(fp);
	free(pPalette);

	return;
}


int DetectLineAver(MEANPERCENT a)
{
	int i, j;
	int m0 = 0, m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0, m7 = 0;
	int group = 0;
	int sum = 0;
	int aver_w = 0, aver_h = 0;
	unsigned char* a0 = new unsigned char[20];
	unsigned char* a1 = new unsigned char[20];
	unsigned char* a2 = new unsigned char[20];
	unsigned char* a3 = new unsigned char[20];
	unsigned char* a4 = new unsigned char[20];
	unsigned char* a5 = new unsigned char[20];
	unsigned char* a6 = new unsigned char[20];
	unsigned char* a7 = new unsigned char[20];
	unsigned char* DetectData_w = new unsigned char[a.w];

	memset(DetectData_w, 0, a.w);

	for (i = 0; i < a.w; i++)
	{
		for (j = 0; j < a.h; j++)
		{
			sum += a.IamgeBuffer[a.w * j + i];
		}
		DetectData_w[i] = sum / a.h;  //DetectData_w表示Y方向每一列的数据相加所求的均值；
		sum = 0;
	}
	for (i = 0; i < a.w; i++)
	{
		group = i % 8;
		switch (group)
		{
		case  0:
			a0[m0] = DetectData_w[i];
			m0++;
			break;
		case  1:
			a1[m1] = DetectData_w[i];
			m1++;
			break;
		case  2:
			a2[m2] = DetectData_w[i];
			m2++;
			break;
		case  3:
			a3[m3] = DetectData_w[i];
			m3++;
			break;
		case  4:
			a4[m4] = DetectData_w[i];
			m4++;
			break;
		case  5:
			a5[m5] = DetectData_w[i];
			m5++;
			break;
		case  6:
			a6[m6] = DetectData_w[i];
			m6++;
			break;
		case  7:
			a7[m7] = DetectData_w[i];
			m7++;
			break;
		default:
			break;
		}
	}

	a.mean_aver[0] = array_and_average(a0, m0, 0, 2, TRUE);
	a.mean_aver[1] = array_and_average(a1, m1, 0, 2, TRUE);
	a.mean_aver[2] = array_and_average(a2, m2, 0, 2, TRUE);
	a.mean_aver[3] = array_and_average(a3, m3, 0, 2, TRUE);
	a.mean_aver[4] = array_and_average(a4, m4, 0, 2, TRUE);
	a.mean_aver[5] = array_and_average(a5, m5, 0, 2, TRUE);
	a.mean_aver[6] = array_and_average(a6, m6, 0, 2, TRUE);
	a.mean_aver[7] = array_and_average(a7, m7, 0, 2, TRUE);

	delete[] a0;
	delete[] a1;
	delete[] a2;
	delete[] a3;
	delete[] a4;
	delete[] a5;
	delete[] a6;
	delete[] a7;
	delete[] DetectData_w;
	return 0;
}

int  GetMax(unsigned char **data, int i, int j)
{
	int sum = 0;
	sum += data[j - 1][i - 1];
	sum += data[j - 1][i];
	sum += data[j - 1][i + 1];
	sum += data[j][i - 1];
	sum += data[j][i];
	sum += data[j][i + 1];
	sum += data[j + 1][i - 1];
	sum += data[j + 1][i];
	sum += data[j + 1][i + 1];
	sum = sum / 9;
	return sum;
}

bool InitData(unsigned char **data, int *aw, int *ah, int *al, int w, int h)
{
	int i, j;

	*al = 0;
	for (j = 0; j < h; j++)
		ah[j] = 0;
	for (i = 0; i < w; i++)
		aw[i] = 0;

	for (j = 0; j < h; j++)
	{
		for (i = 0; i < w; i++)
		{
			aw[i] += data[j][i];
			ah[j] += data[j][i];
			*al += data[j][i];
		}
	}
	for (j = 0; j < h; j++)
		ah[j] = ah[j] * BMP_COE / w;
	for (i = 0; i < w; i++)
		aw[i] = aw[i] * BMP_COE / h;
	*al = (*al) * BMP_COE / (h*w);
	return TRUE;
}

void Avg(unsigned char **data, int *aw, int *ah, int *al, int w, int h)
{
	int t;
	int i, j;

	for (j = 0; j < h; j++)
	{
		for (i = 0; i< w; i++)
		{
			t = ((int)data[j][i] * 256 + (*al) - aw[i] - ah[j] + 128) / 256 + waterline;
			if (t > 255)
				data[j][i] = 255;
			else if (t < 0)
				data[j][i] = 0;
			else
				data[j][i] = t;
		}
	}
}

void DiffMax(unsigned char **data, int *al, int mid_val, int edge_val, int w, int h)
{
	int i, j;
	int m = 0, me = 0, mi = waterline, mj = waterline;
	int t;
	int si = 0, sj = 0;

#define	mei	10

	for (j = 1; j < h - 1; j++)
	{
		for (i = 1; i<w - 1; i++)
		{
			t = GetMax(data, i, j);
			if (i <= mei || j <= mei || i >= w - 1 - mei || j >= h - 1 - mei)
			{
				if (t > me)
				{
					me = t;
				}
				if (t<mi)
				{
					mi = t;
				}
			}
			else
			{
				if (t > m)
				{
					m = t;
				}
				if (t < mj)
				{
					mj = t;
				}
			}
		}
	}
}

int BubbleNear(unsigned char **data, int i2, int j2, int v, int w, int h)
{
	int i, j;
	if (data[j2][i2] != 0xff)
		return FALSE;
	for (j = j2 - 1; j <= j2 + 1; j++)
	{
		for (i = i2 - 1; i <= i2 + 1; i++)
		{
			if (i >= 0 && j >= 0 && i < w && j < h && data[j][i] == v)
				return TRUE;
		}
	}
	return FALSE;
}

void BubbleArea(unsigned char **data, int *al, int mean, int mid_val, int edge_val, int w, int h, int *size, int *n, unsigned char *Image)   //n为气泡个数
{
	int i, j;
	int thres = 0;
	int v, t;
	int avr = 0;

	unsigned char **data_new = new unsigned char *[h];

	for (int i = 0; i < h; i++)
	{
		data_new[i] = new unsigned char[w];
	}

	for (j = 0; j < h; j++)
	{
		for (i = 0; i < w; i++)
		{
			data_new[j][i] = 0;// data[j][i];
		}
	}

	for (j = 1; j < h - 1; j++)
	{
		for (i = 1; i < w - 1; i++)
		{
			if (i <= mei || j <= mei || i >= w - 1 - mei || j >= h - 1 - mei)
			{
				//thres = ((mean * BMP_COE - *al) * edge_val + (50 * BMP_COE)) / (100 * BMP_COE);
				thres = edge_val;
			}
			else
			{
				//thres = ((mean * BMP_COE - *al) * mid_val + (50 * BMP_COE)) / (100 * BMP_COE);
				thres = mid_val;
			}
			avr = 0;
			avr = GetMax(data, i, j);
			if (avr > (waterline + thres) || avr < (waterline - thres))
			{
				data_new[j][i] = 0xff;//data[j][i] = 0xff;
			}
			else
			{
				data_new[j][i] = 0x00;//data[j][i] = 0x00;
			}
		}
	}
	*n = 0;
	while (1)
	{
		v = 0;
		for (j = 0; j < h; j++)
		{
			for (i = 0; i < w; i++)
			{
				if (data_new[j][i] == 0xff && (*n) < 200)
				{
					(*n)++;
					v = 0xff - (*n);
					data_new[j][i] = v;
					goto ba_continue;
				}
			}
		}
		break;
	ba_continue:
		do
		{
			t = FALSE;
			for (j = 0; j < h; j++)
			{
				for (i = 0; i < w; i++)
				{
					if (BubbleNear(data_new, i, j, v, w, h))   //判断该点的旁边是否有气泡，如果有则说明该点属于该气泡，返回TRUE表示是一个气泡
					{
						data_new[j][i] = v;
						t = TRUE;
					}
				}
			}
			for (j = h - 1; j >= 0; j--)
			{
				for (i = w - 1; i >= 0; i--)
				{
					if (BubbleNear(data_new, i, j, v, w, h))
					{
						data_new[j][i] = v;
						t = TRUE;
					}
				}
			}
		} while (t);
	}

	memset(size, 0, 201 * 4);
	for (j = 0; j < h; j++)
	{
		for (i = 0; i<w; i++)
		{
			if (data_new[j][i] > 0xff - 200)
				size[0xff - data_new[j][i]] ++;
		}
	}
	v = 0;
	j = 0;
	for (i = 1; i<200; i++)   //找面积最大的气泡
	{
		if (size[i] > v)
		{
			v = size[i];
			j = i;
		}
	}

	memset(Image, 0, h * w);

	for (i = 0; i < h; i++)
	{
		for (j = 0; j < w; j++)
		{
			Image[j + i*w] = data_new[i][j];
		}
	}

	for (i = 0; i < h; i++)
	{
		delete data_new[i];
	}
	delete[] data_new;
}


void BubbleTest_Avg(MEANPERCENT a)
{
	int *aw = new int[a.w];
	int *ah = new int[a.h];
	int al = 0;
	unsigned char **data = new unsigned char *[a.h];

	memset(aw, 0, a.w * 4);
	memset(ah, 0, a.h * 4);
	for (int i = 0; i < a.h; i++)
		data[i] = &a.Input_Buffer[i * a.w];

	InitData(data, aw, ah, &al, a.w, a.h);
	Avg(data, aw, ah, &al, a.w, a.h);
	DiffMax(data, &al, a.mid_val, a.edge_val, a.w, a.h);
	BubbleArea(data, &al, a.Mean_Bu, a.mid_val, a.edge_val, a.w, a.h, a.size, a.n, a.Output_Buffer);

	if (data)
	{
		delete[] data;
		data = NULL;
	}
	delete[] aw;
	delete[] ah;
}


void FingerBLKTest_sl(unsigned char* img, int width, int height, int* pResult, int ExpSize)
{
	//FingerBLKTest(img, width, height, pResult, ExpSize);
}

int gsl_remove_dead_line_hardware(MEANPERCENT a)
//int gsl_remove_dead_line_hardware(unsigned char * img, int col_num, int row_num, int line_count, unsigned char * line_location, int remove_dead_line_type = 3)
{
	a.remove_dead_line_type = 3;
	if (a.line_count == 0 || a.img == NULL)
		return 0;
	else
	{
		unsigned char dead_line_index = 0;
		for (int i = 0; i < a.line_count; i++)
		{
			dead_line_index = a.line_location[i];
			if (dead_line_index > a.w)
			{
				if (0 != (a.remove_dead_line_type & 2))
				{
					SileadfpInsertRow(a.img, (dead_line_index - a.w - 1), a.w, a.h);
				}
			}
			else
			{
				if (0 != (a.remove_dead_line_type & 1))
				{
					SileadfpInsertCol(a.img, dead_line_index - 1, a.w, a.h);
				}
			}
		}
		return 1;
	}
}

int WLJ_CW(int w, int i, int gap, int scut)
{
	if (i < WLJ_ignore_w + scut)//if (i < WLJ_ignore_w + WLJ_delete_w)
		return 2;
	if (i >= w - gap - WLJ_ignore_w - scut - w % 8)//if (i < WLJ_ignore_w + WLJ_delete_w)
		return 2;
	return 1;
}

int WLJ_CH(int h, int j, int gap, int scut)
{
	if (j < WLJ_ignore_h + scut)//if (j < WLJ_ignore_h + WLJ_delete_h)
		return 2;
	if (j >= h - gap - WLJ_ignore_h - scut)//if (j >= h - gap - WLJ_ignore_h - WLJ_delete_h)
		return 2;
	return 1;
}

#define Before_8  1
bool HardwareDetectLineTest(MEANPERCENT a)
{
	int i = 0, j = 0;
	int k;
	int sum = 0;
	int ret11 = 0;
	int* DetectData_w = new  int[a.w];
	int* DetectData_h = new  int[a.h];
	int* DetectData_w_t = new  int[a.w - 1];
	int* DetectData_h_t = new  int[a.h - 1];
	int ahl = 0, awl = 0;
	//-------------------------------------------------------------------------------------------
	int remainder = a.w % 8;//不可用于切边的图像！！！！！！！！
	int t_m, t_n;
	int del_max[WLJ_del_max > WLJ_del_aw ? WLJ_del_max : WLJ_del_aw];
	if (a.w == 0 || a.h == 0)
		return 1;
	//-------------------------------------------------------------------------------------------
	*a.DetelineNum = 0;
	memset(DetectData_w, 0, (a.w) * 4);
	memset(DetectData_h, 0, (a.h) * 4);
	memset(DetectData_w_t, 0, (a.w - 1) * 4);
	memset(DetectData_h_t, 0, (a.h - 1) * 4);
	for (i = 0; i < a.w; i++)
	{
		sum = 0;
		memset(del_max, 0, WLJ_del_aw * 4);
		for (j = a.scut; j < a.h - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - WLJ_delete_h; j++)
		{
			sum += a.IamgeBuffer[a.w * j + i];
			if (WLJ_del_aw != 0 && a.IamgeBuffer[a.w * j + i] > del_max[WLJ_del_aw - 1])
			{
				del_max[WLJ_del_aw - 1] = a.IamgeBuffer[a.w * j + i];
				SortMax(del_max, WLJ_del_aw);
			}
		}
		for (j = 0; j < WLJ_del_aw; j++)
			sum -= del_max[j];
		DetectData_w[i] = sum * LINE_COE / (a.h - WLJ_del_aw);  //竖向平均值。
	}

	for (j = 0; j < a.h; j++)
	{
		sum = 0;
		for (i = a.scut; i < a.w - a.scut; i++)//for (i = WLJ_delete_w; i < a.w - WLJ_delete_w; i++)
		{
			sum += a.IamgeBuffer[a.w * j + i];
		}
		DetectData_h[j] = sum * LINE_COE / a.w;  //DetectData_w表示X方向每一列的数据相加所求的均值；
	}
	for (i = a.scut; i < a.w - WLJ_w_gap - remainder - a.scut; i++)//for (i = WLJ_delete_w; i < a.w - WLJ_w_gap - remainder - WLJ_delete_w; i++)
	{
#if Before_8
		if (i == 7)
			continue;//前8列集体偏差，忽略
#endif
		if (abs(DetectData_w[i] - DetectData_w[i + WLJ_w_gap]) >= a.DeteV_ww * LINE_COE * WLJ_CW(a.w, i, WLJ_w_gap, a.scut))
		{
			if (abs(DetectData_w[i] - a.mean * LINE_COE) > abs(DetectData_w[i + WLJ_w_gap] - a.mean * LINE_COE))
				DetectLineAdd(a.Deteposition, i + 1, a.DetelineNum, 12);
			else
				DetectLineAdd(a.Deteposition, i + WLJ_w_gap + 1, a.DetelineNum, 12);
		}
	}
	// 		if (abs(DetectData_w[m] - a.mean * LINE_COE) > a.DeteV_m * LINE_COE)
	// 		{
	// 			DetectLineAdd(a.Deteposition, m + 1, a.DetelineNum, 12);
	// 		}
	if (remainder >= 2 + WLJ_delete_rame)
	{
		for (i = 0; i<remainder - WLJ_delete_rame; i++)
		{
			del_max[i] = DetectData_w[a.w - remainder + i];
			if (abs(DetectData_w[a.w - remainder + i] - a.mean * LINE_COE) >(a.DeteV_ww * 2) * LINE_COE * WLJ_rema_coe / 10)
				DetectLineAdd(a.Deteposition, a.w - remainder + i + 1, a.DetelineNum, 12);
		}
		SortMax(del_max, remainder);
		if (del_max[0] - del_max[remainder - 1] > a.DeteV_ww * LINE_COE * 2)
			ret11 = 1;//retrun 0;
	}
	else// if(remainder >= 1)
	{
	}

	for (j = a.scut; j < a.h - WLJ_h_gap - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - WLJ_h_gap - WLJ_delete_h; j++)
	{
		if (abs(DetectData_h[j] - DetectData_h[j + WLJ_h_gap]) >= a.DeteV_hh * LINE_COE * WLJ_CH(a.h, j, WLJ_h_gap, a.scut))
		{
			if (abs(DetectData_h[j] - a.mean * LINE_COE) >= abs(DetectData_h[j + WLJ_h_gap] - a.mean * LINE_COE))
				DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
			else
				DetectLineAdd(a.Deteposition, j + a.w + WLJ_h_gap + 1, a.DetelineNum, 12);
		}
	}
	// 		if (abs(DetectData_h[n] - a.mean * LINE_COE) > a.DeteV_m * LINE_COE)
	// 		{
	// 			DetectLineAdd(a.Deteposition, n + a.w + 1, a.DetelineNum, 12);
	// 		}
	// 	}

	//	DetectLineSlope(DetectData_w, a.w, a.mean*LINE_COE);//减小按压不均匀的影响。

	for (i = a.scut; i < a.w - WLJ_w_gap2 - a.scut - remainder; i++)//for (i = WLJ_delete_w; i < a.w - WLJ_w_gap2 - WLJ_delete_w - remainder; i++)
	{
		DetectData_w_t[i] = abs(DetectData_w[i] - DetectData_w[i + WLJ_w_gap2]);
		awl += DetectData_w_t[i];
	}

	for (j = a.scut; j < a.h - 1 - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - 1 - WLJ_delete_h; j++)
	{
		DetectData_h_t[j] = abs(DetectData_h[j] - DetectData_h[j + 1]);
		ahl += DetectData_h_t[j];
	}

	//----------------------------------------------------------------------
	SortMaxCopy(DetectData_w_t, del_max, a.w - WLJ_w_gap2, WLJ_del_max);
	for (i = 0; i<WLJ_del_max; i++)
		awl -= del_max[i];
	SortMaxCopy(DetectData_h_t, del_max, a.h - 1, WLJ_del_max);
	for (j = 0; j<WLJ_del_max; j++)
		ahl -= del_max[j];
	//----------------------------------------------------------------------
	if (ahl>0 && awl>0)
	{
		for (i = a.scut; i < a.w - WLJ_w_gap2 - remainder - a.scut; i++)//for (i = WLJ_delete_w; i < a.w - WLJ_w_gap2 - remainder - WLJ_delete_w; i++)
		{
			//if (DetectData_w_t[j] * (a.w - 1 - line_del_max) / awl >(int)a.Deteline_w)//10拉出来可设
			if (DetectData_w_t[i] * (a.w - WLJ_w_gap2 - WLJ_del_max) / awl >(int)a.Deteline_w * WLJ_CW(a.w, i, WLJ_w_gap2, a.scut))
			{
				if (abs(DetectData_w[i] - a.mean * LINE_COE) >= abs(DetectData_w[i + WLJ_w_gap2] - a.mean * LINE_COE))
					DetectLineAdd(a.Deteposition, i + 1, a.DetelineNum, 12);
				else
					DetectLineAdd(a.Deteposition, i + WLJ_w_gap2 + 1, a.DetelineNum, 12);
			}
		}
		for (j = a.scut; j < a.h - 1 - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - 1 - WLJ_delete_h; j++)
		{
			if (DetectData_h_t[j] * (a.h - 1 - WLJ_del_max) / ahl >(int)a.Deteline_h * WLJ_CH(a.h, j, 1, a.scut))
			{
				if (abs(DetectData_h[j] - a.mean * LINE_COE) >= abs(DetectData_h[j + 1] - a.mean * LINE_COE))
					DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
				else
					DetectLineAdd(a.Deteposition, j + a.w + 1 + 1, a.DetelineNum, 12);
			}
		}
	}
	else
		ret11 = 1;     //不可能整屏无噪声
	//--------------------------------------------------------------------------------------------------
#define	div_h		2
	for (k = 0; k < div_h; k++)
	{
		ahl = 0;
		for (j = 0; j < a.h; j++)
		{
			DetectData_h[j] = 0;
			for (i = a.w / div_h*k; i < a.w / div_h*(k + 1); i++)
			{
				DetectData_h[j] += a.IamgeBuffer[a.w * j + i];
			}
			DetectData_h[j] = DetectData_h[j] * LINE_COE / (a.w / div_h);
		}
		for (j = a.scut; j < a.h - 1 - a.scut; j++)
		{
			if (abs(DetectData_h[j] - DetectData_h[j + 1]) >= (a.DeteV_hh + div_h / 2) * LINE_COE)
			{//每行均值 和 旁边行均值的比较
				if (abs(DetectData_h[j] - a.mean * LINE_COE) >= abs(DetectData_h[j + 1] - a.mean * LINE_COE))
					DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
				else
					DetectLineAdd(a.Deteposition, j + a.w + 1 + 1, a.DetelineNum, 12);
			}
		}
	}
	// ∑abs(data[j][i]-(data[j][i+1]+data[j][i-1])/2) 较大的行
	memset(DetectData_h, 0, a.h*sizeof(DetectData_h[0]));
	ahl = 0;

	j = a.scut;//j = WLJ_delete_h;
	for (i = 0; i < a.w; i++)
	{
		DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - (int)a.IamgeBuffer[a.w * (j + 1) + i]);
	}
	ahl += DetectData_h[j];

	j = a.h - 1 - a.scut;//j = a.h - 1 - WLJ_delete_h;
	for (i = 0; i < a.w; i++)
	{
		DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - (int)a.IamgeBuffer[a.w * (j - 1) + i]);
	}
	ahl += DetectData_h[j];

	for (j = 1 + a.scut; j < a.h - 1 - a.scut; j++)//for (j = 1 + WLJ_delete_h; j < a.h - 1 - WLJ_delete_h; j++)
	{
		for (i = 0; i < a.w; i++)
		{
			DetectData_h[j] += abs((int)a.IamgeBuffer[a.w * j + i] - ((int)a.IamgeBuffer[a.w * (j + 1) + i] + (int)a.IamgeBuffer[a.w * (j - 1) + i]) / 2);
		}
		ahl += DetectData_h[j];
	}

	SortMaxCopy(DetectData_h, del_max, a.h, WLJ_del_max);
	for (i = 0; i<WLJ_del_max; i++)
		ahl -= del_max[i];

	if (ahl > 0)
	{
		for (j = a.scut; j < a.h - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - WLJ_delete_h; j++)
		{
			DetectData_h[j] = DetectData_h[j] * (a.h - WLJ_del_max) * 4 / ahl;//4,让两个判断阈值相近
		}
		sum = 0;
		for (j = a.scut; j < a.h - a.scut; j++)//for (j = WLJ_delete_h; j < a.h - WLJ_delete_h; j++)
		{
			if (DetectData_h[j] >= (int)a.Deteline_h)
			{
				t_m = DetectData_h[j] * 70 / 100;
				t_n = DetectData_h[j] * 30 / 100;
				if (j == 0 || j == a.h - 1
					|| (DetectData_h[j - 1] >= t_n && DetectData_h[j - 1] <= t_m && DetectData_h[j + 1] >= t_n && DetectData_h[j + 1] <= t_m))
				{
					sum = 0;
					DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
				}
				else if (sum == 0)
				{
					sum++;
				}
				else
				{//存在连续坏线
					ret11 = 1;
					break;
				}
			}
			else
			{
				sum = 0;
			}
		}
	}
	else
		ret11 = 1;
	//--------------------------------------------------------------------------------------------------
	memset(DetectData_h_t, 0, (a.h - 1) * 4);
	for (k = 0; k < a.w / 8; k++)
	{
		for (j = 0; j < a.h; j++)
		{
			DetectData_h[j] = 0;
			for (i = k * 8; i < (k + 1) * 8; i++)
			{
				DetectData_h[j] += a.IamgeBuffer[a.w * j + i];
			}
			DetectData_h[j] = DetectData_h[j] * LINE_COE / 8;
		}
		for (j = 0; j < a.h - 1; j++)
		{
			if (abs(DetectData_h[j] - DetectData_h[j + 1]) >= a.DeteV_hh * 2 * LINE_COE)
			{
				DetectData_h_t[j]++;
			}
		}
	}
	for (j = a.scut; j < a.h - 1 - a.scut; j++)//	for (j = WLJ_delete_h; j < a.h - 1 - WLJ_delete_h; j++)
	{
		if (DetectData_h_t[j] >= 4)
		{
			if (j + 1 < a.h - 1 - a.scut && DetectData_h_t[j + 1] >= 3)//if (j + 1 < a.h - 1 - WLJ_delete_h && DetectData_h_t[j + 1] >= 3)
				DetectLineAdd(a.Deteposition, j + a.w + 1 + 1, a.DetelineNum, 12);
			else
				DetectLineAdd(a.Deteposition, j + a.w + 1, a.DetelineNum, 12);
		}
	}
	//DetectLine_ret:
	delete[] DetectData_w;
	delete[] DetectData_h;
	delete[] DetectData_w_t;
	delete[] DetectData_h_t;
	if (ret11 == 1 && *a.DetelineNum == 0)//err
	{
		DetectLineAdd(a.Deteposition, 255, a.DetelineNum, 12);
	}
	return ret11;
}

void SortMin(int *buf, int num)
{
	int temp = 0;
	for (int i = 0; i < num; i++)
	{
		for (int j = 0; j < num - 1 - i; j++)
		{
			if (buf[j] > buf[j + 1])
			{
				temp = buf[j + 1];
				buf[j + 1] = buf[j];
				buf[j] = temp;
			}
		}
	}
}
int HardwareDeadTest(MEANPERCENT a,unsigned int *max)
{
	int i, j;
	int t;
	int buf[4];
	int n = 0;
	unsigned char **data = new unsigned char *[a.h];
	for (i = 0; i < a.h; i++)
		data[i] = &a.Input_Buffer[i * a.w];

	unsigned char **data1 = new unsigned char *[a.h];
	for (j = 0; j < a.h; j++)
		data1[j] = &a.Input_Buffer[j * a.w];

	for (j = 0; j < a.h; j++)
	{
		for (i = 0; i<a.w; i++)
		{

			if (j>0)
				buf[0] = data[j - 1][i];
			else
				buf[0] = data[j + 1][i];
			if (i > 0)
				buf[1] = data[j][i - 1];
			else
				buf[1] = data[j][i + 1];
			if (i < a.w - 1)
				buf[2] = data[j][i + 1];
			else
				buf[2] = data[j][i - 1];
			if (j<a.h - 1)
				buf[3] = data[j + 1][i];
			else
				buf[3] = data[j - 1][i];
			SortMin(buf, 4);

			t = data[j][i];
			t = abs(t - buf[1]) + abs(t - buf[2]);
			if (t > a.DeadValno)
			{
				if (n<36)
					max[n] = t;
				n++;
				data1[j][i] = 255;
			}
		}
	}

	for (i = 0; i < a.h; i++)
	{
		for (j = 0; j < a.w; j++)
			a.Input_Buffer[i * a.w + j] = data1[i][j];
	}

	if (data)
	{
		delete[] data;
		data = NULL;
	}
	if (data1)
	{
		delete[] data1;
		data1 = NULL;
	}
	return n;
}
