#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <string.h>
#include <windows.h>
#include "SileadCommLib.h"
#include "Interface.h"
#include "InterfaceEx.h"
#include "STM_Interface.h"
//#include "SileadFactoryApi.h"

#define	MULTI_MAX	8
static HANDLE busy[MULTI_MAX] = { NULL };
static void sl_delay(int ms_delay);

static int SL_InitDLL(int selectLib)
{
	CInterface tif;
	int interface_init = 0;
	CSTM_Interface stm;
	if (0 == selectLib)
	{
		memset(&tif, 0, sizeof(tif));
		//Device Init API
//		tif.ScanDevice = stm.ScanDevices;
		tif.FindOpen = stm.FindOpen;
		tif.Close = stm.Close;
		tif.Info = stm.Info;

		//Device GPIO API
		tif.GPIO_Input = stm.GPIO_Input;
		tif.GPIO_Output = stm.GPIO_Output;
//		tif.Set_Avdd = stm.Set_Avdd_tt;
//		tif.Read_IntData = NULL;

		//Device IIC API
		tif.size_iic = 60;
		tif.IIC_ReadBase = stm.IIC_ReadBase;
		tif.IIC_WriteBase = stm.IIC_WriteBase;
		tif.IIC_Read = stm.IIC_Read;
		tif.IIC_Write = stm.IIC_Write;
		tif.IIC_R_GSL = stm.IIC_R_GSL;
		tif.IIC_W_GSL = stm.IIC_W_GSL;

		//Device SPI API
		tif.size_spi = 60;
		tif.SPI_WR = stm.SPI_WR;
		tif.SPI_Read = stm.SPI_Read;
		tif.SPI_Write = stm.SPI_Write;
		tif.SPI_R_GSL = stm.SPI_R_GSL;
		tif.SPI_W_GSL = stm.SPI_W_GSL;

		tif.GPIO_Config = stm.GPIO_Config;
		tif.IIC_Config = stm.IIC_Config;
		tif.SPI_Config = stm.SPI_Config;

		tif.GSL_Function = stm.GSL_Function;
		tif.SPI_R_GSL_FLASH = stm.SPI_R_GSL_FLASH;
		tif.SPI_W_GSL_FLASH = stm.SPI_W_GSL_FLASH;
		tif.ENABLE_W_FLASH = stm.ENABLE_W_FLASH;
		tif.CS_ENABLE_FLASH = stm.CS_ENABLE_FLASH;
	}
#if 0
	else if (1 == selectLib)
	{
		memset(&tif, 0, sizeof(tif));
		//Device Init API
//		tif.ScanDevice = ScanDevices;
		tif.FindOpen = FindOpen;
		tif.Close = CloseDeivecs;
		tif.Info = NULL;// = NULL 可以不写

		//Device GPIO API
		tif.GPIO_Input = GPIO_Input;
		tif.GPIO_Output = GPIO_Output;
//		tif.Set_Avdd = Set_Avdd;
//		tif.Read_IntData = NULL;

		//Device IIC API
		tif.size_iic = 60;
		tif.IIC_ReadBase = NULL;
		tif.IIC_WriteBase = NULL;
		tif.IIC_Read = IIC_Read;
		tif.IIC_Write = IIC_Write;
		tif.IIC_R_GSL = NULL;
		tif.IIC_W_GSL = NULL;

		//Device SPI API
		tif.size_spi = 60;
		tif.SPI_WR = SPI_WR;
		tif.SPI_Read = SPI_Read;
		tif.SPI_Write = SPI_Write;
		tif.SPI_R_GSL = SPI_R_GSL;
		tif.SPI_W_GSL = SPI_W_GSL;

		tif.GPIO_Config = Ginkgo_GPIO_Config;
		tif.IIC_Config = Ginkgo_IIC_Config;
		tif.SPI_Config = Ginkgo_SPI_Config;
	}
#endif
	else
	{
	}

	interface_init = InitIf(&tif);

	return interface_init;
}

//int SL_ScanDevice(int* devnum)
//{
//	return tif.ScanDevice(devnum);
//}

static BOOL GetMutex(int dev)
{
	if (dev < 0)
	{
		int i;
		for (i = 0; i < MULTI_MAX; i++)
		{
			if (WaitForSingleObject(busy[i], 1000) != WAIT_OBJECT_0)
			{
				for (i--; i >= 0;i--)
					ReleaseMutex(busy[i]);
				return FALSE;
			}
		}
		return TRUE;
	}
	else
	{
		if (WaitForSingleObject(busy[dev], 1000) != WAIT_OBJECT_0)
		{
			return FALSE;
		}
		return TRUE;
	}
}

static int DevSave(int dev)
{
	static int dev_prev = 0;
	if (dev >= 0)
		dev_prev = dev;
	return dev_prev;
}

int SL_DevCheck(int dev)
{
	int ret = FALSE;
	if (dev >= 8)
		return FALSE;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	if (DevSave(-1) & (1 << dev))
		ret = TRUE;
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_FindOpen(int spimode, int spi_speed_set, bool flag)
{
	static int dev_kind = 0;
	static int speed_save = -1;
	int i;
	int dev_now;
	int open_mode;
	int dev_init = 0;
	if (busy[0] == NULL)
	{
		busy[0] = CreateMutex(NULL, FALSE, L"Silead factory test busy 0");
		busy[1] = CreateMutex(NULL, FALSE, L"Silead factory test busy 1");
		busy[2] = CreateMutex(NULL, FALSE, L"Silead factory test busy 2");
		busy[3] = CreateMutex(NULL, FALSE, L"Silead factory test busy 3");
		busy[4] = CreateMutex(NULL, FALSE, L"Silead factory test busy 4");
		busy[5] = CreateMutex(NULL, FALSE, L"Silead factory test busy 5");
		busy[6] = CreateMutex(NULL, FALSE, L"Silead factory test busy 6");
		busy[7] = CreateMutex(NULL, FALSE, L"Silead factory test busy 7");
		SL_InitDLL(dev_kind);
	}

	if (GetMutex(-1) == FALSE)
		return -1;
	open_mode = 0;
	if (flag == FALSE)
		open_mode |= 0x10;//屏蔽IIC
	for (i = 0; i < 1; i++)
	{
		dev_now = sif.FindOpen(flag);
// 		if (dev_now == 0)
// 		{
// 			dev_kind++;
// 			if (dev_kind >= 2)
// 				dev_kind = 0;
// 			DevSave(0);
// 			if (SL_InitDLL(dev_kind) == FALSE)
// 				break;//正常写代码，不应该跑这里。
// 			continue;
// 		}

		if (dev_now != DevSave(-1))
		{
			if (dev_now != 0)
			{//初始化
				dev_init = 1;
			}
			DevSave(dev_now);
		}
		if (speed_save != spi_speed_set)
		{
			dev_init = 1;
		}
		if (dev_now != 0 && dev_init != 0)
		{
			UINT data_conf[1];
			int k;
			for (k = 0; k < 8; k++)
			{
				if ((dev_now & (1 << k)) == 0)
					continue;
				data_conf[0] = 100 * 1000;
				sif.IIC_Config(k, 0, data_conf);
				if (spi_speed_set == 1)
					data_conf[0] = 1125 * 1000;
				else if (spi_speed_set == 2)
					data_conf[0] = 2250 * 1000;
				else if (spi_speed_set == 3)
					data_conf[0] = 4500 * 1000;
				else if (spi_speed_set == 4)
					data_conf[0] = 9000 * 1000;
				else if (spi_speed_set == 5)
					data_conf[0] = 18000 * 1000;
				else
					data_conf[0] = 4500 * 1000;
				sif.SPI_Config(k, 0, data_conf);
			//	data_conf[0] = 0xffff802;
				data_conf[0] = 0xfffef002;
				//0为输出，1为输入。没用到的设为输入.
				//目前配置0(O):RST; 1(I):IRQ; 2(O):Power; 3(O)IN1; 4(O)IN2;5(0)IO1; 6(0)IO2; 7(0)IO3; 8(0)IO4; 9(0)IO5; 10(O)IO6; 11(0)DEMO;
				//12(I)电流档位;13(I)ID1;14(I)ID2;15(I)ID4;  
				sif.GPIO_Config(k, 0, data_conf);
				data_conf[0] = 0x0000e000;
				sif.GPIO_Config(k, 1, data_conf);//ID需要上拉
				sif.GPIO_Output(k, 0x18, 0x10);//IN2=1,IN1=0
			}
			speed_save = spi_speed_set;
		}
		if (dev_now)
			break;
	}
	for (i = 0; i < MULTI_MAX; i++)
		ReleaseMutex(busy[i]);
	return dev_now;
}


int	SL_DeviceClose(int dev)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.Close(dev);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_DeviceInfo(int dev, char **str)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.Info(dev, str);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IIC_Read(int dev, unsigned char iic_addr, unsigned char offset, unsigned char *buf_char, int len_char)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
//	iic_addr >>= 1;
	ret = sif.IIC_Read(dev, iic_addr, offset, buf_char, len_char);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IIC_Write(int dev, unsigned char iic_addr, unsigned char offset, unsigned char *buf_char, int len_char)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
//	iic_addr >>= 1;
	ret = sif.IIC_Write(dev, iic_addr, offset, buf_char, len_char);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IIC_R_GSL(int dev, unsigned char iic_addr, unsigned int addr, unsigned int *buf, int len_int)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.IIC_R_GSL(dev, iic_addr, addr, buf, len_int);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IIC_W_GSL(int dev, unsigned char iic_addr, unsigned int addr, unsigned int *buf, int len_int)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.IIC_W_GSL(dev, iic_addr, addr, buf, len_int);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_SPI_Read(int dev, unsigned char spi_ss, unsigned char offset, unsigned char *buf_char, int len_char)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.SPI_Read(dev, spi_ss, offset, buf_char, len_char);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_SPI_Write(int dev, unsigned char spi_ss, unsigned char offset, unsigned char *buf_char, int len_char)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.SPI_Write(dev, spi_ss, offset, buf_char, len_char);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_SPI_R_GSL(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.SPI_R_GSL(dev, spi_ss, addr, buf, len_int);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_SPI_R_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data,unsigned char *buf, int len_int)
{

	for (int n = 0; n<3; n++)
	{
#if 0
		int ret;
		ret = sif.SPI_R_GSL_FLASH(dev, spi_ss, addr, data, buf, len_int);
		return ret;
#else
		if (len_int > 44)
		{
			int count = len_int / 44 + 1;
			for (int i = 0; i < count; i++)
			{
				if (i != count - 1)
				{
					if (GetMutex(dev) == FALSE)
						return 1;
					sif.CS_ENABLE_FLASH(dev, 0);
					sif.SPI_R_GSL_FLASH(dev, spi_ss, addr, data + i * 44, buf + i * 44, 44 + 4);
					sif.CS_ENABLE_FLASH(dev, 1);
					ReleaseMutex(busy[dev]);
				}
				else
				{
				    if (GetMutex(dev) == FALSE)
						return 1;
					sif.CS_ENABLE_FLASH(dev, 0);
					sif.SPI_R_GSL_FLASH(dev, spi_ss, addr, data + i * 44, buf + i * 44, len_int - i * 44 + 4);
					sif.CS_ENABLE_FLASH(dev, 1);
					ReleaseMutex(busy[dev]);
				}
			}
		}
		else
		{
			if (GetMutex(dev) == FALSE)
				return 1;
			sif.CS_ENABLE_FLASH(dev, 0);
			sif.SPI_R_GSL_FLASH(dev, spi_ss, addr, data, buf, len_int + 4);
			sif.CS_ENABLE_FLASH(dev, 1);
			ReleaseMutex(busy[dev]);
		}
	}
	return TRUE;
#endif
}
int	SL_SPI_W_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int)
{
	for (int n = 0; n<3; n++)
	{
		if (len_int > 44)
		{
			int count = len_int / 44 + 1;
			for (int i = 0; i < count; i++)
			{
				if (i != count - 1)
				{
					if (GetMutex(dev) == FALSE)
						return 1;
					sif.CS_ENABLE_FLASH(dev, 0);
					sif.ENABLE_W_FLASH(dev, spi_ss, 0x6);
					sif.CS_ENABLE_FLASH(dev, 1);

					sif.CS_ENABLE_FLASH(dev, 0);
					sif.SPI_W_GSL_FLASH(dev, spi_ss, addr, data + i * 44, buf + i * 44, 44 + 4);
					sif.CS_ENABLE_FLASH(dev, 1);
					ReleaseMutex(busy[dev]);
				}
				else
				{
					if (GetMutex(dev) == FALSE)
						return 1;
					sif.CS_ENABLE_FLASH(dev, 0);
					sif.ENABLE_W_FLASH(dev, spi_ss, 0x6);
					sif.CS_ENABLE_FLASH(dev, 1);
					sif.CS_ENABLE_FLASH(dev, 0);
					sif.SPI_W_GSL_FLASH(dev, spi_ss, addr, data + i * 44, buf + i * 44, len_int - i * 44 + 4);
					sif.CS_ENABLE_FLASH(dev, 1);
					ReleaseMutex(busy[dev]);
				}
			}
		}
		else
		{
			if (GetMutex(dev) == FALSE)
				return 1;
			sif.CS_ENABLE_FLASH(dev, 0);
			sif.ENABLE_W_FLASH(dev, spi_ss, 0x6);
			sif.CS_ENABLE_FLASH(dev, 1);
			sif.CS_ENABLE_FLASH(dev, 0);
			sif.SPI_W_GSL_FLASH(dev, spi_ss, addr, data, buf, len_int + 4);
			sif.CS_ENABLE_FLASH(dev, 1);
			ReleaseMutex(busy[dev]);

		}
		sl_delay(10);
	}
	return TRUE;
}
int	SL_ENABLE_W_FLASH(int dev, unsigned char spi_ss, unsigned int addr)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return 1;
	ret = sif.ENABLE_W_FLASH(dev, spi_ss, addr);
	ReleaseMutex(busy[dev]);
	return ret;
}
int	SL_SPI_W_GSL(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int)
{
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.SPI_W_GSL(dev, spi_ss, addr, buf, len_int);
	ReleaseMutex(busy[dev]);
	return ret;
}

int SPI_WR_GSL(int dev, unsigned char spi_ss, unsigned int w_addr, unsigned char *r_char, int len_char)//len_char<10200
{
	int ret = 0;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	int len = (len_char + 3) / 4;
	unsigned int *buf = (unsigned int *)malloc(len*4);
	ret = sif.SPI_R_GSL(dev,spi_ss,w_addr,buf,len);
	memcpy(r_char,buf,len_char);
	free(buf);
	//unsigned char ww_buf[4] = { 0 };
	//ww_buf[3] = (w_addr >> 24) & 0xff;
	//ww_buf[2] = (w_addr >> 16) & 0xff;
	//ww_buf[1] = (w_addr >> 8) & 0xff;
	//ww_buf[0] = (w_addr >> 0) & 0xff;
	//ret = sif.SPI_WR(dev, spi_ss, ww_buf, r_char, len_char);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_ResetOutput(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1, data);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO1_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1<<5, data<<5);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO2_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1<<6, data<<6);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO3_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1 << 7, (data & 1) << 7);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO4_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1<<8, data<<8);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO5_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1<<9, data<<9);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_IO6_Output(int dev, unsigned int data)
{//RST 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1 << 10, data << 10);
	ReleaseMutex(busy[dev]);
	return ret;
}


int	SL_GPIO_Input(int dev, unsigned int *status)
{//IRQ 读
	int ret;
	unsigned int read_data;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Input(dev, &read_data);
	*status = 0;
	if (read_data & 0x02)
		*status |= 0x100;
	ReleaseMutex(busy[dev]);
	return ret;
}

#if 0
int	SL_GPIO_Demo(int dev, unsigned int *status)
{//IRQ 读
	int ret;
	unsigned int read_data;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Input(dev, &read_data);
	*status = 0;
	if (read_data & (0x01<<11))
		*status |= 0x100;
	ReleaseMutex(busy[dev]);
	return ret;
}
#else
int	SL_GPIO_ID(int dev, unsigned int data)
{//IRQ 读
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1 << 11, data << 11);
	ReleaseMutex(busy[dev]);
	return ret;
}
#endif

int	SL_GPIO_IO1(int dev, unsigned int data)
{//IRQ 读
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 1 << 16, data << 16);
	ReleaseMutex(busy[dev]);
	return ret;
}


int	SL_Power(int dev, unsigned int data)
{//Power 写
	//return tif.GPIO_Output(dev, data);
	int ret;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	ret = sif.GPIO_Output(dev, 4, data);
	ReleaseMutex(busy[dev]);
	return ret;
}

int	SL_Set_Avdd(int dev)
{
	int ret = 0;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	sif.GPIO_Output(dev, (1 << 3) + (1 << 4), (0 << 3) + (1 << 4));
//	sif.GPIO_Output(dev, 1<<4, 0);
	ReleaseMutex(busy[dev]);
	return ret;
}

int SL_Set_Vddcore(int dev)
{
	int ret = 0;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	sif.GPIO_Output(dev, (1 << 3) + (1 << 4), (1 << 3) + (0 << 4));
	ReleaseMutex(busy[dev]);
	return ret;
}

int SL_Set_Vtest(int dev)
{
	int ret = 0;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	sif.GPIO_Output(dev, (1 << 3) + (1 << 4), (1 << 3) + (1 << 4));
	ReleaseMutex(busy[dev]);
	return ret;
}

//int	SL_Read_IntData(int dev, int retdata)
//{
//	return tif.Read_IntData(dev,retdata);
//}

int SL_IRQ_Time(int dev, int flag)
{
	//flag == 0 复位
	//flag == 1 返回IRQ时间
	int ret = 0;
	if (GetMutex(dev) == FALSE)
		return FALSE;
	unsigned int data[4];
	data[0] = 3;//数组有效长度
	data[1] = 1;//哪种功能
	data[2] = flag ? 1 : 0;
	if (sif.GSL_Function(dev, data) == FALSE)
	{
		ReleaseMutex(busy[dev]);
		return ret;
	}
	if (flag)
		ret = (int)(data[0]);
	ReleaseMutex(busy[dev]);
	return ret;
}


void SL_ResetSTM32(int dev)
{
	UINT data_conf[1];
	//data_conf[0] = 0xfffff802;
	data_conf[0] = 0xfffef002;
	if (GetMutex(dev) == FALSE)
		return;
	sif.GPIO_Config(dev, 0, data_conf);
	data_conf[0] = 0x0000e000;
	sif.GPIO_Config(dev, 1, data_conf);//ID需要上拉
	sif.GPIO_Output(dev, 0x18, 0x10);//IN2=1,IN1=0
// 	sif.GPIO_Output(dev, 1 << 17, vdd_state << 17);
// 	sif.GPIO_Output(dev, 1 << 17 + 1 << 10, vdd_state << 17 + vdd_state << 10);
	ReleaseMutex(busy[dev]);
}

int SPI_Read4B(int dev, unsigned char iic_addr, unsigned char *buf_char, int len_char)
{
	return SL_SPI_Read(dev, 0, iic_addr, buf_char, len_char);
}

int SPI_Write4B(int dev, unsigned char iic_addr, unsigned char *buf_char, int len_char)
{
	return SL_SPI_Write(dev, 0, iic_addr, buf_char, len_char);
}

int SPI_RTotal_Address(int dev, unsigned int addr, unsigned int *buf, int len_int)
{
	return  SL_SPI_R_GSL(dev, 0, addr, buf, len_int);
}

int SPI_WTotal_Address(int dev, unsigned int addr, unsigned int *buf, int len_int)
{
	return SL_SPI_W_GSL(dev, 0, addr, buf, len_int);
}

static void sl_delay(int ms_delay)
{
	LARGE_INTEGER time_freq_sl, time_start_sl;
	LARGE_INTEGER time_end;// , time_e2;
	QueryPerformanceCounter(&time_start_sl);//获得整机基准时钟计数
	QueryPerformanceFrequency(&time_freq_sl);//获得整机基准频率
	do
	QueryPerformanceCounter(&time_end);
	while (((time_end.QuadPart - time_start_sl.QuadPart) * 1000 * 1000 / time_freq_sl.QuadPart) < ms_delay * 1000);
}

static int Read_bf(int devindex)
{
	UINT temp = 0;
	unsigned char buf[4] = { 0 };
	int readnum = 0;
	SL_SPI_Read(devindex, 0, 0xbf, buf, 1);
	SL_SPI_Read(devindex, 0, 0xbf, buf, 1);
	while (((buf[0] & 0x1) != 1) && (readnum < 2))
	{
		readnum++;
		sl_delay(40);
		SL_SPI_Read(devindex, 0, 0xbf, buf, 4);
		if (readnum == 2)
			return -1;
	}
	return 1;
}


static void Scan_chip(int devindex)
{
	unsigned char buf[4] = { 0 };
	SL_SPI_Write(devindex, 0, 0xbf, buf, 4);

	UINT tmpvalue24 = 0;
/*	SL_SPI_R_GSL(devindex, 0, 0xff09013c, &tmpvalue24, 1);*/
	tmpvalue24 = 1;
	SL_SPI_W_GSL(devindex, 0, 0xff09013c, &tmpvalue24, 1);
//  	SL_SPI_W_GSL(devindex, 0, 0xff09013c, &tmpvalue24, 1);
//  	SL_SPI_W_GSL(devindex, 0, 0xff09013c, &tmpvalue24, 1);
//  	SL_SPI_W_GSL(devindex, 0, 0xff09013c, &tmpvalue24, 1);
}


int GetImgBuffer(int dev, unsigned int addr, unsigned char *buf, int len_char)   //获取图像，buf为图像缓存，len_char为图像大小
{
	unsigned char r_data[4] = { 0 };
	unsigned int temp = 0;
 	SPI_Read4B(dev, 0xfc, r_data, 4);
 	temp = (r_data[3] << 24) + (r_data[2] << 16) + (r_data[1] << 8) + (r_data[0] << 0);
// 	if ((temp & 0xffff0000) == 0x70110000)
// 	{
		SPI_WR_GSL(dev, 0, addr, buf, len_char);
 		return TRUE;
// 	}
// 	else
// 	{
// 		SPI_WR_GSL(dev, 0, addr, buf, len_char);
// 		return FALSE;
// 	}
}
 


static int ImageJieya(unsigned char *bic, int *bo, int img_len, int bit_len)
{
	//sizeof(bic) >= (img_len*bit_len+7) / 8
	//bic输入的压缩数据
	//bo输出的数据，16位高位对齐
	//img_len 图像的长度 = 图像的长*宽
	//bit_len 压缩数据的精度8 10 12 14 16
	int i;
	int data;
	int eff = 8;//表示当前bic中多少位有效
	int n = 0;//表示bic用到哪个
	int mask = 0;//有效位，防止高位非零
	if (bit_len > 16 || bit_len < 8)
		return -1;//只适用8-16位图像数据
	for (i = 0; i < bit_len; i++)
		mask |= 1 << i;
	for (i = 0; i < img_len; i++)
	{
		data = bic[n] >> (8 - eff);
		while (bit_len > eff)//有效位数不足，继续加载
		{
			n++;
			data += ((int)(bic[n])) << eff;
			eff += 8;
		}
		bo[i] = (data & mask) << (16 - bit_len);//高位对齐到16位
		eff -= bit_len;//bic[n]中剩余的有效位数
		if (eff == 0)
		{
			n++;
			eff = 8;
		}
	}
	return 1;
}


int SL_GetImagebuffer(int devices, int* imageBuffer, unsigned int dWidth, unsigned int dHeight)
{
	if (GetMutex(devices) == FALSE)
		return 1;

	int ret = 0;
	unsigned char *imageout = (unsigned char *)malloc((dWidth*dHeight + 7) / 8 * 12);

	unsigned char *image = (unsigned char *)malloc(dWidth*dHeight);

#if 1
	Scan_chip(devices);
	sl_delay(200);
	int bf = Read_bf(devices);
	unsigned int Image_address = 0x0;
	int len_char = (dWidth*dHeight + 7) / 8 * 12;
	if (bf == 1)
	{
		int sum = 0;
		int num = 0;
		int avg = 0;
		ret = GetImgBuffer(devices, Image_address, imageout, len_char);
		ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);
#if 1
		int num_0 = 0;
		for (int i = 0; i < dWidth*5; i++)
		{
			if ((imageBuffer[i]>>8)!=0)
				num_0++;
		}

		if (num_0 < 20)
		{
			Scan_chip(devices);
			sl_delay(400);
			//Read_bf(devices);
			GetImgBuffer(devices, Image_address, imageout, len_char);
			ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);
		}
		ret = 0;
#endif
	}
	else
	{	
		Sleep(100);
		int sum = 0;
		int num = 0;
		int avg = 0;
		GetImgBuffer(devices, Image_address, imageout, len_char);
		ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);

		int num_0 = 0;
		for (int i = 0; i < dWidth*5; i++)
		{
			if ((imageBuffer[i] >> 8) != 0)
				num_0++;
		}

		if (num_0 < 20)
		{
			Scan_chip(devices);
			sl_delay(400);
			//Read_bf(devices);
			GetImgBuffer(devices, Image_address, imageout, len_char);
			ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);
		}
		ret = 1;
	}



//	ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);
#endif

#if 0

	unsigned int Image_address = 0x0;
	int len_char = (dWidth*dHeight + 7) / 8 * 12;
	int reg_length = (dWidth*dHeight) / 4;

	unsigned int *w_buf = (unsigned int *)malloc(50*1024 * 4);
	unsigned int *r_buf = (unsigned int *)malloc(50*1024* 4);

	memset(w_buf, 0x5a5a5a5a, 50 * 1024);

	SL_SPI_W_GSL(devices, 0, 0x0, w_buf, 50 * 1024/4);
	Sleep(100);
	ret = GetImgBuffer(devices, Image_address, imageout, len_char);

#if 0
	static int ss = 0;
	char LogFileName[200];
	sprintf_s(LogFileName, "./Log/%d_image_file.txt", ss);
	FILE *fp;
	if (fopen_s(&fp, LogFileName, "r") != 0)    //打开文件;若不为0则说明没有此文件名，创建之;
	{
		fopen_s(&fp, LogFileName, "wt");

		for (int j = 0; j < len_char; j++)
		{
			//				for (int i = 0; i < dWidth; i++)
			{
				char log_str[20] = { 0 };
				sprintf_s(log_str, "%3d  ", imageout[j]);
				fputs(log_str, fp);
			}
		}

		fclose(fp);
	}
	else
		fclose(fp);

	ss++;
#endif

	ImageJieya(imageout, imageBuffer, dWidth*dHeight, 12);
#endif


	ReleaseMutex(busy[devices]);

	free(imageout);
	free(image);
	if (ret == 0)
		return 1;
	else
		return 0;
}

static void FWriteLog(const char* fileName, char *logMsg)   //写入;
{
#if 1
	FILE *fLog;

#if 0//_UNICODE
	_wfopen_s(&fLog, fileName, TEXT("a+"));
#else
	fopen_s(&fLog, fileName, "a+");
#endif
	if (fLog == NULL)
	{
		return;
	}
#if 0 //_UNICODE
	fwprintf(fLog, TEXT("%s"), logMsg);
#else
	fprintf(fLog,"%s", logMsg);
#endif

	fflush(fLog);
	fclose(fLog);

#endif
}
int SL_LoadConfig(int devindex, const char* fileName)
{
#if 0
	CStdioFile file;
	CString str_line, str_temp;
	UINT address, data;
	unsigned int temp_data = 0;
	CString str;
	char buf[200] = { 0 };

	if (file.Open(fileName, CFile::modeRead | CFile::shareDenyNone))
	{
		while (file.ReadString(str_line))
		{
			if (str_line == "")
				continue;
			str_temp = str_line.Mid(0, 8);
			address = strtoul(str_temp, NULL, 16);
			str_temp = str_line.Mid(9, 8);
			data = strtoul(str_temp, NULL, 16);

			unsigned char data_buf[4] = { 0 };
			unsigned char data_r[4] = { 0 };
			data_buf[3] = (data >> 24) & 0xff;
			data_buf[2] = (data >> 16) & 0xff;
			data_buf[1] = (data >> 8) & 0xff;
			data_buf[0] = (data >> 0) & 0xff;

			if (((address & 0xff000000) == 0))
			{
				if (!(SL_SPI_Write(devindex, 0, (unsigned char)address, data_buf, 4)))
				{
					return 1;
				}
			}
			else
			{
				if (!(SL_SPI_W_GSL(devindex, 0, address, &data, 1)))
				{
					return 1;
				}
			}
		}
		file.Close();
		return 0;
	}
	else
	{
		return 1;
	}
	return 0;
#else

#define MAXFILE_SISE 200
	char *buf = (char*)malloc(sizeof(char)* 300);

	FILE * fp;
	fopen_s(&fp, fileName, "r");
	if (fp == NULL)
	{
		return -1;
	}
	bool file_state = TRUE;
	while (file_state)
	{
		char *ret = fgets(buf, MAXFILE_SISE, fp);
		if (ret == NULL)
		{
			file_state = FALSE;
		}
		UINT address, data;
		char *stop;
		address = strtoul(buf, &stop, 16);
		data = strtoul(stop + 1, NULL, 16);

		unsigned char data_buf[4] = { 0 };
		unsigned char data_r[4] = { 0 };
		data_buf[3] = (data >> 24) & 0xff;
		data_buf[2] = (data >> 16) & 0xff;
		data_buf[1] = (data >> 8) & 0xff;
		data_buf[0] = (data >> 0) & 0xff;
// 		if (address == 0xff090070)
// 			int retrrr = 1;
		if (((address & 0xff000000) == 0))
		{
			SL_SPI_Write(devindex, 0, (unsigned char)address, data_buf, 4);
// 
// 			unsigned char r_buf_t[4] = { 0 };
// 			SL_SPI_Read(devindex, 0, (unsigned char)address, r_buf_t, 4);
// 			unsigned int temp_data = r_buf_t[3] << 24 + r_buf_t[2] << 16 + r_buf_t[1] << 8 + r_buf_t[0];
// 			char log_tt[100];
// 			sprintf_s(log_tt, "address:0x%08x,default:0x%08x,data:0x%08x\r\n", address, data, temp_data);
// 			FWriteLog("./loadconfig.txt", log_tt);
		}
		else
		{
			SL_SPI_W_GSL(devindex, 0, address, &data, 1);
// 
// 			unsigned int r_buf_t;
// 			SL_SPI_R_GSL(devindex, 0, address, &r_buf_t, 1);
// 			char log_tt[100];
// 			sprintf_s(log_tt, "address:0x%08x,default:0x%08x,data:0x%08x\r\n", address, data, r_buf_t);
// 			FWriteLog("./loadconfig.txt", log_tt);
		}
	}
	fclose(fp);
	return 0;
#endif
}


int SL_AvddOutput(int dev, unsigned int data)
{
	return SL_IO6_Output(dev, data);
}


int	SL_DevicesInit(int speed)
{
	int ret = 0;
	int s_speed = 1;
	if (speed == 1)
		s_speed = 1;
	else if (speed == 2)
		s_speed = 2;
	else if (speed == 3 || speed == 4 || speed == 5 || speed == 6)
		s_speed = 3;
	else if (speed == 7 || speed == 8 || speed == 9 || speed == 10)
		s_speed = 4;
	else
		s_speed = 2;

// 	SL_FindOpen(0, s_speed, 1);
// 	sl_delay(100);
	ret = SL_FindOpen(0, 3, 1);
	sl_delay(10);
	return ret;
}


int SPI_Read_FlashId(int dev,unsigned char *buf)
{
	if (GetMutex(dev) == FALSE)
		return 1;
	sif.CS_ENABLE_FLASH(dev, 0);
	int ret = sif.SPI_R_GSL_FLASH(dev, 0, 0x90, 0, buf, 6);
	sif.CS_ENABLE_FLASH(dev, 1);
	ReleaseMutex(busy[dev]);
	return ret;
}


int SPI_Read_Flash(int dev, unsigned int data, unsigned char *buf, int len)
{
#if 0
	unsigned char r_buf[256] = { 0 };
	int ret = SL_SPI_R_FLASH(dev, 0, 0x03, data, r_buf, len + 4);
	memcpy(buf, r_buf + 4, len);
#else
	if (len > 256)
	{
		int count = len / 256 + 1;
		for (int i = 0; i < count; i++)
		{
			if (i != count - 1)
			{
				SL_SPI_R_FLASH(dev, 0, 0x03, data + i * 256, buf + i * 256, 256);
			}
			else
			{
				SL_SPI_R_FLASH(dev, 0, 0x03, data + i * 256, buf + i * 256, len - i * 256);
			}
		}
	}
	else
	{
		SL_SPI_R_FLASH(dev, 0, 0x03, data, buf, len);
	}
#endif
	return  TRUE;
}


static int SPI_simpleErase_Flash(int dev,unsigned int data)
{
	unsigned char r_buf[4] = { 0 };
	if (GetMutex(dev) == FALSE)
		return 1;
	int ret=SL_SPI_R_FLASH(dev, 0, 0x52, data, r_buf, 4);
	ReleaseMutex(busy[dev]);
	return ret;
}


int SPI_Write_Flash(int dev, unsigned int data, unsigned char *buf, int len)
{
	if (len > 256)
	{
		int count = len / 256 + 1;
		for (int i = 0; i < count; i++)
		{
			if (i != count - 1)
			{
				SL_SPI_W_FLASH(dev, 0, 0x02, data + i * 256, buf + i * 256, 256);
			}
			else
			{
				SL_SPI_W_FLASH(dev, 0, 0x02, data + i * 256, buf + i * 256, len - i * 256);
			}
		}
	}
	else
	{
		SL_SPI_W_FLASH(dev, 0, 0x02, data, buf, len);
	}
	return TRUE;
}


int SPI_Erase_Flash(int dev)
{
	unsigned char r_buf[4] = { 0 };
	if (GetMutex(dev) == FALSE)
		return 1;
	sif.CS_ENABLE_FLASH(dev, 0);
	sif.ENABLE_W_FLASH(dev, 0, 0x6);
	sif.CS_ENABLE_FLASH(dev, 1);
	sif.CS_ENABLE_FLASH(dev, 0);
	SL_ENABLE_W_FLASH(dev, 0, 0x60);
	sif.CS_ENABLE_FLASH(dev, 1);
	ReleaseMutex(busy[dev]);
	sl_delay(300);
	return 0;
}


int SPI_SectorErase_Flash(int dev,int addr)
{
	unsigned char r_buf[4] = { 0 };
	if (GetMutex(dev) == FALSE)
		return 1;
	sif.CS_ENABLE_FLASH(dev, 0);
	sif.ENABLE_W_FLASH(dev, 0, 0x6);
	sif.CS_ENABLE_FLASH(dev, 1);
//	SL_ENABLE_W_FLASH(dev, 0, 0x60);
	sif.CS_ENABLE_FLASH(dev, 0);
	sif.SPI_W_GSL_FLASH(dev, 0, 0x20, addr, r_buf, 2);
	sif.CS_ENABLE_FLASH(dev, 1);
	ReleaseMutex(busy[dev]);
	sl_delay(100);
	return 0;
}


int SPI_Enable_W_Flash(int dev)
{
	unsigned char r_buf[4] = { 0 };
	if (GetMutex(dev) == FALSE)
		return 1;
	sif.CS_ENABLE_FLASH(dev, 0);
	int ret=SL_ENABLE_W_FLASH(dev, 0, 0x06);
	sif.CS_ENABLE_FLASH(dev, 1);
	ReleaseMutex(busy[dev]);
	return ret;
}

