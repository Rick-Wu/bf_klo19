#if !defined(Interface_ex_h_201605311530)
#define Interface_ex_h_201605311530

#include "Interface.h"
#include "define.h"
#include <string.h>

extern CInterface sif;

int InitIf(CInterface *pif);
static int _GSL_IIC_Read (int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
static int _GSL_IIC_Write(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
static int _GSL_IIC_R_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf_int,int len_int);
static int _GSL_IIC_W_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf_int,int len_int);

static int _GSL_SPI_Read (int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);
static int _GSL_SPI_Write(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);
static int _GSL_SPI_R_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf_int,int len_int);
static int _GSL_SPI_W_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf_int,int len_int);

static int _GSL_Info(int dev,char **str);
static int _GSL_Close(int flag);
//static int _GSL_GPIO_Output(int dev,unsigned int data);
//static int _GSL_GPIO_Input(int dev, unsigned int *status);
//static int _GSL_Set_Avdd(int dev);
//static int _GSL_Read_IntData(int dev,int retdata);

static int _GSL_GPIO_Output(int dev, unsigned int port, unsigned int statue);
static int _GSL_GPIO_Input(int dev, unsigned int *pstatus);
static int _GSL_Config(int dev, int flag, unsigned int *conf);

static int _GSL_Set_Avdd(int dev);
static int _GSL_Set_Vddcore(int dev);
static int _GSL_Set_Vtest(int dev);
static int _GSL_FunNone(int dev, unsigned int *data);

#endif