// USB_Driver.cpp : 定义 DLL 应用程序的导出函数。
//

//#include "stdafx.h"
#include "lusb0_usb.h"

#pragma comment(lib, "libusb.lib")

// Enables this example to work with a device running the
// libusb-win32 PIC Benchmark Firmware.
//#define BENCHMARK_DEVICE

//////////////////////////////////////////////////////////////////////////////
// TEST SETUP (User configurable)

// Issues a Set configuration request
//#define TEST_SET_CONFIGURATION

// Issues a claim interface request
#define TEST_CLAIM_INTERFACE

// Use the libusb-win32 async transfer functions. see
// transfer_bulk_async() below.
#define TEST_ASYNC	0

// Attempts one bulk read.
#define TEST_BULK_READ

// Attempts one bulk write.
 #define TEST_BULK_WRITE

//////////////////////////////////////////////////////////////////////////////
// DEVICE SETUP (User configurable)

// Device vendor and product id.
#define USB_VID 0x0483
#define USB_PID 0x1237

// Device configuration and interface id.
#define MY_CONFIG 1
#define MY_INTF 0

#define	DEV_NUM_MAX		32

static struct usb_device *pBoard[DEV_NUM_MAX]={NULL};
static usb_dev_handle *pBoardHandle[DEV_NUM_MAX]={NULL};
static int dev_num_prev = 0;

/**
  * @brief  查找指定的USB设备个数
  * @param  pBoard 设备句柄存放地址
  * @retval 识别到的指定设备个数
  */
static int scan_dev(struct usb_device *pBoard[])
{
    struct usb_bus *bus;
    struct usb_device *dev;
	int devnum=0,i=0;
	if(pBoard == NULL)
		return FALSE;
    for (bus = usb_get_busses(); bus; bus = bus->next)
    {
        for (dev = bus->devices; dev; dev = dev->next)
        {
            if (dev->descriptor.idVendor == USB_VID
                    && dev->descriptor.idProduct == USB_PID)
            {
					pBoard[devnum] = dev;
				devnum++;
				if(devnum >= DEV_NUM_MAX)
					return DEV_NUM_MAX;
            }
        }
    }
	for(i=devnum;i<DEV_NUM_MAX;i++)
		pBoard[i] = NULL;
    return devnum;
}

int __stdcall USBCheck(int id)
{
	if(pBoardHandle[id])
		return TRUE;
	else
		return FALSE;
}

/**
  * @brief  扫描设备连接数
  * @param  NeedInit 是否需要初始化，第一次调用该函数需要初始化
  * @retval 识别到的指定设备个数
  */
int __stdcall USBScanDev(int needinit)
{
	int i;
	struct usb_device *pt[DEV_NUM_MAX]={NULL};
	int dev_num = 0;
	if(needinit)
	{
		usb_init(); /* initialize the library */
	}
	usb_find_busses(); /* find all busses */
	usb_find_devices(); /* find all connected devices */
	dev_num = scan_dev(pt);
	if(dev_num != dev_num_prev)
	{//设备数量不相等，按新扫描的
		for(i=0;i<DEV_NUM_MAX;i++)
			pBoard[i] = pt[i];//这个操作影响OPEN，但不影响CLOSE
		dev_num_prev = dev_num;
		return dev_num;
	}
	//设备数量相等
	for(i=0;i<DEV_NUM_MAX;i++)
	{//检查是否有改变。
		if(pt[i] != pBoard[i])
		{
			for(i=0;i<DEV_NUM_MAX;i++)
				pBoard[i] = pt[i];
			return -dev_num;
		}
	}
	return dev_num;//设备未改变
}

// /**
//   * @brief  打开指定的USB设备
//   * @param  devNum	需要打开的设备号
//   * @param	pBoard	设备句柄存放地址
//   * @retval 已经打开了的设备句柄
//   */
// static usb_dev_handle * open_dev(int devNum,struct usb_device *pBoard[])
// {
//     struct usb_bus *bus;
//     struct usb_device *dev;
// 	int devnum=0;
//     for (bus = usb_get_busses(); bus; bus = bus->next)
//     {
//         for (dev = bus->devices; dev; dev = dev->next)
//         {
//             if (dev->descriptor.idVendor == USB_VID
//                     && dev->descriptor.idProduct == USB_PID)
//             {
//                 if(devnum==devNum){
// 					return usb_open(pBoard[devNum]);
// 				}
// 				devnum++;
//             }
//         }
//     }
//     return NULL;
// }

/**
  * @brief  打开指定的USB设备
  * @param  devNum	需要打开的设备号
  * @retval 打开状态
  */
int __stdcall USBOpenDev(int DevIndex)
{
	pBoardHandle[DevIndex] = usb_open(pBoard[DevIndex]);
	if(pBoardHandle[DevIndex] == NULL)
		return SEVERITY_ERROR;
	else
		return SEVERITY_SUCCESS;
}
// /**
//   * @brief  关闭指定的USB设备
//   * @param  devNum	需要关闭的设备号
//   * @param  pBoardHandle	开打了的设备句柄
//   * @retval 已经打开了的设备句柄
//   */
// static int close_dev(int devNum,struct usb_dev_handle *pBoardHandle[])
// {
//     struct usb_bus *bus;
//     struct usb_device *dev;
// 	int devnum=0;
// 	int ret_close;
//     for (bus = usb_get_busses(); bus; bus = bus->next)
//     {
//         for (dev = bus->devices; dev; dev = dev->next)
//         {
//             if (dev->descriptor.idVendor == USB_VID
//                     && dev->descriptor.idProduct == USB_PID)
//             {
//                 if(devnum==devNum)
// 				{
// 					if(pBoardHandle[devNum])
// 						ret_close = usb_close(pBoardHandle[devNum]);
// 					pBoardHandle[devNum] = NULL;
// 					return ret_close;
// 				}
// 				devnum++;
//             }
//         }
//     }
//     return NULL;
// }

/**
  * @brief  关闭指定的USB设备
  * @param  devNum	需要关闭的设备号
  * @retval 打开状态
  */
int __stdcall USBCloseDev(int DevIndex)
{
	if(pBoardHandle[DevIndex])
	{
		int ret = usb_close(pBoardHandle[DevIndex]);
		pBoardHandle[DevIndex] = NULL;
		return ret;
	}
	else
		return FALSE;
}

/*
* Read/Write using async transfer functions.
*
* NOTE: This function waits for the transfer to complete essentially making
* it a sync transfer function so it only serves as an example of how one might
* implement async transfers into thier own code.
*/
static int transfer_bulk_async(usb_dev_handle *dev,
                               int ep,
                               char *bytes,
                               int size,
                               int timeout)
{
    // Each async transfer requires it's own context. A transfer
    // context can be re-used.  When no longer needed they must be
    // freed with usb_free_async().
    //
    void* async_context = NULL;
    int ret;

    // Setup the async transfer.  This only needs to be done once
    // for multiple submit/reaps. (more below)
    //
    ret = usb_bulk_setup_async(dev, &async_context, ep);
    if (ret < 0)
    {
        //printf("error usb_bulk_setup_async:\n%s\n", usb_strerror());
        goto Done;
    }

    // Submit this transfer.  This function returns immediately and the
    // transfer is on it's way to the device.
    //
    ret = usb_submit_async(async_context, bytes, size);
    if (ret < 0)
    {
        //printf("error usb_submit_async:\n%s\n", usb_strerror());
        usb_free_async(&async_context);
        goto Done;
    }

    // Wait for the transfer to complete.  If it doesn't complete in the
    // specified time it is cancelled.  see also usb_reap_async_nocancel().
    //
    ret = usb_reap_async(async_context, timeout);

    // Free the context.
    usb_free_async(&async_context);

Done:
    return ret;
}

/**
  * @brief  USB Bulk端点写数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  sendbuffer 发送数据缓冲区
  * @param  len 发送数据字节数
  * @param  waittime 超时时间
  * @retval 成功发送的数据字节数
  */

int __stdcall USBBulkWriteData(unsigned int nBoardID,int pipenum,char *sendbuffer,int len,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if (usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#if TEST_ASYNC
    // Running an async write test
    ret = transfer_bulk_async(dev, pipenum, sendbuffer, len, waittime);
#else
	ret = usb_bulk_write(pBoardHandle[nBoardID], pipenum, sendbuffer, len, waittime);
	/*if((len%64) == 0){
		usb_bulk_write(pBoardHandle[nBoardID], pipenum, sendbuffer, 0, waittime);
	}*/
#endif
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}

/**
  * @brief  USB interrupt端点写数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  sendbuffer 发送数据缓冲区
  * @param  len 发送数据字节数
  * @param  waittime 超时时间
  * @retval 成功发送的数据字节数
  */
int __stdcall USBIntWriteData(unsigned int nBoardID,int pipenum,char *sendbuffer,int len,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if (usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif
	ret = usb_interrupt_write(pBoardHandle[nBoardID], pipenum, sendbuffer, len, waittime);
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}

/**
  * @brief  USB 控制端点写数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  sendbuffer 发送数据缓冲区
  * @param  len 发送数据字节数
  * @param  waittime 超时时间
  * @retval 成功发送的数据字节数
  */
int __stdcall USBCtrlData(unsigned int nBoardID,int requesttype,int request,int value, int index, char *bytes, int size,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if (usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif
	ret = usb_control_msg(pBoardHandle[nBoardID], requesttype, request,value, index, bytes, size,waittime);
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}

/**
  * @brief  USB Bulk读数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  readbuffer 读取数据缓冲区
  * @param  len 读取数据字节数
  * @param  waittime 超时时间
  * @retval 读到的数据字节数
  */
int __stdcall USBBulkReadData(unsigned int nBoardID,int pipenum,char *readbuffer,int len,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if (usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#if TEST_ASYNC
    // Running an async read test
    ret = transfer_bulk_async(pGinkgoBoardHandle[nBoardID], pipenum, sendbuffer, len, waittime);
#else
	ret = usb_bulk_read(pBoardHandle[nBoardID], pipenum, readbuffer, len, waittime);
#endif
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}

/**
  * @brief  USB interrupt读数据
  * @param  nBoardID 设备号
  * @param  pipenum 端点号
  * @param  readbuffer 读取数据缓冲区
  * @param  len 读取数据字节数
  * @param  waittime 超时时间
  * @retval 读到的数据字节数
  */
int __stdcall USBIntReadData(unsigned int nBoardID,int pipenum,char *readbuffer,int len,int waittime)
{
	int ret=0;
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
#ifdef TEST_SET_CONFIGURATION
    if (usb_set_configuration(pGinkgoBoardHandle[nBoardID], MY_CONFIG) < 0)
    {
        usb_close(pGinkgoBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif

#ifdef TEST_CLAIM_INTERFACE
    if (usb_claim_interface(pBoardHandle[nBoardID], 0) < 0)
    {
        usb_close(pBoardHandle[nBoardID]);
		pBoardHandle[nBoardID] = NULL;
        return SEVERITY_ERROR;
    }
#endif
	ret = usb_interrupt_read(pBoardHandle[nBoardID], pipenum, readbuffer, len, waittime);
#ifdef TEST_CLAIM_INTERFACE
    usb_release_interface(pBoardHandle[nBoardID], 0);
#endif
    return ret;
}


int __stdcall USBReset(unsigned int nBoardID)
{
	if(pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
//	usb_clear_halt(pBoardHandle[nBoardID],0x01);
//	usb_clear_halt(pBoardHandle[nBoardID],0x81);
	return usb_reset(pBoardHandle[nBoardID]);
}

int __stdcall USBClearHalt(unsigned int nBoardID, int pipenum)
{
	if (pBoardHandle[nBoardID] == NULL){
		return SEVERITY_ERROR;
	}
	return usb_clear_halt(pBoardHandle[nBoardID], pipenum);
}
//放在USB_Driver.cpp
