
#if !defined(define_h_for_MassProduct)
#define define_h_for_MassProduct

#define VERSION_TOOL			"01040000"

#define	SPI_ABLE				1

#define DATA_DRV_MAX			72//32
#define	DATA_SEN_MAX			40//24
#define ORDER_2133_MAX			30
#define KEY_2133_MAX			8

#define INFO					".\\info.ini"
#define DEFAULT_INFO			"D:\\Design\\SileadVC\\info.ini"

#define	CPU_TYPE_NONE			0
#define	CPU_TYPE_ALL			0xffff
#define CPU_TYPE_1680			0x1680
#define	CPU_TYPE_1682			0x1682
#define	CPU_TYPE_1688			0x1688
#define	CPU_TYPE_2680			0x2680
#define	CPU_TYPE_2692			0x2692
#define	CPU_TYPE_3670			0x3670
#define	CPU_TYPE_9120			0x9120
#define	CPU_TYPE_9121			0x9121
#define	CPU_TYPE_9122			0x9122
#define	CPU_TYPE_9123			0x9123
#define	CPU_TYPE_968			0x968
#define	CPU_TYPE_3692			0x3692
#define	CPU_TYPE_5680			0x5680
#define CPU_TYPE_2133			0x2133
#define	CPU_TYPE_3628			0x3628
#define	CPU_TYPE_3680			0x3680

#define	BASE_ADDR(memory,sen,drv)	(memory-(drv+2)*(sen+2)*4 - drv*sen*2*4)


#define CORE_010400XX			0x10400000
#define CORE_01040001			0x10400001
#define CORE_01043000			0x10430001
#define CORE_01050000			0x10500000

#define	LINK_MULT_MIN			0x101
#define	LINK_MULT_MAX			0x1ff
#define	LINK_MULT(n)			(0x100+n)

#define	CONF_ID_FORMAT			".\\set\\conf_id_%x.set"
#define MAIN_ID_SET_FORMAT		".\\set\\main_id_%x.set"
#define	DEFAULT_ID_FORMAT		".\\set\\default_id_%x.h"
#define	MAIN_ID_TYPE_FORMAT		".\\set\\main_id_%s.h"
#define DUMP_FORMAT				".\\set\\main_%x.dump"
#define DUMP_FORMAT_NUM			".\\set\\main_%x_%d.dump"

#define	CONF_FORMAT				".\\set\\conf_%x.set"
#define MAIN_SET_FORMAT			".\\set\\main_%x.set"
#define MAIN_SET_FORMAT_NUM		".\\set\\main_%x_%d.set"
#define DEFAULT_FORMAT			".\\set\\default_%x.h"
#define	MAIN_TYPE_FORMAT		".\\set\\main_%s.h"

#define MAIN_TEST				"\\main.h"
#define READ_FROM_CPU			"\\read.h"
#define FREQUENCE_FILE			"\\frequence.txt"
#define BACKUP_MAIN_H_FILE		"main_bak.h"
#define BACKUP_MAIN_SET_FILE	"main_bak.set"
#define BACKUP_READ_COPY_FILE	"temp.temp"
#define TEST_CONFIGER			"\\test.ini"
#define AVGRAGE_FILE			"\\avg_base.txt"
#define TEMP_FILE				".\\temp.txt"
#define TEST_SAVE_PATH			".\\save\\"
#define MAIN_SET_DEFAULT		"E:\\main_code\\main.set"
#define MAIN_DUMP_DEFAULT		"E:\\gsl_back\\csensor_max_chazhi\\main.dump"
#define	MAIN_DEFINE_DEFAULT		"E:\\gsl_back\\csensor_max_chazhi\\function.h"
#define	CONFIG_PATH_FILE		".\\TouchPanel\\PathSave.txt"
#define	CONFIG_PATH_TEMP		".\\TouchPanel\\PathSave.temp"

#define ADB_SHELL_BUSY			"adb shell busy"
#define DRIVER_HEADER_KEY		"gsl_config_data_id"
#define DRIVER_HEADER_LENGTH	95
#define	VERSIONS_KEY			"gsl_config_versions"
#define	VERSIONS_LENGTH			60
#define VERSIONS_MAX			10
#define VERSIONS_VERS			0xa55a0003
#define	VERSIONS_VERS2			0xa5a50003

#define LINE_MODE_TIMESETEVENT
#define SHOW_FPS
#define	POINT_BUFF_MAX			64	//2的整数幂

#define TEST_SHAKE_DEFAULT		300
#define TEST_DEAD_DEFAULT		1
#define TEST_BASE_DEFAULT		0
#define	TEST_BASE_MAX_DEFAULT	(4095+1000)
#define TEST_BASE_MIN_DEFAULT	(4095-1000)
#define TEST_EXCELLENT_DEFAULT	0
#define TEST_ALLOW				0
#define TEST_DAC_MAX			100
#define TEST_DAC_MIN			60
#define WM_DRAWKEY              WM_USER + 100


#define TEST_KIND				8
#define DATA_FILE_IGNORE		0xffff
//#define LOAD_DEBUG_ABLE				GetPrivateProfileInt("隐藏功能","显示",0,INFO)

// #define TEMP_MAIN_H_FILE			"\\main_h.temp"
// #define TEMP_CONF_SET_FILE		"\\conf_set.temp"
// #define TEMP_MAIN_SET_FILE		"\\main_set.temp"
#define	README_FILE				".\\help\\ReadMe.txt"
#define TEST_HELP_FILE			"测试--配置"
#define CONFIG_HELP_FILE		"基本功能"
#define PARA_APPNAME				"测试参数"



#define PATH_SAVE_FLAG_ALL		0
#define PATH_SAVE_FLAG_MAIN		1
#define PATH_SAVE_FLAG_CONF		2

#define POINT_MAX				10

#define FILE_SAVE_TITLE_NONE	0
#define FILE_SAVE_TITLE_DAC		1
#define FILE_SAVE_TITLE_BASE	2
#define FILE_SAVE_TITLE_SHAKE	3


#define FOR_DRV_EFFECTIVE_GATHER	for(j=num_drv_start;j<=num_drv_end&&j<DATA_DRV_MAX;j++)
#define FOR_SEN_EFFECTIVE_GATHER	for(i=num_sen_start;i<=num_sen_end&&i<DATA_SEN_MAX;i++)
#define FOR_ALL_EFFECTIVE_GATHER	for(j=num_drv_start;j<=num_drv_end&&j<DATA_DRV_MAX;j++)for(i=num_sen_start;i<=num_sen_end&&i<DATA_SEN_MAX;i++)

#define GATHER_AVERAGE_NONE			0
#define GATHER_AVERAGE_LINE			1
#define GATHER_AVERAGE_ROW			2
#define GATHER_AVERAGE_ORDER		4
#define GATHER_AVERAGE_EFFECTIVE	8

#define GATHER_DATA_BASE			1
#define GATHER_DATA_SUB				2
#define GATHER_DATA_REFE			3
#define GATHER_DATA_DAC				4
#define	GATHER_DATA_TEMP			5
#define	GATHER_DATA_OTHER			100

#define CACHE_SIZE					32

#define	LINE_MULTI_SIZE				4

#define UINT						unsigned int
#define BYTE						unsigned char
#define UCHAR						unsigned char
#define FALSE						0
#define TRUE						1
#define SEVERITY_SUCCESS			0
#define	NULL						0


typedef struct  
{
	int *sw;
	char name[32];
	UINT addr;
	UINT length;
	UINT call;
	UINT key;
	char ann[32];
}LINK_TPYE;

typedef union
{
	struct  
	{
		struct  
		{
			unsigned num:8;
			unsigned rev_1:4;
			unsigned pressure_able:1;
			unsigned rev_2:1;
			unsigned ex:1;
			unsigned rev_3:1;
			unsigned c2f:16;
		}index;
		struct
		{
			unsigned y:16;
			unsigned x:12;
			unsigned id:4;
		} data[POINT_MAX];
	};
	UINT data_int[POINT_MAX+1];
	BYTE data_char[(POINT_MAX+1)*4];
}POINT_TYPE;

#define	CONF_PAGE_2680(n)		(0x04 + (n))
#define	START_2680				0x500
#define	START_1682				START_2680
#define	START_1680				(0x9f*0x80 + 0x14)

#define	TEST_PAGE_2680			0x100
#define TEST_PAGE_1680			0x93
#define	INTER_PAGE(n)			(0xe0 + (n))

#define	FUN_KEY_LINK(a,b,c)	((((a-'A')<<10)+((b-'A')<<5)+((c-'A')<<0)+0x8000) & 0xffff)

#define   AUTO_TEST_WAIT_TIME     6

#define	VERSIONS_YEAR(n)		(((n)-2000)*60*24*31*12)
#define	VERSIONS_YEAR_MIN		VERSIONS_YEAR(2010)
#define	VERSIONS_YEAR_MAX		VERSIONS_YEAR(2050)





#endif






			



