// Interface.cpp: implementation of the CInterface class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
//#include "CommLib.h"
#include "Interface.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// CInterface::CInterface()
// {
// 	Scan		= NULL;
// 	Open		= NULL;
// 	Close		= NULL;
// 	Info		= NULL;
// 	IIC_Read	= NULL;
// 	IIC_Write	= NULL;
// 	IIC_R_GSL	= NULL;
// 	IIC_W_GSL	= NULL;
// 	SPI_Read	= NULL;
// 	SPI_Write	= NULL;
// 	SPI_WR		= NULL;
// 	SPI_R_GSL	= NULL;
// 	SPI_W_GSL	= NULL;
// 	GPIO_Output = NULL;
// 	GPIO_Input	= NULL;
// 	size_spi	= 60;
// 	size_iic	= 60;
// }
// 
// CInterface::~CInterface()
// {
// 	
// }
