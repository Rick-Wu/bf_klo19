#if !defined(AFX_STM32_USB_H__D6FE6961_7D4A_4708_B62C_8309B390259C__INCLUDED_)
#define AFX_STM32_USB_H__D6FE6961_7D4A_4708_B62C_8309B390259C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "USB_Driver.h"
#include "define.h"

#define SPI_PHASE_1EDGE                 (0x00000000)//SPI_PHASE_1EDGE
#define SPI_PHASE_2EDGE                 (0x00000001)//SPI_CR1_CPHA
#define SPI_POLARITY_LOW                (0x00000000)
#define SPI_POLARITY_HIGH               (0x00000002) 
#define SPI_FIRSTBIT_MSB                (0x00000000)
#define SPI_FIRSTBIT_LSB                (0x00000080) 
#define SPI_BAUDRATEPRESCALER_2         (0x00000000)
#define SPI_BAUDRATEPRESCALER_4         (0x00000008)
#define SPI_BAUDRATEPRESCALER_8         (0x00000010)
#define SPI_BAUDRATEPRESCALER_16        (0x00000018)
#define SPI_BAUDRATEPRESCALER_32        (0x00000020)
#define SPI_BAUDRATEPRESCALER_64        (0x00000028)
#define SPI_BAUDRATEPRESCALER_128       (0x00000030)
#define SPI_BAUDRATEPRESCALER_256       (0x00000038)
#define	SPI_BRP_CLEAR					(~SPI_BAUDRATEPRESCALER_256)

#define	STM32_USB_SPI_MODE0_1_5M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_16)
#define	STM32_USB_SPI_MODE0_3M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_8)
#define	STM32_USB_SPI_MODE0_6M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_4)

#define	STM32_USB_SPI_MODE0_1M3			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_32)
#define	STM32_USB_SPI_MODE0_2M6			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_16)
#define	STM32_USB_SPI_MODE0_5M25			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_8)
#define	STM32_USB_SPI_MODE0_10M5			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_4)
#define	STM32_USB_SPI_MODE0_21M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_2)

#define	STM32_USB_SPI_MODE1			(SPI_PHASE_2EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_LSB | SPI_BAUDRATEPRESCALER_8)
#define STM32_USB_SPI_TP			(SPI_PHASE_2EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_64)


#define	MULTI_MAX			8

class CSTM32_USB  
{
	static int version_oldest;
	static int lib_init;
	static int effe_dev_id, effe_dev_num;

	static unsigned char iic_addr_save[MULTI_MAX];
	static unsigned char spi_ss_save[MULTI_MAX];
	static int stm32_dev_num;
	static int stm32_dev_id;
	static int dev_id[MULTI_MAX];
	static long multi_open;
	int  CSTM32_USB::ConfigIIC(int dev, UINT *version, int speed);
	int  CSTM32_USB::ConfigSPI(int dev, UINT *version, int baud);
	UINT CSTM32_USB::GetVersion(int dev_n, UINT *ver);

public:
	CSTM32_USB();
	virtual ~CSTM32_USB();
	enum 
	{
		STM32_SPI = 0,
		STM32_IIC = 1,
	};
private:
	int  CSTM32_USB::ReadUSB(int id,unsigned char * data_char,int len,int time_out=100);
	void CSTM32_USB::CloseDev();
	//UINT CSTM32_USB::GetVersion(int id);
	int  CSTM32_USB::SearchId(int dev = -1);
public:
	int  CSTM32_USB::GpioSet(int dev, UINT port, UINT pin, UINT i_o, UINT flag=0);
	int  CSTM32_USB::GpioOutput(int dev, UINT port, UINT pin, UINT value);
	int  CSTM32_USB::GpioInput(int dev, UINT port, UINT *value);
//	int  CSTM32_USB::GPIO_Output(int dev,unsigned port,unsigned int data);
//	int  CSTM32_USB::GPIO_Input(int dev,unsigned int*status);
	int  CSTM32_USB::TestIIC(int dev);
	int  CSTM32_USB::TestSPI(int dev);
	int  CSTM32_USB::ConfigSPI(int dev,UINT conf);
	int  CSTM32_USB::SPI_SS(int dev,int ss);
	int  CSTM32_USB::IIC_Addr(int dev,int addr);
	
	int  CSTM32_USB::FindDev(int flag);
	int  CSTM32_USB::WriteReadSPI(int dev,unsigned char *w_buf,unsigned char*r_buf,int len);
	int  CSTM32_USB::ReadIIC(int dev,unsigned char *buf,int len);
	int  CSTM32_USB::WriteIIC(int dev,unsigned char *buf,int len);
	int  CSTM32_USB::ReadMemIIC(int dev,unsigned char offset,unsigned char *buf,int len);
	int  CSTM32_USB::WriteMemIIC(int dev,unsigned char offset,unsigned char *buf,int len);
	int  CSTM32_USB::ReadMemSPI(int dev,unsigned char offset,unsigned char *buf,int len);
	int  CSTM32_USB::WriteMemSPI(int dev,unsigned char offset,unsigned char *buf,int len);
	int  CSTM32_USB::ReadIntSPI(int dev,UINT addr,UINT*buf,UINT len);
	int  CSTM32_USB::ReadIntIIC(int dev,UINT addr,UINT*buf,UINT len);
	int  CSTM32_USB::WriteIntIIC(int dev,UINT addr,UINT*buf_int,int len_int);
	int  CSTM32_USB::WriteIntSPI(int dev,UINT addr,UINT*buf_int,int len_int);
	int  CSTM32_USB::IRQ_Time(int dev,int flag);
	int  CSTM32_USB::ReadFlashSPI(int dev, UCHAR addr, UINT data, UCHAR *buf, UINT len);
	int  CSTM32_USB::WriteFlashSPI(int dev, UCHAR addr, UINT data, UCHAR *buf, UINT len);
	int  CSTM32_USB::EnableWriteFlashSPI(int dev, UCHAR addr);
//	int  CSTM32_USB::ReadSPI(int dev, UINT addr, unsigned char*buf, UINT len);
};

#endif // !defined(AFX_STM32_USB_H__D6FE6961_7D4A_4708_B62C_8309B390259C__INCLUDED_)
