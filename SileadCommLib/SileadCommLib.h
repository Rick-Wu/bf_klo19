#ifndef _SILEAD_COMMLIB_H_
#define _SILEAD_COMMLIB_H_


#ifdef   __cplusplus
extern "C"{
#endif

//SILEAD_API	int SL_InitDLL(int selectLib);

/***************************Init API**************************/
//SILEAD_API	int SL_ScanDevice(int* devnum);

	_declspec(dllexport) int SL_DevicesInit(int speed);

/*****************************SPI_API***************************/

	//读8位地址为char型地址的的数据；
	//dev为设别索引；iic_addr为地址；buf_char为读出数据；len_char长度，数据为char型的长度；
	_declspec(dllexport) int SPI_Read4B(int dev, unsigned char iic_addr, unsigned char *buf_char, int len_char);

	//写8位地址为char型地址的的数据；
	//dev为设别索引；iic_addr为地址；buf_char为写入的数据；len_char长度，数据为char型的长度；
	_declspec(dllexport) int SPI_Write4B(int dev, unsigned char iic_addr, unsigned char *buf_char, int len_char);

	//读32bit地址地址的的数据；
	//dev为设别索引；addr为地址；buf为读出数据；len_int长度，数据为int型的长度；
	_declspec(dllexport) int SPI_RTotal_Address(int dev, unsigned int addr, unsigned int *buf, int len_int);

	//写32bit地址地址的的数据；
	//dev为设别索引；addr为地址；buf为写入数据；len_int长度，数据为int型的长度；
	_declspec(dllexport) int SPI_WTotal_Address(int dev, unsigned int addr, unsigned int *buf, int len_int);

	_declspec(dllexport) int SL_SPI_R_GSL(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int);//连续读
	_declspec(dllexport) int SL_SPI_W_GSL(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int);//连续写


	//i2c读写接口，只要用于测试电流电压等
	_declspec(dllexport)  int SL_IIC_Read(int dev, unsigned char iic_addr, unsigned char offset, unsigned char *buf_char, int len_char);
	_declspec(dllexport)  int SL_IIC_Write(int dev, unsigned char iic_addr, unsigned char offset, unsigned char *buf_char, int len_char);
	_declspec(dllexport)  int SL_IIC_R_GSL(int dev, unsigned char iic_addr, unsigned int addr, unsigned int *buf, int len_int);
	_declspec(dllexport)  int SL_IIC_W_GSL(int dev, unsigned char iic_addr, unsigned int addr, unsigned int *buf, int len_int);


/*********************************GPIO API**********************/
//RST拉高拉低，data取值只有0(拉低)或1(拉高)
	_declspec(dllexport) int SL_ResetOutput(int dev, unsigned int data);
	_declspec(dllexport) int SL_AvddOutput(int dev, unsigned int data);
	_declspec(dllexport) void SL_ResetSTM32(int dev);



	/**********************  下载配置和获取图像   ***************************/
	_declspec(dllexport) int SL_GetImagebuffer(int devices, int* imageBuffer, unsigned int dWidth, unsigned int dHeight);
	_declspec(dllexport) int GetImgBuffer(int dev, unsigned int addr, unsigned char *buf, int len_char);   //获取图像，buf为图像缓存，len_char为图像大小
	_declspec(dllexport) int SL_LoadConfig(int devindex, const char* fileName);


	//用于flash读写
	_declspec(dllexport) int SPI_Read_FlashId(int dev, unsigned char *buf);
	_declspec(dllexport) int SPI_Read_Flash(int dev, unsigned int data, unsigned char *buf, int len_int);
	_declspec(dllexport) int SPI_Write_Flash(int dev, unsigned int data, unsigned char *buf, int len_int);
	_declspec(dllexport) int SPI_Erase_Flash(int dev);
	_declspec(dllexport) int SPI_SectorErase_Flash(int dev, int addr);


	//用于测试iopenshort
	_declspec(dllexport) int SL_IO1_Output(int dev, unsigned int data);
	_declspec(dllexport) int SL_IO2_Output(int dev, unsigned int data);
	_declspec(dllexport) int SL_IO3_Output(int dev, unsigned int data);
	_declspec(dllexport) int SL_IO4_Output(int dev, unsigned int data);
	_declspec(dllexport) int SL_IO5_Output(int dev, unsigned int data);
	_declspec(dllexport) int SL_Set_Avdd(int dev);


	//额外开发的gpio口，用于跟测试机台的交流
	_declspec(dllexport) int SL_GPIO_IO1(int dev, unsigned int data);
	_declspec(dllexport) int SL_GPIO_ID(int dev, unsigned int data);
#ifdef __cplusplus
}
#endif

#endif