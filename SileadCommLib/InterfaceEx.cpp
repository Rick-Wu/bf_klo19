#include "InterfaceEx.h"

CInterface sif = {NULL};

int InitIf(CInterface *pif)
{
	if(pif->FindOpen == NULL)
		return FALSE;
	if((pif->IIC_Read == NULL || pif->IIC_Write == NULL)
	&& (pif->IIC_ReadBase == NULL || pif->IIC_WriteBase == NULL))
		return FALSE;
	if(pif->SPI_WR == NULL
	&& (pif->SPI_Read == NULL || pif->SPI_Write == NULL))
		return FALSE;

	memcpy(&sif,pif,sizeof(CInterface));
	
	if(sif.IIC_Read == NULL)
		sif.IIC_Read = _GSL_IIC_Read;
	if(sif.IIC_Write == NULL)
		sif.IIC_Write = _GSL_IIC_Write;
	
	if(sif.IIC_R_GSL == NULL)
		sif.IIC_R_GSL = _GSL_IIC_R_GSL;
	if(sif.IIC_W_GSL == NULL)
		sif.IIC_W_GSL = _GSL_IIC_W_GSL;
	
	if(sif.SPI_Read == NULL)
		sif.SPI_Read = _GSL_SPI_Read;
	if(sif.SPI_Write == NULL)
		sif.SPI_Write = _GSL_SPI_Write;
	if(sif.SPI_R_GSL == NULL)
		sif.SPI_R_GSL = _GSL_SPI_R_GSL;
	if(sif.SPI_W_GSL == NULL)
		sif.SPI_W_GSL = _GSL_SPI_W_GSL;

	if(sif.Info == NULL)
		sif.Info = _GSL_Info;
	if(sif.Close == NULL)
		sif.Close = _GSL_Close;
	if(sif.GPIO_Input == NULL)
		sif.GPIO_Input = _GSL_GPIO_Input;
	if(sif.GPIO_Output == NULL)
		sif.GPIO_Output = _GSL_GPIO_Output;
	if(sif.IIC_Config == NULL)
		sif.IIC_Config = _GSL_Config;
	if(sif.SPI_Config == NULL)
		sif.SPI_Config = _GSL_Config;
	if(sif.GPIO_Config == NULL)
		sif.GPIO_Config = _GSL_Config;
	if (sif.GSL_Function == NULL)
		sif.GSL_Function = _GSL_FunNone;
	return TRUE;
}

static int _GSL_IIC_Read(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char)
{//用基础IIC读写，封装批量读
	int rl = 0;
	int rn = 0;
	int ret = TRUE;
	unsigned char w_buf[1];
	int r_max;
	if(sif.size_iic == 0)
		r_max = 0x7fffffff;
	else
		r_max = sif.size_iic;
	while(rl < len_char && ret != FALSE)
	{
		if(len_char-rl >= r_max)
			rn = r_max;
		else
			rn = len_char % r_max;
		w_buf[0] = (offset+rl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		sif.IIC_WriteBase(dev,iic_addr,w_buf,1);
		ret = sif.IIC_ReadBase(dev,iic_addr,buf_char+rl,rn);
		rl += rn;
	}
	return ret;
}

static int _GSL_IIC_Write(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char)
{//用基础IIC写，封装批量写
	int i;
	int wl = 0;//已经写了多少
	int wn = 0;//这一次要写多少
	int w_max;//一次最多写多少
	int ret = TRUE;
	unsigned char w_buf[257];
	if(sif.size_iic <= 1 || sif.size_iic >= sizeof(w_buf))
		w_max = sizeof(w_buf) - 1;//??
	else
		w_max = sif.size_iic - 1;
	while(wl < len_char && ret != FALSE)
	{
		if(len_char-wl >= w_max)
			wn = w_max;
		else
			wn = len_char % w_max;
		w_buf[0] = (offset+wl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		for(i=0;i<wn;i++)
			w_buf[i+1] = buf_char[wl + i];
		ret = sif.IIC_WriteBase(dev,iic_addr,w_buf,wn+1);
		wl += wn;
	}
	return ret;
}

static int _GSL_IIC_R_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf_int,int len_int)
{
	unsigned int page = addr/0x80;
	unsigned int offset = addr%0x80;
	unsigned char data_t[4];
	sif.IIC_Write(dev,iic_addr,0xf0,(unsigned char *)&page,4);
	sif.IIC_Read(dev,iic_addr,(offset+8)&0x5c,data_t,4);//??
	sif.IIC_Read(dev,iic_addr,offset,data_t,4);
	return sif.IIC_Read(dev,iic_addr,offset,(unsigned char *)buf_int,len_int*4);
}

static int _GSL_IIC_W_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf_int,int len_int)
{
	unsigned int page = addr/0x80;
	unsigned int offset = addr%0x80;
	sif.IIC_Write(dev,iic_addr,0xf0,(unsigned char *)&page,4);
	return sif.IIC_Write(dev,iic_addr,offset,(unsigned char *)buf_int,len_int*4);
}

static int _GSL_SPI_Read(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int i;
	int wr_max;
	int wrl = 0;
	int wrn = 0;
	int ret = TRUE;
	unsigned char w_buf[256+2];
	unsigned char r_buf[256+2];
	if(sif.size_spi==0 || sif.size_spi>=256)
		wr_max = 256;
	else
		wr_max = (sif.size_spi-2)/4*4;
	while(wrl < len_char && ret!=FALSE)
	{
		if(len_char-wrl >= wr_max)
			wrn = wr_max;
		else
			wrn = len_char % wr_max;
		w_buf[0] = (offset+wrl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		w_buf[1] = 0x0;
		ret = sif.SPI_WR(dev,spi_ss,w_buf,r_buf,wrn+2);
		for(i=0;i<wrn;i++)
			buf_char[wrl + i] = r_buf[i+2];
		wrl += wrn;
	}
	return ret;
}

static int _GSL_SPI_Write(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int i;
	int wr_max;
	int wrl = 0;
	int wrn = 0;
	int ret = TRUE;
	unsigned char w_buf[256+2];
	unsigned char r_buf[256+2];
	if(sif.size_spi==0 || sif.size_spi>=256)
		wr_max = 256;
	else
		wr_max = (sif.size_spi-2)/4*4;
	while(wrl < len_char && ret!=FALSE)
	{
		if(len_char-wrl >= wr_max)
			wrn = wr_max;
		else
			wrn = len_char % wr_max;
		w_buf[0] = (offset+wrl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		w_buf[1] = 0xff;
		for(i=0;i<wrn;i++)
			w_buf[i+2] = buf_char[wrl + i];
		ret = sif.SPI_WR(dev,spi_ss,w_buf,r_buf,wrn+2);
		wrl += wrn;
	}
	return ret;
}

static int _GSL_SPI_R_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf_int,int len_int)
{
	int i;
	int ret;
	unsigned int page = addr/0x80;
	unsigned int offset = addr%0x80;
	unsigned char data_t[8];
	unsigned char * buf_char = (unsigned char*)buf_int;
	
	page = (addr+(len_int-1)*4)/0x80;
	offset = (addr+(len_int-1)*4)%0x80;
	sif.SPI_Write(dev,spi_ss,0xf0,(unsigned char *)&page,4);
	sif.SPI_Read(dev,spi_ss,(offset+8)&0x5c,data_t,4);
	sif.SPI_Read(dev,spi_ss,offset,data_t,5);
	
	page = addr/0x80;
	offset = addr%0x80;
	if(len_int > 1)
	{
		sif.SPI_Write(dev,spi_ss,0xf0,(unsigned char *)&page,4);
		sif.SPI_Read(dev,spi_ss,(offset+8)&0x5c,data_t,4);
		ret = sif.SPI_Read(dev,spi_ss,offset,(unsigned char *)buf_int,(len_int-1)*4);
		for(i=0;i<(len_int-1)*4;i++)
			buf_char[i] = buf_char[i+1];
	}
	
	for(i=0;i<4;i++)
	{
		buf_char[(len_int-1)*4+i] = data_t[i+1];
	}
	return ret;
}

static int _GSL_SPI_W_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf_int,int len_int)
{
	unsigned int page = addr/0x80;
	unsigned int offset = addr%0x80;
	sif.SPI_Write(dev,spi_ss,0xf0,(unsigned char *)&page,4);
	return sif.SPI_Write(dev,spi_ss,offset,(unsigned char *)buf_int,len_int*4);
}

static int _GSL_Info(int dev,char **str)
{
	static char data_info[]="NULL";
	*str = data_info;
	return TRUE;
}

static int _GSL_Close(int flag)
{
	return TRUE;
}

static int _GSL_GPIO_Output(int dev,unsigned int port,unsigned int statue)
{
	return FALSE;
}

static int _GSL_GPIO_Input(int dev,unsigned int *pstatus)
{
	*pstatus = 0;
	return FALSE;
}

static int _GSL_Config(int dev,int flag,unsigned int *conf)
{
	return FALSE;
}

static int _GSL_Set_Avdd(int dev)
{
	return FALSE;
}

static int _GSL_Set_Vddcore(int dev)
{
	return FALSE;
}

static int _GSL_Set_Vtest(int dev)
{
	return FALSE;
}

static int _GSL_FunNone(int dev, unsigned int *data)
{
	return FALSE;
}
