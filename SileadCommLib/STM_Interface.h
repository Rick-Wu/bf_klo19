// STM_Interface.h: interface for the CSTM_Interface class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STM_Interface_H__125865F9_2DE8_45C8_898C_65303766F396__INCLUDED_)
#define AFX_STM_Interface_H__125865F9_2DE8_45C8_898C_65303766F396__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "STM32_USB.h"

class CSTM_Interface  
{
	static UINT gpio_tab[];
	static CSTM32_USB stm;
public:
	CSTM_Interface();
	virtual ~CSTM_Interface();
	
//	static int ScanDevices(int *devnum);
	static int FindOpen(int flag);//
	static int Close (int flag);//指定设备号
	static int Info (int dev,char **str);//字符串，转接板信息
//---------------------------------------------------------------------------
	//	int IIC_Init (unsigned int *data);//保留
	static int IIC_ReadBase (int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);
	static int IIC_WriteBase(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);
	//要求下面函数读写长度无限
	static int IIC_Read (int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
	static int IIC_Write(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
	
	static int IIC_R_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);//use
	static int IIC_W_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);
	//---------------------------------------------------------------------------
	//	int SPI_Init (unsigned int *data);//保留
	static int SPI_WR   (int dev,unsigned char spi_ss,unsigned char *w_char,unsigned char*r_char,int len_char);
	static int SPI_WR_GSL(int dev, unsigned char spi_ss, unsigned char *addr, unsigned char *buf, int len_int);
	static int SPI_Read (int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);
	static int SPI_Write(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);
	
	static int SPI_R_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int);//use
	static int SPI_W_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int);
	//---------------------------------------------------------------------------

	static int GPIO_Output(int dev, unsigned int port, unsigned int statue);
	static int GPIO_Input(int dev, unsigned int *status);
//	static int Read_IntData(int dev, int retdata);

	static int GPIO_Config(int dev, int flag, unsigned int* conf);
	static int SPI_Config(int dev, int flag, unsigned int* conf);
	static int IIC_Config(int dev, int flag, unsigned int* conf);

	static int GSL_Function(int dev, unsigned int*data);
	static int SPI_R_GSL_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int);
	static int SPI_W_GSL_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int);
	static int ENABLE_W_FLASH(int dev, unsigned char spi_ss, unsigned int addr);
	static int CS_ENABLE_FLASH(int dev, unsigned char spi_ss);

};

#endif // !defined(AFX_STM_Interface_H__125865F9_2DE8_45C8_898C_65303766F396__INCLUDED_)
