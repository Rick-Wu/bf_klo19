// STM32_USB.cpp: implementation of the CSTM32_USB class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "STM32_USB.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
int CSTM32_USB::version_oldest = 0;
int CSTM32_USB::lib_init = FALSE;
int CSTM32_USB::stm32_dev_num = 0;//stm32设备数量
int CSTM32_USB::effe_dev_id = 0;//有效设备的ID
int CSTM32_USB::effe_dev_num = 0;//有效设备的数量
int  CSTM32_USB::dev_id[MULTI_MAX] = {-1,-1,-1,-1, -1,-1,-1,-1,};
long CSTM32_USB::multi_open = 0;
unsigned char CSTM32_USB::iic_addr_save[MULTI_MAX] = {-1,-1,-1,-1, -1,-1,-1,-1,};
unsigned char CSTM32_USB::spi_ss_save[MULTI_MAX] = {-1,-1,-1,-1, -1,-1,-1,-1};
CSTM32_USB::CSTM32_USB()
{
}

CSTM32_USB::~CSTM32_USB()
{

	CloseDev();
 	stm32_dev_num = 0;
}

int  CSTM32_USB::ReadUSB(int id,unsigned char * data_char,int len,int time_out)
{
//	static int usb_reset = TRUE;
	int ret;
	int read_l = 0;
	if(USBCheck(id) == FALSE)
		return FALSE;
	do 
	{
		ret = USBBulkReadData(id,EP1_IN,(char *)data_char+read_l,len-read_l,time_out);
		read_l += ret;
	} while (ret > 0 && read_l < len);

// 	ret = USBBulkReadData(id,EP1_IN,(char *)data_char,len,time_out);
// 	read_l = ret;
// 	while(ret > 0 && read_l < len)
// 	{
// 			ret = USBBulkReadData(id,EP1_IN,(char *)data_char+read_l,len-read_l,time_out);
// 		read_l += ret;
// 	}
	if(ret < 0)
	{
		if (ret == -5 && version_oldest >= 20170420)
		{
			data_char[0] = 0xfd;
			USBBulkWriteData(id, EP1_OUT, (char *)data_char, 1, 50);
			USBClearHalt(id, EP1_IN);
			USBClearHalt(id, EP1_OUT);
		}
		for(int i=0;i<len;i++)
			data_char[i] = 0;
		return FALSE;
	}
	else
		return TRUE;
}

int  CSTM32_USB::TestIIC(int dev)
{
	unsigned char w_data[1]={0};
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x19;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,1,50);
	w_data[0] = 0;
	ReadUSB(dev_n,w_data,1,50);
 	return w_data[0];
}

int  CSTM32_USB::TestSPI(int dev)
{
	unsigned char w_data[1]={0};
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x09;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,1,50);
	w_data[0] = 0;
	ReadUSB(dev_n,w_data,1,50);
	return w_data[0];
}
// 
// int  CSTM32_USB::TestCPU(int dev)
// {
// 	UINT i;
// 	int ret = FALSE;
// 	for(i=0;i<2;i++)
// 	{
// 		if(i)
// 			ChangeMode();
// 		if(TestF0(dev))
// 			return TRUE;
// 	}
//  	return FALSE;
// }

// int  CSTM32_USB::ReadCPU(UINT rip, UINT rio,UINT *out)
// {
// 	if(USBCheck(usb_n) == FALSE)
// 		return FALSE;
// 	unsigned char w_data[7];
// 	if(rio >= 0x80 && rio < 0x100)
// 	{
// 		if(stm32_mode == CSTM32_USB::STM32_IIC)
// 			w_data[0] = 0x10;//iic read
// 		else
// 			w_data[0] = 0x00;//spi read
// 		w_data[1] = rio;
// 		w_data[2] = 0x04;
// 		USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,3,50);
// 	}
// 	else
// 	{
// 		if(stm32_mode == CSTM32_USB::STM32_IIC)
// 			w_data[0] = 0x12;
// 		else
// 			w_data[0] = 0x02;
// 		w_data[1] = ((rip*0x80+rio) >>  0) & 0xff;
// 		w_data[2] = ((rip*0x80+rio) >>  8) & 0xff;
// 		w_data[3] = ((rip*0x80+rio) >> 16) & 0xff;
// 		w_data[4] = ((rip*0x80+rio) >> 24) & 0xff;
// 		w_data[5] = 0x04;
// 		w_data[6] = 0x00;
// 		USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,7,50);
// 	}
// 	return ReadUSB(usb_n,(unsigned char*)out,4);
// }

// int  CSTM32_USB::WriteCPU(UINT wip, UINT wio, UINT wv)
// {
// 	if(USBCheck(usb_n) == FALSE)
// 		return FALSE;
// 	unsigned char w_data[11];
// 	if (wio >= 0x80 && wio < 0x100)
// 	{
// 		if(stm32_mode == CSTM32_USB::STM32_IIC)
// 			w_data[0] = 0x11;//iic write
// 		else
// 			w_data[0] = 0x01;//spi write
// 		w_data[1] = wio;
// 		w_data[2] = 0x04;
// 		w_data[3] = (wv >>  0) & 0xff;
// 		w_data[4] = (wv >>  8) & 0xff;
// 		w_data[5] = (wv >> 16) & 0xff;
// 		w_data[6] = (wv >> 24) & 0xff;
// 		USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,7,50);
// 	}
// 	else
// 	{
// 		if (stm32_mode == CSTM32_USB::STM32_IIC)
// 		{
// 			w_data[0] = 0x11;//iic write
// 			w_data[1] = 0xf0;
// 			w_data[2] = 0x04;
// 			w_data[3] = (wip >>  0) & 0xff;
// 			w_data[4] = (wip >>  8) & 0xff;
// 			w_data[5] = (wip >> 16) & 0xff;
// 			w_data[6] = (wip >> 24) & 0xff;
// 			USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,7,50);
// 			w_data[1] = wio;
// 			w_data[2] = 0x04;
// 			w_data[3] = (wv >>  0) & 0xff;
// 			w_data[4] = (wv >>  8) & 0xff;
// 			w_data[5] = (wv >> 16) & 0xff;
// 			w_data[6] = (wv >> 24) & 0xff;
// 			USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,7,50);
// 		}
// 		else
// 		{
// 			w_data[0] = 0x03;
// 			w_data[1] = ((wip * 0x80 + wio) >> 0) & 0xff;
// 			w_data[2] = ((wip * 0x80 + wio) >> 8) & 0xff;
// 			w_data[3] = ((wip * 0x80 + wio) >> 16) & 0xff;
// 			w_data[4] = ((wip * 0x80 + wio) >> 24) & 0xff;
// 			w_data[5] = 0x04;
// 			w_data[6] = 0x00;
// 			w_data[7] = (wv >> 0) & 0xff;
// 			w_data[8] = (wv >> 8) & 0xff;
// 			w_data[9] = (wv >> 16) & 0xff;
// 			w_data[10] = (wv >> 24) & 0xff;
// 			USBBulkWriteData(usb_n, EP1_OUT, (char *)w_data, 11, 50);
// 		}
// 	}
// 	return TRUE;
// }

int  CSTM32_USB::WriteIntIIC(int dev,UINT addr,UINT*buf_int,int len_int)
{
	unsigned char w_data[128+3];
	unsigned char * buf_char = (unsigned char *)buf_int;
	int i;
	int wl = 0;
	int wn = 0;
	int len_char = len_int *4;
	int w_max = 56;
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	while(wl < len_char)
	{
		if(len_char-wl >= w_max)
			wn = w_max;
		else
			wn = len_char % w_max;
		w_data[0] = 0x13;//iic write
		w_data[1] = ((addr+wl) >>  0) & 0xff;
		w_data[2] = ((addr+wl) >>  8) & 0xff;
		w_data[3] = ((addr+wl) >> 16) & 0xff;
		w_data[4] = ((addr+wl) >> 24) & 0xff;
		w_data[5] = wn & 0xff;
		w_data[6] = wn >> 8;
		for(i=0;i<wn;i++)
			w_data[i+7] = buf_char[wl + i];
		USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,wn+7,50);
		wl += wn;
	}
	return TRUE;
}

int  CSTM32_USB::WriteIntSPI(int dev,UINT addr,UINT*buf_int,int len_int)
{
	unsigned char w_data[128+3];
	unsigned char * buf_char = (unsigned char *)buf_int;
	int i;
	int wl = 0;
	int wn = 0;
	int len_char = len_int *4;
	int w_max = 56;
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	while(wl < len_char)
	{
		if(len_char-wl >= w_max)
			wn = w_max;
		else
			wn = len_char % w_max;
		w_data[0] = 0x03;//spi write
		w_data[1] = ((addr+wl) >>  0) & 0xff;
		w_data[2] = ((addr+wl) >>  8) & 0xff;
		w_data[3] = ((addr+wl) >> 16) & 0xff;
		w_data[4] = ((addr+wl) >> 24) & 0xff;
		w_data[5] = wn & 0xff;
		w_data[6] = wn >> 8;
		for(i=0;i<wn;i++)
			w_data[i+7] = buf_char[wl + i];
		USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,wn+7,50);
		wl += wn;
	}
	return TRUE;
}

int  CSTM32_USB::ReadIntSPI(int dev,UINT addr,UINT*buf,UINT len)
{//SPI是内部分次读，不限大小。
	int time_out = 20;
	unsigned char w_data[7];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
// 	if (addr % 0x80 == 0x7c)
// 	while (1);

	w_data[0] = 0x02;
	w_data[1] = (addr >>  0) & 0xff;
	w_data[2] = (addr >>  8) & 0xff;
	w_data[3] = (addr >> 16) & 0xff;
	w_data[4] = (addr >> 24) & 0xff;
	w_data[5] = (len*4) & 0xff;
	w_data[6] = (len*4) >> 8;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,7,50);
	time_out += len*4/1024*5;//1K < 5ms，SPI16分频
	if(time_out > 1000)
		time_out = 1000;
	return ReadUSB(dev_n,(unsigned char*)buf,len*4,time_out);
}


int  CSTM32_USB::ReadFlashSPI(int dev, UCHAR addr, UINT data, UCHAR *buf, UINT len)
{//SPI是内部分次读，不限大小。
	unsigned char w_buf[4] = { addr, (data >> 16) & 0xff, (data >> 8) & 0xff, data & 0xff };
	unsigned char r_buf[48];
	int dev_n = SearchId(dev);
	if (dev_n < 0)
		return FALSE;
	int ret = WriteReadSPI(dev, w_buf, r_buf, len);
	for (int i = 4; i < len; i++)
		buf[i-4] = r_buf[i];
	return ret;
}


int  CSTM32_USB::WriteFlashSPI(int dev, UCHAR addr, UINT data, UCHAR *buf, UINT len)
{//SPI是内部分次读，不限大小。
	int ret = 0;
	{
		int dev_n = SearchId(dev);
		if (dev_n < 0)
			return FALSE;
		unsigned char r_buf[48];
		unsigned char w_buf[48] = { addr, (data >> 16) & 0xff, (data >> 8) & 0xff, data & 0xff, };
		for (int i = 4; i < len; i++)
			w_buf[i] = buf[i - 4];
		ret = WriteReadSPI(dev, w_buf, r_buf, len);
	}
	return ret;
}
int  CSTM32_USB::EnableWriteFlashSPI(int dev, UCHAR addr)
{//SPI是内部分次读，不限大小。
	unsigned char w_buf[1] = { addr};
	unsigned char r_buf[1];
	int dev_n = SearchId(dev);
	if (dev_n < 0)
		return FALSE;
	int ret = WriteReadSPI(dev, w_buf, r_buf, 1);
	return ret;
}
// int  CSTM32_USB::ReadSPI(int dev, UINT addr, unsigned char*buf, UINT len_char)
// {//SPI是内部分次读，不限大小。
// 	int time_out = 20;
// 	unsigned char w_data[7];
// 	int dev_n = SearchId(dev);
// 	if (dev_n < 0)
// 		return FALSE;
// 	w_data[0] = 0x02;
// 	w_data[1] = (addr >> 0) & 0xff;
// 	w_data[2] = (addr >> 8) & 0xff;
// 	w_data[3] = (addr >> 16) & 0xff;
// 	w_data[4] = (addr >> 24) & 0xff;
// 	w_data[5] = len_char & 0xff;
// 	w_data[6] = len_char >> 8;
// 	USBBulkWriteData(dev_n, EP1_OUT, (char *)w_data, 7, 50);
// 	time_out += len_char/ 1024 * 5;//1K < 5ms，SPI16分频
// 	if (time_out > 1000)
// 		time_out = 1000;
// 	return ReadUSB(dev_n, buf, len_char, time_out);
// }

int  CSTM32_USB::ReadIntIIC(int dev,UINT addr,UINT*buf,UINT len)
{//IIC一次读取量不能大于USB发送缓冲2048字节
	int time_out = 20;
	unsigned char w_data[7];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(len > 2048/4-1)
		len = 2048/4-1;
	w_data[0] = 0x12;
	w_data[1] = (addr >>  0) & 0xff;
	w_data[2] = (addr >>  8) & 0xff;
	w_data[3] = (addr >> 16) & 0xff;
	w_data[4] = (addr >> 24) & 0xff;
	w_data[5] = (len*4) & 0xff;
	w_data[6] = (len*4) >> 8;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,7,50);
	time_out += len*4/5;
	return ReadUSB(dev_n,(unsigned char*)buf,len*4,time_out);
}

void CSTM32_USB::CloseDev()
{
	int i;
	for(i=0;i<MULTI_MAX;i++)
	{
		if(dev_id[i] < 0)
			continue;
		dev_id[i] = -1;
		USBCloseDev(i);
	}
}

int  CSTM32_USB::ConfigIIC(int dev,UINT *version,int speed)
{
	UINT ver[2];
	UCHAR w_data[8];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(version == NULL)
	{
		GetVersion(dev_n,ver);
	}
	else
	{
		ver[0] = version[0];
		ver[1] = version[1];
	}
	if(ver[0] < 0x20161125)
		return 0;//旧版本不支持
	if(speed == 1)
		speed = 400*1000;
	if(speed < 10*1000 || speed > 400*1000)
		return FALSE;
	w_data[0] = 0x8b;
	w_data[1] = (speed >>  0) & 0xff;
	w_data[2] = (speed >>  8) & 0xff;
	w_data[3] = (speed >> 16) & 0xff;
	w_data[4] = (speed >> 24) & 0xff;
	return USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,5,50);
}

int  CSTM32_USB::ConfigSPI(int dev,UINT *version,int baud)
{//返回实际波特率，单位 Hz
	int baud_tab[]=
	{
		SPI_BAUDRATEPRESCALER_2,
		SPI_BAUDRATEPRESCALER_4,
		SPI_BAUDRATEPRESCALER_8,
		SPI_BAUDRATEPRESCALER_16,
		SPI_BAUDRATEPRESCALER_32,
		SPI_BAUDRATEPRESCALER_64,
		SPI_BAUDRATEPRESCALER_128,
		SPI_BAUDRATEPRESCALER_256,
	};
	int apb;
	UINT ver[2];
	if(version == NULL)
	{
		int dev_n = SearchId(dev);
		if(dev_n < 0)
			return FALSE;
		GetVersion(dev_n,ver);
	}
	else
	{
		ver[0] = version[0];
		ver[1] = version[1];
	}
	if(ver[0] < 0x20161125)
		return 0;//旧版本不支持
	apb = (ver[1] >> 8) & 0xff;//HCLK_AHB速度
	apb = apb * 1000 * 1000/2;//APB总线速度
	if(apb == 0)
		return 0;
	if(baud == 0)
		return 0;
	if(baud <= 21)
		baud *= 1000*1000;//输入参数是按MHz计算的
	if(baud <=21*1000 && baud >=156)
		baud *= 1000;//输入参数是按kHz计算的
	if(baud <156*1000 || baud >21*1000*1000)
		return 0;
	int i;
	for(i=0;i<7;i++)
	{
		if(apb / (0x2<<i) <= baud)
			break;
	}
	ConfigSPI(dev,(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | baud_tab[i]));
	return apb / (0x2<<i);
}

int  CSTM32_USB::FindDev(int flag)
{
	int i,id;
	int dev_scan;
	int ret = 0;
	int baud;
	UINT ver[2];
	//设备数量判断
	if(lib_init == FALSE)
	{
		lib_init = TRUE;
		dev_scan = USBScanDev(TRUE);
	}
	else
		dev_scan = USBScanDev(FALSE);
 	if (stm32_dev_num == dev_scan)
	{
		id = 0;
		for (i = 0; i < MULTI_MAX; i++)
		{
			if (USBCheck(i))
				id++;
		}
		if (stm32_dev_num == id)
			goto find_dev_ret;
 	}
	if(dev_scan < 0)
		dev_scan = 0 - dev_scan;
	//关闭上次打开的设备
	CloseDev();
	effe_dev_id = 0;
	effe_dev_num = 0;
	version_oldest = 0x20990101;
	for(i=0;i<MULTI_MAX;i++)
		dev_id[i] = -1;
	for(i=0;i<MULTI_MAX && i<dev_scan;i++)
	{
		if (USBOpenDev(i) != SEVERITY_SUCCESS)
		{
			effe_dev_num = 0;
			dev_scan = 0;
			effe_dev_id = 0;
			continue;
		}
		GetVersion(i,ver);
		if(ver[0] == 0)
		{
			USBCloseDev(i);
			continue;
		}
		if(ver[0] < (UINT)version_oldest)
			version_oldest = ver[0];//保存最老版的版本号
		id = ver[1] & 0x7;//目前只做8个
		if(id < MULTI_MAX)
		{
			dev_id[i] = id;			
			if (effe_dev_id & (0x1 << id))//ID重复
				effe_dev_id |= 0x1 << (id + 16);
			else
				effe_dev_id |= 0x1 << id;
			effe_dev_num ++;
			//设置波特率，
			//CFunction::spi_speed可以直接用常数代替，比如3，就是3MHz
			//返回值是实际波特率，应该是 >1.5M && <=3M 
			baud = ConfigSPI(id,ver,4);
			if (baud == 0)
				ConfigSPI(id, STM32_USB_SPI_MODE0_3M);
// 			if(CFunction::iic_speed)
// 				ConfigIIC(id,ver,CFunction::iic_speed);
		}
		else
		{
			USBCloseDev(i);
		}
	}
	stm32_dev_num = dev_scan;
find_dev_ret:
	if(flag == 0)
		ret = effe_dev_num;
	else if(flag == 1)
		ret = effe_dev_id;
	else
		ret = 0;
	return ret;
}


UINT CSTM32_USB::GetVersion(int dev_n,UINT *ver)
{
	UCHAR w_data[1] = {0x81};
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,1,50);
	return ReadUSB(dev_n,(unsigned char*)ver,8);
}


int  CSTM32_USB::SearchId(int dev)
{
	int i;
	if(dev < 0)
	{
		if(USBCheck(0))
			return 0;
		else
			return -1;
	}
	for(i=0;i<MULTI_MAX;i++)
	{
		if(dev_id[i] == dev)
		{
			if(USBCheck(i))
				return i;
			else
				return -1;
		}
	}
	return -1;
}

int  CSTM32_USB::ConfigSPI(int dev,UINT conf)
{
	unsigned char w_data[9];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x80;//spi config
	w_data[1] = (conf>> 0) & 0xff;
	w_data[2] = (conf>> 8) & 0xff;
	w_data[3] = (conf>>16) & 0xff;
	w_data[4] = (conf>>24) & 0xff;
	w_data[5] = 0x5a;
	w_data[6] = 0x5a;
	w_data[7] = 0x5a;
	w_data[8] = 0x5a;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,9,50);
	return TRUE;
}

// int CSTM32_USB::ReadPoint(UINT *data)
// {
// 	UCHAR w_data[2];
// 	w_data[0] = 0x18;
// 	w_data[1] = 1;//data[0] & 0xf;
// 	USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,2,50);
// 	ReadUSB(usb_n,(unsigned char*)data,44*1,100);
// 	if((data[0] & 0xff) == 0xff)
// 		return FALSE;
// 	w_data[1] = (data[0]>>4) & 0xf;
// 	USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,2,50);
// 	ReadUSB(usb_n,(unsigned char*)(&data[11]),44*w_data[1],100);
// 	int len = w_data[1]+1;
// 	int i;
// 	for(i=0;i<len;i++)
// 		data[i*11] &= ~0xf0;
// 	return len;
// }

// int CSTM32_USB::CommMode(int s)
// {
// 	if(s == CSTM32_USB::STM32_IIC)
// 	{
// 		if(stm32_mode == CSTM32_USB::STM32_IIC)
// 			return TRUE;
// 		else
// 			return FALSE;
// 	}
// 	if(s == CSTM32_USB::STM32_SPI)
// 	{
// 		if(stm32_mode == CSTM32_USB::STM32_SPI)
// 			return TRUE;
// 		else
// 			return FALSE;
// 	}
// 	return stm32_mode;
// }

// int  CSTM32_USB::GetIrq()
// {
// 	unsigned char w_data[5];
// 	w_data[0] = 0x83;
// 	USBBulkWriteData(usb_n,EP1_OUT,(char *)w_data,1,50);
// 	ReadUSB(usb_n,w_data,4,50);
// 	return (w_data[0] & 0x08) ? 1 : 0;//PA3
// }
// 
// int CSTM32_USB::InteMode()
// {
// 	if(GetIrq() != 0)
// 		return FALSE;
// 	WriteCPU(0xff000020/0x80,0xff000020%0x80,1);
// 	Sleep(20);
// 	if(GetIrq() != 1)
// 		return FALSE;
// 	WriteCPU(0xff000020/0x80,0xff000020%0x80,0);
// 	Sleep(20);
// 	if(GetIrq() != 0)
// 		return FALSE;
// 	UINT data[16*11];
// 	ReadPoint(data);//清空缓冲区
// 	return TRUE;
// }
//----------------------------------------------------------------------------------------------------
int CSTM32_USB::WriteReadSPI(int dev,unsigned char *w_buf,unsigned char*r_buf,int len)
{
	int i;
	unsigned char w_data[64];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(len > 60)
		len = 60;
	w_data[0] = 0x05;
	w_data[1] = len;
	for(i=0;i<len;i++)
		w_data[i+2] = w_buf[i];
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,len+2,50);
	return ReadUSB(dev_n,r_buf,len);
}

int  CSTM32_USB::SPI_SS(int dev,int ss)
{
	unsigned char w_data[464];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(dev_n >= sizeof(spi_ss_save)/sizeof(spi_ss_save[0]))
		return FALSE;
	if(ss == spi_ss_save[dev_n])
		return TRUE;
	spi_ss_save[dev_n] = ss;
	w_data[0] = 0x82;
	w_data[1] = ss & 0x3;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,2,50);
	return TRUE;
}

int  CSTM32_USB::IIC_Addr(int dev,int iic_addr)
{
	unsigned char w_data[464];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(dev_n >= sizeof(iic_addr_save)/sizeof(iic_addr_save[0]))
		return FALSE;
	if(iic_addr == iic_addr_save[dev_n])
		return TRUE;
	iic_addr_save[dev_n] = iic_addr;
	w_data[0] = 0x85;
	w_data[1] = (iic_addr*2) & 0xfe;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,2,50);
	return TRUE;
}

int  CSTM32_USB::ReadIIC(int dev,unsigned char *buf,int len)
{
	unsigned char w_data[5];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x14;
	w_data[1] = len;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,2,50);
	return ReadUSB(dev_n,buf,len,50);
}

int  CSTM32_USB::WriteIIC(int dev,unsigned char *buf,int len)
{
	int i;
	unsigned char w_data[64];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(len > 62)
		len = 62;
	w_data[0] = 0x15;
	w_data[1] = len;
	for(i=0;i<len;i++)
		w_data[i+2] = buf[i];
	return USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,len+2,50);
}

int  CSTM32_USB::ReadMemIIC(int dev,unsigned char offset,unsigned char *buf,int len)
{
	unsigned char w_data[5];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x10;//iic read
	w_data[1] = offset;
	w_data[2] = len;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,3,50);
	return ReadUSB(dev_n,(unsigned char*)buf,len,50);

// 	unsigned char w_data[5];
// 	int dev_n = SearchId(dev);
// 	if(dev_n < 0)
// 		return FALSE;
// 	int rl = 0;
// 	int rn = 0;
// 	int ret = TRUE;
// 	int r_max = 60;
// 	while(rl < len_char && ret != FALSE)
// 	{
// 		if(len_char-rl >= r_max)
// 			rn = r_max;
// 		else
// 			rn = len_char % r_max;
// 		w_data[0] = 0x10;//iic read
// 		w_data[1] = (offset+rl) & 0x7f;
// 		if(offset >= 0x80)
// 			w_data[1] |= 0x80;
// 		w_data[2] = rn;
// 		USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,3,50);
// 		ret = ReadUSB(dev_n,(unsigned char*)(buf_char+rl),rn,50);
// 		rl += rn;
// 	}
// 	return ret;
}

int  CSTM32_USB::WriteMemIIC(int dev,unsigned char offset,unsigned char *buf,int len)
{
	int i;
	unsigned char w_data[64];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(len > 60)
		len = 60;
	w_data[0] = 0x11;//iic write
	w_data[1] = offset;
	w_data[2] = len;
	for(i=0;i<len;i++)
		w_data[i+3] = buf[i];
	return USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,len+3,50);
}

int  CSTM32_USB::ReadMemSPI(int dev,unsigned char offset,unsigned char *buf,int len)
{
	unsigned char w_data[5];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	w_data[0] = 0x00;
	w_data[1] = offset;
	w_data[2] = len;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,3,50);
	return ReadUSB(dev_n,(unsigned char*)buf,len,50);
}

int  CSTM32_USB::WriteMemSPI(int dev,unsigned char offset,unsigned char *buf,int len)
{
	int i;
	unsigned char w_data[64];
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	if(len > 60)
		len = 60;
	w_data[0] = 0x01;
	w_data[1] = offset;
	w_data[2] = len;
	for(i=0;i<len;i++)
		w_data[i+3] = buf[i];
	return USBBulkWriteData(dev_n,EP1_OUT,(char *)w_data,len+3,50);
}

int  CSTM32_USB::GpioSet(int dev, UINT port, UINT pin, UINT i_o, UINT flag)
{
	// 	GPIO_InitStruct.Pin = (buf[3]<<8) + buf[2];
	// 	GPIO_InitStruct.Mode = buf[4];
	// 	GPIO_InitStruct.Pull = buf[5];
	// 	GPIO_InitStruct.Speed = buf[6];
	// 	GPIO_InitStruct.Alternate = buf[7];
#ifndef uint32_t
#define uint32_t unsigned int
#endif

#define  GPIO_MODE_INPUT                        ((uint32_t)0x00000000)   /*!< Input Floating Mode                   */
#define  GPIO_MODE_OUTPUT_PP                    ((uint32_t)0x00000001)   /*!< Output Push Pull Mode                 */
#define  GPIO_MODE_OUTPUT_OD                    ((uint32_t)0x00000011)   /*!< Output Open Drain Mode                */
#define  GPIO_MODE_AF_PP                        ((uint32_t)0x00000002)   /*!< Alternate Function Push Pull Mode     */
#define  GPIO_MODE_AF_OD                        ((uint32_t)0x00000012)   /*!< Alternate Function Open Drain Mode    */

#define  GPIO_NOPULL        ((uint32_t)0x00000000)   /*!< No Pull-up or Pull-down activation  */
#define  GPIO_PULLUP        ((uint32_t)0x00000001)   /*!< Pull-up activation                  */
#define  GPIO_PULLDOWN      ((uint32_t)0x00000002)   /*!< Pull-down activation                */

#define  GPIO_SPEED_LOW         ((uint32_t)0x00000000)  /*!< Low speed     */
#define  GPIO_SPEED_MEDIUM      ((uint32_t)0x00000001)  /*!< Medium speed  */
#define  GPIO_SPEED_FAST        ((uint32_t)0x00000002)  /*!< Fast speed    */
#define  GPIO_SPEED_HIGH        ((uint32_t)0x00000003)  /*!< High speed    */

	BYTE buf[8];
	buf[0] = 0x87;
	buf[1] = port & 0x3;//0==GPIOA 1==GPIOB
	buf[2] = pin & 0xff;
	buf[3] = (pin >> 8) & 0xff;
	buf[4] = i_o == 0 ? GPIO_MODE_OUTPUT_PP : GPIO_MODE_INPUT;
	if (flag & 1)
		buf[5] = GPIO_PULLUP;
	else if (flag & 2)
		buf[5] = GPIO_PULLDOWN;
	else
		buf[5] = GPIO_NOPULL;
	buf[6] = GPIO_SPEED_FAST;
	buf[7] = 0;

	int dev_n = SearchId(dev);
	if (dev_n < 0)
		return FALSE;
	USBBulkWriteData(dev_n, EP1_OUT, (char *)buf, 8, 50);
	return TRUE;
}

int  CSTM32_USB::GpioOutput(int dev,UINT port,UINT pin,UINT value)
{
	BYTE buf[6];
	buf[0] = 0x88;
	buf[1] = port&0x3;//0==GPIOA 1==GPIOB
	buf[2] = pin & 0xff;
	buf[3] = (pin >> 8) & 0xff;
	buf[4] = value & 0xff;
	buf[5] = (value >> 8) & 0xff;
	
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)buf,6,50);
	return TRUE;
}

int  CSTM32_USB::GpioInput(int dev,UINT port,UINT *value)
{
	BYTE buf[4];
	buf[0] = 0x89;
	buf[1] = port&0x3;//0==GPIOA 1==GPIOB
	int dev_n = SearchId(dev);
	if(dev_n < 0)
		return FALSE;
	USBBulkWriteData(dev_n,EP1_OUT,(char *)buf,2,50);
	ReadUSB(dev_n,(unsigned char*)buf,4,50);
	*value = (buf[3]<<24) + (buf[2]<<16) + (buf[1]<<8) + buf[0];
	return TRUE;
}

int CSTM32_USB::IRQ_Time(int dev,int flag)
{
	int dev_n = SearchId(dev);
	if (dev_n < 0)
		return FALSE;
	UCHAR wd[2];
	UINT ret = 0;
	wd[0] = 0x8a;
	if (flag == 0)
	{//清空
		wd[1] = 0;
		USBBulkWriteData(dev_n, EP1_OUT, (char *)wd, 2, 50);
		return TRUE;
	}
	else if (flag == 1)
	{
		wd[1] = 1;
		USBBulkWriteData(dev_n, EP1_OUT, (char *)wd, 2, 50);
		ReadUSB(dev_n, (unsigned char*)(&ret), 4, 50);
		return ret;
	}
	return 0;
}
