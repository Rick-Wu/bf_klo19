// STM_Interface.cpp: implementation of the CSTM_Interface class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
//#include "CommLib.h"
#include "STM_Interface.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//DOME IO6 IO5 IO4   IO3 IO2 IO1 in2     in1 power IRQ RST
//0xffff fc02
//RST PA7
//IRQ PA4
//POW PB4
//IN1 PB1
//IN2 PB2
//IO1 PB10
//IO2 PB12
//IO3 PA1
//IO4 PA5
//IO5 PA6
//IO6 PB3
//DOME	PC4
//PB5
//PA8	ID1
//PA10	ID2
//PA15	ID4
//PC1   GPIO7   IO1
CSTM32_USB CSTM_Interface::stm;
#define	GPIO_GROUP_MAX	3
UINT CSTM_Interface::gpio_tab[]=
{
	0x007, 0x004, 0x104, 0x101, 0x102,
	0x10a, 0x10c, 0x001, 0x005, 0x006,
	0x103, 0x204, 0x105, 0x008, 0x00a,
	0x00f, 0x201,
};


CSTM_Interface::CSTM_Interface()
{

}

CSTM_Interface::~CSTM_Interface()
{

}

int CSTM_Interface::FindOpen(int flag)
{
	return stm.FindDev(flag);//flag
}

int CSTM_Interface::Close(int flag)
{
	return TRUE;
}

int CSTM_Interface::Info (int dev,char **str)
{
	static char data[] = "Silead STM32";
	*str = data;
	return TRUE;
}

int CSTM_Interface::IIC_ReadBase (int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char)
{
	stm.IIC_Addr(dev,iic_addr);
	return stm.ReadIIC(dev,buf_char,len_char);
}

int CSTM_Interface::IIC_WriteBase(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char)
{
	stm.IIC_Addr(dev,iic_addr);
	return stm.WriteIIC(dev,buf_char,len_char);
}

int CSTM_Interface::IIC_Read (int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int rl = 0;
	int rn = 0;
	int ret = TRUE;
	unsigned char w_buf[1];
	int r_max = 60;
	stm.IIC_Addr(dev,iic_addr);
	while(rl < len_char && ret != FALSE)
	{
		if(len_char-rl >= r_max)
			rn = r_max;
		else
			rn = len_char % r_max;
		w_buf[0] = (offset+rl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		stm.WriteIIC(dev,w_buf,1);
		ret = stm.ReadIIC(dev,buf_char+rl,rn);
		rl += rn;
	}
	return ret;
}

int CSTM_Interface::IIC_Write(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int i;
	int wl = 0;//已经写了多少
	int wn = 0;//这一次要写多少
	int w_max = 60;//一次最多写多少
	int ret = TRUE;
	unsigned char w_buf[64];
	stm.IIC_Addr(dev,iic_addr);
	while(wl < len_char && ret != FALSE)
	{
		if(len_char-wl >= w_max)
			wn = w_max;
		else
			wn = len_char % w_max;
		w_buf[0] = (offset+wl) & 0x7f;
		if(offset >= 0x80)
			w_buf[0] |= 0x80;
		for(i=0;i<wn;i++)
			w_buf[i+1] = buf_char[wl + i];
		ret = stm.WriteIIC(dev,w_buf,wn+1);
		wl += wn;
	}
	return ret;
}

int CSTM_Interface::IIC_R_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int)
{
	int rl = 0;
	int rn = 0;
	int ret = TRUE;
	int r_max = 256;//len_int
	stm.IIC_Addr(dev,iic_addr);
	while(rl < len_int && ret != FALSE)
	{
		if(len_int-rl >= r_max)
			rn = r_max;
		else
			rn = len_int % r_max;
		ret = stm.ReadIntIIC(dev,addr,buf+rl,rn);
		rl += rn;
	}
	return ret;
}

int CSTM_Interface::IIC_W_GSL(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int)
{
	stm.IIC_Addr(dev,iic_addr);
	return stm.WriteIntIIC(dev,addr,buf,len_int);
// 	unsigned int page = addr/0x80;
// 	unsigned int offset = addr%0x80;
// 	IIC_Write(dev,iic_addr,0xf0,(unsigned char *)&page,4);
// 	return IIC_Write(dev,iic_addr,offset,(unsigned char *)buf,len_int*4);
}

//---------------------------------------------------------------------------
//	int CSTM_Interface::SPI_Init (unsigned int *data);//保留

int CSTM_Interface::SPI_WR(int dev,unsigned char spi_ss,unsigned char *w_char,unsigned char*r_char,int len_char)
{
	stm.SPI_SS(dev,spi_ss);
	stm.WriteReadSPI(dev,w_char,r_char,len_char);
	return TRUE;
}

int CSTM_Interface::SPI_Read (int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int rl = 0;
	int rn = 0;
	unsigned char r_offset;
	int ret = TRUE;
	int r_max = 60;
//	stm.ChangeMode(CSTM32_USB::STM32_SPI);
	stm.SPI_SS(dev,spi_ss);
	while(rl < len_char && ret != FALSE)
	{
		if(len_char-rl >= r_max)
			rn = r_max;
		else
			rn = len_char % r_max;
		r_offset = (offset+rl) & 0x7f;
		if(offset >= 0x80)
			r_offset |= 0x80;
		ret = stm.ReadMemSPI(dev,r_offset,buf_char+rl,rn);
		rl += rn;
	}
	return ret;
}
int CSTM_Interface::SPI_Write(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char)
{
	int rl = 0;
	int rn = 0;
	unsigned char r_offset;
	int ret = TRUE;
	int r_max = 60;
	//	stm.ChangeMode(CSTM32_USB::STM32_SPI);
	stm.SPI_SS(dev,spi_ss);
	while(rl < len_char && ret != FALSE)
	{
		if(len_char-rl >= r_max)
			rn = r_max;
		else
			rn = len_char % r_max;
		r_offset = (offset+rl) & 0x7f;
		if(offset >= 0x80)
			r_offset |= 0x80;
		ret = stm.WriteMemSPI(dev,r_offset,buf_char+rl,rn);
		rl += rn;
	}
	return ret;
}

int CSTM_Interface::SPI_R_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int)
{
	stm.SPI_SS(dev,spi_ss);
	return stm.ReadIntSPI(dev, addr, buf, len_int);;
}

int CSTM_Interface::SPI_R_GSL_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int)
{
//	stm.SPI_SS(dev, spi_ss);
	stm.ReadFlashSPI(dev, addr, data, buf, len_int);
	return TRUE;
}
int CSTM_Interface::SPI_W_GSL_FLASH(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int)
{
	{
//		stm.SPI_SS(dev, spi_ss);
		stm.WriteFlashSPI(dev, addr, data, buf, len_int);
	}
	return TRUE;
}


int CSTM_Interface::ENABLE_W_FLASH(int dev, unsigned char spi_ss, unsigned int addr)
{
//	stm.SPI_SS(dev, spi_ss);
	stm.EnableWriteFlashSPI(dev, addr);
	return TRUE;
}

int CSTM_Interface::CS_ENABLE_FLASH(int dev, unsigned char spi_ss)
{
	stm.SPI_SS(dev, spi_ss);
	return 0;
}


int CSTM_Interface::SPI_WR_GSL(int dev, unsigned char spi_ss, unsigned char *addr, unsigned char *buf, int len_int)
{
	unsigned int addr_t = (addr[3] << 24) + (addr[2] << 16) + (addr[1] << 8) + (addr[0] << 0);
	stm.SPI_SS(dev, spi_ss);
	stm.ReadIntSPI(dev, addr_t, (unsigned int *)buf, (len_int+3)/4);
	return TRUE;
}

int CSTM_Interface::SPI_W_GSL(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int)
{
	stm.SPI_SS(dev,spi_ss);
	stm.WriteIntSPI(dev,addr,buf,len_int);
	return TRUE;
}
//---------------------------------------------------------------------------
int CSTM_Interface::SPI_Config(int dev, int flag, unsigned int* conf)
{
//#define	STM32_USB_SPI_MODE0_1_5M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_16)
//#define	STM32_USB_SPI_MODE0_3M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_8)
//#define	STM32_USB_SPI_MODE0_6M			(SPI_PHASE_1EDGE | SPI_POLARITY_LOW | SPI_FIRSTBIT_MSB | SPI_BAUDRATEPRESCALER_4)
	if (flag == 0)
	{ 
		if (conf[0] >= 18 * 1000 * 1000) //16
			stm.ConfigSPI(dev, STM32_USB_SPI_MODE0_21M);
		else if (conf[0] >= 9 * 1000 * 1000) //16
			stm.ConfigSPI(dev, STM32_USB_SPI_MODE0_10M5);
		else if (conf[0] >= 4.5 * 1000 * 1000) //16
			stm.ConfigSPI(dev, STM32_USB_SPI_MODE0_5M25);
			//stm.ConfigSPI(dev, 4500*1000);
		else if (conf[0] >= 2.25 * 1000 * 1000) //24
			stm.ConfigSPI(dev, STM32_USB_SPI_MODE0_2M6);
			//stm.ConfigSPI(dev, 2250*1000);
		else //32
			stm.ConfigSPI(dev, STM32_USB_SPI_MODE0_1M3);
			//stm.ConfigSPI(dev, 1125 * 1000);
	}
	return TRUE;
}
int CSTM_Interface::IIC_Config(int dev, int flag, unsigned int* conf)
{
	return TRUE;
}

//---------------------------------------------------------------------------
//	int (*GPIO_Config)(int dev,int flag,unsigned int *conf);
//  int  CSTM32_USB::GpioSet(int dev,UINT port,UINT pin,UINT i_o)
int CSTM_Interface::GPIO_Config(int dev, int flag, unsigned int *conf)
{
	int tab_len = sizeof(gpio_tab) / sizeof(gpio_tab[0]);
	int i, k, data;
	UINT pin[GPIO_GROUP_MAX], io[GPIO_GROUP_MAX];
	int  ret = TRUE;
	for (k = 0; k < GPIO_GROUP_MAX; k++)
	{
		pin[k] = 0;
		io[k] = 0;
		for (i = 0; i < tab_len&&i < 32; i++)
		{
			data = gpio_tab[i];
			if (((data & 0xff00) >> 8) == k)
			{
				pin[k] |= 1 << (data & 0x1f);
				io[k] |= ((1 << i) & conf[0]) ? (1 << (data & 0x1f)) : 0;
			}
		}
		if (flag == 0)
		{//常规IO设置
			if (pin[k] & ~io[k])//set porta output
				ret &= stm.GpioSet(dev, k, pin[k] & ~io[k], 0);
			if (pin[k] & io[k])//set porta input
				ret &= stm.GpioSet(dev, k, pin[k] & io[k], 1);
		}
		else if (flag == 1)
		{//设为输入，上拉
			if (pin[k] & io[k])//set porta input
				ret &= stm.GpioSet(dev, k, pin[k] & io[k], 1, 1);
		}
		else if (flag == 2)
		{//设为输入，下拉
			if (pin[k] & io[k])//set porta input
				ret &= stm.GpioSet(dev, k, pin[k] & io[k], 1, 2);
		}
	}
	return ret;
}

//int  CSTM32_USB::GpioOutput(int dev,UINT port,UINT pin,UINT value);
int CSTM_Interface::GPIO_Output(int dev, unsigned int port, unsigned int statue)
{
	int tab_len = sizeof(gpio_tab) / sizeof(gpio_tab[0]);
	int i, k,data;
	UINT pin[GPIO_GROUP_MAX], val[GPIO_GROUP_MAX];
	int  ret = TRUE;
	for(k=0;k<GPIO_GROUP_MAX;k++)
	{
		pin[k] = 0;
		val[k] = 0;
		for (i = 0; i<tab_len&&i<32; i++)
		{
			if ((port & (1 << i)) == 0)
				continue;
			data = gpio_tab[i];
			if (((data & 0xff00)>>8) == k)
			{
				pin[k] |= 1 << (data & 0x1f);
				val[k] |= ((1 << i) & statue) ? (1 << (data & 0x1f)) : 0;
			}
		}
		if (pin[k])
			ret &= stm.GpioOutput(dev, k, pin[k], val[k]);
	}
	return ret;
}

//int  CSTM32_USB::GpioInput(int dev,UINT port,UINT *value);
int CSTM_Interface::GPIO_Input(int dev, unsigned int *status)
{
	int tab_len = sizeof(gpio_tab) / sizeof(gpio_tab[0]);
	int i, k, data;
	UINT val[GPIO_GROUP_MAX];
	int ret = TRUE;
	*status = 0;
	for(k=0;k<GPIO_GROUP_MAX;k++)
	{
		val[k] = 0;
		ret &= stm.GpioInput(dev, k, &val[k]);
		for (i = 0; i<tab_len&&i<32; i++)
		{
			data = gpio_tab[i];
			if (((data & 0xff00)>>8) == k)
			{
				*status |= (val[k] & (1 << (data & 0x1f))) ? (1 << i) : 0;
			}
		}
	}
	return ret;
}

int CSTM_Interface::GSL_Function(int dev, unsigned int*data)
{
	int buf_len = data[0];//[0] 数组长度
	if (data[1] == 1)//IRQ测量功能
	{
		if (buf_len < 3)
			return FALSE;
		data[0] = 0;
		if (data[2] == 0)
			stm.IRQ_Time(dev,0);
		if (data[2] == 1)
			data[0] = stm.IRQ_Time(dev,1);
		return TRUE;//返回值仅表示操作是否成功。需要返回的数据保存在data中
	}
	return FALSE;
}










