// Interface.h: interface for the CInterface class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERFACE_H__6C064248_1F1C_4E40_844C_5F99E62E9C6D__INCLUDED_)
#define AFX_INTERFACE_H__6C064248_1F1C_4E40_844C_5F99E62E9C6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#if 1


//这些接口函数，不带线程保护。需要额外添加。
//原因：以IIC为例，如果别人只封装了基本的读写。而由我们自动生成的低0xff080010之类的寄存器。
//那么，从 往0xf0 写 0x1fe1000，到从0x10读写数据完毕，整个过程都需要线程保护。
//基本的读写中所包含的线程保护是满足不了需求的.

typedef struct   
{
	//如果 find 和 open 分开,需要把 find 做成不用线程保护的.
	//考虑到其他设备可能无法封成该方式,故合成.
	//也就是说，现在的 FindOpen 需要线程保护,当所有已知设备都空闲时才能调用.
	//bit0 , 返回类型 
	//     == 0, 返回设备数量
	//     == 1, 返回设备ID.如返回0x5,表示第0号设备和第2号设备连接.
	//        如果不支持多设备,返回0或1即可.
	//bit4 , 屏蔽IIC
	//bit5 , 屏蔽SPI
	//其他,保留.
	int (*FindOpen )(int flag);//flag,保留.返回设备数量
	int (*Close)(int flag);
	int (*Info )(int dev,char **str);//字符串，转接板信息
	//---------------------------------------------------------------------------
	int size_iic;
	//flag == 0, 配置IIC速度
	//其他,保留
	int (*IIC_Config)(int dev,int flag,unsigned int *conf);
	int (*IIC_ReadBase )(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);//上层不会调用
	int (*IIC_WriteBase)(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);//上层不会调用
	//要求下面函数读写长度无限
	int (*IIC_Read )(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
	//读TP的fc	IIC_Read(0,0x40,0xfc,buf,4);
	int (*IIC_Write)(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);

	int (*IIC_R_GSL)(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);
	//读TP的ff080010	IIC_R_GSL(0,0x40,0xff080010,buf,1);
	//读0x7页	IIC_R_GSL(0,0x40,0x7*0x80,buf,32);
	int (*IIC_W_GSL)(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);
	//---------------------------------------------------------------------------
	int size_spi;
	//flag == 0, 配置SPI速度
	//其他,保留
	int (*SPI_Config)(int dev,int flag,unsigned int *conf);
	int(*SPI_WR)(int dev, unsigned char spi_ss, unsigned char *w_char, unsigned char*r_char, int len_char);//上层不会调用

	int(*SPI_Read)(int dev, unsigned char spi_ss, unsigned char offset, unsigned char *buf_char, int len_char);
	int(*SPI_Write)(int dev, unsigned char spi_ss, unsigned char offset, unsigned char *buf_char, int len_char);

	int(*SPI_R_GSL)(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int);
	//读指纹160*160图像		SPI_R_GSL(0,0,0,buf,160*160/4);
	int(*SPI_W_GSL)(int dev, unsigned char spi_ss, unsigned int addr, unsigned int *buf, int len_int);
	//---------------------------------------------------------------------------
	//flag == 0,配置输入输出.
	//0为输出，1为输入。没用到的设为输入.
	//目前配置0(O):RST; 1(I):IRQ; 2(O):Power; 3(O)IN1; 4(O)IN2;5(0)IO1; 6(0)IO2; 7(0)IO3; 8(0)IO4; 9(0)IO5;
	//unsigned int data = 0xffffffe2;
	//GPIO_Config(dev,0,&data);
	//其他,保留.可能用到的,如输入是否上下拉,输出推挽/开漏等.
	int(*GPIO_Config)(int dev, int flag, unsigned int *conf);
	//GPIO_Output(0,4,4);//Power 拉高
	//GPIO_Output(0,1,0);//RST 拉低
	int(*GPIO_Output)(int dev, unsigned int port, unsigned int statue);
	//获得中断脚的状态
	//unsigned int gpio;
	//GPIO_Input(0,&gpio);
	//IRQ = (gpio>>1) & 1;
	int(*GPIO_Input)(int dev, unsigned int *pstatus);	
	//---------------------------------------------------------------------------
	//自定义函数接口
	//data数组，至少长度2
	//data[0]表示数组的有效长度
	//data[1]表示数组的功能
	//返回1表示操作成功，返回0表示操作失败
	int(*GSL_Function)(int dev, unsigned int *data);
	int(*SPI_R_GSL_FLASH)(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int);
	int(*SPI_W_GSL_FLASH)(int dev, unsigned char spi_ss, unsigned int addr, unsigned int data, unsigned char *buf, int len_int);
	int(*ENABLE_W_FLASH)(int dev, unsigned char spi_ss, unsigned int addr);
	int(*CS_ENABLE_FLASH)(int dev, unsigned char spi_ss);

} CInterface;

#else

typedef struct   
{
	int(*ScanDevice)(int* devnum);
	int(*FindOpen)(int dev, int spimode, int spi_speed_set,bool flag);//flag,保留.返回设备数量
	int(*DeviceClose)(int dev);
	int(*DeviceInfo)(int dev, char **str);//字符串，转接板信息
//---------------------------------------------------------------------------
	int size_iic;
	int (*IIC_ReadBase )(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);//上层不会调用
	int (*IIC_WriteBase)(int dev,unsigned char iic_addr,unsigned char *buf_char,int len_char);//上层不会调用
//要求下面函数读写长度无限
	int (*IIC_Read )(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
	//读TP的fc	IIC_Read(0,0x40,0xfc,buf,4);
	int (*IIC_Write)(int dev,unsigned char iic_addr,unsigned char offset,unsigned char *buf_char,int len_char);
	
	int (*IIC_R_GSL)(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);
	//读TP的ff080010	IIC_R_GSL(0,0x40,0xff080010,buf,1);
	//读0x7页	IIC_R_GSL(0,0x40,0x7*0x80,buf,32);
	int (*IIC_W_GSL)(int dev,unsigned char iic_addr,unsigned int addr,unsigned int *buf,int len_int);
//---------------------------------------------------------------------------
	int size_spi;
	int (*SPI_WR   )(int dev,unsigned char spi_ss,unsigned char *w_char,unsigned char*r_char,int len_char);//上层不会调用

	int (*SPI_Read )(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);
	int (*SPI_Write)(int dev,unsigned char spi_ss,unsigned char offset,unsigned char *buf_char,int len_char);

	int (*SPI_R_GSL)(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int);
	//读指纹160*160图像		SPI_R_GSL(0,0,0,buf,160*160/4);
	int (*SPI_W_GSL)(int dev,unsigned char spi_ss,unsigned int addr,unsigned int *buf,int len_int);
//---------------------------------------------------------------------------
	int (*GPIO_Output)(int dev,unsigned int data);//RST拉高拉低，data取值只有0或1

	int(*GPIO_Input)(int dev, unsigned int *status);

	int(*Set_Avdd)(int dev);

//	int (*Read_IntData)(int dev, int retdata);

} CInterface;
#endif

#endif // !defined(AFX_INTERFACE_H__6C064248_1F1C_4E40_844C_5F99E62E9C6D__INCLUDED_)
