void write_flash_title(int dev);
int Flash_Test(int dev);
void Flash_Erase(int dev);
void Flash_write(int dev, unsigned int addr, unsigned char *buf, int len);
void Flash_read(int dev, unsigned int addr, unsigned char *buf, int len);
int Write_DeadPixel(int dev, unsigned char *imageout, int w, int h);
void Read_FlashId(int dev, unsigned char *buf);