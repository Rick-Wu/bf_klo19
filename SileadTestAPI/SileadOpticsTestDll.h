
#ifndef _H_SILEADRESTAPI_20180711_H_ 
#define _H_SILEADRESTAPI_20180711_H_

struct gx_mtf_info
{
	float mtf_data[5];   //保存mtf的值
	float mtf_mag;       //保存计算出来的黑条纹的长度
	float mtf_mag_max;   //保存最大的黑条纹的长度
	int mtf_cnt;         //保存总的黑条纹的个数，理论值应该小于200
};

struct gx_mtf_offset_info
{
	float mtf_data[5];   //保存mtf的值
	float mtf_mag;       //保存计算出来的黑条纹的长度
	float mtf_mag_max;   //保存最大的黑条纹的长度
	int mtf_cnt;         //保存总的黑条纹的个数，理论值应该小于200
};


struct gx_badpoint_info
{
	int all_deadpoint; //总点数
	int one_deadpoint; //一个连续死点组数
	int two_deadpoint; //两个连续死点组数
	int more_deadpoint; //三个连续死点以及以上的组数
	int max_deadpoint; //最大的连续死点的个数

	int center_deadpoint;  //中间区域
	int edge_deadpoint;    //边缘区域
};

#ifdef __cplusplus
extern "C"{
#endif

	//光心测试，测试光学图像的中心点与实际物理sensor中心点的距离
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//glass_wdith,glass_height--光学中心点坐标
	//glass_distance--光学图像的中心点与实际物理sensor中心点的距离
	_declspec(dllexport) void gx_center(unsigned char *Image_in, int sensor_wdith, int sensor_height, int *glass_wdith,
		int *glass_height, float *glass_distance);



	//坏点测试
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//nearArea，计算坏点领域的大小；
	//threshold，坏点的计算阈值； 
	//blocksize，中心区域的大小；
	//Imageout，用于保存死点的位置；
	//struct gx_badpoint_info *data用于缓存测试的信息；
	_declspec(dllexport) void gx_badpoint(unsigned char *Image_in, int sensor_wdith, int sensor_height, int nearArea, int threshold_mean,
		int threshold_ratio, int blocksize,
		unsigned char *Imageout, struct gx_badpoint_info *data);



	//mtf测试
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//struct gx_mtf_info *data 用于缓存mtf的信息（中，左上，右上，左下，右下）；
	_declspec(dllexport) void gx_mtf(unsigned char *Image_in, int sensor_wdith, int sensor_height, struct gx_mtf_info *data);



	//shading测试
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//shading-- 保存图像的shading值（中，左上，右上，左下，右下）
	_declspec(dllexport) void gx_shading(unsigned char *Image_in, int sensor_wdith, int sensor_height, float *shading);


	//光圈测试,格科算法
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	_declspec(dllexport) int gx_pkgAlgraw(unsigned char *Image_in, int sensor_wdith, int sensor_height, double threshold_sobel_, int limit_size_,
		int skip_, int code_, int code_white_, int code, int count, int grade_, int open_mode_, unsigned char *Image_out);



	//光圈测试，silead算法
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//threshold --光圈测试阈值
	_declspec(dllexport) int gx_checkcircle(unsigned char *Image_in, int sensor_wdith, int sensor_height, double threshold);


	//污点测试
	//Image_in -- 输入的图像
	//sensor_wdith，sensor_height --图像宽和高
	//gain =16,图像放大倍数，变相阈值
	//num_max，  5*5范围内超过阈值的数量 
	//riseData第一次低通滤波后测试大小范围
	//fallData第二次低通滤波后测试大小范围
	//image_out1第一次低通滤波后图像
	//image_out2第二次低通滤波后图像
	_declspec(dllexport) int gx_checkBlot(unsigned char *image, int w, int h, int gain, int num_max, int riseData, int fallData,
		unsigned char *image_out1, unsigned char *image_out2);


	//计算图像清晰度，主要用于华为条纹状chart，用于调焦
	_declspec(dllexport) double gx_calcFreqvalue(unsigned char *image, int width, int height);


	//snr测试算法
	//传入测试图像
	_declspec(dllexport) void gx_noisetest(unsigned char* pSpoofimg, int iWidth, int iHeight, float* noiseout, int iNumofimg);


	//FOV测试算法
	_declspec(dllexport) int gx_fov(unsigned char *bi, int w, int h, int thres_dark, int*oio, int *ojo);


	//对称性测试
	//pSpoofimg 测试图像
	//iWidth，iHeight  图像长宽
	//测试输出图像，用于记录位置
	//th_imean 图像对称性测试灰度差异，，建议拉出来设置
	//th_ird  暂时备用接口，建议拉出来设置
	//th_iNummax 允许超出灰度差的点的最大个数，，建议拉出来设置
	//超出灰度差的点的总个数

	//th_imean  暂时定为30，
	//th_iNummax 暂定为40，
	//判断条件为  如果iNumCount>th_iNummax, 则测试fail； 反之pass
	_declspec(dllexport) int gx_uniformity(unsigned char* pSpoofimg, int iWidth, int iHeight, unsigned char* outimg, int th_imean, int th_ird, int th_iNummax, int *iNumCount);


	//中心亮度测试
	//pSpoofimg 测试图像
	//iWidth，iHeight  图像长宽
	//maxbrightness 中心亮度   如果maxbrightness<250,则测试fail,测试时需要将取图曝光调到0x400
	_declspec(dllexport) int gx_maxbrightness(unsigned char *pSpoofimg, int iWidth, int iHeight, int *maxbrightness);


	//range竖条纹测试
	//pSpoofimg 测试图像
	//iWidth，iHeight  图像长宽
	//stripediff  图像列最大的列平均灰度差  如果stripediff>5，则测试fail
	_declspec(dllexport) int gx_stripe(unsigned char *pSpoofimg, int iWidth, int iHeight, int *stripediff);



	_declspec(dllexport) int gx_imageTestBf(unsigned char *pSpoofimg, int w, int h);


	_declspec(dllexport) void gx_mtf_offset(unsigned char *Image_in, int sensor_wdith, int sensor_height, struct gx_mtf_offset_info *data);

#ifdef __cplusplus
}
#endif

#endif