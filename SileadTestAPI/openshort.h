

struct testData
{
	/************** Add 20170308 START*****************/
	unsigned int Avdd_current_down;
	unsigned int Avdd_current_up;
	unsigned int Avdd_current_work;

	float Op_Avdd_vol;

	/************************* ADD  20170329 ****************************/
	double mosi_Res_down;     //mosi下拉电阻，单位KΩ
	double mosi_Res_up;    //mosi上拉电阻，单位KΩ

	double miso_Res_down;   //miso下拉电阻，单位KΩ
	double miso_Res_up;     //miso上拉电阻，单位KΩ

	double ss_Res_down;    //ss下拉电阻，单位KΩ
	double ss_Res_up;       //ss上拉电阻，单位KΩ

	double sck_Res_down;     //sck下拉电阻，单位KΩ
	double sck_Res_up;     //sck上拉电阻，单位KΩ

	double irq_Res_down;    //irq下拉电阻，单位KΩ
	double irq_Res_up;    //irq上拉电阻，单位KΩ

	double rst_Res_down;     //rst下拉电阻，单位KΩ
	double rst_Res_up;    //rst上拉电阻    单位KΩ
};

unsigned int OpenShortTest(int devices, struct testData *data, const char *filename);