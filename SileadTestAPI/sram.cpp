#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <windows.h>
#include "SileadCommLib.h"

#define MBIST_FLAG  0x80


static void FWriteLog(const char* fileName, char *logMsg)   //写入;
{
#if 1
	FILE *fLog;

#if 0//_UNICODE
	_wfopen_s(&fLog, fileName, TEXT("a+"));
#else
	fopen_s(&fLog, fileName, "a+");
#endif
	if (fLog == NULL)
	{
		return;
	}
#if 0 //_UNICODE
	fwprintf(fLog, TEXT("%s"), logMsg);
#else
	fprintf(fLog, "%s", logMsg);
#endif

	fflush(fLog);
	fclose(fLog);

#endif
}



//传出位置和个数
//point_buf:bad point pos (unsigned char *)malloc(w*h);
//badpointnum: total bad point
//centerbadpointnum: center bad point
//w: image width
//h: image height
//centerw: center area width
//centerh: center area height
int CheckSram(int devices,unsigned char *point_buf, int *badpointnum, int* centerbadpointnum, int w, int h, int centerw, int centerh)
{
	int reg_length = (h*w) / 4;
	unsigned int *w_buf = (unsigned int *)malloc(reg_length * 4);
	unsigned int *r_buf = (unsigned int *)malloc(reg_length * 4);
	int centerbeginh = (h - centerh) / 2;
	int centerbeginw = (w - centerw) / 2;
	*badpointnum = 0;
	*centerbadpointnum = 0;
	if (w_buf == NULL || r_buf == NULL || point_buf == NULL)
	{
		if (w_buf != NULL)
		{
			free(w_buf);
		}
		if (r_buf != NULL)
		{
			free(r_buf);
		}
		return -1;
	}
	memset(point_buf, 0, reg_length * 4);

	for (int num = 0; num < 1; num++)
	{
		//reset
		SL_ResetOutput(devices, 0);
		Sleep(10);
		SL_ResetOutput(devices, 1);
		Sleep(10);

		memset(w_buf, 0xa5a5a5a5, reg_length * 4);
		SL_SPI_W_GSL(devices, 0, 0x0, w_buf, reg_length);

		memset(w_buf, 0x5a5a5a5a, reg_length * 4);
		memset(r_buf, 0, reg_length);

		//连续写以后，连续读
		SL_SPI_W_GSL(devices, 0, 0x0, w_buf, reg_length);
		Sleep(50);
		SL_SPI_R_GSL(devices, 0, 0x0, r_buf, reg_length);

		for (int i = 0; i < reg_length; i++)
		{
			if (r_buf[i] != 0x5a5a5a5a)
			{
				if (((r_buf[i] & 0xe0) != 0x40) && (point_buf[i * 4] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4] = 1;
				}
				if (((r_buf[i] & 0xe000) != 0x4000) && (point_buf[i * 4 + 1] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 1] = 1;
				}
				if (((r_buf[i] & 0xe00000) != 0x400000) && (point_buf[i * 4 + 2] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 2] = 1;
				}
				if (((r_buf[i] & 0xe0000000) != 0x40000000) && (point_buf[i * 4 + 3] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 3] = 1;
				}
			}
		}
		//reset
		SL_ResetOutput(devices, 0);
		Sleep(10);
		SL_ResetOutput(devices, 1);
		Sleep(10);
		memset(w_buf, 0xa5a5a5a5, reg_length * 4);
		memset(r_buf, 0, reg_length * 4);
		//连续写以后，连续读
		SL_SPI_W_GSL(devices, 0, 0x0, w_buf, reg_length);
		Sleep(50);
		SL_SPI_R_GSL(devices, 0, 0x0, r_buf, reg_length);

		for (int i = 0; i < reg_length; i++)
		{
			if (r_buf[i] != 0xa5a5a5a5)
			{
				if (((r_buf[i] & 0xe0) != 0xa0) && (point_buf[i * 4] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4] = 1;
				}
				if (((r_buf[i] & 0xe000) != 0xa000) && (point_buf[i * 4 + 1] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 1] = 1;
				}
				if (((r_buf[i] & 0xe00000) != 0xa00000) && (point_buf[i * 4 + 2] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 2] = 1;
				}
				if (((r_buf[i] & 0xe0000000) != 0xa0000000) && (point_buf[i * 4 + 3] != 1))
				{
					*badpointnum += 1;
					point_buf[i * 4 + 3] = 1;
				}
			}
		}
	}
	//center bad point
	for (int centeri = centerbeginh; centeri < centerbeginh + centerh; centeri++)
	{
		for (int centerj = centerbeginw; centerj < centerbeginh + centerw; centerj++)
		{
			if (point_buf[centeri * w + centerj] == 1)
			{
				(*centerbadpointnum)++;
			}
		}
	}
	if (w_buf!=NULL)
		free(w_buf);
	w_buf = NULL;
	if (r_buf!=NULL)
		free(r_buf);
	r_buf = NULL;
	return 0;
}

static int RegCount(int devices, unsigned int(*of_address)[200], unsigned int *of_address_group, int sn, int st, unsigned int w_data, unsigned int c_data)
{
	for (int i = 0; i <= sn; i++)
	{
		unsigned int w_buf[100];
		for (int j = 0; j < of_address_group[i]; j++)
		{
			if (j % 2 == 0)
				w_buf[j] = w_data;
			else
				w_buf[j] = ~w_data;
		}
		SL_SPI_W_GSL(devices, 0, of_address[i][0], w_buf, of_address_group[i]);
	}

	for (int i = 0; i <= sn; i++)
	{
		unsigned int r_buf = 0;
		for (int j = 0; j < of_address_group[i]; j++)
		{
			SL_SPI_R_GSL(devices, 0, of_address[i][j], &r_buf, 1);
			if (j % 2 == 0)
			{
				if (r_buf != c_data)
				{
					char log_tt[100];
					sprintf_s(log_tt, "3--addr=0x%08x,data=0x%08x\n", of_address[i][j], r_buf);
					FWriteLog("./Log/sram.txt", log_tt);
					return 1;
				}
			}
			else
			{
				if (r_buf != ~c_data)
				{
					char log_tt[100];
					sprintf_s(log_tt, "3--addr=0x%08x,data=0x%08x\n", of_address[i][j], r_buf);
					FWriteLog("./Log/sram.txt", log_tt);
					return 1;
				}
			}
		}
	}
	return 0;
}

static int RegTest(int devices)
{
	int RegResult = 0;
	char *fileName1 = "./config/7012_default.h";
	char *fileName2 = "./config/7012_highval.h";
	char *fileName3 = "./config/7012_dislocation.h";

	SL_ResetOutput(devices, 0);
	Sleep(10);
	SL_ResetOutput(devices, 1);
	Sleep(10);

	remove("./Log/sram.txt");
	char *buf = (char*)malloc(sizeof(char)* 200);
#define MAXFILE_SISE 200
	int Error_code = 0;

	unsigned char wdata[4] = { 0 };//00008381
	wdata[3] = 0;
	wdata[2] = 0;
	wdata[1] = 0x83;
	wdata[0] = 0x81;
	SPI_Write4B(devices, 0xb0, wdata, 4);
	unsigned int temp_dd = 0x0001f303;
	SPI_WTotal_Address(devices, 0xff08004c, &temp_dd, 1);
	Sleep(50);

	FILE * fp;
	fopen_s(&fp, fileName1, "r");
	if (fp == NULL)
	{
		RegResult = 200;
		return 200;
	}

	bool file_state = TRUE;
	while (file_state)
	{
		char *ret = fgets(buf, MAXFILE_SISE, fp);
		if (ret == NULL)
		{
			file_state = FALSE;
		}
		UINT address, data11;
		char *stop;
		address = strtoul(buf, &stop, 16);
		data11 = strtoul(stop + 1, NULL, 16);

		if (((address & 0xff000000) == 0))
		{
			unsigned char data_r[4] = { 0 };
			SPI_Read4B(devices, address, data_r, 4);
			int temp = (data_r[3] << 24) + (data_r[2] << 16) + (data_r[1] << 8) + (data_r[0] << 0);
			if (temp != data11)
			{
				char log_tt[100];
				sprintf_s(log_tt, "0--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, temp, data11);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 0);
			}
		}
		else
		{
			unsigned int data_r;
			SPI_RTotal_Address(devices, address, &data_r, 1);
			if (data11 != data_r)
			{
				char log_tt[100];
				sprintf_s(log_tt, "0--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, data_r, data11);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 0);
			}
		}
	}
	fclose(fp);

	fopen_s(&fp, fileName2, "r");
	if (fp == NULL)
	{
		RegResult = 300;
		return 300;
	}

	file_state = TRUE;
	while (file_state)
	{
		char *ret = fgets(buf, MAXFILE_SISE, fp);
		if (ret == NULL)
		{
			file_state = FALSE;
		}
		UINT address, data11;
		char *stop;
		address = strtoul(buf, &stop, 16);
		data11 = strtoul(stop + 1, NULL, 16);

		if (((address & 0xff000000) == 0))
		{
			unsigned char data_w[4] = { 0x5a, 0x5a, 0x5a, 0x5a };
			unsigned char data_r[4] = { 0 };
			SPI_Write4B(devices, address, data_w, 4);
			//			sl_delay(10);
			SPI_Read4B(devices, address, data_r, 4);
			//			sl_delay(10);
			int temp = (data_r[3] << 24) + (data_r[2] << 16) + (data_r[1] << 8) + (data_r[0] << 0);
			int tt = data11 & 0x5a5a5a5a;
			if (tt != temp)
			{
				char log_tt[100];
				sprintf_s(log_tt, "1--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, temp, tt);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 1);
			}
		}
		else
		{
			unsigned int data_r;
			unsigned int data_w = 0x5a5a5a5a;
			SPI_WTotal_Address(devices, address, &data_w, 1);
			//			sl_delay(10);
			SPI_RTotal_Address(devices, address, &data_r, 1);
			//		sl_delay(10);
			int tt = data11 & 0x5a5a5a5a;
			if (tt != data_r)
			{
				char log_tt[100];
				sprintf_s(log_tt, "1--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, data_r, tt);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 1);
			}
		}
	}
	fclose(fp);

	fopen_s(&fp, fileName2, "r");
	if (fp == NULL)
	{
		RegResult= 400;
		return 400;
	}

	file_state = TRUE;
	while (file_state)
	{
		char *ret = fgets(buf, MAXFILE_SISE, fp);
		if (ret == NULL)
		{
			file_state = FALSE;
		}
		UINT address, data11;
		char *stop;
		address = strtoul(buf, &stop, 16);
		data11 = strtoul(stop + 1, NULL, 16);

		if (((address & 0xff000000) == 0))
		{
			unsigned char data_w[4] = { 0xa5, 0xa5, 0xa5, 0xa5 };
			unsigned char data_r[4] = { 0 };
			SPI_Write4B(devices, address, data_w, 4);
			//			sl_delay(10);
			SPI_Read4B(devices, address, data_r, 4);
			//			sl_delay(10);
			int temp = (data_r[3] << 24) + (data_r[2] << 16) + (data_r[1] << 8) + (data_r[0] << 0);
			int tt = data11 & 0xa5a5a5a5;
			if (tt != temp)
			{
				char log_tt[100];
				sprintf_s(log_tt, "2--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, temp, tt);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 2);
			}
		}
		else
		{
			unsigned int data_r;
			unsigned int data_w = 0xa5a5a5a5;
			SPI_WTotal_Address(devices, address, &data_w, 1);
			SPI_RTotal_Address(devices, address, &data_r, 1);
			int tt = data11 & 0xa5a5a5a5;
			if (tt != data_r)
			{
				char log_tt[100];
				sprintf_s(log_tt, "2--addr=0x%08x,data=0x%08x,default=0x%08x\n", address, data_r, tt);
				FWriteLog("./Log/sram.txt", log_tt);
				Error_code |= (0x1 << 2);
			}
		}
	}
	fclose(fp);

#if 1
	fopen_s(&fp, fileName3, "r");
	if (fp == NULL)
	{
		RegResult = 500;
		return 500;
	}
	unsigned int of_address[10][200] = { 0 };
	unsigned int of_address_group[10] = { 0 };
	int st = 0;//个数
	int sn = 0;//组数
	int ss = 0;//总个数
	file_state = TRUE;
	while (file_state)
	{
		char *ret = fgets(buf, MAXFILE_SISE, fp);
		if (ret == NULL)
		{
			file_state = FALSE;
		}
		UINT address, data22;
		char *stop;
		address = strtoul(buf, &stop, 16);
		//		data22 = strtoul(stop + 1, NULL, 16);
		if (ss == 0)
		{
			of_address[sn][st] = address;
			ss++;
			of_address_group[sn] = ss;
			continue;
		}
		if (address == (of_address[sn][st] + 0x4))
		{
			st++;
			of_address[sn][st] = address;
			of_address_group[sn] = st + 1;
		}
		else
		{
			st = 0;
			sn++;
			if (sn >= 9)
				sn = 9;
			of_address[sn][st] = address;
		}
		ss++;
	}
	fclose(fp);
	int ret = RegCount(devices, of_address, of_address_group, sn, st, 0x55555555, 0x55555555);
	if (ret != 0)
	{
		Error_code |= (0x1 << 3);
	}
	ret = RegCount(devices, of_address, of_address_group, sn, st, 0xaaaaaaaa, 0xaaaaaaaa);
	if (ret != 0)
	{
		Error_code |= (0x1 << 4);
	}

#endif
	RegResult = Error_code;
	return Error_code;
}

int MRegtest(int devices)
{
	unsigned int num0 = 0;
	int mbistResult = 0;
	SL_ResetOutput(devices, 0);
	Sleep(10);
	SL_ResetOutput(devices, 1);
	Sleep(10);

	unsigned char buf[4] = { 0 };
	buf[3] = 0;
	buf[2] = 0;
	buf[1] = 0;
	buf[0] = 0x1;
	SPI_Write4B(devices, 0x80, buf, 4);

	Sleep(30);
	SPI_Read4B(devices, 0xC0, buf, 4);
	num0 = (buf[3] << 24) + (buf[2] << 16) + (buf[1] << 8) + (buf[0] << 0);
	num0 = num0 & 0xff;
	if (num0 == MBIST_FLAG)
	{
		mbistResult = 0;
	}
	else
	{
		mbistResult = 1;
	}
	buf[3] = 0;
	buf[2] = 0;
	buf[1] = 0;
	buf[0] = 0;
	SPI_Write4B(devices, 0x80, buf, 4);
	Sleep(100);

	int ret = RegTest(devices);
	return (mbistResult << 16) | ret;
}









