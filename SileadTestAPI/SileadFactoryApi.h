#ifndef _H_SILEADFACTORYAPI_H_
#define _H_SILEADFACTORYAPI_H_

#define BI_RGB              0L
#define BI_RLE8             1L
#define BI_RLE4             2L
#define BMP_TYPE            0x4d42
#define WIDTHBYTES(bits)    (((bits)+31)/32*4)
#define MIN(x,y)            (x < y ? x : y)

#pragma   pack(1)  // 让编译器按1字节对齐

typedef struct _BITMAPFILEHEADER_
{

	unsigned short		bfType;
	unsigned long		bfSize;
	unsigned short		bfReserved1;
	unsigned short		bfReserved2;
	unsigned long		bfOffBits;

} WL_BITMAPFILEHEADER, *LPWL_BITMAPFILEHEADER;

typedef struct _BITMAPINFOHEADER_
{

	unsigned long		biSize;
	long				biWidth;
	long				biHeight;
	unsigned short      biPlanes;
	unsigned short      biBitCount;
	unsigned long		biCompression;
	unsigned long		biSizeImage;
	long				biXPelsPerMeter;
	long				biYPelsPerMeter;
	unsigned long		biClrUsed;
	unsigned long		biClrImportant;

} WL_BITMAPINFOHEADER, *LPWL_BITMAPINFOHEADER;

typedef struct _RGBQUAD_
{

	unsigned char    rgbBlue;
	unsigned char    rgbGreen;
	unsigned char    rgbRed;
	unsigned char    rgbReserved;

} WL_RGBQUAD, *LPWL_RGBQUAD;


#pragma   pack()  // 还原
typedef struct
{
	unsigned int gain_actual_set;
	unsigned int gain_k_preset;
	unsigned int top_target_preset;
	unsigned int top_zero_preset;
	unsigned int dc_zero_preset;
	float k_preset;
	unsigned int dac;
}DCDATAPARM;

typedef struct
{
	unsigned int mean_percent_low;    //mean
	unsigned int mean_percent_high;   //mean
	unsigned int diff_low;
	unsigned int diff_high;
	int DeadP_5;
	int DeadP_20;

	int sum_mean_one;               //第一副图灰度和
	int sum_mean_two;               //第二幅图灰度和
	int avr_mean_one;               //第一副图灰度均值
	int avr_mean_two;               //第二副图灰度均值

	int w;                 //图像宽
	int h;                 //图像高

	unsigned char *Onebuffer;       //第一幅图像数据
	unsigned char *Twobuffer;       //第二幅图像数据

	unsigned char *IamgeBuffer;     //用于保存一帧图像
	unsigned char *p;               //用于保存平滑度测试数据
	int mean;                       //图像灰度

	int DeteV_20;
	int DeteV_m;
	int Dete_mean;
	int DeteV_hh;                 //横向坏线
	int DeteV_ww;                 //竖向坏线
	unsigned int Deteline_w;
	unsigned int Deteline_h;
	unsigned short *Deteposition;
	unsigned int *DetelineNum;
	unsigned int DeteW_Num;        //竖向允许的坏线个数
	unsigned int DeteH_Num;        //横向允许的坏线个数

	unsigned char **Inprebuffer;    //存放多帧图像的二维数组，用于图像排序
	int Aver_Num;          //图像的帧数
	int Aver_f;
	int Aver_b;

	int Becell;                       //气泡测试
	int BubbleValue;       //气泡阈值
	int scut;
	unsigned char *Input_Buffer;
	unsigned char *Output_Buffer;
	int mid_val;
	int edge_val;
	int *size;     //气泡面积
	int *n;     //气泡个数
	int Mean_Bu; // 气泡图像的灰度均值

	int up, down, left, right;

	//int mean_aver0, mean_aver1, mean_aver2, mean_aver3, mean_aver4, mean_aver5, mean_aver6, mean_aver7;
	unsigned char *mean_aver;

	int Diff_unit;

	int line_count;     //保存坏线的个数
	unsigned short * line_location;  //保存坏线的位置
	unsigned char * img;
	int remove_dead_line_type;
	unsigned int DeadValno;

}MEANPERCENT;

typedef struct
{
	unsigned int gain_actual_set_image1;
	unsigned int gain_actual_set_image2;
}GAINSET;

#ifdef __cplusplus
extern "C"{
#endif

#define SPIOPERATION
#define SILEADAPI     _declspec(dllexport)

	/*划痕检测  DetectLine
	Img.IamgeBuffer                     进行划痕测试的图像
	Img.w = sCutWidth;                  图像的宽
	Img.h = sCutHeight;                 图像的高
	Img.DeteValue                       划痕测试的阈值
	返回结果：非0为此项测试不过*/
	SILEADAPI bool DetectLineScan(MEANPERCENT a);
	SILEADAPI bool DetectLineTest(MEANPERCENT a);

	/*死点检测 RuinpixelTest_one
	Img.mean_percent_high               像素点灰度跟整屏灰度的差的上限（百分比）
	Img.mean_percent_low                像素点灰度跟整屏灰度的差的下限（百分比）
	Img.diff_high                       像素点灰度变化跟整屏灰度变化的差的上限（百分比）
	Img.diff_low                        像素点灰度变化跟整屏灰度变化的差的下限（百分比）
	Img.w                               图像的宽
	Img.h                               图像的高
	Img.avr_mean_one                    保存用于死点检测灰度比较大的图像的整体灰度的均值
	Img.avr_mean_two                    保存用于死点检测会都比较小的图像的整体灰度的均值（必须保证avr_mean_one > avr_mean_two
	Img.Onebuffer                       灰度比较大的图像
	Img.Twobuffer                       灰度比较小的图像
	返回结果：死点检测个数 */
	SILEADAPI int RuinpixelTest_one(MEANPERCENT a);

	/*死点检测 RuinpixelTest_one
	Img.mean_percent_high               像素点灰度跟整屏灰度的差的上限（百分比）
	Img.mean_percent_low                像素点灰度跟整屏灰度的差的下限（百分比）
	Img.diff_high                       像素点灰度变化跟整屏灰度变化的差的上限（百分比）
	Img.diff_low                        像素点灰度变化跟整屏灰度变化的差的下限（百分比）
	Img.w                               图像的宽
	Img.h                               图像的高
	Img.avr_mean_one                    保存用于死点检测灰度比较小的图像的整体灰度的均值
	Img.avr_mean_two                    保存用于死点检测会都比较大的图像的整体灰度的均值（必须保证avr_mean_one < avr_mean_two
	Img.Onebuffer                       灰度比较大的图像
	Img.Twobuffer                       灰度比较小的图像
	返回结果：死点检测个数 */
	SILEADAPI int RuinpixelTest_two(MEANPERCENT a);

	/*灵敏度检测
	a.avr_mean_one                 图像的整体均值
	a.avr_mean_two                 图像的整体均值
	返回结果：avr_mean_one - avr_mean_two,即灵敏度大小           */
	SILEADAPI int SensitiveTest(MEANPERCENT a);

	/*图像一致性检测
	Img.IamgeBuffer				待检测的图像
	Img.DiffUintValue			算法阈值
	Img.w						图像的宽
	Img.h						图像的高
	*/
	SILEADAPI void UnitTest(MEANPERCENT a);

	/*图像裁剪
	参数1：inputbuf原始图像数据，大小为h * w
	参数2：putputbuf预处理后图像数据，大小为（w - sRight） * (h - sDown)
	参数3，4：原始图像长和宽
	参数5，6：原始图像要裁掉的像素 6182芯片该值为0  */
	SILEADAPI void ArangeBuffer(MEANPERCENT a);
	SILEADAPI void Cut(MEANPERCENT a);    //空函数，没有做任何处理，跟ArangeBuffer输入参数相同

	/*排序算法 PixelArrayAverage
	Img.Inprebuffer               二维数组保存多帧图像的数据
	Img.IamgeBuffer               保存进过排序去大小之后图像的数据，是一维数组
	Img.w                         图像的宽
	Img.h                         图像的高
	Img.Aver_Num                  参与排序的图像个数
	Img.Aver_f                    灰度均值最大的图像数
	Img.Aver_b                    灰度均值最小的图像数   */
	SILEADAPI int PixelArrayAverage(MEANPERCENT a);


	/*差值算法
	参数1:采集到的原始图像
	参数2:采集到的原始图像的宽
	参数3:采集到的原始图像的高
	参数4:对于6182的芯片该值为0 */
	SILEADAPI void SileadfpInsert_618285(MEANPERCENT a);
	SILEADAPI void SileadfpInsert61XX(MEANPERCENT a);  //空函数，没有做任何处理跟SileadfpInsert_618285输入参数相同

	/*增益算法
	参数1:增益调试需要的相关参数，放在结构体中
	参数2:寄存器ff08000c的值
	参数3:寄存器ff000020的值
	参数4:寄存器ff00002c的值*/
	SILEADAPI int DcCalc(DCDATAPARM dc, unsigned int* value0c, unsigned int* value20, unsigned int* value2c);

	/*切边算法
	Img.Onebuffer                 进行切边的图像
	Img.Twobuffer = pbuffer;      切边后的图像
	Img.w                         进行切边的图像的宽
	Img.h                         进行切边的图像的高
	Img.scut = sCut;              切边的大小             */
	SILEADAPI  void sArangeBuffer(MEANPERCENT a);

	/* 均值算法
	Img.IamgeBuffer               需要求均值的图像
	Img.w                         图像的宽
	Img.h                         图像的高
	返回值：该图像的整体均值        */
	SILEADAPI int Imageaverage(MEANPERCENT a);

	/* 均值算法
	a.IamgeBuffer               需要求均值的图像
	a.w                         图像的宽
	a.h                         图像的高
	返回值：周期为8的8个列平均       */
	SILEADAPI int DetectLineAver(MEANPERCENT a);
	/*保存图像
	参数1:需要保存的图片目录和名称 比如：.//Image.bmp
	参数2:图像的数据
	参数3:图像的高
	参数4:图像的宽 */
	SILEADAPI void GFP_Write_Bmp_8(const char filename[], unsigned char *ucImg, int h, int w);

	/*加载图像
	参数1:需要录入的图像的路径
	参数2:图像的数据
	参数3:图像的高
	参数4:图像的宽 */
	SILEADAPI void BubbleTest_Avg(MEANPERCENT a);
	SILEADAPI int gsl_remove_dead_line_hardware(MEANPERCENT a);
	SILEADAPI void FingerBLKTest_sl(unsigned char* img, int width, int height, int* pResult, int ExpSize);
	SILEADAPI bool HardwareDetectLineTest(MEANPERCENT a);
	SILEADAPI int HardwareDeadTest(MEANPERCENT a, unsigned int *max);

	SILEADAPI int GFPLoadImage(const char filename[], unsigned char *ucImage, int *h, int *w);
#ifdef __cplusplus
}
#endif


#endif
