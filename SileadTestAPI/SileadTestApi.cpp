﻿#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <windows.h>
#include <direct.h>
#include <io.h>
#include "SileadCommLib.h"
#include "SileadTestApi.h"
#include "flash_readwrite.h" 
#include "sram.h"
#include "openshort.h"
#include "SileadFactoryApi.h"
#include "SileadOpticsTestDll.h"
#include "winbase.h"


#define random(x) (rand()%x)
#define IMAGE_WIDTH 320
#define IMAGE_HEIGHT 320
#define SENSORID_LENGTH 40
#define TESTSTRING_LENGTH 2000
#define TESTRESULT_LENGTH 100

#define BRIGHTSOURCE_MEAN  30
#define DARKSOURCE_MEAN   20

#define IFIAMGENUM  5

//struct testdata *data_t[DEVICESMAX];

static void sl_delay(int ms_delay);
int SPI_WTotal_Address_n(int dev, unsigned int addr, unsigned int *buf, int len_int, int n);

struct gx_basedata
{
	int width_org;
	int height_org;
	int width_msd;
	int height_msd;
}sparam;



struct gx_settingData
{
	char openshort_filename[300];
	int speed;
	int center_block_w;
	int center_block_h;
	int deadpoint_total;
	int deadpoint_center;
	int focus_max;
	int focus_min;
	int focus_target_mean;
	int shading_threshold;
	int shadingdiff;
	int mtf_threshold;
	int mtf_threshold_edge_ud;
	int mtf_threshold_edge_lr;
	float mtf_mag_min;
	float mtf_mag_max;
	int lg_center;
	int center_deadpoint;
	int edge_deadpoint;
	int deadpoint_threshold_mean;
	int deadpoint_threshold_ratio;
	double threshold_sobel_;
	int limit_size_;
	int skip_;
	int code_;
	int code_white_;
	int code;
	int count;
	int grade_;
	int open_mode_;
	int target_mean_half;
	int expo_min;
	int expo_max;
	int target_mean220;
	int gain;
	int num_max;
	int blotData_min;
	int blotData_max;
	int aperture;
	int pkgmean;
	int linearchangemean;
	int linearImageW;
	int linearImageH;
	int expotimeMax;
	int expotimeMin;
	int Freqvalue;
	int darknoise_max;
	int darknoise_min;
	int brightnoise_max;
	int brightnoise_min;
	int fov_max;
	int fov_min;
	int maxbrightness;
	int stripediff;
	int th_imean;
	int th_ird;
	int th_iNummax;
	int blotnummax;

}setting;


static void sl_delay(int time)
{
	LARGE_INTEGER time_freq_sl, time_start_sl;
	LARGE_INTEGER time_end;// , time_e2;
	QueryPerformanceCounter(&time_start_sl);//获得整机基准时钟计数
	QueryPerformanceFrequency(&time_freq_sl);//获得整机基准频率
	do
	QueryPerformanceCounter(&time_end);
	while (((time_end.QuadPart - time_start_sl.QuadPart) * 1000 * 1000 / time_freq_sl.QuadPart) < time * 1000);
}

static void FWriteLog(const char* fileName, char *logMsg)   //写入;
{
#if 1
	FILE *fLog;

#if 0//_UNICODE
	_wfopen_s(&fLog, fileName, TEXT("a+"));
#else
	fopen_s(&fLog, fileName, "a+");
#endif
	if (fLog == NULL)
	{
		return;
	}
#if 0 //_UNICODE
	fwprintf(fLog, TEXT("%s"), logMsg);
#else
	fprintf(fLog, "%s", logMsg);
#endif

	fflush(fLog);
	fclose(fLog);

#endif
}


int gx_opendevices(int devices)
{
	int ret=SL_DevicesInit(setting.speed);
	Sleep(50);
	SL_ResetSTM32(devices);
//	SL_SetVddHigh(devices, 0);
	Sleep(50);
	return ret;
}

void gx_malloc(struct testdatainfo *data)
{
	data->image = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	data->image_out = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	data->imageDarknoise = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);
	data->imageBrightnoise = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);
	data->sensorid = (char*)malloc(SENSORID_LENGTH);

	memset(data->sensorid, 0, SENSORID_LENGTH);
	memset(data->testString, 0, TESTSTRING_LENGTH);
}

void gx_free(struct testdatainfo *data)
{
	if (data->image != NULL)
		free(data->image);
	data->image = NULL;

	if (data->image_out != NULL)
		free(data->image_out);
	data->image_out = NULL;

	if (data->imageDarknoise != NULL)
		free(data->imageDarknoise);
	data->imageDarknoise = NULL;

	if (data->imageBrightnoise != NULL)
		free(data->imageBrightnoise);
	data->imageBrightnoise = NULL;

	if (data->sensorid != NULL)
		free(data->sensorid);
	data->sensorid = NULL;

}

 
void gx_initdata(int devices, char *filename)
{
// 	char log_tt[100];
// 	sprintf_s(log_tt, "D://Silead//SettingData.ini");
// 	char filename[100];
// 	memset(filename, 0, 100);
// 	memcpy(filename, log_tt, strlen(log_tt));

//	setting.openfilename = filename;
	
	sparam.width_org = GetPrivateProfileIntA("Cfg", "width", 160, filename);
	sparam.height_org = GetPrivateProfileIntA("Cfg", "height", 160, filename);
	sparam.width_msd = GetPrivateProfileIntA("Cfg", "width_msd", 160, filename);
	sparam.height_msd = GetPrivateProfileIntA("Cfg", "height_msd", 160, filename);
	setting.center_block_w = GetPrivateProfileIntA("Cfg", "center_block_w", 70, filename);
	setting.center_block_h = GetPrivateProfileIntA("Cfg", "center_block_h", 70, filename);
	setting.deadpoint_total = GetPrivateProfileIntA("Cfg", "deadpoint_total", 40, filename);
	setting.deadpoint_center = GetPrivateProfileIntA("Cfg", "deadpoint_center", 10, filename);
	setting.mtf_threshold = GetPrivateProfileIntA("Cfg", "mtf_threshold", 65, filename);
	setting.mtf_threshold_edge_ud = GetPrivateProfileIntA("Cfg", "mtf_threshold_edge_ud", 40, filename);
	setting.mtf_threshold_edge_lr = GetPrivateProfileIntA("Cfg", "mtf_threshold_edge_lr", 40, filename);
	setting.shading_threshold = GetPrivateProfileIntA("Cfg", "shading_threshold", 10, filename);
	setting.shadingdiff = GetPrivateProfileIntA("Cfg", "shadingdiff", 12, filename);
	setting.center_deadpoint = GetPrivateProfileIntA("Cfg", "center_deadpoint", 10, filename);
	setting.edge_deadpoint = GetPrivateProfileIntA("Cfg", "edge_deadpoint", 10, filename);
	setting.deadpoint_threshold_mean = GetPrivateProfileIntA("Cfg", "deadpoint_threshold_mean", 15, filename);
	setting.deadpoint_threshold_ratio = GetPrivateProfileIntA("Cfg", "deadpoint_threshold_ratio", 40, filename);
	setting.lg_center = GetPrivateProfileIntA("Cfg", "lg_center", 8, filename);

	setting.threshold_sobel_ = GetPrivateProfileIntA("Cfg", "threshold_sobel_", 37, filename);
	setting.limit_size_ = GetPrivateProfileIntA("Cfg", "limit_size_", 5, filename);
	setting.skip_ = GetPrivateProfileIntA("Cfg", "skip_", 7, filename);
	setting.code_ = GetPrivateProfileIntA("Cfg", "code_", 123, filename);
	setting.code_white_ = GetPrivateProfileIntA("Cfg", "code_white_", 173, filename);
	setting.code = GetPrivateProfileIntA("Cfg", "code", 127, filename);
	setting.count = GetPrivateProfileIntA("Cfg", "count", 20, filename);
	setting.grade_ = GetPrivateProfileIntA("Cfg", "grade_", 5, filename);
	setting.open_mode_ = GetPrivateProfileIntA("Cfg", "open_mode_", 1, filename);

	setting.target_mean_half = GetPrivateProfileIntA("Cfg", "target_mean_half", 165, filename);
	setting.expo_min = GetPrivateProfileIntA("Cfg", "expo_min", 20, filename);
	setting.expo_max = GetPrivateProfileIntA("Cfg", "expo_max", 400, filename);
	setting.target_mean220 = GetPrivateProfileIntA("Cfg", "target_mean220", 220, filename);

	setting.gain = GetPrivateProfileIntA("Cfg", "gain", 16, filename);
	setting.num_max = GetPrivateProfileIntA("Cfg", "num_max", 8, filename);
	setting.blotData_min = GetPrivateProfileIntA("Cfg", "blotData_min", 50, filename);
	setting.blotData_max = GetPrivateProfileIntA("Cfg", "blotData_max", 100, filename);

	setting.aperture = GetPrivateProfileIntA("Cfg", "aperture", 500, filename);
	setting.pkgmean = GetPrivateProfileIntA("Cfg", "pkgmean", 100, filename);

	setting.linearchangemean = GetPrivateProfileIntA("Cfg", "linearchangemean", 186, filename);
	setting.linearImageH = GetPrivateProfileIntA("Cfg", "linearImageH", 20, filename);
	setting.linearImageW = GetPrivateProfileIntA("Cfg", "linearImageW", 20, filename);

	setting.expotimeMax = GetPrivateProfileIntA("Cfg", "expotimeMax", 100, filename);
	setting.expotimeMin = GetPrivateProfileIntA("Cfg", "expotimeMin", 10, filename);

	setting.Freqvalue = GetPrivateProfileIntA("Cfg", "Freqvalue", 50, filename);


	setting.darknoise_max = GetPrivateProfileIntA("Cfg", "DarkNoise_max", 2, filename);
	setting.darknoise_min = GetPrivateProfileIntA("Cfg", "DarkNoise_min", 0, filename);
	
	setting.brightnoise_max = GetPrivateProfileIntA("Cfg", "BrightNoise_max", 20, filename);
	setting.brightnoise_min = GetPrivateProfileIntA("Cfg", "BrightNoise_min", 5, filename);


	setting.fov_max = GetPrivateProfileIntA("Cfg", "fov_max", 120, filename);
	setting.fov_min = GetPrivateProfileIntA("Cfg", "fov_min", 90, filename);

	setting.maxbrightness = GetPrivateProfileIntA("Cfg", "maxbrightness", 250, filename);
	setting.stripediff = GetPrivateProfileIntA("Cfg", "stripediff", 10, filename);
	setting.th_imean = GetPrivateProfileIntA("Cfg", "th_imean", 30, filename);
	setting.th_ird = GetPrivateProfileIntA("Cfg", "th_ird", 10, filename);
	setting.th_iNummax = GetPrivateProfileIntA("Cfg", "th_iNummax", 40, filename);
	setting.blotnummax = GetPrivateProfileIntA("Cfg", "blotnummax", 8, filename);

	setting.speed = GetPrivateProfileIntA("Cfg", "speed", 2, filename);


	char data[20] = { 0 };
	GetPrivateProfileStringA("Cfg", "mtf_mag_max", "8.5", data, 18, filename);
	setting.mtf_mag_max = atof(data);
	GetPrivateProfileStringA("Cfg", "mtf_mag_min", "4.5", data, 18, filename);
	setting.mtf_mag_min = atof(data);


// 	char log_str[50];
// 	sprintf_s(log_str, "speed=%d\r\n", setting.speed);
// 	FWriteLog("./openshort_test.txt", log_str);

	memset(setting.openshort_filename, 0, 300);
	memcpy(setting.openshort_filename, filename, strlen(filename));
}


// static void sl_delay(int ms_delay)
// {
// 	LARGE_INTEGER time_freq_sl, time_start_sl;
// 	LARGE_INTEGER time_end;// , time_e2;
// 	QueryPerformanceCounter(&time_start_sl);//获得整机基准时钟计数
// 	QueryPerformanceFrequency(&time_freq_sl);//获得整机基准频率
// 	do
// 	QueryPerformanceCounter(&time_end);
// 	while (((time_end.QuadPart - time_start_sl.QuadPart) * 1000 * 1000 / time_freq_sl.QuadPart) < ms_delay * 1000);
// }

static int Read_bf(int devindex)
{
#define  DELAYTIME    5
	UINT temp = 0;
	unsigned char buf[4] = { 0 };
	int readnum = 0;
	SPI_Read4B(devindex, 0xbf, buf, 1);
	SPI_Read4B(devindex, 0xbf, buf, 1);
	while (((buf[0] & 0x1) != 1) && (readnum < 30))
	{
		readnum++;
		sl_delay(DELAYTIME);
		SPI_Read4B(devindex, 0xbf, buf, 4);
	}
	return readnum * DELAYTIME;
}


static void Scan_chip(int devindex)
{
	unsigned char buf[4] = { 0 };
	SPI_Write4B(devindex, 0xbf, buf, 4);
	UINT tmpvalue24 = 1;
	SPI_WTotal_Address(devindex, 0xff09013c, &tmpvalue24, 1);
	SPI_WTotal_Address(devindex, 0xff09013c, &tmpvalue24, 1);
	SPI_WTotal_Address(devindex, 0xff09013c, &tmpvalue24, 1);
}


int gx_loadmain(int devices, char *filename)
{
	SL_ResetOutput(devices, 0);
	sl_delay(30);
	SL_ResetOutput(devices, 1);
	sl_delay(30);
	int load = SL_LoadConfig(devices, filename);
	sl_delay(200);
	return load;
}


int gx_getimage(int devices, int *image, int w, int h)
{
	return SL_GetImagebuffer(devices, image, w, h);
}

#if 1
int gx_lightbadpoint_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(w*h);
	unsigned char *imageout = (unsigned char *)malloc(w*h);
	memset(image, 0, w*h);
	memset(imageout, 0, w*h);

	SL_GetImagebuffer(devices, image_s, w, h);

	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);

	unsigned int num = 0;
// 	struct deadpoint_t *badpoint_t = (struct deadpoint_t *)malloc(sizeof(struct deadpoint_t));
// 	SL_deadpoint(image, sparam.width_org, sparam.height_org, 25, 50, &num, imageout, badpoint_t);

	struct gx_badpoint_info *badpoint_t = (struct gx_badpoint_info *)malloc(sizeof(struct gx_badpoint_info));
	gx_badpoint(image, w, h, 5, setting.deadpoint_threshold_mean, setting.deadpoint_threshold_ratio, 120, imageout, badpoint_t);
	memcpy(data->image_out, imageout, w*h);

	data->all_lightdeadpoint = badpoint_t->all_deadpoint; //总点数
	data->max_lightdeadpoint = badpoint_t->max_deadpoint; //最大的连续死点的面积
	data->center_lightdeadpoint = badpoint_t->center_deadpoint;
	data->edge_lightdeadpoint = badpoint_t->edge_deadpoint;

	int light_sum = 0;
	int light_num = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	if (image_s != NULL)
		free(image_s);
	image_s = NULL;

	if (image!=NULL)
		free(image);
	image = NULL;

	if (imageout != NULL)
		free(imageout);
	imageout = NULL;

	if (badpoint_t != NULL)
		free(badpoint_t);
	badpoint_t = NULL;

	int ret = 0;
	if ((data->center_lightdeadpoint > setting.center_deadpoint) || (data->edge_lightdeadpoint > setting.edge_deadpoint) 
		|| (mean_result != 0) || (data->max_lightdeadpoint>=2))
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "Lightbadpoint=Fail_%d_%d_%d|", data->center_lightdeadpoint, data->edge_lightdeadpoint, data->max_lightdeadpoint);
	else
		sprintf_s(Log_str, "Lightbadpoint=Pass_%d_%d_%d|", data->center_lightdeadpoint, data->edge_lightdeadpoint, data->max_lightdeadpoint);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 8;
		sprintf_s(Log_str, "_8");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}


int gx_darkbadpoint_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(w*h);
	unsigned char *imageout = (unsigned char *)malloc(w*h);
	memset(image, 0, w*h);
	memset(imageout, 0, w*h);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);
	unsigned int num = 0;
	struct gx_badpoint_info *badpoint_t = (struct gx_badpoint_info *)malloc(sizeof(struct gx_badpoint_info));
	gx_badpoint(image, w, h, 5, setting.deadpoint_threshold_mean, 110, 120, imageout, badpoint_t);
	memcpy(data->image_out, imageout, w*h);

	data->all_darkdeadpoint = badpoint_t->all_deadpoint; //总点数
	data->max_darkdeadpoint = badpoint_t->max_deadpoint; //最大的连续死点的面积
	data->center_darkdeadpoint = badpoint_t->center_deadpoint;
	data->edge_darkdeadpoint = badpoint_t->edge_deadpoint;

	int dark_sum = 0;
	int dark_num = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			dark_sum += data->image[i + j*w];
			dark_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (dark_num != 0)
		mean = dark_sum / dark_num;
	else
		mean = 255;
	if (mean > DARKSOURCE_MEAN)
		mean_result = 1;

	if (image != NULL)
		free(image);
	image = NULL;

	if (image_s != NULL)
		free(image_s);
	image_s = NULL;

	if (imageout != NULL)
		free(imageout);
	imageout = NULL;

	if (badpoint_t != NULL)
		free(badpoint_t);
	badpoint_t = NULL;

	int ret = 0;
	if ((data->center_darkdeadpoint > setting.center_deadpoint) || (data->edge_darkdeadpoint > setting.edge_deadpoint)
		|| (data->max_darkdeadpoint >= 2) || (mean_result!=0))
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "Darkbadpoint=Fail_%d_%d_%d|", data->center_darkdeadpoint, data->edge_darkdeadpoint, data->max_darkdeadpoint);
	else
		sprintf_s(Log_str, "Darkbadpoint=Pass_%d_%d_%d|", data->center_darkdeadpoint, data->edge_darkdeadpoint, data->max_darkdeadpoint);

	if ((strlen(data->testString) + strlen(Log_str))<TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 11;
		sprintf_s(Log_str, "_11");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}

static int DacSet(int devices, unsigned int data);

int gx_mft_t(int devices, struct testdatainfo *data)
{
	int ret = 0;
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(w*h);
	unsigned char *imageout = (unsigned char *)malloc(w*h);
	memset(image, 0, w*h);
	memset(imageout, 0, w*h);

#if 1
	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

//	GFP_Write_Bmp_8("./Log/mtf_org.bmp", image, h, w);
	memcpy(data->image, image, w*h);
	
	int cnt_num = 0;
	//SL_mtftest(image, sparam.width_msd, sparam.height_msd, result, mtf_mag, &cnt_num);
	struct gx_mtf_info mtf_info;
	gx_mtf(image, w, h, &mtf_info);
	memcpy(data->mtf_data, mtf_info.mtf_data, 5 * sizeof(float));
	data->mtf_cnt = mtf_info.mtf_cnt;
	data->mtf_mag = mtf_info.mtf_mag;
	data->mtf_mag_max = mtf_info.mtf_mag_max;


	int mtf_data_edge = 0;
	for (int m = 1; m < 5; m++)
	{
		if ((m == 1) || (m == 4))
		{
			if (mtf_info.mtf_data[m] < setting.mtf_threshold_edge_ud)
				mtf_data_edge = 1;
		}
		if ((m == 2) || (m == 3))
		{
			if (mtf_info.mtf_data[m] < setting.mtf_threshold_edge_lr)
				mtf_data_edge = 1;
		}
	}

	int light_sum = 0;
	int light_num = 0;
	for (int j = 20; j < h-20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;



	if ((mtf_info.mtf_data[0] < setting.mtf_threshold) || (data->mtf_mag>setting.mtf_mag_max) || (data->mtf_mag<setting.mtf_mag_min) || (mean_result != 0) || (mtf_data_edge != 0))
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "MTF=Fail_%.2f_%.2f|", data->mtf_data[0], data->mtf_mag);
	else
		sprintf_s(Log_str, "MTF=Pass_%.2f_%.2f|", data->mtf_data[0], data->mtf_mag);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 4;
		sprintf_s(Log_str, "_4");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
#endif

// 
// 	DacSet(devices, 100);
// 	for (int i = 0; i < 5;i++)
// 		SL_GetImagebuffer(devices, image_s, w, h);
// 
// 	DacSet(devices, 3000);
// 	for (int i = 0; i < 5; i++)
// 		SL_GetImagebuffer(devices, image_s, w, h);

	free(image_s);
	free(image);
	free(imageout);

	return ret;
}

int gx_mft_t_BF(int devices, struct testdatainfo *data)
{
	int ret = 0;
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(w*h);
	unsigned char *imageout = (unsigned char *)malloc(w*h);
	memset(image, 0, w*h);
	memset(imageout, 0, w*h);

#if 1
	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	//	GFP_Write_Bmp_8("./Log/mtf_org.bmp", image, h, w);
	memcpy(data->image, image, w*h);

	int cnt_num = 0;
	//SL_mtftest(image, sparam.width_msd, sparam.height_msd, result, mtf_mag, &cnt_num);
	struct gx_mtf_offset_info mtf_info;
	gx_mtf_offset(image, w, h, &mtf_info);
	memcpy(data->mtf_data, mtf_info.mtf_data, 5 * sizeof(float));
	data->mtf_cnt = mtf_info.mtf_cnt;
	data->mtf_mag = mtf_info.mtf_mag;
	data->mtf_mag_max = mtf_info.mtf_mag_max;


	int mtf_data_edge = 0;
	for (int m = 1; m < 5; m++)
	{
		if ((m == 1) || (m == 4))
		{
			if (mtf_info.mtf_data[m] < setting.mtf_threshold_edge_ud)
				mtf_data_edge = 1;
		}
		if ((m == 2) || (m == 3))
		{
			if (mtf_info.mtf_data[m] < setting.mtf_threshold_edge_lr)
				mtf_data_edge = 1;
		}
	}

	int light_sum = 0;
	int light_num = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;



	if ((mtf_info.mtf_data[0] < setting.mtf_threshold) || (data->mtf_mag>setting.mtf_mag_max) || (data->mtf_mag<setting.mtf_mag_min) || (mean_result != 0) || (mtf_data_edge != 0))
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "MTF=Fail_%.2f_%.2f|", data->mtf_data[0], data->mtf_mag);
	else
		sprintf_s(Log_str, "MTF=Pass_%.2f_%.2f|", data->mtf_data[0], data->mtf_mag);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 4;
		sprintf_s(Log_str, "_4");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
#endif

	// 
	// 	DacSet(devices, 100);
	// 	for (int i = 0; i < 5;i++)
	// 		SL_GetImagebuffer(devices, image_s, w, h);
	// 
	// 	DacSet(devices, 3000);
	// 	for (int i = 0; i < 5; i++)
	// 		SL_GetImagebuffer(devices, image_s, w, h);

	free(image_s);
	free(image);
	free(imageout);

	return ret;
}

int gx_imageTestBf_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;

	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);
	unsigned char *image_out = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image_out, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);
	int ret = 0;
	ret = gx_imageTestBf(image, w, h);

	free(image);
	free(image_s);
	free(image_out);

	//char Log_str[100] = { 0 };
	//if (ret != 0)
	//	sprintf_s(Log_str, "imageCheck=Fail|");
	//else
	//	sprintf_s(Log_str, "imageCheck=Pass|");

	//if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
	//	strcat_s(data->testString, Log_str);

	//if (ret != 0)
	//{
	//	if (data->Allresult == 0)
	//		data->Allresult = 21;
	//	sprintf_s(Log_str, "_21");
	//	if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
	//		strcat_s(data->resultString, Log_str);
	//}
	return ret;
}

static int GetNM(int d)
{
	int i;
	for (i = 15; i > 0; i--)
	{
		if (d > (1 << i))
			return i;
	}
	return 0;
}

static int DacSet(int devices,unsigned int data)
{
	unsigned int temp = 0x00000f04;
//	SPI_RTotal_Address(devices, 0xff090000, &temp, 1);
	data = data / 4 * 4;
	if (data < 4)
		data = 4;
	if (data > 8191)
		data = 8191;
	temp = (temp & 0xffffe000) + data;

	//temp = data & 0x1fff;
	SPI_WTotal_Address(devices, 0xff090200, &temp, 1);
	return temp & 0x1fff;
}

static int DacGet(int devices)
{
	unsigned int temp = 0;
	SPI_RTotal_Address(devices, 0xff090200, &temp, 1);
	return temp & 0x1fff;
}

static int GetAverage(int devices,int w,int h)
{
	static int ss = 0;
	int *image_s = (int*)malloc(w*h*4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

//	GFP_Write_Bmp_8("./Log/Apkg_org.bmp", image, h, w);

	unsigned int sum = 0;
	unsigned int num = 0;
#define  AUTOWIDTH     w/5
#define  AUTOHEIGHT    h/5
	for (int j = h / 2 - AUTOHEIGHT / 2; j < h / 2 + AUTOHEIGHT / 2; j++)
	{
		for (int i = w / 2 - AUTOWIDTH / 2; i < w / 2 + AUTOWIDTH / 2; i++)
		{
			sum += image[i + j*w];
			num++;
		}
	}
	int tt = 0;
	if (num == 0)
		tt = 0;
	else
		tt = sum / num;

	free(image);
	free(image_s);
	return tt;
}


static int GetAverage_global(int devices, int w, int h)
{
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	int sum = 0;
	for (int j = 0 ; j < h ; j++)
	{
		for (int i = 0; i < w; i++)
		{
			sum += image[i + j*w];
		}
	}

	free(image_s);
	free(image);
	if ((w == 0) || (h == 0))
		return 0;
	return sum / (w*h);
}

#if 0
void gx_autoAgc(int devices)
{
	int goal = setting.target_mean_half;// setting.focus_target_mean;
	int dac_min = setting.expo_min;// setting.focus_min;//factoryParameter.dac_min;//0x71;
	int dac_max = setting.expo_max;// setting.focus_max;//factoryParameter.dac_max;//0x90;//max最好等于min +3或+7或+15或+31……

	int temp;
	int base_average = 0;
	int avg_save;
	int bit_n;
	int n_max = GetNM(dac_max - dac_min);
	for (bit_n = 0; bit_n <= n_max + 2; bit_n++)
	{
		temp = DacGet(devices) - dac_min;//返回当前dac的值
// 		if (bit_n == 0)
// 			temp = 0;
// 		else if (temp < 0)
		if (temp < 0)
			break;//正常不会跑到这里
// 		else if (temp<30)
// 			break;
		else// if(bit_n <= n_max+2)
			base_average = GetAverage(devices, sparam.width_org ,sparam.height_org);//返回当前的原始值采样值
		if ((base_average > (goal - 10)) && (base_average < (goal + 10)))
			break;
		if (bit_n <= n_max)
		{
			if (base_average > goal)
				temp &= ~(0x1 << (n_max + 1 - bit_n));
			temp |= (0x1 << (n_max - bit_n));
		}
// 		if ((bit_n == n_max + 2 || (bit_n == n_max + 1 && base_average > goal)) && deviation == 0)
// 		{//处理最后1的误差。
// 			unsigned int r0, r1;
// 			r0 = avg_save > goal ? avg_save - goal : goal - avg_save;
// 			r1 = base_average > goal ? base_average - goal : goal - base_average;
// 			if (r0 < r1)
// 				temp--;
// 			bit_n += 0x100;
// 		}
		if (bit_n == n_max + 1)
		{
			if (temp < 0x1fff)
				temp++;
		}
		avg_save = base_average;
		int tempdc = temp + dac_min;
		DacSet(devices, tempdc);
	}
	temp = DacGet(devices);
}
#else
void gx_autoAgc(int devices)
{
	int goal = setting.target_mean_half;// setting.focus_target_mean;
	int dac_min = setting.expo_min;// setting.focus_min;//factoryParameter.dac_min;//0x71;
	int dac_max = setting.expo_max;// setting.focus_max;//factoryParameter.dac_max;//0x90;//max最好等于min +3或+7或+15或+31……

#define  MAX_TIME    (dac_max+dac_min)/2
#define  MEAN_DIFF   2

	int base_average = 0;
	int time_t = MAX_TIME;
	int temp = 0;

	base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值
	if ((base_average > (goal - MEAN_DIFF)) && (base_average < (goal + MEAN_DIFF)))
		return;

	int bit_n;
	int n_max = GetNM((MAX_TIME - dac_min) / 2);
	int time_st = (MAX_TIME - dac_min);
	int mm = 0;

// 	char log_str[40];
// 	sprintf_s(log_str, "\r\n\r\n");
// 	FWriteLog("./Log/mean160.txt", log_str);
	for (bit_n = 0; bit_n <= n_max + 2; bit_n++)
	{
		DacSet(devices, time_t);
		base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值

// 		sprintf_s(log_str, "time=%d,mean=%d,target_mean=%d\r\n", time_t, base_average, goal);
// 		FWriteLog("./Log/mean160.txt", log_str);
		if ((base_average > (goal - MEAN_DIFF)) && (base_average < (goal + MEAN_DIFF)))
			break;
		mm = time_t;

		if (base_average > goal)
		{
			if ((mm - (time_st >> bit_n)) < dac_min){
				time_t = dac_min;
				// 				DacSet(devices, time_t);
				break;
			}
			else
				time_t = time_t - (time_st >> bit_n);
		}
		else
		{
			//time_t = DacGet(devices) + (time_st >> bit_n);
			time_t = time_t + (time_st >> bit_n);
			if (time_t > dac_max){
				time_t = dac_max;
				// 				DacSet(devices, time_t);
				break;
			}
		}
// 		base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值
// 		char log_str[40];
// 		sprintf_s(log_str, "time=%d,mean=%d,target_mean=%d\r\n", time_t, base_average, goal);
// 		FWriteLog("./Log/mean11111111111.txt", log_str);
	}
	DacSet(devices, time_t);
END:
	return;
}
#endif


#if 0
void gx_autoAgc220(int devices)
{
	int goal = setting.target_mean220;// setting.focus_target_mean;
	int dac_min = setting.expo_min;// setting.focus_min;//factoryParameter.dac_min;//0x71;
	int dac_max = 3000;// setting.expo_max;// setting.focus_max;//factoryParameter.dac_max;//0x90;//max最好等于min +3或+7或+15或+31……

	int temp;
	int base_average = 0;
	int avg_save;
	int bit_n;
	int n_max = GetNM(dac_max - dac_min);
	for (bit_n = 0; bit_n <= n_max + 2; bit_n++)
	{
		temp = DacGet(devices) - dac_min;  //返回当前dac的值
// 		if (bit_n == 0)
// 			temp = 0;
// 		else if (temp < 0)
		if (temp < 0)
			break;//正常不会跑到这里
		else// if(bit_n <= n_max+2)
		{
			base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值

		}
		if ((base_average >(goal - 10)) && (base_average < (goal + 10)))
			break;
		if (bit_n <= n_max)
		{
			if (base_average > goal)
				temp &= ~(0x1 << (n_max + 1 - bit_n));
			temp |= (0x1 << (n_max - bit_n));
		}

		if (bit_n == n_max + 1)
		{
			if (temp < 0x1fff)
				temp++;
		}
		avg_save = base_average;
		int tempdc = temp + dac_min;
		DacSet(devices, tempdc);
	}
	temp = DacGet(devices);
}

#else
void gx_autoAgc220(int devices)
{
	int goal = setting.target_mean220;// setting.focus_target_mean;
	int dac_min = setting.expo_min;// setting.focus_min;//factoryParameter.dac_min;//0x71;
	int dac_max = setting.expo_max;// setting.focus_max;//factoryParameter.dac_max;//0x90;//max最好等于min +3或+7或+15或+31……

#define  MAX_TIME    (dac_max+dac_min)/2
#define  MEAN_DIFF   10

	int base_average = 0;
	int time_t = MAX_TIME;
	int temp = 0;

	base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值
	if ((base_average > (goal - MEAN_DIFF)) && (base_average < (goal + MEAN_DIFF)))
		return;

	int bit_n;
	int n_max = GetNM((MAX_TIME - dac_min) / 2);
	int time_st = (MAX_TIME - dac_min);
	int mm = 0;
	for (bit_n = 0; bit_n <= n_max; bit_n++)
	{
		DacSet(devices, time_t);
		base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值

// 		char log_str[40];
// 		sprintf_s(log_str, "time=%d,mean=%d,target_mean=%d\r\n", time_t, base_average, goal);
// 		FWriteLog("./Log/mean220.txt", log_str);
		if ((base_average > (goal - MEAN_DIFF)) && (base_average < (goal + MEAN_DIFF)))
			break;
		mm = time_t;
		if (base_average > goal)
		{
			if ((mm - (time_st >> bit_n)) < dac_min){
				time_t = dac_min;
				// 				DacSet(devices, time_t);
				break;
			}
			else
				time_t = time_t - (time_st >> bit_n);
		}
		else
		{
			//time_t = DacGet(devices) + (time_st >> bit_n);
			time_t = time_t + (time_st >> bit_n);
			if (time_t > dac_max){
				time_t = dac_max;
				// 				DacSet(devices, time_t);
				break;
			}
		}
		// 		base_average = GetAverage(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值
		// 		char log_str[40];
		// 		sprintf_s(log_str, "time=%d,mean=%d,target_mean=%d\r\n", time_t, base_average, goal);
		// 		FWriteLog("./Log/mean11111111111.txt", log_str);
	}
	DacSet(devices, time_t);
END:
	return;
}

#endif

static void gx_autoAgc_global(int devices)
{
	int goal = setting.pkgmean;// setting.focus_target_mean;
	int dac_min = setting.expo_min;// setting.focus_min;//factoryParameter.dac_min;//0x71;
	int dac_max = setting.expo_max;// setting.focus_max;//factoryParameter.dac_max;//0x90;//max最好等于min +3或+7或+15或+31……

	int temp;
	int base_average = 0;
	int avg_save;
	int bit_n;
	int n_max = GetNM(dac_max - dac_min);
	for (bit_n = 0; bit_n <= n_max + 2; bit_n++)
	{
		temp = DacGet(devices) - dac_min;//返回当前dac的值
		if (bit_n == 0)
			temp = 0;
		else if (temp < 0)
			break;//正常不会跑到这里
		// 		else if (temp<30)
		// 			break;
		else// if(bit_n <= n_max+2)
		{
			base_average = GetAverage_global(devices, sparam.width_org, sparam.height_org);//返回当前的原始值采样值
		}
		if ((base_average >(goal - 10)) && (base_average < (goal + 10)))
			break;
		if (bit_n <= n_max)
		{
			if (base_average > goal)
				temp &= ~(0x1 << (n_max + 1 - bit_n));
			temp |= (0x1 << (n_max - bit_n));
		}

		if (bit_n == n_max + 1)
		{
			if (temp < 0xff)
				temp++;
		}
		avg_save = base_average;
		int tempdc = temp + dac_min;
		DacSet(devices, tempdc);

	}
	temp = DacGet(devices);
}


int gx_shading_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;

	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;


//	GFP_Write_Bmp_8("./Log/shading_org.bmp", image, sparam.height_msd, sparam.width_msd);
	memcpy(data->image, image, w*h);

#if 0
#define  EXPOTIME  0x10
	for (int i = 0; i < 10; i++)
	{
		unsigned int time_t = 0;
		time_t = i*EXPOTIME;
		DacSet(devices, time_t);
		SL_GetImagebuffer(devices, image, w, h);

		char log_s[30];
		memset(log_s, 0, 30);
		sprintf_s(log_s, "./Log/EXPOTIEM_%d.bmp", time_t);
		GFP_Write_Bmp_8(log_s, image, h, w);
	}
#endif


	float shading[5] = { 0.0 };

	gx_shading(image, w, h, shading);
	memcpy(data->shading, shading, 5 * sizeof(float));

	float shading_max = data->shading[1];
	float shading_min = data->shading[1];
	for (int i = 1; i<5; i++)
	{
		if (shading_max < data->shading[i])
			shading_max = data->shading[i];
		if (shading_min > data->shading[i])
			shading_min = data->shading[i];
	}

	float shading_diff = shading_max - shading_min;
	int ret = 0;
	if ((data->shading[1] > setting.shading_threshold) && (data->shading[2] > setting.shading_threshold)
		&& (data->shading[3] > setting.shading_threshold) && (data->shading[4] > setting.shading_threshold)
		&& (shading_diff <= setting.shadingdiff))
		ret = 0;
	else
		ret = 1;

	char Log_str[200] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "Shading=Fail_%.2f_%.2f_%.2f_%.2f|", data->shading[1], data->shading[2], data->shading[3], data->shading[4]);
	else
		sprintf_s(Log_str, "Shading=Pass_%.2f_%.2f_%.2f_%.2f|", data->shading[1], data->shading[2], data->shading[3], data->shading[4]);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 5;
		sprintf_s(Log_str, "_5");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	int light_sum = 0;
	int light_num = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	free(image);
	free(image_s);
	return ret | mean_result;
}

int gx_pkgAlgraw_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	unsigned char *image_out = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;


	memcpy(data->image, image, w*h);

	data->pkgdata = gx_pkgAlgraw(image, w, h, setting.threshold_sobel_,
		setting.limit_size_, setting.skip_, setting.code_,
		setting.code_white_, setting.code, setting.count, setting.grade_, setting.open_mode_, image_out);
	memcpy(data->image_out, image_out, w*h);


	if (data->pkgdata >= 999)
		data->pkgdata = 999;

	char Log_str[100] = { 0 };
	if (data->pkgdata != 0)
		sprintf_s(Log_str, "pkgAlgraw=Fail_%d|", data->pkgdata);
	else
		sprintf_s(Log_str, "pkgAlgraw=Pass_%d|", data->pkgdata);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);


	if (data->pkgdata != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 10;
		sprintf_s(Log_str, "_10");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	free(image);
	free(image_out);
	free(image_s);
	if ((data->pkgdata == 0) && (mean_result == 0))
		return 0;
	else
		return 1;
}


int gx_aperture_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

 	GFP_Write_Bmp_8("./Log/Apture_org.bmp", image, h, w);

	memcpy(data->image, image, w*h);
	data->apture = gx_checkcircle(image, w, h, 0);

	char Log_str[100] = { 0 };
	if (data->apture > setting.aperture)
		sprintf_s(Log_str, "aperture_=Fail|", data->apture);
	else
		sprintf_s(Log_str, "aperture_=Pass|", data->apture);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (data->apture > setting.aperture)
	{
		if (data->Allresult == 0)
			data->Allresult = 9;
		sprintf_s(Log_str, "_9");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	free(image);
	free(image_s);
	if ((data->apture > setting.aperture) || (mean_result != 0))
		return 1;
	else
		return 0;
}


int gx_checkBlot_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	unsigned char *image_out = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	unsigned char *image_out1 = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);
	int ret = gx_checkBlot(image, w, h, setting.gain, setting.num_max, setting.blotData_max,
		setting.blotData_min, image_out, image_out1);

	data->blotnum = ret;
	memcpy(data->image_out, image_out, w*h);

	char Log_str[100] = { 0 };
	if (ret > setting.blotnummax)
		sprintf_s(Log_str, "checkBlot=Fail|");
	else
		sprintf_s(Log_str, "checkBlot=Pass|");

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (ret > setting.blotnummax)
	{
		if (data->Allresult == 0)
			data->Allresult = 7;
		sprintf_s(Log_str, "_7");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	int light_sum = 0;
	int light_num = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	free(image);
	free(image_out);
	free(image_out1);
	free(image_s);

	if ((ret <= setting.blotnummax) && (mean_result == 0))
		return 0;
	else
		return 1;
}



int gx_calcFreqvalue_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	// 	GFP_Write_Bmp_8("./Log/Apture_org.bmp", image, h, w);

	memcpy(data->image, image, w*h);
	data->Freqvalue = gx_calcFreqvalue(image, w, h);

	char Log_str[100] = { 0 };
	if (data->apture > setting.aperture)
		sprintf_s(Log_str, "Freqvalue=Fail|", data->Freqvalue);
	else
		sprintf_s(Log_str, "Freqvalue=Pass|", data->Freqvalue);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (data->Freqvalue > setting.Freqvalue)
	{
		if (data->Allresult == 0)
			data->Allresult = 17;
		sprintf_s(Log_str, "_17");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	free(image);
	free(image_s);
	if ((data->Freqvalue > setting.Freqvalue) || (mean_result != 0))
		return 1;
	else
		return 0;
}

static int ImageJieya(unsigned char *bic, int *bo, int img_len, int bit_len)
{
	//sizeof(bic) >= (img_len*bit_len+7) / 8
	//bic输入的压缩数据
	//bo输出的数据，16位高位对齐
	//img_len 图像的长度 = 图像的长*宽
	//bit_len 压缩数据的精度8 10 12 14 16
	int i;
	int data;
	int eff = 8;//表示当前bic中多少位有效
	int n = 0;//表示bic用到哪个
	int mask = 0;//有效位，防止高位非零
	if (bit_len > 16 || bit_len < 8)
		return -1;//只适用8-16位图像数据
	for (i = 0; i < bit_len; i++)
		mask |= 1 << i;
	for (i = 0; i < img_len; i++)
	{
		data = bic[n] >> (8 - eff);
		while (bit_len > eff)//有效位数不足，继续加载
		{
			n++;
			data += ((int)(bic[n])) << eff;
			eff += 8;
		}
		bo[i] = (data & mask) << (16 - bit_len);//高位对齐到16位
		eff -= bit_len;//bic[n]中剩余的有效位数
		if (eff == 0)
		{
			n++;
			eff = 8;
		}
	}
	return 1;
}


struct regRange
{
	unsigned int address;
	unsigned int regdata;
};

#if 0
#if 1
//3.0v
struct regRange RegRangeData[]={
	0x00000098, 0x20044700,
	0x000000e8, 0x00000053,
	0x000000b0, 0x00008381,
	0xff0900f8, 0x0011000c,
	0xff0900f4, 0x80002e10,
	0xff090004, 0x01a40010,
	0xff090008, 0x00000000,
	0xff09000c, 0x00000e04,
	0xff090018, 0x030300c1,
	0xff090020, 0x00020b10,
	0xff090024, 0x330920b4,
	0xff090028, 0x090130d3,
	0xff090030, 0x90550002,
	0xff090038, 0xe0581400,
	0xff090040, 0x12010203,
	0xff090044, 0x04050607,
	0xff090048, 0x0f171f1f,
	0xff090050, 0x2200ff00,
	0xff090054, 0x08003c00,
	0xff090060, 0x18100084,
	0xff090080, 0x00000303,
	0xff090088, 0x00120400,
	0xff090094, 0x000000d0,
	0xff090098, 0x00a0141c,
	0xff09009c, 0x0000a002,
	0xff0900ac, 0x0000d002,
	0xff0900bc, 0x00000101,
	0xff0900d0, 0x07f003ff,
	0xff090000, 0x00001ffc,
	0xff0900b4, 0x80804010,
	0xff0900b8, 0x00001016,
	0xff080000, 0x20220000,
	0xff08004c, 0x0001f303,
	0xff080054, 0x00002080,
	0xff08007c, 0x008000c0,
	0xff0900b4, 0x80804010,
	0xff0900b8, 0x00001016,
	0xff090000, 0x00001ffc,
	0xff090054, 0x08003c00,
};

#else

struct regRange RegRangeData[] = {
	0x00000098, 0x20044700,
	0x000000e8, 0x00000053,
	0x000000b0, 0x00008381,
	0xff0900f8, 0x0011000c,
	0xff0900f4, 0x80002e10,
	0xff090004, 0x01a40010,
	0xff090008, 0x00000000,
	0xff09000c, 0x00000e04,
	0xff090018, 0x030300c1,
	0xff090020, 0x00020b10,
	0xff090024, 0x330920b4,
	0xff090028, 0x090130d3,
	0xff090030, 0x90550002,
	0xff090038, 0xe0581400,
	0xff090040, 0x12010203,
	0xff090044, 0x04050607,
	0xff090048, 0x0f171f1f,
	0xff090050, 0xa200ff00,
	0xff090054, 0x08003c00,
	0xff090060, 0x18100084,
	0xff090080, 0x00000303,
	0xff090088, 0x00120400,
	0xff090094, 0x000000d0,
	0xff090098, 0x00a0141c,
	0xff09009c, 0x0000a002,
	0xff0900ac, 0x0000d002,
	0xff0900bc, 0x00000101,
	0xff0900d0, 0x07f003ff,
	0xff090000, 0x00001ffc,
	0xff0900b4, 0x80804010,
	0xff0900b8, 0x00001016,
	0xff080000, 0x20220000,
	0xff08004c, 0x0001f303,
	0xff080054, 0x00002080,
	0xff08007c, 0x008000c0,
};
#endif

#endif

struct regRange RegRangeData[] = {
	0x000000e8, 0x00000053,
	0x000000b0, 0x0001d321,
	0xff080024, 0x000000c1,
	0xff080054, 0x00008a10,
	0xff0902d4, 0x0f6014ff,
	0xff09020c, 0x040001b4,
	0xff090104, 0x0024005e,
	0xff090118, 0x03a00a00,
	0xff090124, 0x00bc00bc,
	0xff090128, 0x02300030,
	0xff090270, 0x003700bf,
	0xff090230, 0x00382403,
	0xff090220, 0x12200420,
	0xff090218, 0x9a10012b,
	0xff09021c, 0x3ca72074,
	0xff090208, 0x00209018,
	0xff09024c, 0x00c10030,
	0xff090288, 0x18000430,
	0xff0902a8, 0x00443005,
	0xff090248, 0x5005040a,
	0xff090204, 0x00100100,
	0xff0902e4, 0x70883018,
	0xff090200, 0x00000400,
	0xff090210, 0x020b1020,
	0xff090138, 0x01000002,
// 	0xff090040, 0x00000000,
// 	0xff080000, 0x30000000,
};

int gx_linearChange_t(int devices, struct testdatainfo *data)
{

	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *imageout = (unsigned char *)malloc((w*h + 7) / 8 * 12);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	int len = sizeof(RegRangeData) / sizeof(RegRangeData[0]);
	for (int i = 0; i < len; i++)
	{
		gx_write(devices, RegRangeData[i].address, &RegRangeData[i].regdata, 1);
	}
	Sleep(100);
#if 1
//	SL_GetImagebuffer(devices, image, w, h);
	Scan_chip(devices);
	Sleep(400);
//	int ss = Read_bf(devices); 
//	if (ss)

	int len_char = (w*h + 7) / 8 * 12;
	GetImgBuffer(devices, 0x0, imageout, len_char);
	ImageJieya(imageout, image_s, w*h, 12);

#endif
//	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);

//	GFP_Write_Bmp_8("./Log/linearChange_org.bmp", image, h, w);
	int stripediff = 0;
	gx_stripe(image, w, h, &stripediff);
	data->stripediff = stripediff;

	unsigned int sum = 0;
	unsigned int num = 0;
	int ImageW = setting.linearImageW;
	int ImageH = setting.linearImageH;
	for (int j = h / 2 - ImageH / 2; j < h / 2 + ImageH / 2; j++)
	{
		for (int i = w / 2 - ImageW / 2; i < w / 2 + ImageW / 2; i++)
		{
			sum += image[i + j*w];
			num++;
		}
	}
	data->linearmean = sum / num;

	int meanResult = 0;
	if (data->linearmean < setting.linearchangemean || data->stripediff>setting.stripediff)
		meanResult = 1;


	char Log_str[200] = { 0 };
	if (meanResult != 0)
		sprintf_s(Log_str, "LinearChange=Fail_%d_%d|", data->linearmean, data->stripediff);
	else
		sprintf_s(Log_str, "LinearChange=Pass_%d_%d|", data->linearmean, data->stripediff);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (meanResult != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 15;
		sprintf_s(Log_str, "_15");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	if (image != NULL)
		free(image);
	image = NULL;

	if (imageout != NULL)
		free(imageout);
	imageout = NULL;

	if (image_s != NULL)
		free(image_s);
	image_s = NULL;

	return meanResult;
}


int gx_expotime_t(int devices, struct testdatainfo *data)
{
	Scan_chip(devices);
	data->exposure = Read_bf(devices);

	int ret = 0;
	if (data->exposure <= setting.expotimeMax && data->exposure >= setting.expotimeMin)
		ret = 0;
	else
		ret = 1;

	char Log_str[200] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "exposureTime=Fail_%d|", data->exposure);
	else
		sprintf_s(Log_str, "exposureTime=Pass_%d|", data->exposure);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 16;
		sprintf_s(Log_str, "_16");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	return ret;
}
#endif

void gx_read(int devices, unsigned int addr, unsigned int *data, unsigned int len_int)
{
	if ((addr & 0xff000000) == 0x0)
	{
		unsigned char data_r[4] = { 0 };
		SPI_Read4B(devices, (unsigned char)addr, data_r, len_int * 4);
		*data = (data_r[3] << 24) + (data_r[2] << 16) + (data_r[1] << 8) + data_r[0];
	}
	else
	{
		SPI_RTotal_Address(devices, addr, data, len_int);
	}
}

void gx_write(int devices, unsigned int addr, unsigned int *data, unsigned int len_int)
{
	if ((addr & 0xff000000) == 0x0)
	{
		unsigned char data_r[4] = { 0 };
		data_r[3] = (*data >> 24) & 0xff;
		data_r[2] = (*data >> 16) & 0xff;
		data_r[1] = (*data >> 8) & 0xff;
		data_r[0] = (*data >> 0) & 0xff;
		SPI_Write4B(devices, (unsigned char)addr, data_r, len_int * 4);
	}
	else
	{
		SPI_WTotal_Address(devices, addr, data, len_int);
	}
}


void gx_vdd_output(int devices, unsigned int data)
{
	SL_AvddOutput(devices, data);
}

void gx_rst_output(int devices, unsigned int data)
{
	SL_ResetOutput(devices, data);
}

static short int get_FlashId(int devices)
{
	unsigned char r_buf[2] = { 0 };
	Read_FlashId(devices, r_buf);
	return (r_buf[1] << 8) + r_buf[0];
}

int gx_flash(int devices, struct testdatainfo *data)
{
	SL_AvddOutput(devices, 0);
	sl_delay(40);
	SL_AvddOutput(devices, 1);
	sl_delay(50);
	SL_ResetSTM32(devices);
	sl_delay(50);
	SL_ResetOutput(devices, 0);
	sl_delay(20);
	SL_ResetOutput(devices, 1);
	sl_delay(20);
	unsigned int temp_da = 0;
	gx_read(devices, 0xfc, &temp_da, 1);
	SL_ResetOutput(devices, 0);
	sl_delay(20);
	SL_ResetOutput(devices, 1);
	sl_delay(20);

	unsigned char buf[4] = { 0 };
	buf[3] = 0;
	buf[2] = 0;
	buf[1] = 0;
	buf[0] = 1;
	SPI_Write4B(devices, 0xa0, buf, 4);
	sl_delay(100);


	data->flashid = get_FlashId(devices);

	int flash_only = 0;
	if ((data->flashid & 0xffff) != 0x11c8)
		flash_only = 1;

	int flash_result = Flash_Test(devices);
	{
		if (flash_result != 0)
		{
			buf[3] = 0;
			buf[2] = 0;
			buf[1] = 0;
			buf[0] = 0;
			SPI_Write4B(devices, 0xa0, buf, 4);
			sl_delay(50);


			SL_AvddOutput(devices, 0);
			sl_delay(40);
			SL_AvddOutput(devices, 1);
			sl_delay(50);
			SL_ResetSTM32(devices);
			sl_delay(100);
			SL_ResetOutput(devices, 0);
			sl_delay(20);
			SL_ResetOutput(devices, 1);
			sl_delay(20);
			unsigned int temp_da = 0;
			gx_read(devices, 0xfc, &temp_da, 1);
			SL_ResetOutput(devices, 0);
			sl_delay(20);
			SL_ResetOutput(devices, 1);
			sl_delay(100);

			buf[3] = 0;
			buf[2] = 0;
			buf[1] = 0;
			buf[0] = 1;
			SPI_Write4B(devices, 0xa0, buf, 4);
			sl_delay(100);

			flash_result = Flash_Test(devices);
		}
	}

	buf[3] = 0;
	buf[2] = 0;
	buf[1] = 0;
	buf[0] = 0;
	SPI_Write4B(devices, 0xa0, buf, 4);
	sl_delay(50);


	char Log_str[100] = { 0 };
	if ((flash_result | flash_only) != 0)
		sprintf_s(Log_str, "flash=Fail_0x%04x|", data->flashid & 0xffff);
	else
		sprintf_s(Log_str, "flash=Pass_0x%04x|", data->flashid & 0xffff);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if ((flash_result | flash_only) != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 2;
		sprintf_s(Log_str, "_2");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return flash_result | flash_only;
}


int gx_sram(int devices, struct testdatainfo *data)
{
	SL_ResetOutput(devices, 0);
	sl_delay(10);
	SL_ResetOutput(devices, 1);
	sl_delay(10);
#if 0
	int point_total = 0;
	int centerbadpointnum = 0;
	int result = 0;
	int w = sparam.width_org;
	int h = sparam.height_org;

 	unsigned char *point_buf = (unsigned char *)malloc(sparam.width_org * sparam.height_org);
 	unsigned char *point_out = (unsigned char *)malloc(sparam.width_org * sparam.height_org);

	memset(point_out, 0, w * h);

	int ret = CheckSram(devices, point_buf, &point_total, &centerbadpointnum, w, h, 
		setting.center_block_w, setting.center_block_h);

// 	remove("./Log/sram.txt");
// 	for (int i = 0; i < w*h; i++)
// 	{
// 		if (point_buf[i] == 1)
// 		{
// 			char log_gt[30];
// 			sprintf_s(log_gt, "%d\r\n", i);
// 			FWriteLog("./Log/sram.txt", log_gt);
// 		}
// 	}

	data->deadpoint_total = point_total;
	data->deadpoint_center = centerbadpointnum;

	for (int j = 0; j < w * h; j++)
	{
		if (point_buf[j] == 1)
			point_out[j] = 255;
		else
			point_out[j] = 0;
	}

// 	unsigned char buf[4] = { 0 };
// 	buf[3] = 0;
// 	buf[2] = 0;
// 	buf[1] = 0;
// 	buf[0] = 1;
// 	SPI_Write4B(devices, 0xa0, buf, 4);
// 	sl_delay(50);
// 	int flash_result = Flash_Test(devices);
// 	if (flash_result == 0)
// 	{
// 		Flash_Erase(devices);
// 		write_flash_title(devices);
// //		data->deadpoint_failbit = Write_DeadPixel(devices, point_out, sparam.width_org, sparam.height_org);
// 
// 	}
// 	buf[3] = 0;
// 	buf[2] = 0;
// 	buf[1] = 0;
// 	buf[0] = 0;
// 	SPI_Write4B(devices, 0xa0, buf, 4);

	if ((ret == 0) && (point_total < setting.deadpoint_total) && (centerbadpointnum < setting.deadpoint_center))
		result = 0;
	else
	{
		result = 1;
	}

#endif
	int ret = MRegtest(devices);

	data->sram_result = ret;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "sram=Fail_%08x|", ret);
	else
		sprintf_s(Log_str, "sram=Pass_%08x|", ret);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 3;
		sprintf_s(Log_str, "_3");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	return ret;
}

#if 1
int gx_center_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;

	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);
// 	guangxin(image, sparam.width_org, sparam.height_org, &data->lgcenter_w, &data->lgcenter_h);
// 	float juli = sqrt(((double)data->lgcenter_w - sparam.width_org / 2)*((double)data->lgcenter_w - sparam.width_org/2) 
// 		+ ((double)data->lgcenter_h - sparam.height_org/2)*((double)data->lgcenter_h - sparam.height_org / 2));

	gx_center(image, w, h, &data->lgcenter_w, &data->lgcenter_h, &data->distance);

	free(image);
	free(image_s);
	int ret = 0;
	if (data->distance > setting.lg_center)
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "OpticsCenter=Fail_%.2f|", data->distance);
	else
		sprintf_s(Log_str, "OpticsCenter=Pass_%.2f|", data->distance);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 6;
		sprintf_s(Log_str, "_6");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}

int gx_openshort(int devices, struct testdatainfo *data)
{
	struct testData *openshort = (struct testData *)malloc(sizeof(struct testData));

	int ret = OpenShortTest(devices, openshort, setting.openshort_filename);

	data->Avdd_current_down = openshort->Avdd_current_down;
	data->Avdd_current_up = openshort->Avdd_current_up;
	data->Avdd_current_work = openshort->Avdd_current_work;
	data->Op_Avdd_vol = openshort->Op_Avdd_vol;

	data->mosi_Res_down = openshort->mosi_Res_down;     //mosi下拉电阻，单位KΩ
	data->mosi_Res_up = openshort->mosi_Res_up;     //mosi上拉电阻，单位KΩ

	data->miso_Res_down = openshort->miso_Res_down;  //miso下拉电阻，单位KΩ
	data->miso_Res_up = openshort->miso_Res_up;     //miso上拉电阻，单位KΩ

	data->ss_Res_down = openshort->ss_Res_down;    //ss下拉电阻，单位KΩ
	data->ss_Res_up = openshort->ss_Res_up;        //ss上拉电阻，单位KΩ

	data->sck_Res_down = openshort->sck_Res_down;      //sck下拉电阻，单位KΩ
	data->sck_Res_up = openshort->sck_Res_up;      //sck上拉电阻，单位KΩ

	data->irq_Res_down = openshort->irq_Res_down;    //irq下拉电阻，单位KΩ
	data->irq_Res_up = openshort->irq_Res_up;     //irq上拉电阻，单位KΩ

	data->rst_Res_down = openshort->rst_Res_down;     //rst下拉电阻，单位KΩ
	data->rst_Res_up = openshort->rst_Res_up;    //rst上拉电阻    单位KΩ

	char Log_str[100] = { 0 };
	char Log_all[300] = { 0 };
	if ((ret & 0x1) == 0x1)
		sprintf_s(Log_str, "Vdd=Fail_%.2f|", data->Op_Avdd_vol);
	else
		sprintf_s(Log_str, "Vdd=Pass_%.2f|", data->Op_Avdd_vol);
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (((ret>>1) & 0x1) == 0x1)
		sprintf_s(Log_str, "SleepCurrent=Fail_%d|", data->Avdd_current_down);
	else
		sprintf_s(Log_str, "SleepCurrent=Pass_%d|", data->Avdd_current_down);
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (((ret >> 2) & 0x1) == 0x1)
		sprintf_s(Log_str, "IdleCurrent=Fail_%d|", data->Avdd_current_up);
	else
		sprintf_s(Log_str, "IdleCurrent=Pass_%d|", data->Avdd_current_up);
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if (((ret >> 3) & 0x1) == 0x1)
		sprintf_s(Log_str, "WorkCurrent=Fail_%d|", data->Avdd_current_work);
	else
		sprintf_s(Log_str, "WorkCurrent=Pass_%d|", data->Avdd_current_work);
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	if ((ret & 0xfffffff0) == 0)
		sprintf_s(Log_str, "Openshort=Pass");
	else
		sprintf_s(Log_str, "Openshort=Fail");
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);

	int num = 0;
	if ((((ret >> 4) & 0x1) == 0x1) || (((ret >> 5) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_Mosi");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}

	if ((((ret >> 6) & 0x1) == 0x1) || (((ret >> 7) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_Miso");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}

	if ((((ret >> 8) & 0x1) == 0x1) || (((ret >> 9) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_SS");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}


	if ((((ret >> 10) & 0x1) == 0x1) || (((ret >> 11) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_Sck");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}

	if ((((ret >> 12) & 0x1) == 0x1) || (((ret >> 13) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_Irq");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}

	if ((((ret >> 14) & 0x1) == 0x1) || (((ret >> 15) & 0x1) == 0x1))
	{
		if (num >= 3)
			goto END;
		num++;
		sprintf_s(Log_str, "_Rst");
		if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
			strcat_s(data->testString, Log_str);
	}


END:
	sprintf_s(Log_str, "|");
	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
	{
		strcat_s(data->testString, Log_str);
		strcat_s(data->testString, Log_all);
	}
	free(openshort);

	if (ret != 0)
	{
		if (data->Allresult==0)
			data->Allresult = 1;
		sprintf_s(Log_str, "_1");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}


int getSensorid(int devices, struct testdatainfo *data)
{
	SL_ResetOutput(devices, 0);
	sl_delay(10);
	SL_ResetOutput(devices, 1);
	sl_delay(50);

	unsigned int temp0 = 0, temp1 = 0, temp2 = 0;
	bool Already_Writed = FALSE;

	SPI_RTotal_Address(devices, 0xff100000, &temp0, 1);
	if ((temp0 & 0xffff) != 0)
		Already_Writed = TRUE;
	SPI_RTotal_Address(devices, 0xff100004, &temp1, 1);
	if ((temp1 & 0xffff) != 0)
		Already_Writed = TRUE;
	SPI_RTotal_Address(devices, 0xff100010, &temp2, 1);
	if ((temp2 & 0xff) != 0)
		Already_Writed = TRUE;

	if (0)
	{
		unsigned int tempData_00 = 0;
		unsigned int tempData_04 = 0;
		unsigned int tempData_10 = 0;

		SYSTEMTIME stTime;
		GetLocalTime(&stTime);

		WORD wMonth = stTime.wMonth;
		WORD wDay = stTime.wDay;
		WORD wHour = stTime.wHour;
		WORD wMinute = stTime.wMinute;
		WORD wSecond = stTime.wSecond;
		WORD wmSecond = stTime.wMilliseconds;

		tempData_00 = (0x00DD << 16) + ((wDay & 0x1f) << 11) + ((wHour & 0x1f) << 6) + (wMinute & 0x3f);
		tempData_04 = (0x007F << 16) + ((wSecond & 0x3f) << 10) + (wmSecond & 0x3ff);
		tempData_10 = (0x00D7 << 16) + (random(0xf) << 4) + (wMonth & 0xf);

// 		tempData_00 = (0x00DD << 16) + (random(0xf) << 12) + (random(0xf) << 8) + (random(0xf) << 4) + (random(0xf) << 0);
// 		tempData_04 = (0x007F << 16) + (random(0xf) << 12) + (random(0xf) << 8) + (random(0xf) << 4) + (random(0xf) << 0);
// 		tempData_10 = (0x00D7 << 16) + (random(0xf) << 4) + (random(0xf) << 0);

		SPI_WTotal_Address_n(devices, 0xff100000, &tempData_00, 1, 2);
		SPI_WTotal_Address_n(devices, 0xff100004, &tempData_04, 1, 2);
		SPI_WTotal_Address_n(devices, 0xff100010, &tempData_10, 1, 2);
		sl_delay(20);
		SPI_RTotal_Address(devices, 0xff100000, &temp0, 1);
		SPI_RTotal_Address(devices, 0xff100004, &temp1, 1);
		SPI_RTotal_Address(devices, 0xff100010, &temp2, 1);
	}

	char Log_str[100];
	memset(data->sensorid, 0, SENSORID_LENGTH);
	sprintf_s(Log_str, "0x%04x%04x%02x", temp1 & 0xffff, temp0 & 0xffff, temp2 & 0xff);
	memcpy(data->sensorid, Log_str, strlen(Log_str));

	sprintf_s(Log_str, "sensorid=0x%04x%04x%02x|", temp1 & 0xffff, temp0 & 0xffff, temp2 & 0xff);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH-10)
		strcat_s(data->testString, Log_str);
	return 0;
}


char *getString(struct testdatainfo *data)
{
	if (data->Allresult == 0)
	{
		char Log_str[100];
		sprintf_s(Log_str, "Pass");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	else
	{
		char Log_str[100];
		char Log_str1[100];
		char Log_res[10];

		memset(Log_str, 0, 100);
		memset(Log_str1, 0, 100);
		memset(Log_res, 0, 10);
		sprintf_s(Log_res, "Fail");
		strcat_s(Log_str, Log_res);

		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(Log_str, data->resultString);
		strcpy_s(Log_str1, Log_str);
		memset(data->resultString, 0, 100);
		strcat_s(data->resultString, Log_str1);
	}

	return data->testString;
}

static void str_cut(char *str, int m, int n) //从m开始，截取n长的数据
{
	char str_tt[1000];
	memcpy(str_tt, str + m, n);
	memcpy(str, str_tt, n);
}

#define ACCESS _access  
#define MKDIR(a) _mkdir((a))  
static int CreatDir(char *pDir)
{
	int i = 0;
	int iRet;
	int iLen;
	char* pszDir;

	if (NULL == pDir)
	{
		return 0;
	}

	pszDir = _strdup(pDir);
	iLen = strlen(pszDir);

	// 创建中间目录  
	for (i = 0; i < iLen; i++)
	{
		if (pszDir[i] == '\\' || pszDir[i] == '/')
		{
			pszDir[i] = '\0';
			//如果不存在,创建  
			iRet = ACCESS(pszDir, 0);
			if (iRet != 0)
			{
				iRet = MKDIR(pszDir);
				if (iRet != 0)
				{
					return -1;
				}
			}
			pszDir[i] = '/';
		}
	}

//	iRet = MKDIR(pszDir);
	free(pszDir);
	return iRet;
}


static int GettDir(char *pDir,char *pDir_out)
{
	int i = 0;
	int iLen;
	char* pszDir;

	if (NULL == pDir)
	{
		return 0;
	}

	pszDir = _strdup(pDir);
	iLen = strlen(pszDir);

	// 创建中间目录  
	int retnum = 0;
	for (i = 0; i < iLen; i++)
	{
		if (pszDir[i] == '\\' || pszDir[i] == '/')
		{
			pszDir[i] = '/';
			retnum = i;
		}
	}

	memcpy(pDir_out, pDir, retnum + 1);
	free(pszDir);
	return retnum + 1;
}


int gx_saveinfotoexcel(int devices, struct testdatainfo *data, char *LogFileName)
{
// 	char log_dir_str[200];
// 	sprintf_s(log_dir_str, "%s\r\n", LogFileName);
// 	FWriteLog("./Silead/dirlog.txt", log_dir_str);

	if (LogFileName == NULL || LogFileName == "")
		return 1;
	bool savetotal = TRUE;
	char TestToolDirect[500];//用于保存工具的当前目录，
	char strResult[1000];
	char FilePath[1000];//D:\用户目录\我的文档\2016\10\17\1111_OF1017-154210-00000
	char q[] = "LogData.csv";
	FILE *fp;
	GetCurrentDirectoryA(490, TestToolDirect);   //获取当前路径

	int ret = CreatDir(LogFileName);
	if (ret < 0)
		return 1;

	char Logdata[500];
	memset(Logdata, 0, 500);
	ret = GettDir(LogFileName, Logdata);
	if (ret <= 0)
	{
		savetotal = FALSE;
	}
	strcpy_s(FilePath, Logdata);
	strcat_s(FilePath, q);

	char Log_time_str[100];
	SYSTEMTIME sys;
	GetLocalTime(&sys);
	sprintf_s(Log_time_str, "%4d-%2d-%2d %2d:%2d:%2d", sys.wYear, sys.wMonth, sys.wDay, sys.wHour, sys.wMinute, sys.wSecond);

	if (fopen_s(&fp, LogFileName, "r") != 0)    //打开文件;若不为0则说明没有此文件名，创建之;
	{
		char *str = "Time,Sensorid,AllResult,vdd,leakage_rstdown,leakage_rstup,leadkage_work,\
					mosi_res_up,mosi_res_down,miso_res_up,miso_res_down,\
					sck_res_up,sck_res_down,cs_res_up,cs_res_down,\
					irq_res_up,irq_res_down,rst_res_up,rst_res_down,\
					mtf_data1,mtf_data2,mtf_data3,mtf_data4,mtf_data5,mtf_mag,mtf_mag_max,mtf_cnt_num,\
					shading1,shading2,shading3,shading4,shading5,\
					lightdeadpont_all,lightdeadpont_max,lightdeadpont_center,lightdeadpont_edge,\
					darkdeadpont_all,darkdeadpont_max,darkdeadpont_center,darkdeadpont_edge,\
					pkgAlgraw,aperture,maxbrightness,uniformity,\
					gx_center,flashid,sram_result\n";

		fopen_s(&fp, LogFileName, "wt");
			fputs(str, fp);
			fclose(fp);
	}
	else
		fclose(fp);
	if (fopen_s(&fp, LogFileName, "r") == 0)    //打开文件;若不为0则说明没有此文件名，创建之;
	{
		fclose(fp);
		memset(strResult, 0, sizeof(strResult));
		sprintf_s(strResult,"%s,%s,%s,%.2f,%d,%d,%d,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,\
							%.2f,%.2f,%.2f,%.2f,%.2f,\
							%d,%d,%d,%d,\
							%d,%d,%d,%d,\
							%d,%d,%d,%d,\
							%.2f,0x%08x,%08x\n",
			Log_time_str, data->sensorid, data->resultString, data->Op_Avdd_vol, data->Avdd_current_down, data->Avdd_current_up, data->Avdd_current_work,
			data->mosi_Res_up,data->mosi_Res_down,data->miso_Res_up,data->miso_Res_down,
			data->sck_Res_up,data->sck_Res_down,data->ss_Res_up,data->ss_Res_down,
			data->irq_Res_up,data->irq_Res_down,data->rst_Res_up,data->rst_Res_down,
			data->mtf_data[0], data->mtf_data[1], data->mtf_data[2], data->mtf_data[3], data->mtf_data[4],data->mtf_mag,data->mtf_mag_max,data->mtf_cnt,
			data->shading[0], data->shading[1], data->shading[2], data->shading[3], data->shading[4],
			data->all_lightdeadpoint, data->max_lightdeadpoint, data->center_lightdeadpoint, data->edge_lightdeadpoint,
			data->all_darkdeadpoint, data->max_darkdeadpoint, data->center_darkdeadpoint, data->edge_darkdeadpoint,
			data->pkgdata, data->apture, data->maxbrightness, data->iNumCount,
			data->distance, data->flashid, data->sram_result);
		FWriteLog(LogFileName, strResult);
	}

	/*********************  保存总log  **********************/
	if (savetotal == FALSE)
		return 0;
	if (fopen_s(&fp, FilePath, "r") != 0)    //打开文件;若不为0则说明没有此文件名，创建之;
	{
		char *str = "Time,Sensorid,AllResult,vdd,leakage_rstdown,leakage_rstup,leadkage_work,\
					mosi_res_up,mosi_res_down,miso_res_up,miso_res_down,\
					sck_res_up,sck_res_down,cs_res_up,cs_res_down,\
					irq_res_up,irq_res_down,rst_res_up,rst_res_down,\
					mtf_data1,mtf_data2,mtf_data3,mtf_data4,mtf_data5,mtf_mag,mtf_mag_max,mtf_cnt_num,\
					shading1,shading2,shading3,shading4,shading5,\
					lightdeadpont_all,lightdeadpont_max,lightdeadpont_center,lightdeadpont_edge,\
					darkdeadpont_all,darkdeadpont_max,darkdeadpont_center,darkdeadpont_edge,\
					pkgAlgraw,aperture,maxbrightness,uniformity,\
					gx_center,flashid,sram_result\n";

		fopen_s(&fp, FilePath, "wt");
			fputs(str, fp);
			fclose(fp);
	}
	else
		fclose(fp);
	if (fopen_s(&fp, FilePath, "r") == 0)    //打开文件;若不为0则说明没有此文件名，创建之;
	{
		fclose(fp);
		memset(strResult, 0, sizeof(strResult));
		sprintf_s(strResult,"%s,%s,%s,%.2f,%d,%d,%d,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,\
							%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,\
							%.2f,%.2f,%.2f,%.2f,%.2f,\
							%d,%d,%d,%d,\
							%d,%d,%d,%d,\
							%d,%d,%d,%d,\
							%.2f,0x%08x,%08x\n",
			Log_time_str, data->sensorid, data->resultString, data->Op_Avdd_vol, data->Avdd_current_down, data->Avdd_current_up, data->Avdd_current_work,
			data->mosi_Res_up,data->mosi_Res_down,data->miso_Res_up,data->miso_Res_down,
			data->sck_Res_up,data->sck_Res_down,data->ss_Res_up,data->ss_Res_down,
			data->irq_Res_up,data->irq_Res_down,data->rst_Res_up,data->rst_Res_down,
			data->mtf_data[0], data->mtf_data[1], data->mtf_data[2], data->mtf_data[3], data->mtf_data[4],data->mtf_mag,data->mtf_mag_max,data->mtf_cnt,
			data->shading[0], data->shading[1], data->shading[2], data->shading[3], data->shading[4],
			data->all_lightdeadpoint, data->max_lightdeadpoint, data->center_lightdeadpoint, data->edge_lightdeadpoint,
			data->all_darkdeadpoint, data->max_darkdeadpoint, data->center_darkdeadpoint, data->edge_darkdeadpoint,
			data->pkgdata, data->apture, data->maxbrightness, data->iNumCount,
			data->distance, data->flashid, data->sram_result);
		FWriteLog(FilePath, strResult);
	}

	_chdir(TestToolDirect);
	return 0;
}


void gx_io1_output(int devices, unsigned int data)
{
	SL_GPIO_IO1(devices, data);
}


void gx_id_output(int devices, unsigned int data)
{
	SL_GPIO_ID(devices, data);
}


static char *substring(const char* ch, int pos, int length)
{
	const char* pch = ch;
	//定义一个字符指针，指向传递进来的ch地址。  
	char* subch = (char*)calloc(sizeof(char), length + 1);
	//通过calloc来分配一个length长度的字符数组，返回的是字符指针。  
	int i;
	//只有在C99下for循环中才可以声明变量，这里写在外面，提高兼容性。  
	pch = pch + pos;
	//是pch指针指向pos位置。  
	for (i = 0; i < length; i++)
	{
		subch[i] = *(pch++);
		//循环遍历赋值数组。  
	}
	subch[length] = '\0';//加上字符串结束符。  
	return subch;       //返回分配的字符数组地址。  
}

int SPI_WTotal_Address_n(int dev, unsigned int addr, unsigned int *buf, int len_int, int n)
{
	for (int i = 0; i < n; i++)
	{
		SPI_WTotal_Address(dev, addr, buf, len_int);
		sl_delay(50);
	}
	return 0;
}


int  gx_checkotp(int devices, const char *str_code_qrnum, struct testdatainfo *data)
{
	int result_otp = 0;
	int resutl_already_write = 0;

//	SL_AvddOutput(devices, 0);
//	sl_delay(10);
//	SL_AvddOutput(devices, 1);
//	sl_delay(30);
//	SL_ResetOutput(devices, 0);
//	sl_delay(10);
//	SL_ResetOutput(devices, 1);
//	sl_delay(10);
//	unsigned int temp_fc = 0;
//	gx_read(devices, 0xfc, &temp_fc, 1);
	SL_ResetOutput(devices, 0);
	sl_delay(10);
	SL_ResetOutput(devices, 1);
	sl_delay(50);

	unsigned int Otp_addr_7011a[8] = { 0xff100000, 0xff100004, 0xff100008, 0xff10000c, 0xff100010, 0xff100014, 0xff100018, 0xff10001c };
	unsigned int Otp_High16Bit_7011a[8] = { 0x00DD, 0x007F, 0x0075, 0x00F5, 0x00D7, 0x0077, 0x00ff, 0x005d };

	unsigned int data_otp = 0;
	for (int i = 0; i < 8; i++)
	{
		if ((Otp_addr_7011a[i] == 0xff100000) || (Otp_addr_7011a[i] == 0xff100004))
			continue;
		else if (Otp_addr_7011a[i] == 0xff100010)
		{
			SPI_RTotal_Address(devices, Otp_addr_7011a[i], &data_otp, 1);
			if ((data_otp & 0x0000ff00) != 0)
			{
				resutl_already_write = 1;  //表示Otp里已经有值
			}
		}
		else
		{

			SPI_RTotal_Address(devices, Otp_addr_7011a[i], &data_otp, 1);
			if (data_otp != 0)
			{
				resutl_already_write = 1;  //表示Otp里已经有值
				//break;
			}
		}
	}


	unsigned int data_byte1 = 0;
	unsigned int data_byte2 = 0;
	unsigned int data_byte3 = 0;
	unsigned int data_byte4 = 0;
	unsigned int data_byte5 = 0;
	unsigned int data_byte6 = 0;
	unsigned int data_byte7 = 0;
	unsigned int data_byte8 = 0;
	unsigned int data_byte9 = 0;
	unsigned int data_byte10 = 0;
	unsigned int data_byte11 = 0;

	char *hwCode = substring(str_code_qrnum, 5, 3);
	char *moderFactory = substring(str_code_qrnum, 8, 1);
	char *moderId = substring(str_code_qrnum, 9, 1);

	unsigned int hw_code = atoi(hwCode);

	data_byte1 = (hw_code >> 2) & 0xff;

	unsigned int mode_fac = 0;
	if ((moderFactory[0] == 'C') || (moderFactory[0] == 'c'))
		mode_fac = 3;
	else if ((moderFactory[0] == 'F') || (moderFactory[0] == 'f'))
		mode_fac = 0;
	else if((moderFactory[0] == 'P') || (moderFactory[0] == 'p'))
		mode_fac = 2;
	else if ((moderFactory[0] == 'Q') || (moderFactory[0] == 'q'))
		mode_fac = 1;
	else if ((moderFactory[0] == 'T') || (moderFactory[0] == 't'))
		mode_fac = 5;
	else if ((moderFactory[0] == 'L') || (moderFactory[0] == 'l'))
		mode_fac = 4;
	else
		mode_fac = 5;

	unsigned int mode_id = 0;
	if (moderId[0] == 'A' || moderId[0] == 'a')
		mode_id = 0;
	else if (moderId[0] == 'B' || moderId[0] == 'b')
		mode_id = 1;
	else if (moderId[0] == 'C' || moderId[0] == 'c')
		mode_id = 2;
	else if (moderId[0] == 'D' || moderId[0] == 'd')
		mode_id = 3;
	else if (moderId[0] == 'E' || moderId[0] == 'e')
		mode_id = 4;
	else if (moderId[0] == 'F' || moderId[0] == 'f')
		mode_id = 5;
	else if (moderId[0] == 'G' || moderId[0] == 'g')
		mode_id = 6;

	data_byte2 = mode_id + (mode_fac << 3) + ((hw_code & 0x3) << 6);


	char *chip_fc = substring(str_code_qrnum, 10, 1);
	char *package = substring(str_code_qrnum, 11, 1);

	unsigned int chip_fac = 0;
	if (chip_fc[0] == '1')
		chip_fac = 0;
	else if (chip_fc[0] == '2')
		chip_fac = 1;
	else if (chip_fc[0] == '3')
		chip_fac = 2;
	else if (chip_fc[0] == '4')
		chip_fac = 3;
	else if (chip_fc[0] == '5')
		chip_fac = 4;
	else if (chip_fc[0] == '6')
		chip_fac = 5;
	else if (chip_fc[0] == '7')
		chip_fac = 6;
	else
		chip_fac = 0;

	unsigned int t_package = 0;
	if (package[0] == '1')
		t_package = 0;
	else if (package[0] == '2')
		t_package = 1;
	else if (package[0] == '3')
		t_package = 2;
	else if (package[0] == '4')
		t_package = 3;
	else if (package[0] == '5')
		t_package = 4;
	else if (package[0] == '6')
		t_package = 5;
	else if (package[0] == '7')
		t_package = 6;
	else if (package[0] == '8')
		t_package = 7;
	else if (package[0] == '9')
		t_package = 8;
	else if ((package[0] == 'A') || (package[0] == 'a'))
		t_package = 9;
	else if ((package[0] == 'B') || (package[0] == 'b'))
		t_package = 10;
	else if ((package[0] == 'Z') || (package[0] == 'z'))
		t_package = 0xf;
	else
		t_package = 0;

	data_byte3 = (chip_fac << 4) + t_package;

	char *color = substring(str_code_qrnum, 12, 1);
	data_byte4 = color[0];

	char *hwAppe = substring(str_code_qrnum, 13, 1);
	unsigned int mode_appe = 0;
	if (hwAppe[0] == '1')
		mode_appe = 0;
	else if (hwAppe[0] == '2')
		mode_appe = 1;
	else if (hwAppe[0] == '3')
		mode_appe = 2;
	else if (hwAppe[0] == '4')
		mode_appe = 3;
	else if (hwAppe[0] == '5')
		mode_appe = 4;
	else if (hwAppe[0] == '6')
		mode_appe = 5;
	else if (hwAppe[0] == '9')
		mode_appe = 0xf;

	char *FPC_fac = substring(str_code_qrnum, 14, 1);
	unsigned int mode_fpc = 0;
	if (FPC_fac[0] == '1')
		mode_fpc = 0;
	else if (FPC_fac[0] == '2')
		mode_fpc = 1;
	else if (FPC_fac[0] == '3')
		mode_fpc = 2;
	else if (FPC_fac[0] == '4')
		mode_fpc = 3;

	char *year_str = substring(str_code_qrnum, 15, 1);
	unsigned int year = 0;
	if (year_str[0] == '6')
		year = 0;
	else if (year_str[0] == '7')
		year = 1;
	else if (year_str[0] == '8')
		year = 2;
	else if (year_str[0] == '9')
		year = 3;
	else if (year_str[0] == 'A')
		year = 4;
	else if (year_str[0] == 'B')
		year = 5;
	else if (year_str[0] == 'C')
		year = 6;
	else if (year_str[0] == 'D')
		year = 7;

	char *month_str = substring(str_code_qrnum, 16, 1);
	unsigned int month = 0;
	if (month_str[0] == '1')
		month = 1;
	else if (month_str[0] == '2')
		month = 2;
	else if (month_str[0] == '3')
		month = 3;
	else if (month_str[0] == '4')
		month = 4;
	else if (month_str[0] == '5')
		month = 5;
	else if (month_str[0] == '6')
		month = 6;
	else if (month_str[0] == '7')
		month = 7;
	else if (month_str[0] == '8')
		month = 8;
	else if (month_str[0] == '9')
		month = 9;
	else if ((month_str[0] == 'A') || (month_str[0] == 'a'))
		month = 0xa;
	else if ((month_str[0] == 'B') || (month_str[0] == 'b'))
		month = 0xb;
	else if ((month_str[0] == 'C') || (month_str[0] == 'c'))
		month = 0xc;

	char *date_str = substring(str_code_qrnum, 17, 2);
	unsigned int date = atoi(date_str);

	char *linebody_str = substring(str_code_qrnum, 19, 1);
	unsigned int linebody = atoi(linebody_str) - 1;

	char *backup_str = substring(str_code_qrnum, 20, 1);
	unsigned int backup = 0;
	if (backup_str[0] == '1')
		backup = 0;
	else if (backup_str[0] == '2')
		backup = 1;
	else if (backup_str[0] == 'R')
		backup = 2;

	data_byte5 = (mode_appe << 4) + (mode_fpc << 2) + (date >> 3);
	data_byte6 = (month >> 2) + (year << 2) + ((date & 0x7) << 5);
	data_byte7 = ((month & 0x3) << 6) + (linebody << 2) + backup;

	char *numerical_str_byte8 = substring(str_code_qrnum, 21, 1);
	data_byte8 = numerical_str_byte8[0];

	char *numerical_str_byte9 = substring(str_code_qrnum, 22, 1);
	data_byte9 = numerical_str_byte9[0];

	char *numerical_str_byte10 = substring(str_code_qrnum, 23, 1);
	data_byte10 = numerical_str_byte10[0];

	char *numerical_str_byte11 = substring(str_code_qrnum, 24, 1);
	data_byte11 = numerical_str_byte11[0];

	if (resutl_already_write == 0)
	{
		unsigned int data_write = ((Otp_High16Bit_7011a[2] << 16) & 0xffff0000) + ((data_byte2 << 8) & 0xff00) + (data_byte1 & 0xff);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[2], &data_write, 1, 2);   //xie byte 2-1

		data_write = ((Otp_High16Bit_7011a[4] << 16) & 0xffff0000) + ((data_byte3 << 8) & 0xff00);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[4], &data_write, 1, 2);   //xie byte 3

		data_write = ((Otp_High16Bit_7011a[3] << 16) & 0xffff0000) + ((data_byte5 << 8) & 0xff00) + (data_byte4 & 0xff);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[3], &data_write, 1, 2);   //xie byte 5-4

		data_write = ((Otp_High16Bit_7011a[5] << 16) & 0xffff0000) + ((data_byte7 << 8) & 0xff00) + (data_byte6 & 0xff);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[5], &data_write, 1, 2);   //xie byte 7-6

		data_write = ((Otp_High16Bit_7011a[6] << 16) & 0xffff0000) + ((data_byte9 << 8) & 0xff00) + (data_byte8 & 0xff);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[6], &data_write, 1, 2);   //xie byte 9-8

		data_write = ((Otp_High16Bit_7011a[7] << 16) & 0xffff0000) + ((data_byte11 << 8) & 0xff00) + (data_byte10 & 0xff);
		SPI_WTotal_Address_n(devices, Otp_addr_7011a[7], &data_write, 1, 2);   //xie byte 11-10


		//SL_AvddOutput(devices, 0);
		//sl_delay(10);
		//SL_AvddOutput(devices, 1);
		//sl_delay(30);
		//SL_ResetOutput(devices, 0);
		//sl_delay(10);
		//SL_ResetOutput(devices, 1);
		//sl_delay(10);
		//gx_read(devices, 0xfc, &temp_fc, 1);
		//SL_ResetOutput(devices, 0);
		//sl_delay(50);
		//SL_ResetOutput(devices, 1);
		//sl_delay(50);
	}


	unsigned int data_read12 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[2], &data_read12, 1);
	if ((data_read12 & 0xffff) != (((data_byte2 << 8) & 0xff00) + (data_byte1 & 0xff)))
		result_otp = 1;

	unsigned int data_read3 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[4], &data_read3, 1);
	if ((data_read3 & 0xff00) != ((data_byte3 << 8) & 0xff00))
		result_otp = 1;

	unsigned int data_read45 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[3], &data_read45, 1);
	if ((data_read45 & 0xffff) != (((data_byte5 << 8) & 0xff00) + (data_byte4 & 0xff)))
		result_otp = 1;

	unsigned int data_read67 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[5], &data_read67, 1);
	if ((data_read67 & 0xffff) != (((data_byte7 << 8) & 0xff00) + (data_byte6 & 0xff)))
		result_otp = 1;

	unsigned int data_read89 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[6], &data_read89, 1);
	if ((data_read89 & 0xffff) != (((data_byte9 << 8) & 0xff00) + (data_byte8 & 0xff)))
		result_otp = 1;

	unsigned int data_read1011 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[7], &data_read1011, 1);
	if ((data_read1011 & 0xffff) != (((data_byte11 << 8) & 0xff00) + (data_byte10 & 0xff)))
		result_otp = 1;

	char otp_datainfo[200];
	memset(otp_datainfo, 0, 200);
	sprintf_s(otp_datainfo, "0x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
		data_read12 & 0xff, (data_read12 >> 8) & 0xff, (data_read3 >> 8) & 0xff, data_read45 & 0xff,
		(data_read45 >> 8) & 0xff, data_read67 & 0xff, (data_read67 >> 8) & 0xff, data_read89 & 0xff,
		(data_read89 >> 8) & 0xff, data_read1011 & 0xff, (data_read1011 >> 8) & 0xff);
	memset(data->otpdatainfo, 0, 200);
	strcat_s(data->otpdatainfo, otp_datainfo);


	char Log_str[100] = { 0 };
	sprintf_s(Log_str, "Otp=0x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x|",
		data_read12 & 0xff, (data_read12 >> 8) & 0xff, (data_read3 >> 8) & 0xff, data_read45 & 0xff,
		(data_read45 >> 8) & 0xff, data_read67 & 0xff, (data_read67 >> 8) & 0xff, data_read89 & 0xff,
		(data_read89 >> 8) & 0xff, data_read1011 & 0xff, (data_read1011 >> 8) & 0xff);
	strcat_s(data->testString, Log_str);

	if (result_otp != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 99;
		sprintf_s(Log_str, "_99");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	return result_otp;
}



int  gx_readotp(int devices, struct testdatainfo *data)
{
	int result_otp = 0;
	int resutl_already_write = 0;

//	SL_AvddOutput(devices, 0);
//	sl_delay(10);
//	SL_AvddOutput(devices, 1);
//	sl_delay(30);
//	SL_ResetOutput(devices, 0);
//	sl_delay(10);
//	SL_ResetOutput(devices, 1);
//	sl_delay(10);
//	unsigned int temp_fc = 0;
//	gx_read(devices, 0xfc, &temp_fc, 1);
	SL_ResetOutput(devices, 0);
	sl_delay(10);
	SL_ResetOutput(devices, 1);
	sl_delay(50);

	unsigned int Otp_addr_7011a[8] = { 0xff100000, 0xff100004, 0xff100008, 0xff10000c, 0xff100010, 0xff100014, 0xff100018, 0xff10001c };
	unsigned int Otp_High16Bit_7011a[8] = { 0x00DD, 0x007F, 0x0075, 0x00F5, 0x00D7, 0x0077, 0x00ff, 0x005d };

	unsigned int data_otp = 0;
	for (int i = 0; i < 8; i++)
	{
		if ((Otp_addr_7011a[i] == 0xff100000) || (Otp_addr_7011a[i] == 0xff100004))
			continue;
		else if (Otp_addr_7011a[i] == 0xff100010)
		{
			SPI_RTotal_Address(devices, Otp_addr_7011a[i], &data_otp, 1);
			if ((data_otp & 0x0000ff00) != 0)
			{
				resutl_already_write = 1;  //表示Otp里已经有值
			}
		}
		else
		{

			SPI_RTotal_Address(devices, Otp_addr_7011a[i], &data_otp, 1);
			if (data_otp != 0)
			{
				resutl_already_write = 1;  //表示Otp里已经有值
				//break;
			}
		}
	}

	if (resutl_already_write == 0)
		result_otp = 1;

	unsigned int data_read12 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[2], &data_read12, 1);

	unsigned int data_read3 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[4], &data_read3, 1);

	unsigned int data_read45 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[3], &data_read45, 1);

	unsigned int data_read67 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[5], &data_read67, 1);

	unsigned int data_read89 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[6], &data_read89, 1);

	unsigned int data_read1011 = 0;
	SPI_RTotal_Address(devices, Otp_addr_7011a[7], &data_read1011, 1);

	char otp_datainfo[200];
	memset(otp_datainfo, 0, 200);
	sprintf_s(otp_datainfo, "0x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
		data_read12 & 0xff, (data_read12 >> 8) & 0xff, (data_read3 >> 8) & 0xff, data_read45 & 0xff,
		(data_read45 >> 8) & 0xff, data_read67 & 0xff, (data_read67 >> 8) & 0xff, data_read89 & 0xff,
		(data_read89 >> 8) & 0xff, data_read1011 & 0xff, (data_read1011 >> 8) & 0xff);
	memset(data->otpdatainfo, 0, 200);
	strcat_s(data->otpdatainfo, otp_datainfo);


	char Log_str[100] = { 0 };
	sprintf_s(Log_str, "Otp=0x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x|",
		data_read12 & 0xff, (data_read12 >> 8) & 0xff, (data_read3 >> 8) & 0xff, data_read45 & 0xff,
		(data_read45 >> 8) & 0xff, data_read67 & 0xff, (data_read67 >> 8) & 0xff, data_read89 & 0xff,
		(data_read89 >> 8) & 0xff, data_read1011 & 0xff, (data_read1011 >> 8) & 0xff);
	strcat_s(data->testString, Log_str);

	if (result_otp != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 99;
		sprintf_s(Log_str, "_99");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	return result_otp;
}


int gx_darkniose_t(int devices, struct testdatainfo *data)
{

	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);

	DacSet(devices, 0x800);

	for (int i = 0; i < IFIAMGENUM; i++)
	{
		SL_GetImagebuffer(devices, image_s + i*w*h, w, h);
// 		if (i == 4)
// 			memcpy(data->image, image + i*w*h, w*h);
	}

	for (int i = 0; i < IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image + 2 * w*h, w*h);

	memcpy(data->imageDarknoise, image, w*h*IFIAMGENUM);
	float noise = 0.0;
	gx_noisetest(image, w, h, &noise, IFIAMGENUM);

	data->Darknoise = noise;


	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean > DARKSOURCE_MEAN)
		mean_result = 1;

	char Log_str[100] = { 0 };
	if ((data->Darknoise > setting.darknoise_max) || (data->Darknoise < setting.darknoise_min) || (mean_result != 0))
		sprintf_s(Log_str, "Darknoise_=Fail_%.2f|", data->Darknoise);
	else
		sprintf_s(Log_str, "Darknoise_=Pass_%.2f|", data->Darknoise);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if ((data->Darknoise > setting.darknoise_max) || (data->Darknoise < setting.darknoise_min) || (mean_result != 0))
	{
		if (data->Allresult == 0)
			data->Allresult = 17;
		sprintf_s(Log_str, "_17");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}


	free(image);
	free(image_s);
	if ((data->Darknoise > setting.darknoise_max) || (data->Darknoise < setting.darknoise_min) || (mean_result != 0))
		return 1;
	else
		return 0;
}


int gx_brightniose_t(int devices, struct testdatainfo *data)
{

	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM);

	for (int i = 0; i < IFIAMGENUM; i++)
	{
		SL_GetImagebuffer(devices, image_s + i*w*h, w, h);
// 		if (i == 4)
// 			memcpy(data->image, image + i*w*h, w*h);
	}

	for (int i = 0; i < IMAGE_WIDTH*IMAGE_HEIGHT*IFIAMGENUM; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->imageBrightnoise, image, w*h*IFIAMGENUM);
	float noise = 0.0;
	gx_noisetest(image, w, h, &noise, IFIAMGENUM);

	data->Brightnoise = noise;


	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	char Log_str[100] = { 0 };
	if ((data->Brightnoise > setting.brightnoise_max) || (data->Brightnoise < setting.brightnoise_min) || (mean_result != 0))
		sprintf_s(Log_str, "Brightnoise_=Fail_%.2f|", data->Brightnoise);
	else
		sprintf_s(Log_str, "Brightnoise_=Pass_%.2f|", data->Brightnoise);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if ((data->Brightnoise > setting.brightnoise_max) || (data->Brightnoise < setting.brightnoise_min) || (mean_result != 0))
	{
		if (data->Allresult == 0)
			data->Allresult = 18;
		sprintf_s(Log_str, "_18");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}

	free(image);
	free(image_s);
	if ((data->Brightnoise > setting.brightnoise_max) || (data->Brightnoise < setting.brightnoise_min) || (mean_result != 0))
		return 1;
	else
		return 0;
}



int gx_fov_t(int devices, struct testdatainfo *data)
{

	int w = sparam.width_org;
	int h = sparam.height_org;
	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);

	int oio = 0;
	int ojo = 0;
	gx_fov(image, w, h, 20, &oio, &ojo);

	data->fovdata = gx_fov(image, w, h, 20, &oio, &ojo);


	int light_sum = 0;
	int light_num = 0;
	int ret = 0;
	for (int j = 20; j < h - 20; j++)
	{
		for (int i = 0; i < w; i++)
		{
			light_sum += data->image[i + j*w];
			light_num++;
		}
	}
	int mean = 0;
	int mean_result = 0;
	if (light_num != 0)
		mean = light_sum / light_num;
	else
		mean = 0;
	if (mean < BRIGHTSOURCE_MEAN)
		mean_result = 1;

	char Log_str[100] = { 0 };
	if ((data->fovdata > setting.fov_max) || (data->fovdata < setting.fov_min) || (mean_result == 1))
		sprintf_s(Log_str, "Fov_=Fail_%d|", data->fovdata);
	else
		sprintf_s(Log_str, "Fov_=Pass_%d|", data->fovdata);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if ((data->fovdata > setting.fov_max) || (data->fovdata < setting.fov_min) || (mean_result == 1))
	{
		if (data->Allresult == 0)
			data->Allresult = 19;
		sprintf_s(Log_str, "_19");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}


	free(image);
	free(image_s);
	if ((data->fovdata > setting.fov_max) || (data->fovdata < setting.fov_min) || (mean_result == 1))
		return 1;
	else
		return 0;
}

#endif


int gx_maxbrightness_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;

	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	DacSet(devices, 0x200);
	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);

	int maxbrightness = 0;
	gx_maxbrightness(image, w, h, &maxbrightness);
	data->maxbrightness = maxbrightness;

	free(image);
	free(image_s);
	int ret = 0;
	if (data->maxbrightness < setting.maxbrightness)
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "maxbrightness=Fail_%d|", data->maxbrightness);
	else
		sprintf_s(Log_str, "maxbrightness=Pass_%d|", data->maxbrightness);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 20;
		sprintf_s(Log_str, "_20");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}

int gx_uniformity_t(int devices, struct testdatainfo *data)
{
	int w = sparam.width_org;
	int h = sparam.height_org;

	int *image_s = (int*)malloc(w*h * 4);
	unsigned char *image = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image, 0, IMAGE_WIDTH*IMAGE_HEIGHT);
	unsigned char *image_out = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT);
	memset(image_out, 0, IMAGE_WIDTH*IMAGE_HEIGHT);

	SL_GetImagebuffer(devices, image_s, w, h);
	for (int i = 0; i < w*h; i++)
		image[i] = (image_s[i] >> 8) & 0xff;

	memcpy(data->image, image, w*h);

	int maxbrightness = 0;
	int iNumCount = 0;
	gx_uniformity(image, w, h, image_out, setting.th_imean, setting.th_ird, 0, &iNumCount);
	data->maxbrightness = maxbrightness;
	data->iNumCount = iNumCount;

	free(image);
	free(image_s);
	free(image_out);
	int ret = 0;
	if (data->iNumCount>setting.th_iNummax)
		ret = 1;
	else
		ret = 0;

	char Log_str[100] = { 0 };
	if (ret != 0)
		sprintf_s(Log_str, "uniformity=Fail_%d_%d|", data->maxbrightness, data->iNumCount);
	else
		sprintf_s(Log_str, "uniformity=Pass_%d_%d|", data->maxbrightness, data->iNumCount);

	if ((strlen(data->testString) + strlen(Log_str)) < TESTSTRING_LENGTH - 10)
		strcat_s(data->testString, Log_str);

	if (ret != 0)
	{
		if (data->Allresult == 0)
			data->Allresult = 21;
		sprintf_s(Log_str, "_21");
		if ((strlen(data->resultString) + strlen(Log_str)) < TESTRESULT_LENGTH - 10)
			strcat_s(data->resultString, Log_str);
	}
	return ret;
}