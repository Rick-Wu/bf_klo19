#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "snr.h"
float sqrta(float n)
{
	float ans = n;
	while (((ans*ans - n) > 0.001) || ((ans*ans - n) < -0.001))
	{
		ans = (ans + n / ans) / 2;
	}
	return ans;
}


int fMax(unsigned char* segment, int cell_width, int cell_hight)
{
	int iHist[256] = { 0 };
	int iHistSum = 0, iHisthigh = 0;
	for (int i = 0; i < cell_hight; i++)
	{
		for (int j = 0; j < cell_width; j++)
		{
			iHist[*(segment + i*cell_width + j)]++;
		}
	}
	for (int i = 255; i > 0; i--)
	{
		iHistSum += iHist[i];
		if (iHistSum >= 0.1*cell_width*cell_hight)
		{
			iHisthigh = i;
			break;
		}
	}
	return iHisthigh;
}

int fMin(unsigned char* segment, int cell_width, int cell_hight)
{
	int iHist[256] = { 0 };
	int iHistSum = 0, iHistlow = 0;
	for (int i = 0; i < cell_hight; i++)
	for (int j = 0; j < cell_width; j++)
	{
		iHist[*(segment + i*cell_width + j)]++;
	}

	for (int i = 0; i < 256; i++)
	{
		iHistSum += iHist[i];
		if (iHistSum >= 0.1*cell_width*cell_hight)
		{
			iHistlow = i;
			break;
		}
	}
	return iHistlow;
}

int singleSignal(unsigned char* segment, int cell_width, int cell_hight)
{
	int iHist[256] = { 0 };
	int iHistSum = 0, iHistlow = 0, iHisthigh = 0, signal = 0;
	for (int i = 0; i < cell_hight; i++)
	for (int j = 0; j < cell_width; j++)
	{
		iHist[*(segment + i*cell_width + j)]++;
	}

	//去掉前后各10%像素点统计最大最小灰度值
	iHistSum = 0;
	for (int i = 0; i < 256; i++)
	{
		iHistSum += iHist[i];
		if (iHistSum >= 0.1*cell_width*cell_hight)
		{
			iHistlow = i;
			break;
		}
	}
	iHistSum = 0;
	for (int i = 255; i > 0; i--)
	{
		iHistSum += iHist[i];
		if (iHistSum >= 0.1*cell_width*cell_hight)
		{
			iHisthigh = i;
			break;
		}
	}
	signal = (iHisthigh - iHistlow);
	return signal;
}


float testSignal(unsigned char* spoofimg, int width, int height, float* Area)
{
	float signal = 0;
	int cellWidth = 11, cellHight = 11, threshold = 0;
	int xcrop = 0, ycrop = 0;
	int iCount = 0, iDots = 0;
	int iHistSum = 0, iHisthigh = 0, iHistlow = 0, maxHistMax = 0, minHistMax = 0;
	int MAX = 0, MIN = 0;
	int signalHist[256] = { 0 };
	int maxHist[256] = { 0 };
	int minHist[256] = { 0 };
	unsigned char* segmentspoof = (unsigned char*)malloc(cellWidth*cellHight);
	if (width > height)
	{
		xcrop = 15;
		ycrop = 12;
	}
	else
	{
		xcrop = 12;
		ycrop = 15;
	}
	//计算MAX,MIN和阈值
	for (int i = ycrop; i < height - ycrop - cellHight; i++)
	for (int j = xcrop; j < width - xcrop - cellWidth; j++)
	{
		for (int m = 0; m < cellHight; m++)
		{
			for (int n = 0; n < cellWidth; n++)
			{
				*(segmentspoof + m*cellWidth + n) = *(spoofimg + (i + m)*width + n + j);
			}
		}
		maxHist[fMax(segmentspoof, cellWidth, cellHight)]++;
		minHist[fMin(segmentspoof, cellWidth, cellHight)]++;
	}
	for (int i = 0; i < 256; i++)
	{
		if (maxHist[i] > maxHistMax)
		{
			maxHistMax = maxHist[i];
		}
		if (minHist[i] > minHistMax)
		{
			minHistMax = minHist[i];
		}
	}
	for (int i = 0; i < 256; i++)
	{
		if (maxHist[i] == maxHistMax)
		{
			MAX = i;
			break;
		}
	}
	for (int i = 0; i < 256; i++)
	{
		if (minHist[i] == minHistMax)
		{
			MIN = i;
			break;
		}
	}
	threshold = (int)((MAX - MIN) / 2);
	//筛选子块
	for (int i = ycrop; i < height - ycrop - cellHight; i++)
	for (int j = xcrop; j < width - xcrop - cellWidth; j++)
	{
		int signal = 0, max = 0, min = 255, maxgap = 0, mingap = 0;
		for (int m = 0; m < cellHight; m++)
		{
			for (int n = 0; n < cellWidth; n++)
			{
				*(segmentspoof + m*cellWidth + n) = *(spoofimg + (i + m)*width + n + j);
			}
		}
		max = fMax(segmentspoof, cellWidth, cellHight);
		min = fMin(segmentspoof, cellWidth, cellHight);
		maxgap = (((max - MAX) > 0) ? (max - MAX) : (MAX - max));
		mingap = (((min - MIN) > 0) ? (min - MIN) : (MIN - min));
		if (maxgap <= threshold && mingap <= threshold)
		{
			signal = singleSignal(segmentspoof, cellWidth, cellHight);
			signalHist[signal]++;
			iCount++;
		}
	}
	*Area = (float)(iCount*100.0 / ((height - 2 * ycrop - cellHight)*(width - 2 * xcrop - cellHight)));
	//前后截断并计算信号值
	for (int i = 0, iHistSum = 0; i <= 255; i++)
	{
		iHistSum += signalHist[i];
		if (iHistSum >= iCount*0.15)
		{
			iHistlow = i;
			break;
		}
	}
	for (int i = 255, iHistSum = 0; i >= 0; i--)
	{
		iHistSum += signalHist[i];
		if (iHistSum >= iCount*0.15)
		{
			iHisthigh = i;
			break;
		}
	}
	iHistSum = 0;
	iDots = 0;
	for (int i = iHistlow; i <= iHisthigh; i++)
	{
		iHistSum += i*signalHist[i];
		iDots += signalHist[i];
	}
	if (iDots > 0)
	{
		signal = (float)(iHistSum*1.0 / iDots);
	}
	else
	{
		signal = 0;
	}
	free(segmentspoof);
	return signal;
}

float testNoise(unsigned char* pImage, int width, int height, int iNumofImg, float fcrop = 0.2)
{
	int i = 0, j = 0, k = 0;
	float fMean = 0, fNoise = 0;
	int offset = (int)(((width > height) ? height : width)*fcrop);
	int iCrop = offset;
	int ** cImgblock = 0;
	cImgblock = (int**)malloc(sizeof(int*)*height);
	cImgblock[0] = (int*)malloc(sizeof(int)*height*width);
	for (i = 1; i < height; i++)
	{
		cImgblock[i] = cImgblock[i - 1] + width;
	}
	for (j = 0; j < height; j++)
	for (k = 0; k < width; k++)
	{
		cImgblock[j][k] = 0;
	}

	for (i = 0; i < iNumofImg; i++)
	for (j = 0; j < height; j++)
	{
		for (k = 0; k < width; k++)
		{
			cImgblock[j][k] += (int)*(i*width*height + (pImage + j*width + k));
		}
	}

	for (i = 0; i < height; i++)
	for (j = 0; j < width; j++)
	{
		cImgblock[i][j] /= iNumofImg;
		fMean += cImgblock[i][j];
	}

	fMean /= width*height;
	float fstd3 = 0;
	if (iNumofImg != 1)
	{
		for (j = iCrop; j < height - iCrop; j++)
		for (k = iCrop; k < width - iCrop; k++)
		{
			float iStd1 = 0;
			for (i = 0; i < iNumofImg; i++)
			{
				float iStd2 = 0;
				iStd2 = (float)(*(i*width*height + (pImage + j*width + k)) - cImgblock[j][k]);
				iStd1 += iStd2*iStd2;
			}
			fstd3 += sqrta(iStd1 / iNumofImg);
		}

		fstd3 /= (float)((width - 2 * iCrop)*(height - 2 * iCrop)*1.0);//jacky.		
	}
	free(cImgblock[0]);
	free(cImgblock);
	return fstd3;
}

float SNR(unsigned char* pWhiteImg, unsigned char* pSpoofImg, int width, int height, float* signalout, float* noiseout, int iNumofImg, float* activeArea)
{
	int xcrop = 15, ycrop = 15, cellsize = 11;
	float noise = 0, percent = 0, avgnoise = 0, iCount = 0;
	unsigned char* segmentwhite = (unsigned char*)malloc(cellsize*cellsize*iNumofImg);
	if (width > height)
	{
		xcrop = 15, ycrop = 12;
	}
	else
	{
		xcrop = 12, ycrop = 15;
	}
	for (int i = ycrop; i < height - ycrop - 11; i++)
	{
		for (int j = xcrop; j < width - xcrop - 11; j++)
		{
			for (int m = 0; m < 11; m++)
			{
				for (int n = 0; n < 11; n++)
				{
					for (int p = 0; p < iNumofImg; p++)
					{
						*(segmentwhite + m * 11 + n + p * 11 * 11) = *(pWhiteImg + (i + m)*width + n + j + p*width*height);
					}
				}
			}
			noise = testNoise(segmentwhite, 11, 11, iNumofImg, 0);
			avgnoise += noise;
			iCount++;
			
		}
	}
	*noiseout = avgnoise / iCount;
	*signalout = testSignal(pSpoofImg, width, height, &percent);
	*activeArea = percent;
	free(segmentwhite);
	return (float)((*signalout *1.0) / (*noiseout));
}
