#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "tune.h"

#if 0
void OpticalTune()
{
	int cutsize = 40;
	int greythreshold[2] = {150,200};
	const int maxloop = 10;
	const unsigned int ShutterAddr = 0xff090000;
	int loop = 0;
	unsigned int Shutter[maxloop] = { 0 };
	unsigned int imagemean[maxloop] = { 0 };
	int i = 0;
	int j = 0;
	unsigned int dark = 0x0;
	unsigned int bright = 0xfff;
	spi.SLFP_HWRegisterGet(ShutterAddr, Shutter[0]);
	dark += Shutter[0] & 0xfffff000;
	bright += Shutter[0] & 0xfffff000;

	for (loop = 0; loop < maxloop; loop++)
	{
		configOperation.SLFP_ContHWFrameGet(configOperation.imageBuffer, imageHeight*imageWidth, imageWidth, imageHeight, 0, NULL, HWAGCFlag, 0, 0);
		for (i = cutsize; i < imageHeight - cutsize; i++)
		{
			for (j = cutsize; j < imageWidth - cutsize; j++)
			{
				imagemean[loop] += configOperation.imageBuffer[i * imageWidth + j];
			}
		}
		imagemean[loop] = imagemean[loop] / ((imageHeight - cutsize)*(imageWidth - cutsize));

		if (imagemean[loop] > greythreshold[1])
		{
			Shutter[loop] = bright;
			Shutter[loop + 1] = (Shutter[loop] + dark);
			spi.SLFP_HWRegisterGet(ShutterAddr, Shutter[loop + 1]);
		}
		else if (imagemean[loop] < greythreshold[0])
		{
			Shutter[loop] = dark;
			Shutter[loop+1] = (Shutter[loop] + bright);
			spi.SLFP_HWRegisterGet(ShutterAddr, Shutter[loop + 1]);
		}
		else
		{
			break;
		}
	}
}

#endif