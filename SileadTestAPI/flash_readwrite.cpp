#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <string.h>
#include <windows.h>
#include "SileadCommLib.h"

static int Title[14] =
{
	0xAD1E5100, 0x00A01170,
	0x00A01170, 0xC8601200,
	0x01000300, 0x01000064,
	0x00010000, 0x00000000,
	0x02000064, 0x00700000,
	0x00000000, 0x03000064,
	0x00E00000, 0x00000000
};


void write_flash_title(int dev)
{
	unsigned char title_char[400] = { 0 };
	int len = sizeof(Title) / sizeof(Title[0]);
	for (int i = 0; i < len; i++)
	{
		title_char[i * 4 + 0] = (Title[i] >> 24) & 0xff;
		title_char[i * 4 + 1] = (Title[i] >> 16) & 0xff;
		title_char[i * 4 + 2] = (Title[i] >> 8) & 0xff;
		title_char[i * 4 + 3] = (Title[i] >> 0) & 0xff;
	}
	SPI_Write_Flash(dev, 0x0, title_char, len * 4);
}


int Flash_Test(int dev)
{
#define  address  0x0
	unsigned char title_char[4] = { 0x5a, 0x5a, 0x5a, 0x5a };
	SPI_SectorErase_Flash(dev, address);
	//SPI_Erase_Flash(dev);
	SPI_Write_Flash(dev, address, title_char, 4);
	Sleep(200);
	SPI_Read_Flash(dev, address, title_char, 4);
	if ((title_char[3] != 0x5a) || (title_char[2] != 0x5a) || (title_char[1] != 0x5a) || (title_char[0] != 0x5a))
	{
#if 0
		Sleep(100);
		SPI_SectorErase_Flash(dev, address);
		//SPI_Erase_Flash(dev);
		SPI_Write_Flash(dev, address, title_char, 4);
		Sleep(200);
		SPI_Read_Flash(dev, address, title_char, 4);
//		Sleep(60);
		if ((title_char[3] != 0x5a) || (title_char[2] != 0x5a) || (title_char[1] != 0x5a) || (title_char[0] != 0x5a))
		{
			return 1;
		}
		else
			return 0;
#endif
		return 1;
	}
	else
		return 0;
}

void Flash_Erase(int dev)
{
	SPI_Erase_Flash(dev);
}


void Flash_write(int dev, unsigned int addr, unsigned char *buf, int len)
{
	SPI_Write_Flash(dev, addr, buf, len);
}

void Flash_read(int dev, unsigned int addr, unsigned char *buf, int len)
{
	SPI_Read_Flash(dev, addr, buf, len);
}

int Write_DeadPixel(int dev, unsigned char *imageout, int w, int h)
{
	bool gray;
	int location = 0;
	int offset = 0;
	unsigned char deadpixel[160 * 160 / 8] = { 0 };
	for (int j = 0; j < h; j++)
	{
		for (int i = 0; i < w; i++)
		{
			if (imageout[i + j*w] == 255)
				gray = 1;
			else
				gray = 0;
			location = (i + j*w) % 8;
			offset = (i + j*w) / 8;
			deadpixel[offset] |= (gray << (7 - location));
		}
	}
	SPI_Write_Flash(dev, 0xe000, deadpixel, w * h / 8);
	return 0;
}

void Read_FlashId(int dev, unsigned char *buf)
{
	SPI_Read_FlashId(dev, buf);
}