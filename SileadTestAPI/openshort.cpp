
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <io.h>
#include "SileadCommLib.h"
#include "openshort.h"

#define SLAVEADDR      0x40
#define SLAVEADDR1     0x41

struct Debug_data
{
	unsigned int vdd_voltage_max;
	unsigned int vdd_voltage_min;
	unsigned int vdd_rstdown_current_max;
	unsigned int vdd_rstdown_current_min;
	unsigned int vdd_rstup_current_max;
	unsigned int vdd_rstup_current_min;
	unsigned int vdd_work_current_max;
	unsigned int vdd_work_current_min;
	int mosi_Res_down_max;
	int mosi_Res_down_min;
	int mosi_Res_up_max;
	int mosi_Res_up_min;
	int miso_Res_down_max;
	int miso_Res_down_min;
	int miso_Res_up_max;
	int miso_Res_up_min;
	int ss_Res_down_max;
	int ss_Res_down_min;
	int ss_Res_up_max;
	int ss_Res_up_min;
	int sck_Res_down_max;
	int sck_Res_down_min;
	int sck_Res_up_max;
	int sck_Res_up_min;
	int irq_Res_down_max;
	int irq_Res_down_min;
	int irq_Res_up_max;
	int irq_Res_up_min;
	int rst_Res_down_max;
	int rst_Res_down_min;
	int rst_Res_up_max;
	int rst_Res_up_min;
}setting_openshort;

static void FWriteLog(const char* fileName, char *logMsg)   //写入;
{
#if 1
	FILE *fLog;

#if 0//_UNICODE
	_wfopen_s(&fLog, fileName, TEXT("a+"));
#else
	fopen_s(&fLog, fileName, "a+");
#endif
	if (fLog == NULL)
	{
		return;
	}
#if 0 //_UNICODE
	fwprintf(fLog, TEXT("%s"), logMsg);
#else
	fprintf(fLog, "%s", logMsg);
#endif

	fflush(fLog);
	fclose(fLog);

#endif
}

static void GetDataFromIni(const char *InifileName)
{
	setting_openshort.vdd_voltage_max = GetPrivateProfileIntA("OpenShort", "vdd_voltage_max", 3100, InifileName);	
	setting_openshort.vdd_voltage_min = GetPrivateProfileIntA("OpenShort", "vdd_voltage_min", 2600, InifileName);
	setting_openshort.vdd_rstdown_current_max = GetPrivateProfileIntA("OpenShort", "vdd_rstdown_current_max", 100, InifileName);
	setting_openshort.vdd_rstdown_current_min = GetPrivateProfileIntA("OpenShort", "vdd_rstdown_current_min", 15, InifileName);
	setting_openshort.vdd_rstup_current_max = GetPrivateProfileIntA("OpenShort", "vdd_rstup_current_max", 10000, InifileName);
	setting_openshort.vdd_rstup_current_min = GetPrivateProfileIntA("OpenShort", "vdd_rstup_current_min", 7000, InifileName);
	setting_openshort.vdd_work_current_max = GetPrivateProfileIntA("OpenShort", "vdd_work_current_max", 10000, InifileName);
	setting_openshort.vdd_work_current_min = GetPrivateProfileIntA("OpenShort", "vdd_work_current_min", 7000, InifileName);
	setting_openshort.mosi_Res_down_max = GetPrivateProfileIntA("OpenShort", "mosi_Res_down_max", 300, InifileName);
	setting_openshort.mosi_Res_down_min = GetPrivateProfileIntA("OpenShort", "mosi_Res_down_min", 50, InifileName);
	setting_openshort.mosi_Res_up_max = GetPrivateProfileIntA("OpenShort", "mosi_Res_up_max", -1, InifileName);
	setting_openshort.mosi_Res_up_min = GetPrivateProfileIntA("OpenShort", "mosi_Res_up_min", 800, InifileName);
	setting_openshort.miso_Res_down_max = GetPrivateProfileIntA("OpenShort", "miso_Res_down_max", -1, InifileName);
	setting_openshort.miso_Res_down_min = GetPrivateProfileIntA("OpenShort", "miso_Res_down_min", 600, InifileName);
	setting_openshort.miso_Res_up_max = GetPrivateProfileIntA("OpenShort", "miso_Res_up_max", -1, InifileName);
	setting_openshort.miso_Res_up_min = GetPrivateProfileIntA("OpenShort", "miso_Res_up_min", 800, InifileName);
	setting_openshort.ss_Res_down_max = GetPrivateProfileIntA("OpenShort", "ss_Res_down_max", -1, InifileName);
	setting_openshort.ss_Res_down_min = GetPrivateProfileIntA("OpenShort", "ss_Res_down_min", 800, InifileName);
	setting_openshort.ss_Res_up_max = GetPrivateProfileIntA("OpenShort", "ss_Res_up_max", 500, InifileName);
	setting_openshort.ss_Res_up_min = GetPrivateProfileIntA("OpenShort", "ss_Res_up_min", 100, InifileName);
	setting_openshort.sck_Res_down_max = GetPrivateProfileIntA("OpenShort", "sck_Res_down_max", 300, InifileName);   //???
	setting_openshort.sck_Res_down_min = GetPrivateProfileIntA("OpenShort", "sck_Res_down_min", 50, InifileName);
	setting_openshort.sck_Res_up_max = GetPrivateProfileIntA("OpenShort", "sck_Res_up_max", -1, InifileName);
	setting_openshort.sck_Res_up_min = GetPrivateProfileIntA("OpenShort", "sck_Res_up_min", 800, InifileName);
	setting_openshort.irq_Res_down_max = GetPrivateProfileIntA("OpenShort", "irq_Res_down_max", 1, InifileName);
	setting_openshort.irq_Res_down_min = GetPrivateProfileIntA("OpenShort", "irq_Res_down_min", 0, InifileName);
	setting_openshort.irq_Res_up_max = GetPrivateProfileIntA("OpenShort", "irq_Res_up_max", -1, InifileName);
	setting_openshort.irq_Res_up_min = GetPrivateProfileIntA("OpenShort", "irq_Res_up_min", 800, InifileName);
	setting_openshort.rst_Res_down_max = GetPrivateProfileIntA("OpenShort", "rst_Res_down_max", -1, InifileName);
	setting_openshort.rst_Res_down_min = GetPrivateProfileIntA("OpenShort", "rst_Res_down_min", 2000, InifileName);
	setting_openshort.rst_Res_up_max = GetPrivateProfileIntA("OpenShort", "rst_Res_up_max", 600, InifileName);
	setting_openshort.rst_Res_up_min = GetPrivateProfileIntA("OpenShort", "rst_Res_up_min", 100, InifileName);
}


static void sl_delay(int ms_delay)
{
	LARGE_INTEGER time_freq_sl, time_start_sl;
	LARGE_INTEGER time_end;// , time_e2;
	QueryPerformanceCounter(&time_start_sl);//获得整机基准时钟计数
	QueryPerformanceFrequency(&time_freq_sl);//获得整机基准频率
	do
	QueryPerformanceCounter(&time_end);
	while (((time_end.QuadPart - time_start_sl.QuadPart) * 1000 * 1000 / time_freq_sl.QuadPart) < ms_delay * 1000);
}


static void spi_write_t(int devices, unsigned int addr, unsigned int *data, unsigned int len_int)
{
	if ((addr & 0xff000000) == 0x0)
	{
		unsigned char data_r[4] = { 0 };
		data_r[3] = (*data >> 24) & 0xff;
		data_r[2] = (*data >> 16) & 0xff;
		data_r[1] = (*data >> 8) & 0xff;
		data_r[0] = (*data >> 0) & 0xff;
		SPI_Write4B(devices, (unsigned char)addr, data_r, len_int * 4);
	}
	else
	{
		SPI_WTotal_Address(devices, addr, data, len_int);
	}
}

struct work_reg
{
	unsigned int address;
	unsigned int data;
};

struct work_reg work_current[] = {
	0x00000098, 0x20044700,
	0x000000e8, 0x00000053,
	0x000000b0, 0x00008381,
	0xff080000, 0x20220000,
	0xff080010, 0x00a00013,
	0xff080014, 0x08140504,
	0xff080018, 0x0000000a,
	0xff08001c, 0x0000005a,
	0xff080034, 0x10000780,
	0xff080038, 0x100f0010,
	0xff08004c, 0x0001f303,
	0xff080054, 0x00002080,
	0xff08007c, 0x008000c0,
	0xff0900f8, 0x0011000c,
	0xff0900f4, 0x80002e10,
	0xff090004, 0x01a40010,
	0xff090018, 0x030300c1,
	0x01000400, 0x00000280,
	0x01000404, 0x00060006,
	0x01000408, 0x001e001e,
	0x0100040c, 0x0244001e,
	0x01000410, 0x02740006,
	0xff080054, 0x00002080,
};

unsigned int OpenShortTest(int devices, struct testData *data,const char *filename)
{
	SL_ResetSTM32(devices);
	sl_delay(20);
	SL_ResetOutput(devices, 0);
	sl_delay(20);
	SL_ResetOutput(devices, 1);
	sl_delay(20);

// 	char log_str[100];
// 	sprintf_s(log_str, "%s\r\n", filename);
// 	FWriteLog("./openshort_test.txt", log_str);

	GetDataFromIni(filename);

	//Optest
	unsigned int Op_result = 0;//保存测试的结果，bit[0]:vdd_voltage,bit[1]:vdd_
	int ret0 = 0;
	double num1 = 0.0;
	unsigned char r_buf[4] = { 0 };
	//	int cpu_flatt = -1;    //0表示0516a,1表示0516b
	/*****************  vdd电压  ************************/
	//SL_Set_Avdd(devices);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	data->Op_Avdd_vol = num1;
	if (num1 * 1000 > setting_openshort.vdd_voltage_max || num1 * 1000 < setting_openshort.vdd_voltage_min)
		Op_result = Op_result | (0x1 << 0);

	/*****************  vdd_rst拉低的电流  ************************/
	SL_ResetOutput(devices, 1);   //rst拉低
	sl_delay(20);
	SL_ResetOutput(devices, 0);   //rst拉低

#if 1
#if 1

#if 0
	double data_c[15];
	memset(data_c, 0, 15 * sizeof(double));

	char m_str[10000];
	memset(m_str, 0, 10000);
	int len = 0;
	for (int i = 0; i < 15; i++)
	{
		sl_delay(10);
		SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
		ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
		num1 = ((double)(ret0)) * 2.5 / 2;
		data_c[i] = num1;

		char log_s[30];
		memset(log_s, 0, 30);
		sprintf_s(log_s, "%4d--%d\r\n", i * 10, (int)num1);

		len += strlen(log_s);
		if (len < 10000)
			strcat_s(m_str, log_s);
	}

	char log_tt[50];
	memset(log_tt, 0, 50);
	sprintf_s(log_tt, "./Log/Current/Current_catLog.txt");
	FWriteLog(log_tt, m_str);

	double max_c = data_c[0];
	for (int i = 0; i < 15; i++)
	{
		if (max_c < data_c[i])
		{
			max_c = data_c[i];
		}
	}
#endif

#if 0
	sl_delay(2000);
	double c_numtotal = 0.0;
	for (int i = 0; i < 10; i++)
	{
		SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
		ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
		num1 = ((double)(ret0)) * 2.5 / 2;
		c_numtotal += num1;
		sl_delay(10);
	}

		num1 = c_numtotal / 10;
#else
	sl_delay(2000);
	SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
	ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
	num1 = ((double)(ret0)) * 2.5 / 2;
#endif


#else
//	remove("./Current20181102.txt");
	char m_str[100000];
	memset(m_str, 0, 100000);
	int len = 0;
	for (int i = 0; i<300; i++)
	{
		sl_delay(10);
		SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
		ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
		num1 = ((double)(ret0)) * 2.5 / 2;

		char log_s[30];
		memset(log_s, 0, 30);
		sprintf_s(log_s, "%4d--%d\r\n", i * 10, (int)num1);

		len += strlen(log_s);
		if (len<100000)
			strcat_s(m_str, log_s);
	}
	
	static int m = 0;
	m++;
	char log_tt[50];
	memset(log_tt, 0, 50);
	sprintf_s(log_tt, "./Current20181116_%d.txt", m);
	FWriteLog(log_tt, m_str);
#endif
#endif

#if 0
	double data_c[100];
	memset(data_c, 0, 100 * sizeof(double));

// 	char m_str[100000];
// 	memset(m_str, 0, 100000);
	for (int i = 0; i < 100; i++)
	{
		sl_delay(10);
		SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
		ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
		num1 = ((double)(ret0)) * 2.5 / 2;
		data_c[i] = num1;

// 		char log_s[50];
// 		memset(log_s, 0, 50);
// 		sprintf_s(log_s, "%4d--%d\r\n", i * 10, (int)num1);
// 		strcat_s(m_str, log_s);
	}
//	FWriteLog("./Log/Current20181019.txt", m_str);
	double max_c = data_c[0];
	int i_num = 0;
	for (int i = 0; i<100; i++)
	{
		if (max_c<data_c[i])
		{
			max_c = data_c[i];
			i_num = i;
		}
	}

	double max_c_t[100] = { 0 };
	int num_c = 0;
	for (int i = i_num; i < 100; i++)
	{
		max_c_t[i - i_num] = data_c[i];
		num_c++;
	}
	
	double temp;
	for (int i = 0; i < num_c; i++)
	{
		for (int j = 0; j < num_c - 1 - i; j++)
		{
			if (max_c_t[j] > max_c_t[j + 1])
			{
				temp = max_c_t[j + 1];
				max_c_t[j + 1] = max_c_t[j];
				max_c_t[j] = temp;
			}
		}
	}

	double data_c_avg = 0.0;
	double data_c_sum = 0.0;
	int data_c_num = 0;
	for (int i = 1; i<10; i++)
	{
		if (max_c_t[i]==0.0)
			continue;
		data_c_sum += max_c_t[i];
		data_c_num++;
	}

	if (data_c_num != 0)
		num1 = data_c_sum / data_c_num;
	else
		num1 = max_c;

#endif

	data->Avdd_current_down = num1;
//	if ((num1 > setting_openshort.vdd_rstdown_current_max) || (num1 < setting_openshort.vdd_rstdown_current_min) || (max_c < 180))
	if ((num1 > setting_openshort.vdd_rstdown_current_max) || (num1 < setting_openshort.vdd_rstdown_current_min))
		Op_result = Op_result | (0x1 << 1);
	 

	/*****************  vdd_rst拉高的电流  ************************/
	SL_ResetOutput(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
	ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
	num1 = ((double)(ret0)) * 2.5 / 2;
	//	num1 -= 30;
	data->Avdd_current_up = num1;
	if (num1 > setting_openshort.vdd_rstup_current_max || num1 < setting_openshort.vdd_rstup_current_min)
		Op_result = Op_result | (0x1 << 2);

	/*****************  vdd_work的电流  ************************/
	SL_ResetOutput(devices, 0);
	sl_delay(20);
	SL_ResetOutput(devices, 1);
	sl_delay(20);

	for (int i = 0; i < sizeof(work_current) / sizeof(work_current[0]); i++)
	{
		spi_write_t(devices, work_current[i].address, &work_current[i].data, 1);
	}
	sl_delay(20);

	SL_IIC_Read(devices, SLAVEADDR, 0x01, r_buf, 4);
	ret0 = ((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff);
	num1 = ((double)(ret0)) * 2.5 / 2;
	//	num1 -= 30;
	data->Avdd_current_work = num1;
	if (num1 > setting_openshort.vdd_work_current_max || num1 < setting_openshort.vdd_work_current_min)
		Op_result = Op_result | (0x1 << 3);



	/***************** 测试vddio的电压  ************************/
	double vddio = 0.0;
	SL_IO5_Output(devices, 0);
	SL_ResetOutput(devices, 0);
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 1);
	SL_IO3_Output(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	vddio = num1;

	/*****************  mosi下拉电阻(mosi上拉电压)  ************************/
	//	SL_GPIO_Output(devices, 0);
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);

	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->mosi_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->mosi_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->mosi_Res_down = 10 * 1000;
	}
	if (data->mosi_Res_down<  setting_openshort.mosi_Res_down_min || (setting_openshort.mosi_Res_down_max > 0 && data->mosi_Res_down >setting_openshort.mosi_Res_down_max))
		Op_result = Op_result | (0x1 << 4);

	/*****************  mosi上拉电阻(mosi下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);

	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->mosi_Res_up = 0;

	else
	{
		if (num1 >= 0.01)
			data->mosi_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->mosi_Res_up = 10 * 1000;
	}

	if (data->mosi_Res_up <  setting_openshort.mosi_Res_up_min || (setting_openshort.mosi_Res_up_max > 0 && data->mosi_Res_up > setting_openshort.mosi_Res_up_max))
		Op_result = Op_result | (0x1 << 5);
#if 1
	/*****************  miso下拉电阻(mosi上拉电压)  ************************/

	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->miso_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->miso_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->miso_Res_down = 10 * 1000;
	}
	if (data->miso_Res_down <  setting_openshort.miso_Res_down_min || (setting_openshort.miso_Res_down_max > 0 && data->miso_Res_down >setting_openshort.miso_Res_down_max))
		Op_result = Op_result | (0x1 << 6);

	/*****************  miso上拉电阻(mosi下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->miso_Res_up = 0.0;
	else
	{
		if (num1 >= 0.01)
			data->miso_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->miso_Res_up = 10 * 1000;
	}
	if (data->miso_Res_up<  setting_openshort.miso_Res_up_min || (setting_openshort.miso_Res_up_max > 0 && data->miso_Res_up > setting_openshort.miso_Res_up_max))
		Op_result = Op_result | (0x1 << 7);


	/*****************  ss下拉电阻(ss上拉电压)  ************************/
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 1);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->ss_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->ss_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->ss_Res_down = 10 * 1000;
	}
	if (data->ss_Res_down < setting_openshort.ss_Res_down_min || (setting_openshort.ss_Res_down_max > 0 && data->ss_Res_down >setting_openshort.ss_Res_down_max))
		Op_result = Op_result | (0x1 << 8);

	/*****************  ss上拉电阻(ss下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 1);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->ss_Res_up = 0.0;
	else
	{
		if (num1 >= 0.01)
			data->ss_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->ss_Res_up = 10 * 1000;
	}
	if (data->ss_Res_up < setting_openshort.ss_Res_up_min || (setting_openshort.ss_Res_up_max > 0 && data->ss_Res_up > setting_openshort.ss_Res_up_max))
		Op_result = Op_result | (0x1 << 9);

	/*****************  sck下拉电阻(sck上拉电压)  ************************/
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 1);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->sck_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->sck_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->sck_Res_down = 10 * 1000;
	}
	if (data->sck_Res_down < setting_openshort.sck_Res_down_min || (setting_openshort.sck_Res_down_max > 0 && data->sck_Res_down >setting_openshort.sck_Res_down_max))
		Op_result = Op_result | (0x1 << 10);
	/*****************  sck上拉电阻(sck下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 1);
	SL_IO3_Output(devices, 0);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->sck_Res_up = 0.0;
	else
	{
		if (num1 >= 0.01)
			data->sck_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->sck_Res_up = 10 * 1000;
	}
	if (data->sck_Res_up < setting_openshort.sck_Res_up_min || (setting_openshort.sck_Res_up_max > 0 && data->sck_Res_up >setting_openshort.sck_Res_up_max))
		Op_result = Op_result | (0x1 << 11);

	/*****************  irq下拉电阻(irq上拉电压)  ************************/
	//IRQ上拉电压
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->irq_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->irq_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->irq_Res_down = 10 * 1000;
	}
	if (data->irq_Res_down < setting_openshort.irq_Res_down_min || (setting_openshort.irq_Res_down_max > 0 && data->irq_Res_down >setting_openshort.irq_Res_down_max))
		Op_result = Op_result | (0x1 << 12);

	/*****************  irq上拉电阻(irq下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 0);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->irq_Res_up = 0;
	else
	{
		if (num1 >= 0.01)
			data->irq_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->irq_Res_up = 10 * 1000;
	}
	if (data->irq_Res_up < setting_openshort.irq_Res_up_min || (setting_openshort.irq_Res_up_max > 0 && data->irq_Res_up >setting_openshort.irq_Res_up_max))
		Op_result = Op_result | (0x1 << 13);

	/*****************  rst下拉电阻(rst上拉电压)  ************************/
	SL_IO4_Output(devices, 0);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (num1 < 0.01)
		data->rst_Res_down = 0;
	else
	{
		if (vddio - num1 >= 0.01)
			data->rst_Res_down = num1 * 47.0 / (vddio - num1);
		else
			data->rst_Res_down = 10 * 1000;
	}
	if (data->rst_Res_down < setting_openshort.rst_Res_down_min || (setting_openshort.rst_Res_down_max > 0 && data->rst_Res_down >setting_openshort.rst_Res_down_max))
		Op_result = Op_result | (0x1 << 14);
	/*****************  rst上拉电阻(rst下拉电压)  ************************/
	SL_IO4_Output(devices, 1);
	SL_IO1_Output(devices, 1);
	SL_IO2_Output(devices, 0);
	SL_IO3_Output(devices, 1);
	sl_delay(20);
	SL_IIC_Read(devices, SLAVEADDR1, 0x02, r_buf, 4);
	ret0 = (((r_buf[0] & 0xff) << 8) + (r_buf[1] & 0xff));
	num1 = ((double)(ret0)) * 0.00125;
	if (vddio - num1 < 0.01)
		data->rst_Res_up = 0.0;
	else
	{
		if (num1 >= 0.01)
			data->rst_Res_up = (vddio - num1)*47.0 / num1;
		else
			data->rst_Res_up = 10 * 1000;
	}
	if (data->rst_Res_up < setting_openshort.rst_Res_up_min || (setting_openshort.rst_Res_up_max > 0 && data->rst_Res_up >setting_openshort.rst_Res_up_max))
		Op_result = Op_result | (0x1 << 15);
#endif
	SL_IO5_Output(devices, 1);
	sl_delay(10);
	SL_ResetSTM32(devices);
	sl_delay(50);
	SL_ResetOutput(devices, 0);
	sl_delay(10);
	SL_ResetOutput(devices, 1);
	sl_delay(10);
	return Op_result;
}