#ifndef _H_SILEADRESTAPI_H_ 
#define _H_SILEADRESTAPI_H_

struct testdatainfo
{
	unsigned char *image;//用于测试的图像
	unsigned char *image_out;//用于测试输出的图像

	unsigned char *imageDarknoise;//保存黑图noise测试的图像
	unsigned char *imageBrightnoise;//保存白图noise测试的图像

	int sram_result;

	unsigned int flashid;
	unsigned int Avdd_current_down;   //rst拉低电流
	unsigned int Avdd_current_up;     //rst拉高电流
	unsigned int Avdd_current_work;   //工作电流

	int all_lightdeadpoint; //总点数
	int max_lightdeadpoint; //最大的连续死点的个数
	int center_lightdeadpoint;  //中间区域
	int edge_lightdeadpoint;    //边缘区域

	int all_darkdeadpoint; //总点数
	int max_darkdeadpoint; //最大的连续死点的个数
	int center_darkdeadpoint;  //中间区域
	int edge_darkdeadpoint;    //边缘区域

	float mtf_data[5];     //中，左上，右上，左下，右下五个区域的mtf的值
	float mtf_mag;         //放大率的值
	float mtf_mag_max;     
	int mtf_cnt;
	float shading[5];      //中，左上，右上，左下，右下五个区域的shading的值
	float distance;        //光学中心跟物理中心的距离

	int lgcenter_w;       //光学中心横坐标
	int lgcenter_h;       //光学中心纵坐标

	int pkgdata;         //格科光圈算法
	int apture;          //silead光圈算法

	int exposure;        //曝光时间
	int linearmean;
	int stripediff;

	double Freqvalue;

	float Darknoise;
	float Brightnoise;
	int fovdata;
	int blotnum;

	int maxbrightness;
	int iNumCount;

	float Op_Avdd_vol;       //vdd电压
	double mosi_Res_down;     //mosi下拉电阻，单位KΩ
	double mosi_Res_up;    //mosi上拉电阻，单位KΩ
	double miso_Res_down;   //miso下拉电阻，单位KΩ
	double miso_Res_up;     //miso上拉电阻，单位KΩ
	double ss_Res_down;    //ss下拉电阻，单位KΩ
	double ss_Res_up;       //ss上拉电阻，单位KΩ
	double sck_Res_down;     //sck下拉电阻，单位KΩ
	double sck_Res_up;     //sck上拉电阻，单位KΩ
	double irq_Res_down;    //irq下拉电阻，单位KΩ
	double irq_Res_up;    //irq上拉电阻，单位KΩ
	double rst_Res_down;     //rst下拉电阻，单位KΩ
	double rst_Res_up;    //rst上拉电阻    单位KΩ

	char *sensorid;
	char testString[2000];
	char resultString[100];
	int Allresult;
	char otpdatainfo[200];


};


#ifdef __cplusplus
extern "C"{
#endif

#define SPIOPERATION
#define SILEADIMGAPI     _declspec(dllexport)

	//初始化silead测试板
	//devices       :设备ID
	//返回值        
	//   0          :fail,表示无测试板连接
	//其他值        ：测试板连接成功
	_declspec(dllexport) int  gx_opendevices(int devices);

	 
	//下载出图的配置文件
	//devices        :设备ID
	//filename       :下载的配置文件名称
	//返回值        
	//   -1          :fail,表示无该配置文件或者文件格式出错
	//    0          ：下载成功
	_declspec(dllexport) int  gx_loadmain(int devices, char *filename);


	//加载测试标准文件
	//devices        :设备ID
	//filename       :需要加载的配置文件名称
	_declspec(dllexport) void gx_initdata(int devices, char *filename);


	//自动曝光
	//devices        :设备ID
	_declspec(dllexport) void gx_autoAgc(int devices);
// 
// 
// 
	//自动曝光,将中心区域曝光到220以上的灰度，用于测试光圈
	//devices        :设备ID
	_declspec(dllexport) void gx_autoAgc220(int devices);
// 	


	//取图
	//devices        :设备ID
	//image          :图像缓存
	//w，h           :图像的宽高
	//filename       :下载的配置文件名称
	//返回值        
	//    0          ：取图成功
	//其他值         ：取图失败
	_declspec(dllexport) int gx_getimage(int devices, int *image, int w, int h);

 
 	//坏点测试
 	//devices        :设备ID
 	//data           :用于保存死点的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_lightbadpoint_t(int devices, struct testdatainfo *data);
 	_declspec(dllexport) int gx_darkbadpoint_t(int devices, struct testdatainfo *data);
 
 
 	//坏点测试
 	//devices        :设备ID
 	//data           :用于保存mtf的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int  gx_mft_t(int devices, struct testdatainfo *data);

	//坏点测试
	//devices        :设备ID
	//data           :用于保存mtf的测试信息
	//返回值        
	//    0          ：测试pass
	//其他值         ：测试fail
	_declspec(dllexport) int  gx_mft_t_BF(int devices, struct testdatainfo *data);

	//Check if the image is normal.
	//devices        :control board ID
	//data           :save the test result
	//return 0		: image is normal
	//return != 0	: image is abnormal
	_declspec(dllexport) int  gx_imageTestBf_t(int devices, struct testdatainfo *data);
 
 
 	//坏点测试
 	//devices        :设备ID
 	//data           :用于保存shading的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int  gx_shading_t(int devices, struct testdatainfo *data);
 
 
 	//光心测试
 	//devices        :设备ID
 	//data           :用于保存shading的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int  gx_center_t(int devices, struct testdatainfo *data);
 
 
 
 	//光圈测试,格科测试算法
 	//devices        :设备ID
 	//data           :用于保存shading的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int  gx_pkgAlgraw_t(int devices, struct testdatainfo *data);
 
 
 	//光圈测试,silead测试算法
 	//devices        :设备ID
 	//data           :用于保存shading的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_aperture_t(int devices, struct testdatainfo *data);
 
 
 
 	//污点测试
 	//devices        :设备ID
 	//data           :用于保存shading的测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_checkBlot_t(int devices, struct testdatainfo *data);
 
 
 	//线性变化测试
 	//devices        :设备ID
 	//data           :用于保存测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_linearChange_t(int devices, struct testdatainfo *data);
 
 
 	//针对华为chart的调焦算法
 	//devices        :设备ID
 	//data           :用于保存测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_calcFreqvalue_t(int devices, struct testdatainfo *data);
 
 
 	//曝光时间测试
 	//devices        :设备ID
 	//data           :用于保存测试信息
 	//返回值        
 	//    0          ：测试pass
 	//其他值         ：测试fail
 	_declspec(dllexport) int gx_expotime_t(int devices, struct testdatainfo *data);
 
 
 
 
 	//读操作
 	//devices        :设备ID
 	//addr           :寄存器地址
 	//data           :读数据缓存
 	//len_int        :读数据的长度
 	_declspec(dllexport) void gx_read(int devices, unsigned int addr, unsigned int *data, unsigned int len_int);
 
 
 	//写操作
 	//devices        :设备ID
 	//addr           :寄存器地址
 	//data           :写数据缓存
 	//len_int        :写数据的长度
 	_declspec(dllexport) void gx_write(int devices, unsigned int addr, unsigned int *data, unsigned int len_int);
 
 
 	//vdd操作
 	//devices        :设备ID
 	//data           
 	//   0           :拉低
 	//   1           :拉高
 	_declspec(dllexport) void gx_vdd_output(int devices, unsigned int data);
 
 
 	//rst操作
 	//devices        :设备ID
 	//data           
 	//   0           :拉低
 	//   1           :拉高
 	_declspec(dllexport) void gx_rst_output(int devices, unsigned int data);
 
 
 	//sram测试
 	//devices        :设备ID
 	//data           :用于保存sram的测试信息
 	//返回值
 	//   0           :测试pass
 	//   1           :测试fail
 	_declspec(dllexport) int  gx_sram(int devices, struct testdatainfo *data);
 
 
 	//flash测试
 	//devices        :设备ID
 	//data           :用于保存flash的测试信息
 	//返回值
 	//   0           :测试pass
 	//   1           :测试fail
 	_declspec(dllexport) int  gx_flash(int devices, struct testdatainfo *data);
 
 
 
 	//openshort测试
 	//devices        :设备ID
 	//data           :用于保存openshort的测试信息
 	//返回值
 	//     0         :测试pass
 	//其他值         :测试fail
 	_declspec(dllexport) int  gx_openshort(int devices, struct testdatainfo *data);
 
 
 
 	//申请内存
 	//devices        :设备ID
 	_declspec(dllexport) void gx_malloc(struct testdatainfo *data);
 
 
 	//释放内存
 	//devices        :设备ID
 	_declspec(dllexport) void gx_free(struct testdatainfo *data);
 
 
 	//输出测试数据，主要供MES系统用
 	_declspec(dllexport) char *getString(struct testdatainfo *data);
 
 
 	_declspec(dllexport) int getSensorid(int devices, struct testdatainfo *data);
 
 
 	//保存测试log。
 	//LogFileName的个数必须以“\\”或者“/”结尾，比如"D:\2018\08\11\"或"D://2018//08//11//"
 	_declspec(dllexport) int gx_saveinfotoexcel(int devices, struct testdatainfo *data, char *LogFileName);
 
 
 
 	//IO1引脚输出高低电平，用于控制机台的光源开关
 	//对应测试板的IO1,即PC1引脚
 	_declspec(dllexport) void gx_io1_output(int devices, unsigned int data);
 
 
 	//IO1引脚输出高低电平，用于控制机台的光源开关
 	//对应测试板的ID,即PC4引脚
 	_declspec(dllexport) void gx_id_output(int devices, unsigned int data);
 
 
 
 	_declspec(dllexport) int  gx_checkotp(int devices, const char *str_code_qrnum, struct testdatainfo *data);
 
 
 
 	_declspec(dllexport) int  gx_readotp(int devices, struct testdatainfo *data);
 
 
 	//测试黑环境下噪声
 	//devices        :设备ID
 	_declspec(dllexport) int gx_darkniose_t(int devices, struct testdatainfo *data);
 
 
 	//测试白环境下噪声
 	//devices        :设备ID
 	_declspec(dllexport) int gx_brightniose_t(int devices, struct testdatainfo *data);
 
 
 
 	//测试白环境下FOV测试
 	//devices        :设备ID
 	_declspec(dllexport) int gx_fov_t(int devices, struct testdatainfo *data);



	//测试白环境下中心亮度测试
	//devices        :设备ID
	_declspec(dllexport) int gx_maxbrightness_t(int devices, struct testdatainfo *data);




	//测试白环境下对称性测试
	//devices        :设备ID
	_declspec(dllexport) int gx_uniformity_t(int devices, struct testdatainfo *data);


#ifdef __cplusplus
}
#endif

#endif